//
//  HealthCheckWrapper.swift
//  healthcheck
//
//  Created by Admin on 31/03/22.
//
import Foundation
import core
import Cleanse
import videocomponent
public class HealthCheck {

    var dalCapture: DALCapture
    var videoComponent:VideoComponent
    var loggerWrapper:LoggerWrapper

    public init(dalCapture:DALCapture,videoComponent:VideoComponent,loggerWrapper:LoggerWrapper) {
        self.dalCapture = dalCapture
        self.videoComponent = videoComponent
        self.loggerWrapper = loggerWrapper
    }

    public func startHealthCheck(modelHealthCheck:ModelHealthCheck) -> HealthCheckView {
        let hcView = HealthCheckView(dalCapture: dalCapture, loggerWrapper: loggerWrapper, modelHealthCheck: modelHealthCheck, videoComponent: videoComponent)
        return hcView
    }
}
