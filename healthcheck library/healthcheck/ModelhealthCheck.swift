//
//  ModelhealthCheck.swift
//  Hostapp
//
//  Created by Admin on 31/03/22.
//

import Foundation
import videocomponent

public class ModelHealthCheck {
    private var modelVcConfig: ModelVcConfig
    private var init_url: String = ""
    private var submit_url: String = ""
    private var requestId: String = ""
    private var isRoomJoinNeeded: Bool = false
    private var timeOut: Int = 0

    public init(modelVcConfig: ModelVcConfig,init_url:String,submit_url:String,requestId:String,isRoomJoinNeeded:Bool,timeOut:Int) {
        self.modelVcConfig = modelVcConfig
        self.init_url = init_url
        self.submit_url = submit_url
        self.requestId = requestId
        self.isRoomJoinNeeded = isRoomJoinNeeded
        self.timeOut = timeOut
    }
    public func getModelVcConfig() -> ModelVcConfig {
        return modelVcConfig
    }

    public func setModelVcConfig(modelVcConfig:ModelVcConfig) {
        self.modelVcConfig = modelVcConfig
    }

    public func getInit_url() -> String {
        return init_url
    }

    public func setInit_url(init_url:String) {
        self.init_url = init_url
    }

    public func getSubmit_url() -> String {
        return submit_url
    }

    public func setSubmit_url(submit_url:String) {
        self.submit_url = submit_url
    }

    public func getRequestId() -> String {
        return requestId
    }

    public func setRequestId(requestId:String) {
        self.requestId = requestId
    }

    public func isRoomJoinNeed() -> Bool {
        return isRoomJoinNeeded
    }

    public func setRoomJoinNeeded(roomJoinNeeded:Bool) {
        self.isRoomJoinNeeded = roomJoinNeeded
    }

    public func getTimeOut() -> Int {
        return timeOut
    }

    public func setTimeOut(timeOut:Int) {
        self.timeOut = timeOut
    }


}

