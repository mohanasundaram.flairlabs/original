//
//  HCComponent.swift
//  healthcheck
//
//  Created by Admin on 31/03/22.
//

import Foundation
import Cleanse
import videocomponent
import core

public struct HCStructure {

    let healthcheck: HealthCheck
    public func gethealthCheck() -> HealthCheck {
        return healthcheck
    }
}
public struct HCComponent : Cleanse.RootComponent {
    public typealias Root = HCStructure
    public typealias Seed = HCFeed

    public static func configure(binder: UnscopedBinder) {
        binder.install(dependency: DalComponent.self)
        binder.install(dependency: VcComponent.self)
    }

    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dd:Seed) in
            Root.init(healthcheck:HealthCheck(dalCapture: dd.dalComponent.getDalCapture(), videoComponent: dd.videoComponent.getVideoComponent(), loggerWrapper: dd.dalComponent.getLoggerWrapper()))
        }
    }
}
public struct HCFeed {
    let dalComponent:DalComponent.Root
    let videoComponent:VcComponent.Root

    public init(dalComponent:DalComponent.Root,videoComponent:VcComponent.Root) {
        self.dalComponent = dalComponent
        self.videoComponent = videoComponent
    }
}
