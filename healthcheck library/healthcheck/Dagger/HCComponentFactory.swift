//
//  HCComponentFactory.swift
//  healthcheck
//
//  Created by FL Mohan on 05/05/22.
//

import core
import videocomponent
import Cleanse

public class HCComponentFactory {

    private static var privateSharedInstance: HCComponentFactory?

    private static var hcComponent:HCComponent.Root?

    public static var sharedInstance: HCComponentFactory = {

        if privateSharedInstance    == nil {
            privateSharedInstance = HCComponentFactory()
        }

        return privateSharedInstance!
    }()

    public static func getHealthCheckComponent(dalComponent:DalComponent.Root) -> HCComponent.Root {
        if hcComponent == nil {
            let vcComponent = VCComponentFactory.getVcComponent(dalComponent: dalComponent)
            hcComponent = try! ComponentFactory.of(HCComponent.self).build(HCFeed(dalComponent: dalComponent, videoComponent: vcComponent))
        }
        return hcComponent!
    }
}
