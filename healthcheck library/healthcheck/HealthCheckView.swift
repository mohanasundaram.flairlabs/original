//
//  HealthCheckView.swift
//  app
//
//  Created by Admin on 09/12/21.
//

import Foundation
import UIKit
import core
import videocomponent

public protocol HealthCheckCallback {
    func onSuccessCallback()
    func onFailureCallback(code: String)

}

public class HealthCheckView: UIView, VideoComponentCallBack, InstructionActionDelegate {
    
    var timer: Timer?
    
    var loggerHCheck: Logger?
    
    var loggerStartTime: Double = 0
    
    var networkCheckId: String = ""
    
    var infoView = InstructionView()
    
    var videoComponentFragment:VideoComponentView?

    var modelHealthCheck: ModelHealthCheck?

    var loggerWrapper = LoggerWrapper()

    var videoComponent:VideoComponent?
    
    var dalCapture = DALCapture()
    var isRoomJoined: Bool = false
    
    var delegate: HealthCheckCallback?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    public init(dalCapture:DALCapture,loggerWrapper:LoggerWrapper,modelHealthCheck:ModelHealthCheck,videoComponent:VideoComponent) {
        self.dalCapture = dalCapture
        self.loggerWrapper = loggerWrapper
        self.videoComponent = videoComponent
        self.modelHealthCheck = modelHealthCheck
        super.init(frame: .zero)
    }
    public func initialiseHealthCheck(view: UIView, delegate: HealthCheckCallback) {
        self.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.delegate = delegate
        self.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
        infoViewInit()
        initiateVideoComponent()
        startHealthCheck()
        initLogger()
    }
    func infoViewInit() {
        infoView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        infoView.delegate = self
    }
    func initLogger() {

        loggerHCheck = loggerWrapper.createLogger(key: EnumLogger.HEALTHCHECK.rawValue, token: dalCapture.getToken(), loggerUrl: modelHealthCheck?.getModelVcConfig().getVc_logger_url() ?? "")
        loggerHCheck?.setDefaults(defaultLogDetails: ModalLogDetails(reference_id: dalCapture.getRequestId(), reference_type:dalCapture.getReferenceType() ))
        loggerHCheck?.logPageVisit(pageName: "MSConnectionCheck", pageComponent: "HealthCheck", source: "")
        loggerHCheck?.logPageRender(loggerStartTime: loggerStartTime, component: "HealthCheck",referenceId: dalCapture.getRequestId() != "" ? dalCapture.getRequestId() : dalCapture.getCaptureId(),referenceType: dalCapture.getReferenceType(), meta: [:])

    }
    func initiateVideoComponent() {
        DispatchQueue.main.async { [self] in
            videoComponentFragment = videoComponent?.startVcView()
            videoComponentFragment?.InitialiseCameraView(modelVcConfig: (modelHealthCheck?.getModelVcConfig())!, view: self, delegate: self)
            self.addSubview(videoComponentFragment!)
            videoComponentFragment?.hideAllOptions()
            if self.subviews.contains(infoView) {
                self.infoView.removeFromSuperview() // Remove it
            } 
            self.infoView.loadNetworkCheckInfoView()
            self.addSubview(infoView)
        }
    }
    public func onVCStateCallBack(event: String, object: NSObject) {
        switch event {
        case "start":
            isRoomJoined = true
            break
        case "publish_started":
            invalidateTimer()
            self.submitHealthCheck(networkCheckId: networkCheckId, roomJoin: isRoomJoined, publish: true, iceStaConnected: "iceState", error: event, message: "\(object.value(forKey: "state") ?? "")")
            break
        case "USER_EXIT":
            videoComponentFragment?.cleanUp()
            videoComponentFragment?.removeFromSuperview()
            self.delegate?.onFailureCallback(code: "USER_EXIT")
            break
        case "WebRTCIceStateChanged":
            if object.value(forKey: "state") as! String == "failed" || object.value(forKey: "state") as! String == "disconnected" {
                self.submitHealthCheck(networkCheckId: networkCheckId, roomJoin: isRoomJoined, publish: false, iceStaConnected: "iceConnectionError", error: "iceConnectionError", message: "\(object.value(forKey: "state") ?? "")")
            }
            break
        default:
            break
        }
    }
    public func onVCError(code: String, message: String, canReconnect: Bool) {
        if code != "NETWORK_DISCONNECTED" {
        submitHealthCheck(networkCheckId: networkCheckId, roomJoin: isRoomJoined, publish: false, iceStaConnected: message, error: "", message: message)
        } else {
            self.delegate?.onFailureCallback(code: code)
        }
    }
    func startHealthCheck() {

        let networkStatus: [String: Any] = [
            "room_join": isRoomJoined,
            "timeout": modelHealthCheck?.getTimeOut() ?? 0
        ]
        let param: [String: Any] = [
            "reference_id": dalCapture.getRequestId() ,
            "reference_type": "AV.TaskID",
            "config": networkStatus,
            "capture_session_id": dalCapture.getCaptureSessionId()
        ]
        print(param)
        guard let data = try? JSONSerialization.data(withJSONObject: param, options: []) else { return  }
        self.startTimer(timeout: modelHealthCheck?.getTimeOut() ?? 0)
        
        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "HealthCheck"
        logDetails.event_name = "FetchDetails"
        logDetails.event_type = "Initiate"
        logDetails.event_source = "startHealthCheck"
        logDetails.component = "HealthCheck"
        logDetails.reference_id = dalCapture.getRequestId()
        logDetails.reference_type = "AV.TaskID"
        logDetails.liveMonitoring = true
        logDetails.publish_to_dlk = true
        loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
        dalCapture.callApi(view: self, info: data, url: modelHealthCheck?.getInit_url() ?? "") { [self] success, failure  in
            if let onResponse = success {
                
                var logDetails = ModalLogDetails()
                logDetails.service_category = "captureSDK"
                logDetails.service = "HealthCheck"
                logDetails.event_name = self.dalCapture.getTatSince(start: self.loggerStartTime)
                logDetails.event_type = "FetchDetails.Success.TAT"
                logDetails.event_source = "initHealthCheck"
                logDetails.component = "HealthCheck"
                logDetails.reference_id = self.dalCapture.getRequestId()
                logDetails.reference_type = "AV.TaskID"
                logDetails.liveMonitoring = true
                logDetails.publish_to_dlk = true
                self.loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
                
                var logDetail =  ModalLogDetails()
                logDetail.service_category = "CaptureSDK"
                logDetail.service = "HealthCheck"
                logDetail.event_name = "FetchDetails"
                logDetail.event_type = "Success"
                logDetail.event_source = "initHealthCheck"
                logDetail.component = "HealthCheck"
                logDetail.reference_id = self.dalCapture.getRequestId()
                logDetail.reference_type = "AV.TaskID"
                logDetail.liveMonitoring = true
                logDetails.publish_to_dlk = true
                var meta: [String: Any] = [:]
                meta.updateValue(JsonToMap(value: success!), forKey: "response")
                
                self.loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)

                if let networkcheckid = onResponse.value(forKey: "network_check_id") as? String {
                self.networkCheckId = networkcheckid
                let participant_Id = onResponse.value(forKey: "participant_id") as? String ?? ""
                let room_Id = onResponse.value(forKey: "room_id") as? String ?? ""
                self.videoComponentFragment?.setUpSocket(roomId: room_Id, participantId: participant_Id, msSocketURL: "", purpose: "")
                self.videoComponentFragment?.delegate = self
                } else {
                    var logDetail =  ModalLogDetails()
                    logDetail.service_category = "CaptureSDK"
                    logDetail.service = "HealthCheck"
                    logDetail.event_name = "Failed"
                    logDetail.event_type = "Exception"
                    logDetail.event_source = "initHealthCheck"
                    logDetail.component = "HealthCheck"
                    logDetail.reference_id = self.dalCapture.getRequestId()
                    logDetail.reference_type = "AV.TaskID"
                    logDetail.liveMonitoring = true
                    logDetails.publish_to_dlk = true
                    var meta: [String: Any] = [:]
                    meta.updateValue(JsonToMap(value: success!), forKey: "error")

                    self.loggerHCheck?.log(logLevel: LogLevel.shared.Error, logDetails: logDetails, meta: meta)

                    delegate?.onSuccessCallback()
                }
            } else {
                var logDetails = ModalLogDetails()
                logDetails.service_category = "captureSDK"
                logDetails.service = "HealthCheck"
                logDetails.event_name = "Failed"
                logDetails.event_type = "Api Failure"
                logDetails.event_source = "initHealthCheck"
                logDetails.component = "HealthCheck"
                logDetails.reference_id = dalCapture.getRequestId()
                logDetails.reference_type = "AV.TaskID"
                logDetails.liveMonitoring = true
                logDetails.publish_to_dlk = true
                var meta: [String: Any] = [:]
                meta.updateValue(failure!, forKey: "response")
                loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)
                if let _ = failure?.value(forKey: "status") {
                    self.handleApiError(errorDetails: failure!, invokeSuccessCallback: true)
                } else {
                self.delegate?.onFailureCallback(code: "init_failure")
                }
            }
        }
    }
    func startTimer(timeout: Int) {
        print("Timeout time: \(timeout)")
        print("Time Started")
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(timeout), target: self, selector: #selector(self.timeoutOccured), userInfo: nil, repeats: false)
    }
    @objc func timeoutOccured() {
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "HealthCheck", timestamp: "", event_type: "Timeout", event_name: "TimerExpired", component: "HealthCheck", event_source: "timeoutOccured", logger_session_id: "", reference_id: dalCapture.getRequestId(), reference_type: "AV.TaskID", liveMonitoring: true, publish_to_dlk: true)

        loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
        invalidateTimer()
        submitHealthCheck(networkCheckId: networkCheckId, roomJoin: false, publish: false, iceStaConnected: "notconnected", error: "", message: "")
    }
    public func BackPressed() {
        if videoComponentFragment?.isDescendant(of: self) == true {
            videoComponentFragment?.BackPressed()
        } else {
            print("In health check page")
        }
    }
    func submitHealthCheck(networkCheckId: String, roomJoin: Bool, publish: Bool, iceStaConnected: String, error: String, message: String) {

        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "HealthCheck", timestamp: "", event_type: "Initiate", event_name: "SubmitResult", component: "HealthCheck", event_source: "submitHealthCheck", logger_session_id: "", reference_id: dalCapture.getRequestId(), reference_type: "AV.TaskID", liveMonitoring: true, publish_to_dlk: true)
        loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])

        var jsonData = Data()
        let webRtc = WebrtcStats()
        var errors = ErrorMessage(error: "", message: "")
        if !error.isEmpty && !message.isEmpty {
            errors = ErrorMessage(error: error, message: message)
            let stat = Statistics(roomJoin: roomJoin, publish: publish, iceConnectionState: iceStaConnected, webrtcStats: webRtc, errors: [errors])
            let healthCheck = HealthCheckModel(referenceID: dalCapture.getRequestId(), networkCheckID: networkCheckId, statistics: stat)
            jsonData = try! JSONEncoder().encode(healthCheck)
        } else {
            let stat = Statistics(roomJoin: roomJoin, publish: publish, iceConnectionState: iceStaConnected, webrtcStats: webRtc, errors: [])
            let healthCheck = HealthCheckModel(referenceID: dalCapture.getRequestId(), networkCheckID: networkCheckId, statistics: stat)
            jsonData = try! JSONEncoder().encode(healthCheck)
        }

        dalCapture.callApi(view: self, info: jsonData, url: modelHealthCheck?.getSubmit_url() ?? "") { [self] success, failure  in
            invalidateTimer()
            if success != nil {

                let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "HealthCheck", timestamp: "", event_type: "SubmitResult.Success.TAT", event_name: self.dalCapture.getTatSince(start: self.loggerStartTime), component: "HealthCheck", event_source: "submitHealthCheck", logger_session_id: "", reference_id: self.dalCapture.getRequestId(), reference_type: "AV.TaskID", liveMonitoring: true, publish_to_dlk: true)
                self.loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])

                if success?.value(forKey: "network_check_pass") as? Bool ?? false == true {

                    let logDetail = ModalLogDetails(service_category: "CaptureSDK", service: "HealthCheck", timestamp: "", event_type: "Success", event_name: "SubmitResult", component: "HealthCheck", event_source: "submitHealthCheck", logger_session_id: "", reference_id: self.dalCapture.getRequestId(), reference_type: "AV.TaskID", liveMonitoring: true, publish_to_dlk: true)

                    var meta: [String: Any] = [:]
                    meta.updateValue(JsonToMap(value: success!), forKey: "response")
                    meta.updateValue(success?.value(forKey: "network_check_pass") as? Bool ?? false, forKey: "networkCheckStats")

                    self.loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetail, meta: meta)

                    videoComponentFragment?.cleanUp()
                    DispatchQueue.main.async { [self] in
                        if self.subviews.contains(infoView) {
                            self.infoView.removeFromSuperview() // Remove it
                    }
                    }

                    let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "HealthCheck", timestamp: "", event_type: "HealthCheckResult", event_name: "Passed", component: "HealthCheck", event_source: "submitHealthCheck", logger_session_id: "", reference_id: self.dalCapture.getRequestId(), reference_type: "AV.TaskID", liveMonitoring: true, publish_to_dlk: true)

                    var metas: [String: Any] = [:]
                    metas.updateValue(JsonToMap(value: success!), forKey: "response")
                    self.loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: metas)

                    delegate?.onSuccessCallback()
                } else {

                    let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "HealthCheck", timestamp: "", event_type: "HealthCheckResult", event_name: "Failed", component: "HealthCheck", event_source: "submitHealthCheck", logger_session_id: "", reference_id: self.dalCapture.getRequestId(), reference_type: "AV.TaskID", liveMonitoring: true, publish_to_dlk: true)

                    var meta: [String: Any] = [:]
                    meta.updateValue(JsonToMap(value: success!), forKey: "response")
                    self.loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)

                    delegate?.onFailureCallback(code: "submit_failure")
                }
            } else {

                let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "HealthCheck", timestamp: "", event_type: "Exception", event_name: "Failed", component: "HealthCheck", event_source: "submitHealthCheck", logger_session_id: "", exceptionName: "", exceptionDescription: "", reference_id: self.dalCapture.getRequestId(), reference_type: "AV.TaskID", liveMonitoring: true, publish_to_dlk: true)

                var meta: [String: Any] = [:]
                meta.updateValue(JsonToMap(value: failure!), forKey: "error")
                self.loggerHCheck?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)

                if let _ = failure?.value(forKey: "status") {
                    self.handleApiError(errorDetails: failure!, invokeSuccessCallback: true)
                } else {
                self.delegate?.onFailureCallback(code: "submit_failure")
                }
            }
        }
    }
    func handleApiError(errorDetails: NSObject, invokeSuccessCallback: Bool) {

        guard let statusCode = errorDetails.value(forKey: "status") as? Int else { return }
        let message = errorDetails.value(forKey: "message") as? String

        if statusCode >= 500 {

            var logDetail =  ModalLogDetails()
            logDetail.service_category = "CaptureSDK"
            logDetail.service = "HealthCheck"
            logDetail.event_name = String(statusCode) + ""
            logDetail.event_type = "ServerError"
            logDetail.event_source = "handleApiError"
            logDetail.component = "HealthCheck"
            logDetail.exceptionName = message
            logDetail.exceptionDescription = "Received error code " + String(statusCode) + "The health check service is probably unavailable. Allowing the user to go through to the video call."
            logDetail.liveMonitoring = true
            logDetail.publish_to_dlk = true
            var meta: [String: Any] = [:]
            meta.updateValue(errorDetails, forKey: "error")

            self.loggerHCheck?.log(logLevel: LogLevel.shared.Error, logDetails: logDetail, meta: meta)

            if invokeSuccessCallback {
                delegate?.onSuccessCallback()
            }
        } else {
            delegate?.onFailureCallback(code: "failure")
        }

    }
    func invalidateTimer() {
        timer?.invalidate()
        timer = nil

    }
    public func didSetRetryButton(screen: RetryOptions) {
        infoView.removeFromSuperview()
        startHealthCheck()
    }
    public func cleanUp() {
        videoComponentFragment?.cleanUp()
    }
}
