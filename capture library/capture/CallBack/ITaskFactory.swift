//
//  ITaskFactory.swift
//  capture
//
//  Created by Admin on 19/04/22.
//

import Foundation
import UIKit
import core

public protocol ITaskFactory {
    func getTextComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> TextView
    func getTextComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> TextView
    func getGroupComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> GroupView
    func getGroupComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> GroupView
    func getButtonComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> Button
    func getButtonComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> Button
    func getFormComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> FormView
//    func getFormComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> FormView
    func getImageComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> Image
    func getImageComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> Image
    func getSectionComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> SectionView
    func getSectionComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> SectionView
    func getDocumentComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> Document
    func getDocumentComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> Document
    func getCheckBoxComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> CheckBox
    func getCheckBoxComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> CheckBox
    func getRadioComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> RadioBox
    func getRadioComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> RadioBox
    func getDropdownComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> DropDown
    func getDropdownComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> DropDown
    func getFilePickerComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> FilePicker
    func getFilePickerComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> FilePicker
}

