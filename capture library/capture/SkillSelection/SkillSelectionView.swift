//
//  SkillSelectionView.swift
//  capture
//
//  Created by Admin on 05/04/22.
//

import Foundation
import UIKit
import core


public class SkillSelectionView: UIView, SkillSelectionCallBack,IRedirectNext {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var skillContentView:UIView!
    @IBOutlet weak var submitButton:UIButton!

    let kCONTENT_XIB_NAME           =   "SkillSelectionView"

    var skillView = UIScrollView()

    var contentOfScrollView = UIStackView()

    var dalCapture = DALCapture()

    var skillObject:NSObject?

    var delegate:ICapture?

    var skillSelectionArray:[NSObject] = []

    var taskContainer:[String:Bool] = [:]

    private var AVKYC:String = "vkyc.assisted_vkyc"

    public init(view: UIView,dalCapture:DALCapture,skillObject:NSObject,delegate:ICapture) {
        self.skillObject = skillObject
        self.dalCapture = dalCapture
        self.delegate = delegate
        super.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        setupScrollView()
        setupContentofScrollView()

        submitButton.tintColor = .gray
        submitButton.isEnabled = false
        submitButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)

        initiateSkillSelection()

    }
    func initiateSkillSelection() {
        print(skillObject)
        let pageSequence = dalCapture.getNextPageName()
        if pageSequence.getPage() == AVKYC {
        self.submitButton.setTitle("START VIDEO KYC", for: .normal)
        }
        if ((skillObject?.value(forKey: "skill_configuration") as? [NSObject]) != nil) {
            self.skillSelectionArray = skillObject?.value(forKey: "skill_configuration") as! [NSObject]
        } else  if ((skillObject?.value(forKey: "config") as? [NSObject]) != nil) {
            self.skillSelectionArray = skillObject?.value(forKey: "config") as! [NSObject]
        }
        for(_,skill) in skillSelectionArray.enumerated() {
            DispatchQueue.main.async { [self] in
                let cardView = SkillCardView(frame: .zero, skillObject: skill, delegate: self)
                contentOfScrollView.addArrangedSubview(cardView)
            }
        }
    }
    public func updateSelection(key:String,value:String) {
        if taskContainer.isEmpty {
            if value != "" {
                taskContainer = [key:true]
            } else {
                taskContainer = [key:false]
            }
        } else {
            if value != "" {
                taskContainer.updateValue(true, forKey: key)
            } else {
                taskContainer.updateValue(false, forKey: key)
            }
        }

        for(_,name) in skillSelectionArray.enumerated() {
            var obj = name as! [String:Any]
            if obj["skill"] as? String == key {
                obj.updateValue(value, forKey: "value")
                if let row = self.skillSelectionArray.firstIndex(where: {$0.value(forKey: "skill") as! String == key}) {
                    skillSelectionArray[row] = obj as NSObject
                    var finalSkillObj = skillObject as! [String:Any]
                    if ((skillObject?.value(forKey: "skill_configuration") as? [NSObject]) != nil) {
                        finalSkillObj.updateValue(skillSelectionArray, forKey: "skill_configuration")
                    } else  if ((skillObject?.value(forKey: "config") as? [NSObject]) != nil) {
                        finalSkillObj.removeValue(forKey: "config")
                        finalSkillObj.updateValue(skillSelectionArray, forKey: "skill_configuration")
                    }

                    skillObject = finalSkillObj as NSObject
                    if taskContainer.count == skillSelectionArray.count {
                        DispatchQueue.main.async { [self] in
                            if taskContainer.allSatisfy({ $0.value == true }) {
                                submitButton.isEnabled = true
                                submitButton.tintColor = dalCapture.getThemeConfig().getPrimaryMainColor()
                            } else {
                                submitButton.isEnabled = false
                                submitButton.backgroundColor = .gray
                            }
                        }
                    }
                }
            }
        }
    }
    @IBAction func didTapButton(sender:UIButton) {
        submitButton.isEnabled = false
        dalCapture.sendEvent(event: "session:update_av_skills", skillObj:  skillObject as! [String:Any]) { onResponse in
            let response = onResponse.value(forKey: "response") as! String
            if response == "success" {
                self.delegate?.redirectToNextPage(iRedirectNext: self)
            }
        }
    }
    public func BackPressed() {
      //  print("In SKill Selection")
    }
    public func onSuccess() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = false
        }
    }

    public func onFailure() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = true
        }
    }
}
extension SkillSelectionView {

        private func setupScrollView() {

            skillView.backgroundColor = .clear

            skillContentView.addSubview(skillView)

            skillView.translatesAutoresizingMaskIntoConstraints = false

            skillView.leadingAnchor.constraint(equalTo: skillContentView.leadingAnchor).isActive = true
            skillView.trailingAnchor.constraint(equalTo: skillContentView.trailingAnchor).isActive = true
            skillView.topAnchor.constraint(equalTo: skillContentView.topAnchor).isActive = true
            skillView.bottomAnchor.constraint(equalTo: skillContentView.bottomAnchor).isActive = true
        }

        private func setupContentofScrollView() {

            contentOfScrollView.axis            = .vertical
            contentOfScrollView.distribution    = .fill
            contentOfScrollView.alignment       = .fill
            contentOfScrollView.spacing         = 15

            skillView.addSubview(contentOfScrollView)

            contentOfScrollView.translatesAutoresizingMaskIntoConstraints = false
            contentOfScrollView.leadingAnchor.constraint(equalTo: skillContentView.leadingAnchor).isActive = true
            contentOfScrollView.trailingAnchor.constraint(equalTo: skillContentView.trailingAnchor).isActive = true
            contentOfScrollView.topAnchor.constraint(equalTo: skillView.topAnchor).isActive = true
            contentOfScrollView.bottomAnchor.constraint(equalTo: skillView.bottomAnchor).isActive = true
        }
}
// Skill selection card view

public protocol SkillSelectionCallBack {
    func updateSelection(key:String,value:String)
}
public class SkillCardView: UIStackView,SkillSelectionCallBack {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var stackView:UIStackView!


    let kCONTENT_XIB_NAME           =   "CardView"

    var skillObject:NSObject?

    var delegate:SkillSelectionCallBack?

    public init(frame: CGRect,skillObject:NSObject,delegate:SkillSelectionCallBack) {
        self.delegate = delegate
        self.skillObject = skillObject
        super.init(frame: frame)
        commonInit()
    }

    required init(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        cardView.backgroundColor = ThemeConfig.shared.getSecondaryMainColor()
        cardView.layer.cornerRadius = 7.0
        cardView.layer.shadowColor = hexStringToUIColor(hex: "1E1E1E").cgColor
        cardView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        cardView.layer.shadowRadius = 1.5
        cardView.layer.shadowOpacity = 0.7
        cardView.translatesAutoresizingMaskIntoConstraints = false

        createCardView()
    }
    func createCardView() {

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 10
        stackView.distribution = .fill

        let labelText = skillObject?.value(forKey: "label") as! String

        if labelText != "" {
            let label = Header()
            label.headerLabel(title: labelText, frameWidthReference: stackView)
            stackView.addArrangedSubview(label)
        }
        let type = skillObject?.value(forKey: "type") as? String ?? ""
        if type == "radio" || type == "checkbox" {
            let cardContentView = SkillCardContent(frame: .zero,skillObject: skillObject!, delegate: self)
            stackView.addArrangedSubview(cardContentView)
        } else if type == "dropdown" {
            let dropdown = SkillDropDown(frame: .zero, skillObject: skillObject!, delegate: self)
            stackView.addArrangedSubview(dropdown)
        }
    }
    public func updateSelection(key:String,value:String) {
        self.delegate?.updateSelection(key:key, value: value)
    }
}
public class SkillCardContent: UIView,SkillSelectionCallBack {


    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var tableViewHeightConstraint:NSLayoutConstraint!

    let kCONTENT_XIB_NAME           =   "RadioBox"

    var optionList: [String] = []
    var selectedOption: [String] = []

    var delegate:SkillSelectionCallBack?

    var skillObject:NSObject?
    var typeofSkill:String = ""
    var typeofView:String = ""

    public init(frame: CGRect,skillObject:NSObject,delegate:SkillSelectionCallBack) {
        self.skillObject = skillObject
        self.delegate = delegate
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        tableView.register(UINib(nibName: "RadioBoxCell", bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: "RadioBoxCell")
        tableView.register(UINib(nibName: "CheckBoxCell", bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: "CheckBoxCell")

        tableView.separatorColor = .clear

        initiateView()
    }
    func initiateView() {
        let taskLabel = skillObject?.value(forKey: "label") as? String ?? ""
        optionList = skillObject?.value(forKey: "options") as? [String] ?? []
        typeofSkill = skillObject?.value(forKey: "skill") as? String ?? ""
        typeofView = skillObject?.value(forKey: "type") as? String ?? ""
        titleLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        self.titleLabel.text = taskLabel

        tableViewHeightConstraint.constant = CGFloat(60 * optionList.count)
        self.tableView.reloadData()
    }
    func checkForSelectedOption() {
        if selectedOption.count == 0 {
            titleLabel.textColor = .red
            self.delegate?.updateSelection(key: typeofSkill, value: "")
        } else {
            self.titleLabel.textColor =  ThemeConfig.shared.getPrimaryMainColor()
            let formattedArray = (selectedOption.map{String($0)}).joined(separator: ",")
            self.delegate?.updateSelection(key: typeofSkill, value: formattedArray)
        }
    }
    public func updateSelection(key:String,value:String) {
        self.delegate?.updateSelection(key: key, value: value)
    }
}
extension SkillCardContent: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionList.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if typeofView == "radio" {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RadioBoxCell") as? RadioBoxCell else {
                return UITableViewCell()
            }
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            cell.configureCell(title: optionList[indexPath.row], textColor: ThemeConfig.shared.getSecondaryContrastColor())
            if selectedOption.contains(self.optionList[indexPath.row]) {
                cell.imageAccessory.image = UIImage(named: "Radio_Icon_Selected", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(ThemeConfig.shared.getPrimaryMainColor())
            } else {
                cell.imageAccessory.image = UIImage(named: "Radio_Icon_Unselected", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(ThemeConfig.shared.getSecondaryContrastColor())
            }
            return cell
        } else if typeofView == "checkbox" {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CheckBoxCell") as? CheckBoxCell else {
                return UITableViewCell()
            }
            cell.backgroundColor = .clear

            cell.configureCell(title: optionList[indexPath.row], textColor: ThemeConfig.shared.getSecondaryContrastColor())
            if selectedOption.contains(self.optionList[indexPath.row]) {
                cell.imageAccessory.image = UIImage(named: "CheckBox", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(ThemeConfig.shared.getPrimaryMainColor())
            } else {
                cell.imageAccessory.image = UIImage(named: "UncheckBox", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(ThemeConfig.shared.getSecondaryContrastColor())
            }
            return cell
        } else if typeofView == "" {
        }
        return UITableViewCell()
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if typeofView == "radio" {
            selectedOption = [optionList[indexPath.row]]
        } else if typeofView == "checkbox" {
            if selectedOption.contains(optionList[indexPath.row]) {
                if let index = selectedOption.firstIndex(of: optionList[indexPath.row]) {
                    selectedOption.remove(at: index)
                }
            } else {
                selectedOption.append(optionList[indexPath.row])
            }
        }
        self.tableView.reloadData()
        checkForSelectedOption()
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
public class SkillDropDown: UIView, DropDownDataSourceProtocol,UIGestureRecognizerDelegate {


    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var selectedNameLabel:UITextField!
    @IBOutlet weak var dropDownImageView:UIImageView!


    let kCONTENT_XIB_NAME           =   "DropDown"

    var dropDown = DropDownList()

    var dropDownRowHeight: CGFloat = 50

    var artifactKey:String = ""

    var isDropDownPresent:Bool = false

    var optionList: [String] = []
    var skillObject:NSObject?

    var typeOfSkill:String = ""

    private var fullScreenView = UIView()


    var delegate:SkillSelectionCallBack?


    public init(frame: CGRect,skillObject:NSObject,delegate:SkillSelectionCallBack) {
        self.skillObject = skillObject
        self.delegate = delegate
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        dropDownImageView.image?.withTintColor(.white)

        initiateDropDown()
    }
    func initiateDropDown() {
        let taskLabel = skillObject?.value(forKey: "label") as? String ?? ""
        optionList = skillObject?.value(forKey: "options") as? [String] ?? []
        typeOfSkill = skillObject?.value(forKey: "skill") as? String ?? ""
        dropDownImageView.image = dropDownImageView.image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        dropDownImageView.tintColor = ThemeConfig.shared.getSecondaryContrastColor()

        titleLabel.textColor = ThemeConfig.shared.getPrimaryMainColor()
        selectedNameLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        self.titleLabel.text = taskLabel
        if !optionList.isEmpty {
        self.selectedNameLabel.text = optionList[0]
        }

    }
    @IBAction func didTapButton(sender:UIButton) {
        if !isDropDownPresent {
        dropDown = DropDownList()
        setUpDropDown()
        fullScreenView = UIView(frame: CGRect(x: 0, y: 0, width: (self.window?.rootViewController?.view.frame.width)!, height: (self.window?.rootViewController?.view.frame.height)!))
        fullScreenView.backgroundColor = .clear
        fullScreenView.addSubview(dropDown)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self
        fullScreenView.addGestureRecognizer(tap)
        self.window?.rootViewController?.view.addSubview(fullScreenView)
        self.window?.rootViewController?.view.bringSubviewToFront(fullScreenView)
            self.dropDown.showDropDown(height: 250)
        }  else {
            self.dropDown.hideDropDown()
            isDropDownPresent = false
        }
        UIView.animate(withDuration: 0.5) {
            self.dropDownImageView.transform = CGAffineTransform(rotationAngle: self.isDropDownPresent ? CGFloat(Double.pi) : CGFloat(0))
        }
    }
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard let view = touch.view else { return true }
        if view.isDescendant(of: dropDown) {
            return false
        }
        return true
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        removeGesture()
    }
    func setUpDropDown(){
        dropDown.dropDownDataSourceProtocol = self
        guard let keyWindow = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) else { return }
        let frame = self.convert(self.bounds, to: keyWindow)
        dropDown.setUpDropDown(viewPositionReference: (frame), offset: 2, optionlist: optionList, backgroundColor:ThemeConfig.shared.getSecondaryMainColor(), havingTitle: false)
        dropDown.setRowHeight(height: self.dropDownRowHeight)
    }
    func selectItemInDropDown(indexPos: Int) {
        removeGesture()
        self.selectedNameLabel.text = optionList[indexPos]
        self.delegate?.updateSelection(key:typeOfSkill, value: optionList[indexPos])
    }
    func removeGesture() {
        self.dropDown.hideDropDown()
        isDropDownPresent = false
        if fullScreenView.isDescendant(of: (self.window?.rootViewController?.view)!) {
            fullScreenView.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.5) {
            self.dropDownImageView.transform = CGAffineTransform(rotationAngle: self.isDropDownPresent ? CGFloat(Double.pi) : CGFloat(0))
        }
    }
}
