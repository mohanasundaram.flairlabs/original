//
//  DynamicView.swift
//  captureapp
//
//  Created by FL Mohan on 06/07/22.
//

import Foundation
import UIKit
import core


public class DynamicView: UIView, PluginCallBack {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var messageInfo: UILabel!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    
    let kCONTENT_XIB_NAME           =   "DynamicView"

    var dalCapture = DALCapture()
    var delegate:DynamicViewCallBack?

    var filledValues:[String:Any] = [:]
    var details:[String] = []

    public override init(frame:CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    public func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        proceedButton.isEnabled = false
        stackView.spacing  = 15
    }
    public func setValues(dalCapture:DALCapture,details:[String],delegate:DynamicViewCallBack) {
        self.dalCapture = dalCapture
        self.delegate = delegate
        self.details = details
        setCardView()
        for value in self.details {
            let pluginView = PluginTextView(frame: .zero, textValue: "", placeholder: value, delegate: self)
            stackView.addArrangedSubview(pluginView)
        }
    }
    public func setCardView() {
        popupView.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
        popupView.layer.cornerRadius = 7.0
        popupView.layer.shadowColor = dalCapture.getThemeConfig().getSecondaryContrastColor().cgColor
        popupView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        popupView.layer.shadowRadius = 1.5
        popupView.layer.shadowOpacity = 0.7
        popupView.translatesAutoresizingMaskIntoConstraints = false
        proceedButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)
        proceedButton.backgroundColor = .gray
    }
    public func didUpdateValue(title: String, updatedText: String) {
        for detail in details {
            if detail == title {
                if updatedText != "" {
                    filledValues.updateValue(updatedText, forKey: detail)
                } else {
                    filledValues.removeValue(forKey: detail)
                }
            }
        }
        if details.count == filledValues.count && updatedText != "" {
            proceedButton.isEnabled = true
            proceedButton.backgroundColor = dalCapture.getThemeConfig().getPrimaryMainColor()
        } else {
            proceedButton.isEnabled = false
            proceedButton.backgroundColor = .gray
        }
    }
    @IBAction func didTapProceedButton(sender:UIButton) {
        delegate?.didUpdateDetails(details: filledValues as NSObject)
    }
    @IBAction func didTapCloseButton(sender:UIButton) {
        delegate?.didCancel()
    }
}
