//
//  CustomCamera.swift
//  capture
//
//  Created by Admin on 23/04/22.
//

import Foundation
import AVFoundation
import UIKit
import core

public protocol ImageDelegate {
    func didCloseCustomCamera(openGallery:Bool)
}
enum CameraPosition {
    case front
    case back
}

public class CustomCamera:UIView, UIGestureRecognizerDelegate {

    @IBOutlet var contentView: UIView!
    @IBOutlet var cameraView:UIView!
    @IBOutlet var messageLabel:UILabel!
    @IBOutlet var galleryButton:UIButton!
    @IBOutlet var captureButton:UIButton!
    @IBOutlet var switchCameraButton:UIButton!
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var messageView: UIView!

    private var captureSession: AVCaptureSession!
    private var cameraOutput: AVCapturePhotoOutput!
    private var previewLayer: AVCaptureVideoPreviewLayer!

    var exposureValue: Float = 0.1 // EV
    var translationY: Float = 0
    var startPanPointInPreviewLayer: CGPoint?
    open var focusMode: AVCaptureDevice.FocusMode = .continuousAutoFocus
    open var exposureMode: AVCaptureDevice.ExposureMode = .continuousAutoExposure

    fileprivate var zoomScale       = CGFloat(1.0)
    fileprivate var beginZoomScale  = CGFloat(1.0)
    fileprivate var maxZoomScale    = CGFloat(1.0)
    fileprivate weak var embeddingView: UIView?

    fileprivate lazy var focusGesture = UITapGestureRecognizer()
    fileprivate lazy var zoomGesture = UIPinchGestureRecognizer()

    fileprivate var lastFocusRectangle: CAShapeLayer?
    fileprivate var lastFocusPoint: CGPoint?

    let kCONTENT_XIB_NAME           =   "CustomCamera"

    private var deviceInput: AVCaptureDeviceInput?
    private var cameraPosition: CameraPosition = .back

    private var device: AVCaptureDevice?

    private var taskFinishedCont = 0

    private var documentKey = ""
    private var allowUpload = false
    private var images:[UIImage] = []
    private var tasksList:[Tasks] = []

    private var delegate:ImageDelegate?

    private var dalCapture = DALCapture()

    public init(frame:CGRect,delegate:ImageDelegate,tasksList:[Tasks],documentKey:String,dalCapture:DALCapture,allowUpload:Bool) {
        self.delegate = delegate
        self.tasksList = tasksList
        self.documentKey = documentKey
        self.dalCapture = dalCapture
        self.allowUpload = allowUpload
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        messageView.setCornerBorder(color: .white, cornerRadius: 20.0, borderWidth: 1)
        galleryButton.imageEdgeInsets = UIEdgeInsets(top: 70, left: 70, bottom: 70, right: 70)
        captureButton.imageEdgeInsets = UIEdgeInsets(top: 70, left: 70, bottom: 70, right: 70)
        switchCameraButton.imageEdgeInsets = UIEdgeInsets(top: 70, left: 70, bottom: 70, right: 70)
        messageLabel.text = getTitle(document_type_key: documentKey, connectedtext: " Capture Front of")
        titleLabel.text = getTitle(document_type_key: documentKey, connectedtext: "")

        if documentKey == DocumentKey.SELFIE.rawValue {
            captureButton.isEnabled = false
            switchCameraButton.isEnabled = false
            cameraPosition = .front
            startCamera(position: .front)
        } else {
            cameraPosition = .back
            startCamera(position: .back)
        }
        if !allowUpload {
            galleryButton.isEnabled = false
        }

    }
    private func startCamera(position: AVCaptureDevice.Position) {
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        cameraOutput = AVCapturePhotoOutput()

        guard let device: AVCaptureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: position) else { return }
        self.device = device
        if let currentInput = self.deviceInput {
            self.captureSession.removeInput(currentInput)
            self.deviceInput = nil
        }

        if let input = try? AVCaptureDeviceInput(device: device) {
            if (captureSession.canAddInput(input)) {
                captureSession.addInput(input)
                if (captureSession.canAddOutput(cameraOutput)) {
                    captureSession.addOutput(cameraOutput)
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                    previewLayer.frame = cameraView.bounds
                    embeddingView = cameraView
                    previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    cameraView.layer.addSublayer(previewLayer)
                    captureSession.startRunning()
                }
            } else {
                print("issue here : captureSesssion.canAddInput")
            }
            if documentKey == DocumentKey.SELFIE.rawValue {
                messageView.setCornerBorder(color: .clear, cornerRadius: 0, borderWidth: 0)
                messageLabel.setCornerBorder(color: .clear, cornerRadius: 10.0, borderWidth: 0.5)
                let output = AVCaptureVideoDataOutput()
                output.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String : NSNumber(value: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]

                output.alwaysDiscardsLateVideoFrames = true

                if captureSession.canAddOutput(output) {
                    captureSession.addOutput(output)
                }

                captureSession.commitConfiguration()

                let queue = DispatchQueue(label: "output.queue")
                output.setSampleBufferDelegate(self, queue: queue)
            }
        } else {
            print("some problem here")
        }
        createOverlay()
        attachFocus(cameraView)
        attachZoom(cameraView)
    }
    private func createOverlay() {
        let overlayView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - 330))
        overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.6)

        let path = CGMutablePath()
        let shape = CAShapeLayer()

        if documentKey == DocumentKey.SELFIE.rawValue {
            path.addRoundedRect(in: CGRect(x: 15, y: overlayView.center.y-230, width: overlayView.frame.width-30, height: overlayView.frame.height - ((overlayView.center.y-230)*2)), cornerWidth:(overlayView.frame.width-30) / 2, cornerHeight: (overlayView.frame.height - ((overlayView.center.y-230)*2)) / 2)
        } else {
            path.addRoundedRect(in: CGRect(x: 5, y: overlayView.center.y-100, width: overlayView.frame.width-10, height: 250), cornerWidth: 5, cornerHeight: 5)
        }
        path.closeSubpath()
        shape.lineWidth = 1.0
        shape.path = path

        shape.strokeColor = UIColor.white.cgColor
        shape.fillColor = UIColor.white.cgColor

        overlayView.layer.addSublayer(shape)

        path.addRect(CGRect(origin: .zero, size: overlayView.frame.size))

        let maskLayer = CAShapeLayer()
        maskLayer.backgroundColor = UIColor.black.cgColor
        maskLayer.path = path
        maskLayer.fillRule = CAShapeLayerFillRule.evenOdd

        overlayView.layer.mask = maskLayer
        overlayView.clipsToBounds = false

        cameraView.addSubview(overlayView)
    }

    @IBAction func switchCamera() {
        switch self.cameraPosition {
        case .front:
            self.cameraPosition = .back
            self.startCamera(position: .back)
        case .back:
            self.cameraPosition = .front
            self.startCamera(position: .front)
        }
    }
    func detectFaces(image:CIImage) {
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: image, options: [CIDetectorSmile:true, CIDetectorEyeBlink: true])
        if !faces!.isEmpty {
            if faces!.count > 1 {
                messageLabel.backgroundColor = .systemRed
                let image = (UIImage(systemName: "face.smiling.fill")?.withTintColor(.white))!
                messageLabel.addLeading(image: image, text: "  Multiple faces present")
                captureButton.isEnabled = false
            } else {
                messageLabel.backgroundColor = .systemGreen
                let image = (UIImage(systemName: "face.smiling.fill")?.withTintColor(.white))!
                messageLabel.addLeading(image: image, text: "  Face visible")
                captureButton.isEnabled = true
            }
        } else {
            messageLabel.backgroundColor = .systemRed
            let image = (UIImage(systemName: "face.smiling.fill")?.withTintColor(.white))!
            messageLabel.addLeading(image: image, text: "  Place your face within circle    ")
            captureButton.isEnabled = false
        }
    }
    @IBAction func takePhoto(_ sender: UITapGestureRecognizer) {
        captureButton.isUserInteractionEnabled = false
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        settings.previewPhotoFormat = previewFormat
        cameraOutput.capturePhoto(with: settings, delegate: self)
    }
}
extension CustomCamera : AVCapturePhotoCaptureDelegate,AVCaptureVideoDataOutputSampleBufferDelegate {
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if let _ = CMSampleBufferGetImageBuffer(sampleBuffer) {
            if CMSampleBufferGetNumSamples(sampleBuffer) != 1 || !CMSampleBufferIsValid(sampleBuffer) ||
                !CMSampleBufferDataIsReady(sampleBuffer) {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
                let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
                let ciimage = CIImage(cvPixelBuffer: imageBuffer)
                let image = crop(image: ciimage, withWidth: UIScreen.main.bounds.size.width, andHeight: UIScreen.main.bounds.size.height - 500)
                detectFaces(image: image ?? CIImage())
            }
        }
    }
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {

        if let error = error {
            print("error occured : \(error.localizedDescription)")
        }

        if let dataImage = photo.fileDataRepresentation() {
            let dataProvider = CGDataProvider(data: dataImage as CFData)
            let cgImageRef: CGImage! = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
            let image = UIImage(cgImage: cgImageRef, scale: 1.0, orientation: UIImage.Orientation.right)
            taskFinishedCont = taskFinishedCont + 1
            if images.isEmpty {
                images = [image]
            } else {
                images.append(image)
            }
            if taskFinishedCont == tasksList.count {
                taskFinishedCont = 0
                for (index,image) in images.enumerated() {
                    uploadImage(image: image,task:tasksList[index])
                    delegate?.didCloseCustomCamera(openGallery: false)
                    captureSession.stopRunning()
                }
            } else {
                messageLabel.text = getTitle(document_type_key: documentKey, connectedtext: " Capture Back of")
                captureButton.isUserInteractionEnabled = true
            }
        } else {
            print("some error here")
        }
    }
    @IBAction func setPicturefromLocal(_ sender: Any) {
        delegate?.didCloseCustomCamera(openGallery: true)
        captureSession.stopRunning()
    }
    @IBAction func closeCamera(_ sender: Any) {
        delegate?.didCloseCustomCamera(openGallery: false)
        captureSession.stopRunning()
    }
    func uploadImage(image:UIImage?,task:Tasks) {
        guard let image = image else { return }
        let resizedImage = resizeImage(image: image, targetSize: CGSize(width: 480, height: 640))
        guard let imageData = resizedImage.jpegData(compressionQuality: 1.0) else { return }
        dalCapture.saveTask(view:self,taskKey:task.getTaskKey(), contentType: "image/jpeg", documentType: documentKey, data: imageData)
    }

}
extension CustomCamera {
    func convertCIImageToCGImage(inputImage: CIImage) -> CGImage? {
        let context = CIContext(options: nil)
        if let cgImage = context.createCGImage(inputImage, from: inputImage.extent) {
            return cgImage
        }
        return nil
    }
    func crop(image: CIImage, withWidth width: Double, andHeight height: Double) -> CIImage? {

        if let cgImage = convertCIImageToCGImage(inputImage: image) {

            let contextImage: UIImage = UIImage(cgImage: cgImage)

            let contextSize: CGSize = contextImage.size

            var posX: CGFloat = 0.0
            var posY: CGFloat = 0.0
            var cgwidth: CGFloat = CGFloat(width)
            var cgheight: CGFloat = CGFloat(height)

            // See what size is longer and create the center off of that
            if contextSize.width > contextSize.height {
                posX = ((contextSize.width - contextSize.height) / 2)
                posY = 0
                cgwidth = contextSize.height
                cgheight = contextSize.height
            } else {
                posX = 0
                posY = ((contextSize.height - contextSize.width) / 2)
                cgwidth = contextSize.width
                cgheight = contextSize.width
            }

            let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)

            // Create bitmap image from context using the rect
            var croppedContextImage: CGImage? = nil
            if let contextImage = contextImage.cgImage {
                if let croppedImage = contextImage.cropping(to: rect) {
                    croppedContextImage = croppedImage
                }
            }

            // Create a new image based on the imageRef and rotate back to the original orientation
            if let croppedImage:CGImage = croppedContextImage {
                let image: CIImage = CIImage(cgImage: croppedImage)
//                let image: UIImage = UIImage(cgImage: croppedImage, scale: image.scale, orientation: image.imageOrientation)
                return image
            }

        }

        return nil
    }


    // Touch to focus

    fileprivate func attachFocus(_ view: UIView) {
        DispatchQueue.main.async {
            self.focusGesture.addTarget(self, action: #selector(self._focusStart(_:)))
            view.addGestureRecognizer(self.focusGesture)
            self.focusGesture.delegate = self
        }
    }
    @objc fileprivate func _focusStart(_ recognizer: UITapGestureRecognizer) {

        _changeExposureMode(mode: .continuousAutoExposure)
        translationY = 0
        exposureValue = 0.5

        if let validDevice = device,
            let validPreviewLayer = previewLayer,
            let view = recognizer.view {
                let pointInPreviewLayer = view.layer.convert(recognizer.location(in: view), to: validPreviewLayer)
                let pointOfInterest = validPreviewLayer.captureDevicePointConverted(fromLayerPoint: pointInPreviewLayer)

                do {
                    try validDevice.lockForConfiguration()

                    _showFocusRectangleAtPoint(pointInPreviewLayer, inLayer: validPreviewLayer)

                    if validDevice.isFocusPointOfInterestSupported {
                        validDevice.focusPointOfInterest = pointOfInterest
                    }

                    if  validDevice.isExposurePointOfInterestSupported {
                        validDevice.exposurePointOfInterest = pointOfInterest
                    }

                    if validDevice.isFocusModeSupported(focusMode) {
                        validDevice.focusMode = focusMode
                    }

                    if validDevice.isExposureModeSupported(exposureMode) {
                        validDevice.exposureMode = exposureMode
                    }

                    validDevice.unlockForConfiguration()
                } catch let error {
                    print(error)
                }
        }
    }
    func _changeExposureMode(mode: AVCaptureDevice.ExposureMode) {

        if device?.exposureMode == mode {
            return
        }

        do {
            try device?.lockForConfiguration()
        } catch {
            return
        }
        if device?.isExposureModeSupported(mode) == true {
            device?.exposureMode = mode
        }
        device?.unlockForConfiguration()
    }
    fileprivate func _showFocusRectangleAtPoint(_ focusPoint: CGPoint, inLayer layer: CALayer, withBrightness brightness: Float? = nil) {

        if let lastFocusRectangle = lastFocusRectangle {

            lastFocusRectangle.removeFromSuperlayer()
            self.lastFocusRectangle = nil
        }

        let size = CGSize(width: 75, height: 75)
        let rect = CGRect(origin: CGPoint(x: focusPoint.x - size.width / 2.0, y: focusPoint.y - size.height / 2.0), size: size)

        let endPath = UIBezierPath(rect: rect)
        endPath.move(to: CGPoint(x: rect.minX + size.width / 2.0, y: rect.minY))
        endPath.addLine(to: CGPoint(x: rect.minX + size.width / 2.0, y: rect.minY + 5.0))
        endPath.move(to: CGPoint(x: rect.maxX, y: rect.minY + size.height / 2.0))
        endPath.addLine(to: CGPoint(x: rect.maxX - 5.0, y: rect.minY + size.height / 2.0))
        endPath.move(to: CGPoint(x: rect.minX + size.width / 2.0, y: rect.maxY))
        endPath.addLine(to: CGPoint(x: rect.minX + size.width / 2.0, y: rect.maxY - 5.0))
        endPath.move(to: CGPoint(x: rect.minX, y: rect.minY + size.height / 2.0))
        endPath.addLine(to: CGPoint(x: rect.minX + 5.0, y: rect.minY + size.height / 2.0))
        if brightness != nil {
            endPath.move(to: CGPoint(x: rect.minX + size.width + size.width / 4, y: rect.minY))
            endPath.addLine(to: CGPoint(x: rect.minX + size.width + size.width / 4, y: rect.minY + size.height))

            endPath.move(to: CGPoint(x: rect.minX + size.width + size.width / 4 - size.width / 16, y: rect.minY + size.height - CGFloat(brightness!) * size.height))
            endPath.addLine(to: CGPoint(x: rect.minX + size.width + size.width / 4 + size.width / 16, y: rect.minY + size.height - CGFloat(brightness!) * size.height))
        }

        let startPath = UIBezierPath(cgPath: endPath.cgPath)
        let scaleAroundCenterTransform = CGAffineTransform(translationX: -focusPoint.x, y: -focusPoint.y).concatenating(CGAffineTransform(scaleX: 2.0, y: 2.0).concatenating(CGAffineTransform(translationX: focusPoint.x, y: focusPoint.y)))
        startPath.apply(scaleAroundCenterTransform)

        let shapeLayer = CAShapeLayer()
        shapeLayer.path = endPath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor(red: 1, green: 0.83, blue: 0, alpha: 0.95).cgColor
        shapeLayer.lineWidth = 1.0

        layer.addSublayer(shapeLayer)
        lastFocusRectangle = shapeLayer
        lastFocusPoint = focusPoint

        CATransaction.begin()

        CATransaction.setAnimationDuration(0.2)
        CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut))

        CATransaction.setCompletionBlock {
            if shapeLayer.superlayer != nil {
                shapeLayer.removeFromSuperlayer()
                self.lastFocusRectangle = nil
            }
        }
        if brightness == nil {
            let appearPathAnimation = CABasicAnimation(keyPath: "path")
            appearPathAnimation.fromValue = startPath.cgPath
            appearPathAnimation.toValue = endPath.cgPath
            shapeLayer.add(appearPathAnimation, forKey: "path")

            let appearOpacityAnimation = CABasicAnimation(keyPath: "opacity")
            appearOpacityAnimation.fromValue = 0.0
            appearOpacityAnimation.toValue = 1.0
            shapeLayer.add(appearOpacityAnimation, forKey: "opacity")
        }

        let disappearOpacityAnimation = CABasicAnimation(keyPath: "opacity")
        disappearOpacityAnimation.fromValue = 1.0
        disappearOpacityAnimation.toValue = 0.0
        disappearOpacityAnimation.beginTime = CACurrentMediaTime() + 0.8
        disappearOpacityAnimation.fillMode = CAMediaTimingFillMode.forwards
        disappearOpacityAnimation.isRemovedOnCompletion = false
        shapeLayer.add(disappearOpacityAnimation, forKey: "opacity")

        CATransaction.commit()
    }

// Zoom
    fileprivate func attachZoom(_ view: UIView) {
        DispatchQueue.main.async {
            self.zoomGesture.addTarget(self, action: #selector(self._zoomStart(_:)))
            view.addGestureRecognizer(self.zoomGesture)
            self.zoomGesture.delegate = self
            self._setupMaxZoomScale()
        }
    }

    @nonobjc open override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {

        if gestureRecognizer.isKind(of: UIPinchGestureRecognizer.self) {
            beginZoomScale = zoomScale
        }

        return true
    }

    @objc fileprivate func _zoomStart(_ recognizer: UIPinchGestureRecognizer) {
        guard let view = embeddingView,
            let previewLayer = previewLayer
            else { return }

        var allTouchesOnPreviewLayer = true
        let numTouch = recognizer.numberOfTouches

        for i in 0 ..< numTouch {
            let location = recognizer.location(ofTouch: i, in: view)
            let convertedTouch = previewLayer.convert(location, from: previewLayer.superlayer)
            if !previewLayer.contains(convertedTouch) {
                allTouchesOnPreviewLayer = false
                break
            }
        }
        if allTouchesOnPreviewLayer {
            _zoom(recognizer.scale)
        }
    }

    fileprivate func _zoom(_ scale: CGFloat) {

        do {
            try device?.lockForConfiguration()

            zoomScale = max(1.0, min(beginZoomScale * scale, maxZoomScale))

            device?.videoZoomFactor = zoomScale

            device?.unlockForConfiguration()

        } catch {
            print("Error locking configuration")
        }
    }
    fileprivate func _setupMaxZoomScale() {
        maxZoomScale = device?.activeFormat.videoMaxZoomFactor ?? CGFloat(1.0)
    }
}
public func getTitle(document_type_key:String, connectedtext:String) -> String {
    var title:String = "Capture/Upload"
    switch DocumentKey(rawValue: document_type_key) {
    case .IND_PAN:
        title = "\(connectedtext) Pan Card "
        break
    case .IND_AADHAR:
        title = "\(connectedtext) Aadhaar Card "
        break
    case .IND_VOTER_ID,.PHL_VOTER_ID:
        title = "\(connectedtext) Voter Card "
        break
    case .IND_DRIVING_LICENSE,.PHL_DRIVING_LICENSE:
        title = "\(connectedtext) Driving License "
        break
    case .IND_PASSPORT,.PHL_PASSPORT:
        title = "\(connectedtext) Passport "
        break
    case .IND_CHEQUE:
        title = "\(connectedtext) Ind Cheque "
        break
    case .WET_SIGN:
        title = "  Wet Signature  "
        break
    case .SELFIE:
        title = "  Selfie  "
        break
    case .BANK_ACCOUNT_PROOF:
        title = "\(connectedtext) Bank Account Proof "
        break
    case .IND_GST_CERTIFICATE:
        title = "\(connectedtext) GST Certificate "
        break
    case .PHL_SSS:
        title = "\(connectedtext) Social Security Card "
        break
    case .PHL_TIN:
        title = "\(connectedtext) Tax Identification Number "
    case .SHOP_EST_LICENSE:
        title = "\(connectedtext) Shop Est License "
        break
    case .STORE_FRONT:
        title = "\(connectedtext) Store Front "
        break
    default:
        title = "\(connectedtext) Document "
        break
    }
    return title
}
public class OvalView: UIView {

    public override func layoutSubviews() {
        super.layoutSubviews()
        layoutOvalMask()
    }

    private func layoutOvalMask() {
        let mask = self.shapeMaskLayer()
        let bounds = self.bounds
        if mask.frame != bounds {
            mask.frame = bounds
            mask.path = CGPath(ellipseIn: bounds, transform: nil)
        }
    }

    private func shapeMaskLayer() -> CAShapeLayer {
        if let layer = self.layer.mask as? CAShapeLayer {
            return layer
        }
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.white.cgColor
        self.layer.mask = layer
        return layer
    }

}
//extension CIFaceFeature {
//        func leftEyePosition(for image: UIImage?) -> CGPoint {
//            return point(for: image, from: leftEyePosition)
//        }
//
//        func rightEyePosition(for image: UIImage?) -> CGPoint {
//            return point(for: image, from: rightEyePosition)
//        }
//
//        func mouthPosition(for image: UIImage?) -> CGPoint {
//            return point(for: image, from: mouthPosition)
//        }
//
//    func bounds(for image: UIImage?, inView viewSize: CGSize) -> CGRect {
//        let normalizedBounds = self.normalizedBounds(for: image, fromBounds: bounds)
//        return bounds(inView: viewSize, fromNormalizedBounds: normalizedBounds)
//    }
//    func normalizedBounds(for image: UIImage?) -> CGRect {
//        return normalizedBounds(for: image, fromBounds: bounds)
//    }
//    func normalizedBounds(for image: UIImage?, fromBounds originalBounds: CGRect) -> CGRect {
//        var normalizedBounds = bounds(for: image, fromBounds: originalBounds)
//        normalizedBounds.origin.x /= image?.size.width ?? 0.0
//        normalizedBounds.origin.y /= image?.size.height ?? 0.0
//        normalizedBounds.size.width /= image?.size.width ?? 0.0
//        normalizedBounds.size.height /= image?.size.height ?? 0.0
//        return normalizedBounds
//    }
//    func bounds(inView viewSize: CGSize, fromNormalizedBounds normalizedBounds: CGRect) -> CGRect {
//        return CGRect(
//            x: normalizedBounds.origin.x * viewSize.width,
//            y: normalizedBounds.origin.y * viewSize.height,
//            width: normalizedBounds.size.width * viewSize.width,
//            height: normalizedBounds.size.height * viewSize.height)
//    }
//    func bounds(for image: UIImage?) -> CGRect {
//        return bounds(for: image, fromBounds: bounds)
//    }
//
//    func normalizedLeftEyePosition(for image: UIImage?) -> CGPoint {
//        return normalizedPoint(for: image, from: leftEyePosition)
//    }
//
//    func normalizedRightEyePosition(for image: UIImage?) -> CGPoint {
//        return normalizedPoint(for: image, from: rightEyePosition)
//    }
//    func normalizedMouthPosition(for image: UIImage?) -> CGPoint {
//        return normalizedPoint(for: image, from: mouthPosition)
//    }
//    func leftEyePosition(for image: UIImage?, inView viewSize: CGSize) -> CGPoint {
//        let normalizedPoint = normalizedLeftEyePosition(for: image)
//        return point(inView: viewSize, fromNormalizedPoint: normalizedPoint)
//    }
//    func rightEyePosition(for image: UIImage?, inView viewSize: CGSize) -> CGPoint {
//        let normalizedPoint = normalizedRightEyePosition(for: image)
//        return point(inView: viewSize, fromNormalizedPoint: normalizedPoint)
//    }
//
//    func mouthPosition(for image: UIImage?, inView viewSize: CGSize) -> CGPoint {
//        let normalizedPoint = normalizedMouthPosition(for: image)
//        return point(inView: viewSize, fromNormalizedPoint: normalizedPoint)
//    }
//    func point(inView viewSize: CGSize, fromNormalizedPoint normalizedPoint: CGPoint) -> CGPoint {
//        return CGPoint(x: normalizedPoint.x * viewSize.width, y: normalizedPoint.y * viewSize.height)
//    }
//    func point(for image: UIImage?, from originalPoint: CGPoint) -> CGPoint {
//        let imageWidth = image?.size.width ?? 0.0
//        let imageHeight = image?.size.height ?? 0.0
//        var convertedPoint = CGPoint()
//        switch image?.imageOrientation {
//        case .up:
//            convertedPoint.x = originalPoint.x
//            convertedPoint.y = imageHeight - originalPoint.y
//        case .down:
//            convertedPoint.x = imageWidth - originalPoint.x
//            convertedPoint.y = originalPoint.y
//        case .left:
//            convertedPoint.x = imageWidth - originalPoint.y
//            convertedPoint.y = imageHeight - originalPoint.x
//        case .right:
//            convertedPoint.x = originalPoint.y
//            convertedPoint.y = originalPoint.x
//        case .upMirrored:
//            convertedPoint.x = imageWidth - originalPoint.x
//            convertedPoint.y = imageHeight - originalPoint.y
//        case .downMirrored:
//            convertedPoint.x = originalPoint.x
//            convertedPoint.y = originalPoint.y
//        case .leftMirrored:
//            convertedPoint.x = imageWidth - originalPoint.y
//            convertedPoint.y = originalPoint.x
//        case .rightMirrored:
//            convertedPoint.x = originalPoint.y
//            convertedPoint.y = imageHeight - originalPoint.x
//        default:
//            break
//        }
//        return convertedPoint
//    }
//    func normalizedPoint(for image: UIImage?, from originalPoint: CGPoint) -> CGPoint {
//        var normalizedPoint = point(for: image, from: originalPoint)
//        normalizedPoint.x /= image?.size.width ?? 0.0
//        normalizedPoint.y /= image?.size.height ?? 0.0
//        return normalizedPoint
//    }
//    func size(for image: UIImage?, from originalSize: CGSize) -> CGSize {
//        var convertedSize = CGSize()
//        switch image?.imageOrientation {
//        case .up, .down, .upMirrored, .downMirrored:
//            convertedSize.width = originalSize.width
//            convertedSize.height = originalSize.height
//        case .left, .right, .leftMirrored, .rightMirrored:
//            convertedSize.width = originalSize.height
//            convertedSize.height = originalSize.width
//        default:
//            break
//        }
//        return convertedSize
//    }
//    func normalizedSize(for image: UIImage?, from originalSize: CGSize) -> CGSize {
//        var normalizedSize = size(for: image, from: originalSize)
//        normalizedSize.width /= image?.size.width ?? 0.0
//        normalizedSize.height /= image?.size.height ?? 0.0
//
//        return normalizedSize
//    }
//    func size(inView viewSize: CGSize, fromNormalizedSize normalizedSize: CGSize) -> CGSize {
//        return CGSize(width: normalizedSize.width * viewSize.width, height: normalizedSize.height * viewSize.height)
//    }
//    func bounds(for image: UIImage?, fromBounds originalBounds: CGRect) -> CGRect {
//        var convertedOrigin = point(for: image, from: originalBounds.origin)
//        let convertedSize = size(for: image, from: originalBounds.size)
//        switch image?.imageOrientation {
//        case .up:
//            convertedOrigin.y -= convertedSize.height
//        case .down:
//            convertedOrigin.x -= convertedSize.width
//        case .left:
//            convertedOrigin.x -= convertedSize.width
//            convertedOrigin.y -= convertedSize.height
//        case .right:
//            break
//        case .upMirrored:
//            convertedOrigin.y -= convertedSize.height
//            convertedOrigin.x -= convertedSize.width
//        case .downMirrored:
//            break
//        case .leftMirrored:
//            convertedOrigin.x -= convertedSize.width
//            convertedOrigin.y += convertedSize.height
//        case .rightMirrored:
//            convertedOrigin.y -= convertedSize.height
//        default:
//            break
//        }
//        return CGRect(
//            x: convertedOrigin.x,
//            y: convertedOrigin.y,
//            width: convertedSize.width,
//            height: convertedSize.height)
//    }
//
//}
