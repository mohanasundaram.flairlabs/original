//
//  Image.swift
//  capture
//
//  Created by Admin on 19/04/22.
//

import Foundation
import UIKit
import core
import Lottie

public protocol DynamicViewCallBack {
    func didUpdateDetails(details:NSObject)
    func didCancel()
}

public class Image:ArtifactComponent,ImageDelegate,DocFetcherCallback,DropDownDataSourceProtocol, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var errorMessagelabel: UILabel!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var recaptureButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var recaptureButtonView: UIView!
    @IBOutlet weak var clearImageButton: UIButton!
    @IBOutlet weak var placeholderImageView: UIView!
    @IBOutlet weak var clickToCapture: UILabel!
    @IBOutlet weak var processingView:UIView!
    @IBOutlet weak var closeImageView:UIImageView!
    @IBOutlet weak var imageActivityIndicator:UIActivityIndicatorView!
    @IBOutlet weak var imageNameView:UIView!
    @IBOutlet weak var errorMessageView:UIView!
    @IBOutlet weak var cardNameLabel:UILabel!
    @IBOutlet weak var verifiedMessageView:UIView!
    @IBOutlet weak var imageSetView:UIView!
    @IBOutlet weak var captureImageView:UIImageView!
    @IBOutlet weak var digiLockerView:UIView!
    @IBOutlet weak var digiLockerButton:UIButton!
    @IBOutlet weak var widthConstraints:NSLayoutConstraint!
    @IBOutlet weak var processingActivityIndicator:UIActivityIndicatorView!
    @IBOutlet weak var processingLabel:UILabel!
    @IBOutlet weak var digiLockerErrorMessageView:UIView!
    @IBOutlet weak var digiLockerErrorLabel:UILabel!
    @IBOutlet weak var digiLockerMessageLabel:UILabel!


    private var imagePicker = UIImagePickerController()

    private var templateObject:NSObject?

    private var dalCapture = DALCapture()

    private var imageCapture:CustomCamera?

    private var dropDown = DropDownList()

    private var artifactKey = ""

    private var artifactHashMap:[String:Any] = [:]

    private var optionList: [String] = []

    private var templateId = 0

    private var document_type_key = ""

    private var error = ""

    private var captureMechanism:NSObject?

    private var isImageLoaded = false

    private var webViewController = WebViewController()

    private var themeConfig:ThemeConfig? = nil

    private var fullScreenView = UIView()

    private var task:Tasks? = nil

    private var taskKey = ""

    private var details:[String] = []

    private var allowUpload = false


    let kCONTENT_XIB_NAME           =   "Image"

    public override init(frame:CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    public func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        imageView.isHidden = true
        digiLockerView.isHidden = true
        recaptureButtonView.isHidden = true
        clearImageButton.isHidden = true
        processingView.isHidden = true
        closeImageView.isHidden = true
        imageNameView.isHidden = true
        errorMessageView.isHidden = true
        imageActivityIndicator.isHidden = true
        cardNameLabel.isHidden = true
        verifiedMessageView.isHidden = true
        digiLockerErrorMessageView.isHidden = true

        widthConstraints.constant = 150

    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager) {
        setConfig(dalCapture: dalCapture, templateId: templateId)
        taskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager,themeConfig:ThemeConfig) {
        self.themeConfig = themeConfig
        setConfig(dalCapture: dalCapture, templateId: templateId)
        taskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,formTaskManager:FormTaskManager) {
        setConfig(dalCapture: dalCapture, templateId: templateId)
        formTaskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    private func setConfig(dalCapture:DALCapture,templateId:Int) {
        if themeConfig == nil {
            themeConfig = dalCapture.getThemeConfig()
        }
        setColors()
        self.dalCapture = dalCapture
        self.templateId = templateId
        self.templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")
        self.titleLabel.text = templateObject?.value(forKey: "label") as? String ?? ""

        self.document_type_key = templateObject?.value(forKey: "document_type_key") as? String ?? ""
        self.taskKey = templateObject?.value(forKey: "task_key") as? String ?? ""
        allowUpload = templateObject?.value(forKey: "allow_upload") as? Bool ?? false
        let side = templateObject?.value(forKey: "side") as? String ?? ""
        task = Tasks()
        task?.setTaskKey(taskKey: taskKey)
        task?.setSide(side: side)
        captureMechanism = templateObject?.value(forKey: "capture_mechanisms") as? NSObject

        artifactKey = dalCapture.getArtifactKey(object: templateObject ?? [] as NSObject)

        if ((captureMechanism?.value(forKey: "digilocker")) != nil) {
            let digiLocker = captureMechanism?.value(forKey: "digilocker") as! NSObject
            details = digiLocker.value(forKey: "details") as? [String] ?? []
            widthConstraints.constant = self.frame.width
        } else {
            widthConstraints.constant = (self.frame.width - 25) / 2
        }
        showPlaceholderImage()
        initialise(dalCapture: dalCapture, templateId: templateId, artifactKey: artifactKey)
    }
    public override func setArtifact(resultObject: NSObject) {
        if ((captureMechanism?.value(forKey: "digilocker")) != nil) {
            webViewController.dismissWebView()
            imageSetView.isHidden = true
            digiLockerView.isHidden = false


            if let result = resultObject.value(forKey: "error") as? String {
                error = result
            } else if let _ = resultObject.value(forKey: "error") as? NSNull {
                error = ""
            }
            if error == "" {
                if resultObject.value(forKey: "present") as! Bool {
                    DispatchQueue.main.async { [self] in
                        showDigilockerSuccessView()
                        digiLockerView.isHidden = true
                        if artifactKey.contains("ind_pan") {
                            digiLockerMessageLabel.text = StringHandler.DigiLockerPanMessage.rawValue
                        } else {
                            digiLockerMessageLabel.text = StringHandler.DigiLockerAadharMessage.rawValue
                        }
                    }
                }
            } else {
                error = StringHandler.DigiLockerErrorMessage.rawValue
                DispatchQueue.main.async { [self] in
                    digiLockerErrorLabel.text = self.error
                    digiLockerErrorLabel.textColor = themeConfig?.getErrorMainColor() ?? .red
                    digiLockerErrorMessageView.isHidden = false
                    recaptureButtonView.isHidden = true
                    imageButton.isUserInteractionEnabled = true
                }
                return
            }
        } else {
            self.document_type_key = resultObject.value(forKey: "document_type_key") as? String ?? ""
            cardNameLabel.text = getTitle(document_type_key: self.document_type_key, connectedtext: "")
            let imageUrl = self.getImageUrl(imageObject: resultObject)
            let present = resultObject.value(forKey: "present") as! Bool
            let uploading = resultObject.value(forKey: "uploading") as? Bool ?? false
            let artifactKey = resultObject.value(forKey: "key") as! String
            var error = ""
            if let result = resultObject.value(forKey: "error") as? String {
                error = result
            } else if let _ = resultObject.value(forKey: "error") as? NSNull {
                error = ""
            }
            let divStatus = resultObject.value(forKey: "div_status") as? String ?? ""
            DispatchQueue.main.async { [self] in
                if dalCapture.divProgressList.contains(artifactKey) {
                    processingView.isHidden = false
                } else {
                    processingView.isHidden = true
                }
                if uploading {
                    showActualImage()
                }
                if !uploading && !present {
                    showPlaceholderImage()
                } else if present {
                    showActualImage()
                    if !isImageLoaded {
                        imageActivityIndicator.isHidden = false
                        if verifyUrl(urlString: imageUrl) {
                        UIImage.loadFrom(url: URL(string: imageUrl)!) { [self] image in
                            imageActivityIndicator.isHidden = true
                            if image != nil {
                                imageView.image = image
                                isImageLoaded = true
                                clearImageButton.isEnabled  = true
                            } else {
                                isImageLoaded = false
                            }
                        }
                        }
                    }
                    if divStatus == "div_pending" {
                        errorMessageView.isHidden = true
                        errorMessagelabel.isHidden = true
                        recaptureButtonView.isHidden = true
                        recaptureButton.isHidden = true
                    }
                    if error != ""  {
                        errorMessageView.isHidden = false
                        errorMessagelabel.isHidden = false
                        recaptureButtonView.isHidden = false
                        recaptureButton.isHidden = false
                        if artifactKey.contains("back") {
                            recaptureButton.isHidden = true
                            recaptureButtonView.isHidden = true
                        }
                        if error == "INCORRECT_DOCUMENT" || error == "UNREADABLE_DOCUMENT" {
                            errorMessagelabel.attributedText = NSAttributedString(string: StringHandler.ImageErrorMessage.rawValue, attributes: [.foregroundColor: themeConfig?.getErrorMainColor() ?? .red])
                        } else if error == "DOCUMENT_TAMPERED" {
                            errorMessagelabel.attributedText = NSAttributedString(string: StringHandler.PanTamperedErrorMessage.rawValue, attributes: [.foregroundColor: themeConfig?.getErrorMainColor() ?? .red])
                        } else {
                            errorMessagelabel.attributedText = NSAttributedString(string: error, attributes: [.foregroundColor: themeConfig?.getErrorMainColor() ?? .red])
                        }
                    }
                } else {
                    showPlaceholderImage()
                }
            }
        }
    }
    private func showPlaceholderImage() {
        imageButton.isUserInteractionEnabled = true
        imageView.isHidden = true
        imageNameView.isHidden = true
        placeholderImageView.isHidden = false
        clearImageButton.isHidden = true
        closeImageView.isHidden = true
        errorMessagelabel.isHidden = true
        titleLabel.isHidden = true
        recaptureButtonView.isHidden = true
        cardNameLabel.isHidden = true
        if ((captureMechanism?.value(forKey: "digilocker")) != nil) {
            captureImageView.image = UIImage(named: "DigiLocker", in: Bundle(for: type(of: self)), compatibleWith: nil)!
            clickToCapture.text = "Click to verify"
        }
    }
    private func showActualImage() {
        imageButton.isUserInteractionEnabled = false
        imageView.isHidden = false
        imageNameView.isHidden = false
        placeholderImageView.isHidden = true
        clearImageButton.isHidden = false
        closeImageView.isHidden = false
        errorMessagelabel.isHidden = false
        titleLabel.isHidden = false
        recaptureButtonView.isHidden = true
        cardNameLabel.isHidden = false
        clearImageButton.isEnabled = true
        if artifactKey.contains("back") {
            cardNameLabel.text = "  "
        }
    }
    public func enableRecaptureButton() {
        recaptureButtonView.isHidden = false
        recaptureButton.isHidden = false
    }
    private func showDigilockerSuccessView() {
        cardNameLabel.isHidden = true
        imageSetView.isHidden = true
        imageNameView.isHidden = true
        processingView.isHidden = true
        errorMessageView.isHidden = true
        recaptureButtonView.isHidden = true
        verifiedMessageView.isHidden = false
    }
    private func setColors() {
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 1.0
        imageView.layer.cornerRadius = 5
        imageView.layer.borderColor = themeConfig?.getSecondaryContrastColor().cgColor
        captureImageView.tintColor = themeConfig?.getPrimaryMainColor()
//        captureImageView.image?.withTintColor()
        clickToCapture.textColor = themeConfig?.getPrimaryMainColor()
        placeholderImageView.setCornerBorder(color: themeConfig?.getPrimaryMainColor(), cornerRadius: 5, borderWidth: 1)
        titleLabel.textColor = themeConfig?.getSecondaryContrastColor()
        cardNameLabel.textColor = themeConfig?.getSecondaryContrastColor()
        imageActivityIndicator.color =  themeConfig?.getSecondaryContrastColor()
        processingActivityIndicator.color =  themeConfig?.getSecondaryContrastColor()
        processingLabel.textColor =  themeConfig?.getSecondaryContrastColor()
        recaptureButton.tintColor = themeConfig?.getPrimaryMainColor()
        closeImageView.addShadowToView()

        digiLockerButton.setCornerBorder(color: themeConfig?.getPrimaryMainColor(), cornerRadius: 10, borderWidth: 1)
        verifiedMessageView.setCornerBorder(color: themeConfig?.getPrimaryMainColor(), cornerRadius: 10, borderWidth: 1)
        digiLockerButton.backgroundColor = themeConfig?.getPrimaryMainColor()
    }
    private  func getImageUrl(imageObject:NSObject) -> String {
        var imageUrl:String = ""
        if let _ = imageObject.value(forKey: "download_urls") as? NSObject {
            let downloadObject = imageObject.value(forKey: "download_urls") as? NSObject
            let pngObject = downloadObject?.value(forKey: "image/jpeg") as? NSObject
            imageUrl = pngObject?.value(forKey: "url") as? String ?? ""
        } else {
            imageUrl = imageObject.value(forKey: "value") as? String ?? ""
            imageUrl = imageUrl+"&t="+dalCapture.getToken()+"%2F"+dalCapture.getCaptureSessionId()+"&auth_service=pg&type=session"
        }
        return imageUrl
    }
    @IBAction func imageButtonAction(_ sender: UIButton) {

        if document_type_key == DocumentKey.SELFIE.rawValue || document_type_key == DocumentKey.WET_SIGN.rawValue {
            self.selectOption(title: getTitle(document_type_key: document_type_key, connectedtext: ""))
        } else {
            processDocumentOption()
        }

    }
    @IBAction func digiLockerButtonAction(_ sender: UIButton) {
        if details.count > 0 {
            prefillDetails()
        } else {
            dalCapture.documentFetcher(artifactKey: artifactKey, uploading: true, error: "", docFetcherCallback: self)
        }
    }
    private func processDocumentOption() {
        if !document_type_key.isEmpty {
            updateArtifactObject(documentType: document_type_key)
            self.selectOption(title: getTitle(document_type_key: document_type_key, connectedtext: ""))
        } else if !optionList.isEmpty {
            if optionList.count == 1 {
                document_type_key = optionList[0]
                updateArtifactObject(documentType: document_type_key)
                self.selectOption(title: getTitle(document_type_key: document_type_key, connectedtext: ""))
            } else {
                showOptions()
            }
        } else {
            if let categoryType = templateObject?.value(forKey: "category_type") as? String {
                if categoryType.contains("poa") {
                    optionList = dalCapture.getPoaOptionList()
                } else if categoryType.contains("poi") {
                    optionList = dalCapture.getPoiOptionList()
                } else {
//                    document_type_key = categoryType
                    self.selectOption(title: getTitle(document_type_key: document_type_key, connectedtext: ""))
                }
            }
            if optionList.count == 1 {
                document_type_key = optionList[0]
                updateArtifactObject(documentType: document_type_key)
                self.selectOption(title: getTitle(document_type_key: document_type_key, connectedtext: ""))
            } else {
                showOptions()
            }
        }
    }
    private func showOptions() {
        dropDown = DropDownList()
        setUpDropDown()
        fullScreenView = UIView(frame: CGRect(x: 0, y: 0, width: (self.window?.rootViewController?.view.frame.width)!, height: (self.window?.rootViewController?.view.frame.height)!))
        fullScreenView.backgroundColor = .clear
        fullScreenView.addSubview(dropDown)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self
        fullScreenView.addGestureRecognizer(tap)
        self.window?.rootViewController?.view.addSubview(fullScreenView)
        self.window?.rootViewController?.view.bringSubviewToFront(fullScreenView)
        self.dropDown.showDropDown(height: 50 * CGFloat(optionList.count))
    }
    private func setUpDropDown() {
        dropDown.dropDownDataSourceProtocol = self
        dropDown.setUpDropDown(viewPositionReference: (CGRect(x: 50, y: 300, width: 300, height: 0)), offset: 2, optionlist: optionList, backgroundColor:ThemeConfig.shared.getSecondaryMainColor(), havingTitle: true)
        dropDown.setRowHeight(height: 50)
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        hidePopup()
    }
    private func hidePopup() {
        self.dropDown.hideDropDown()
        if fullScreenView.isDescendant(of: (self.window?.rootViewController?.view)!) {
            fullScreenView.removeFromSuperview()
        }
    }
    internal func selectItemInDropDown(indexPos: Int) {
        self.document_type_key = optionList[indexPos - 1]
        updateArtifactObject(documentType: document_type_key)
        //        title.text = getTitle(document_type_key: self.document_type_key, connectedtext: "")
        self.dropDown.hideDropDown()
        self.window?.rootViewController?.checkforCameraAccess(completion: { success in
            self.selectOption(title: getTitle(document_type_key: self.document_type_key, connectedtext: "Capture/Upload Front of"))
            self.hidePopup()
        })
    }
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.dropDown) == true {
            return false
        }
        return true
    }
    private func updateArtifactObject(documentType:String) {
        for k in artifactHashMap.keys {
            var artifactObject = dalCapture.getArtifactsObjectByKey(key: k) as! [String:Any]
            artifactObject.updateValue(documentType, forKey: "document_type_key")
            dalCapture.updateArtifactObject(updateObject: artifactObject as NSObject, artifactKey: k)
        }
    }
    public func didCloseCustomCamera(openGallery:Bool) {
        if ((imageCapture?.isDescendant(of: (window?.rootViewController?.view)!)) != nil) {
            imageCapture?.removeFromSuperview()
        }
        if openGallery {
            self.selectImageFromLocal()
            return
        }
    }
    @IBAction func recaptureButton(_ sender: UIButton) {
        removeImage()
        dalCapture.resetTask(taskKey: taskKey)
//        dalCapture.resetComponent(artifactKey: artifactKey)
    }
    @IBAction func clearButton(_ sender:UIButton) {
        removeImage()
        dalCapture.resetTask(taskKey: taskKey)
//        dalCapture.resetComponent(artifactKey: artifactKey)
    }
    public func onFailure(event: String, jsonObject: NSObject) {
        DispatchQueue.main.async { [self] in
            let message = jsonObject.value(forKey: "message") as? String
            window?.rootViewController?.showAlert(title: message ?? "", message: "Please try again after some time", actionTitle: "Close", completion: { [self] success in
                showPlaceholderImage()
                return
            })
        }
    }
    public func removeImage() {
        isImageLoaded = false
        DispatchQueue.main.async {
            self.imageView.image =  nil
            self.imageActivityIndicator.isHidden = true
        }
    }
    public func onSuccess(event: String, jsonObject: NSObject) {
        DispatchQueue.main.async { [self] in
            showPlaceholderImage()
        }
    }
    private func prefillDetails() {

        fullScreenView = UIView(frame: CGRect(x: 0, y: 0, width: (self.window?.rootViewController?.view.frame.width)!, height: (self.window?.rootViewController?.view.frame.height)!))
        fullScreenView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let dynamicView = DynamicView(frame: fullScreenView.frame)
        fullScreenView.addSubview(dynamicView)
        dynamicView.setValues(dalCapture: dalCapture, details: details, delegate: self)
        self.window?.rootViewController?.view.addSubview(fullScreenView)
        self.window?.rootViewController?.view.bringSubviewToFront(fullScreenView)
    }
    private func selectOption(title:String) {
        if allowUpload {
        let alertController = UIAlertController(title: "Capture Option", message: title, preferredStyle: .alert)

        let actionOne = UIAlertAction(title: "Take photo", style: .default) { (action) in
            self.openCamera()
        }
        alertController.addAction(actionOne)
        let actionTwo = UIAlertAction(title: "Photo library", style: .default) { (action) in
            self.selectImageFromLocal()
        }
        alertController.addAction(actionTwo)

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = self
            alertController.popoverPresentationController?.sourceRect = self.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        self.window?.rootViewController?.present(alertController, animated: true)
        } else {
            openCamera()
        }
    }
    private func openCamera() {

        self.window?.rootViewController?.checkforCameraAccess(completion: { [self] success in
            DispatchQueue.main.async { [self] in
                if success ?? false {
                    imageCapture = CustomCamera(frame: window!.frame, delegate: self,tasksList:[task ?? Tasks()],documentKey: document_type_key, dalCapture: dalCapture, allowUpload: allowUpload)
                    imageCapture?.tag = 100
                    UIView.transition(with: imageCapture!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        self.window?.rootViewController?.view.addSubview(self.imageCapture!)
                    })
                } else {
                    self.window?.rootViewController?.showAlert(title: "Unable to open Camera", message: "Please enable Camera features in settings to proceed", actionTitle: "Ok", completion: { success in

                    })
                }
            }
        })
    }
    private func selectImageFromLocal() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            DispatchQueue.main.async {
            self.window?.rootViewController?.present(self.imagePicker, animated: true)
            }
        }
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            guard let task = task else {return}
            let resizedImage = resizeImage(image: image, targetSize: CGSize(width: 480, height: 640))
            guard let imageData = resizedImage.jpegData(compressionQuality: 1.0) else { return }
            dalCapture.saveTask(view:self,taskKey: task.getTaskKey(), contentType: "image/jpeg", documentType: document_type_key, data: imageData, artifactCallback: nil)
        }
    }
}
extension Image:DynamicViewCallBack {
    public func didUpdateDetails(details: NSObject) {
        self.didCancel()
        self.dalCapture.documentFetcher(artifactKey: self.artifactKey, uploading: true, error: "",details: details, docFetcherCallback: self)
    }
    public func didCancel() {
        if fullScreenView.isDescendant(of: (self.window?.rootViewController?.view)!) {
            fullScreenView.removeFromSuperview()
        }
    }

    public func setBackgroundColor(color:UIColor) {
        contentView.backgroundColor = color
    }
    public func setImageNameColor(color:UIColor) {
        cardNameLabel.textColor = color
    }
    public func setTitleNameColor(color:UIColor) {
        titleLabel.textColor = color
    }
    public func setErrorMessageNameColor(color:UIColor) {
        errorMessagelabel.textColor = color
    }

    public func onArtifactUpdate(event: String, jsonObject: NSObject) {
        if event == "ARTIFACT_UPDATE" {
            if let result = jsonObject.value(forKey: "error") as? String {
                error = result
            } else if let _ = jsonObject.value(forKey: "error") as? NSNull {
                error = ""
            }
        }
    }

    public func onArtifactFailure(event: String, message: String) {

    }

    public func onDocumentFetch(event: String, jsonObject: NSObject) {
        if jsonObject.value(forKey: "status") as! String == "success" {
            if let redirect_url = jsonObject.value(forKey: "redirect_url") as? String {
                DispatchQueue.main.async { [self] in
                    webViewController.renderWebView(url: redirect_url)
                    self.window?.rootViewController?.present(webViewController, animated: true, completion: nil)
                }
            }
        }
    }
}
