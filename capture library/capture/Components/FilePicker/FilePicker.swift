//
//  FilePicker.swift
//  capture
//
//  Created by FL Mohan on 25/05/22.
//

import Foundation
import UIKit
import core
import MobileCoreServices
import UniformTypeIdentifiers


public class FilePicker: ArtifactComponent,UIDocumentPickerDelegate,UINavigationControllerDelegate {


@IBOutlet weak var contentView: UIView!
@IBOutlet weak var button:UIButton!
@IBOutlet weak var titelLabel:UILabel!
@IBOutlet weak var uploadView:UIView!
@IBOutlet weak var uploadedView:UIView!
@IBOutlet weak var textLabel:UILabel!
@IBOutlet weak var clearButton:UIButton!
@IBOutlet weak var loadingView:UIView!
@IBOutlet weak var afterUploadView:UIView!

    let kCONTENT_XIB_NAME           =   "FilePicker"

    private var dalCapture = DALCapture()

    private var templateId = 0

    private var error = ""

    private var templateObject = NSObject()

    private var artifactKey = ""

    private var fileName = ""

    private var taskKey = ""

    public override init(frame:CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        button.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)
        uploadedView.isHidden = true
        loadingView.isHidden = true

    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager) {
        setConfig(dalCapture: dalCapture, templateId:templateId)
        taskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,formTaskManager:FormTaskManager) {
        setConfig(dalCapture: dalCapture, templateId:templateId)
        formTaskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    private func setConfig(dalCapture:DALCapture,templateId:Int) {
        self.dalCapture = dalCapture
        self.templateId = templateId
        guard let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)") else { return }
        self.templateObject = templateObject
        artifactKey = dalCapture.getArtifactKey(object:templateObject)
        taskKey = templateObject.value(forKey: "task_key") as? String ?? ""
        titelLabel.text = templateObject.value(forKey: "label") as? String ?? "Upload Document"
        initialise(dalCapture: dalCapture, templateId: templateId, artifactKey: artifactKey)
    }
    private func updateUi(object:NSObject) {
        uploadView.setCornerBorder(color: dalCapture.getThemeConfig().getPrimaryMainColor(), cornerRadius: 5, borderWidth: 1)
        afterUploadView.setCornerBorder(color: dalCapture.getThemeConfig().getPrimaryMainColor(), cornerRadius: 5, borderWidth: 1)
        if let result = object.value(forKey: "error") as? String {
            error = result
        } else if let _ = object.value(forKey: "error") as? NSNull {
            error = ""
        }
        if error == "" {
            DispatchQueue.main.async { [self] in
                loadingView.isHidden = true
                if object.value(forKey: "present") as! Bool {
                    uploadView.isHidden = true
                    uploadedView.isHidden = false
                    if fileName == "" {
                        textLabel.text = "Document already uploaded."
                    } else {
                        textLabel.text = fileName
                    }
                } else {
                    uploadView.isHidden = false
                    uploadedView.isHidden = true
                }
            }
        } else {
            return
        }
    }
    public override func setTemplate(resultObject: NSObject) {

    }
    public override func setArtifact(resultObject: NSObject) {
        updateUi(object: resultObject)
    }
    @IBAction func didPressButton(_ tag: UIButton) {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String], in: UIDocumentPickerMode.import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .formSheet
        self.window?.rootViewController?.present(documentPicker, animated: true)
    }
    @IBAction func didClearButton(_ tag: UIButton) {
        fileName = ""
        dalCapture.resetTask(taskKey: taskKey)
    }
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let finalURL = urls.first else { return }
        fileName = finalURL.lastPathComponent
        if fileSize(forURL: finalURL) < 5 {
            uploadView.isHidden = true
            loadingView.isHidden = false
            do {
             let fileData = try Data(contentsOf: finalURL)
            dalCapture.saveTask(view: self, taskKey: taskKey, contentType: "", documentType: "", data: fileData, artifactCallback: nil)
            }
            catch {

            }
        } else {
            self.window?.rootViewController?.showAlert(title: "File Size Too Large", message: "File size should be less than 5 MB.", actionTitle: "Ok", completion: { _ in
            })
        }
    }
    func fileSize(forURL url:URL) -> Double {
        var fileSize: Double = 0.0
        var fileSizeValue = 0.0
        try? fileSizeValue = (url.resourceValues(forKeys: [URLResourceKey.fileSizeKey]).allValues.first?.value as! Double?)!
        if fileSizeValue > 0.0 {
            fileSize = (Double(fileSizeValue) / (1024 * 1024))
        }
        return fileSize
    }
}


