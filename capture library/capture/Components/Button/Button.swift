//
//  Button.swift
//  capture
//
//  Created by FL Mohan on 12/05/22.
//

import Foundation
import UIKit
import core

public class Button: UIView, ITaskUpdate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var button: UIButton!

    let kCONTENT_XIB_NAME           =   "Button"

    var dalCapture = DALCapture()

    var templateId = 0

    var themeConfig:ThemeConfig? = nil

    var object = NSObject()

    var taskType = ""
    var taskKey = ""

    public override init(frame:CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        button.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)

    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager) {
        setConfig(dalCapture: dalCapture, templateId: templateId)
        taskManager.registerToTaskKey(taskKey: taskKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager,themeConfig:ThemeConfig) {
        self.themeConfig = themeConfig
        setConfig(dalCapture: dalCapture, templateId: templateId)
        taskManager.registerToTaskKey(taskKey: taskKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,formTaskManager:FormTaskManager) {
        setConfig(dalCapture: dalCapture, templateId: templateId)
        formTaskManager.registerToTaskKey(taskKey: taskKey)
    }
    private func setConfig(dalCapture:DALCapture,templateId:Int) {
        self.dalCapture = dalCapture
        self.templateId = templateId
        if themeConfig == nil {
            themeConfig = dalCapture.getThemeConfig()
        }
        guard let taskIdObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)") else { return }
        object = taskIdObject
        let taskLabel = object.value(forKey: "label") as? String ?? ""
        button.setTitle(taskLabel.uppercased(), for: .normal)
        taskType = object.value(forKey: "task_type") as? String ?? ""
        taskKey = object.value(forKey: "task_key") as? String ?? ""

        _ = dalCapture.subscribeToTaskKey(key: taskKey, iTaskUpdate: self)
    }
    public func onUpdate(taskObject: NSObject) {
        disable(enable: !(taskObject.value(forKey: "status") as! String == "completed"))

    }
    @IBAction func didPressButton(_ tag: UIButton) {
        dalCapture.buttonBehaviorSubject.onNext(taskType)
    }
    private func disable(enable:Bool) {
        DispatchQueue.main.async { [self] in
            button.isEnabled = enable
            if enable {
                button.backgroundColor = themeConfig?.getPrimaryMainColor()
            } else {
                button.backgroundColor = .lightGray
            }
        }
    }
}

