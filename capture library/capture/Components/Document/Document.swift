//
//  Document.swift
//  Hostapp
//
//  Created by Admin on 18/03/22.
//

import Foundation
import UIKit
import core
import WebKit

public class Document: ArtifactComponent, DocFetcherCallback {



    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var placeholderImageView: UIView!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var afterVerificationView:UIView!
    @IBOutlet weak var beforeVerificationView:UIView!
    @IBOutlet weak var errormessageView:UIView!
    @IBOutlet weak var errorLabel:UILabel!

    let kCONTENT_XIB_NAME           =   "Document"

    var artifactKey = ""
    var error = ""
    var templateId = 0
    var dalCapture = DALCapture()
    var artifactObject:NSObject?
    var templateObject:NSObject?
    private var webViewController = WebViewController()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    public func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        afterVerificationView.isHidden = true
        errormessageView.isHidden = true

    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager) {
        setConfig(dalCapture: dalCapture, templateId:templateId)
        taskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,formTaskManager:FormTaskManager) {
        setConfig(dalCapture: dalCapture, templateId:templateId)
        formTaskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    private func setConfig(dalCapture:DALCapture,templateId:Int) {
        self.dalCapture = dalCapture
        self.templateId = templateId

        self.templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")
        artifactKey = dalCapture.getArtifactKey(object: templateObject ?? [] as NSObject)
        self.artifactObject = dalCapture.getArtifactsObjectByKey(key: artifactKey)
        afterVerificationView.setCornerBorder(color: dalCapture.getThemeConfig().getPrimaryMainColor(), cornerRadius: 5, borderWidth: 1)
        renderUi()
    }

    private func renderUi() {
        let present = artifactObject?.value(forKey: "present") as! Bool
            if present == true {
                beforeVerificationView.isHidden = true
                afterVerificationView.isHidden = false
            } else {
                beforeVerificationView.isHidden = false
                afterVerificationView.isHidden = true
            }
        initialise(dalCapture: dalCapture, templateId: templateId, artifactKey: artifactKey)
    }
    public override func setTemplate(resultObject: NSObject) {

    }
    public override func setArtifact(resultObject: NSObject) {
        webViewController.dismissWebView()
        updateUi(object: resultObject)
    }
    func updateUi(object:NSObject) {
        if let result = object.value(forKey: "error") as? String {
            error = result
        } else if let _ = object.value(forKey: "error") as? NSNull {
            error = ""
        }
        if error == "" {
            if object.value(forKey: "present") as! Bool {
                DispatchQueue.main.async { [self] in
                    self.errormessageView.isHidden = true
                    beforeVerificationView.isHidden = true
                    afterVerificationView.isHidden = false
                }
            }
        } else {
            error = "Looks like you denied Digilocker access. Please provide access on Digilocker to continue"
            DispatchQueue.main.async {
                self.errorLabel.text = self.error
                self.errormessageView.isHidden = false
            }
            return
        }
    }
    @IBAction func didTapButton(_ sender:UIButton) {
        dalCapture.documentFetcher(artifactKey: artifactKey, uploading: true, error: "", docFetcherCallback: self)
    }
    public func onArtifactUpdate(event: String, jsonObject: NSObject) {
        if event == "ARTIFACT_UPDATE" {
            if let result = jsonObject.value(forKey: "error") as? String {
                error = result
            } else if let _ = jsonObject.value(forKey: "error") as? NSNull {
                error = ""
            }
        }
    }

    public func onArtifactFailure(event: String, message: String) {

    }

    public func onDocumentFetch(event: String, jsonObject: NSObject) {
        if jsonObject.value(forKey: "status") as! String == "success" {
            if let redirect_url = jsonObject.value(forKey: "redirect_url") as? String {
                DispatchQueue.main.async { [self] in
                    webViewController.renderWebView(url: redirect_url)
                    self.window?.rootViewController?.present(webViewController, animated: true, completion: nil)
                }
            }
        }
    }
}

class WebViewController: UIViewController,UIWebViewDelegate {
    var webView = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()

        loadWebView()
        view.backgroundColor = .white

    }
    func loadWebView() {
        let screenSize: CGRect = UIScreen.main.bounds
        webView.frame = CGRect(x: 0, y: 44, width: screenSize.width, height: screenSize.height - 44)
        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let navItem = UINavigationItem(title: "")
        let doneItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: nil, action: #selector(close))
        navItem.leftBarButtonItem = doneItem
        navBar.setItems([navItem], animated: false)
        self.view.addSubview(navBar)
        self.view.addSubview(webView)
    }
    @objc func close() {
        dismissWebView()
    }
    func renderWebView(url:String) {
        let myURL = URL(string:url)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
    public func dismissWebView() {
        webView.stopLoading()
        self.dismiss(animated: true, completion: nil)
    }
}
