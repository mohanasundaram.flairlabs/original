//
//  DropDownList.swift
//  captureapp
//
//  Created by Admin on 21/03/22.
//


import Foundation
import UIKit
import core

protocol DropDownDataSourceProtocol{
    func selectItemInDropDown(indexPos: Int)
}
class DropDownList: UIView {
    var dropDownTableView: UITableView?
    var width: CGFloat = 0
    var offset:CGFloat = 0
    var optionList:[String] = []
    var dropDownDataSourceProtocol: DropDownDataSourceProtocol?
    var viewPositionRef: CGRect?
    var isDropDownPresent: Bool = false
    var coordinates:CGPoint?
    var havingTitle:Bool = false


    //MARK: - DropDown Methods

    // Make Table View Programatically

    func setUpDropDown(viewPositionReference: CGRect, offset: CGFloat,optionlist:[String],backgroundColor:UIColor,havingTitle:Bool){
        self.addBorders()
        self.addShadowToView()
        self.optionList = optionlist
        if havingTitle {
        self.optionList.insert("Select Document Type", at: 0)
        }
        self.havingTitle = havingTitle
        self.frame = CGRect(x: viewPositionReference.minX, y: viewPositionReference.maxY + offset, width: 0, height: 0)
        dropDownTableView = UITableView(frame: CGRect(x: self.frame.minX, y: self.frame.minY, width: 0, height: 0))
        self.width = viewPositionReference.width
        self.offset = offset
        self.viewPositionRef = viewPositionReference
        dropDownTableView?.register(UINib(nibName: "DropDownTableViewCell", bundle: Bundle(for: type(of: self))), forCellReuseIdentifier:"DropDownTableViewCell")
        dropDownTableView?.showsVerticalScrollIndicator = false
        dropDownTableView?.showsHorizontalScrollIndicator = false
        dropDownTableView?.backgroundColor = backgroundColor
        dropDownTableView?.separatorStyle = .none
        dropDownTableView?.delegate = self
        dropDownTableView?.dataSource = self
        dropDownTableView?.allowsSelection = true
        dropDownTableView?.isUserInteractionEnabled = true
        dropDownTableView?.tableFooterView = UIView()
        self.addSubview(dropDownTableView!)
    }

    // Shows Drop Down Menu
    func showDropDown(height: CGFloat){
            if isDropDownPresent {
                self.hideDropDown()
            }else{
                isDropDownPresent = true
                self.frame = CGRect(x: (self.viewPositionRef?.minX)!, y: (self.viewPositionRef?.maxY)! + self.offset, width: width, height: 0)
                self.dropDownTableView?.frame = CGRect(x: 0, y: 0, width: width, height: 0)
                self.dropDownTableView?.reloadData()

                UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.05, options: .curveLinear
                    , animations: {
                    self.frame.size = CGSize(width: self.width, height: height)
                    self.dropDownTableView?.frame.size = CGSize(width: self.width, height: height)
                })
            }

        }

    // Use this method if you want change height again and again
    // For eg in UISearchBar DropDownMenu
    func reloadDropDown(height: CGFloat){
           self.frame = CGRect(x: (self.viewPositionRef?.minX)!, y: (self.viewPositionRef?.maxY)!
               + self.offset, width: width, height: 0)
           self.dropDownTableView?.frame = CGRect(x: 0, y: 0, width: width, height: 0)
           self.dropDownTableView?.reloadData()
           UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.05, options: .curveLinear
               , animations: {
               self.frame.size = CGSize(width: self.width, height: height)
               self.dropDownTableView?.frame.size = CGSize(width: self.width, height: height)
           })
       }

    //Sets Row Height of your Custom XIB
    func setRowHeight(height: CGFloat){
        self.dropDownTableView?.rowHeight = height
        self.dropDownTableView?.estimatedRowHeight = height
    }

    //Hides DropDownMenu
    func hideDropDown(){
        isDropDownPresent = false
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.3, options: .curveLinear
            , animations: {
            self.frame.size = CGSize(width: self.width, height: 0)
            self.dropDownTableView?.frame.size = CGSize(width: self.width, height: 0)
        })
    }

    // Removes DropDown Menu
    // Use it only if needed
    func removeDropDown(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.3, options: .curveLinear
            , animations: {
            self.dropDownTableView?.frame.size = CGSize(width: 0, height: 0)
        }) { (_) in
            self.removeFromSuperview()
            self.dropDownTableView?.removeFromSuperview()
        }
    }

}

// MARK: - Table View Methods

extension DropDownList: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = dropDownTableView?.dequeueReusableCell(withIdentifier: "DropDownTableViewCell") as? DropDownTableViewCell else {
            return UITableViewCell()
        }
        if havingTitle {
            if indexPath.row == 0 {
                cell.listNameLabel.textAlignment = .center
            } else {
                cell.listNameLabel.textAlignment = .left
            }
        } else {
            cell.listNameLabel.textAlignment = .left
        }
        cell.selectionStyle = .none
        cell.configureCell(title: optionList[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dropDownDataSourceProtocol?.selectItemInDropDown(indexPos: indexPath.row)
    }

}

//MARK: - UIView Extension
extension UIView{
    func addBorders(borderWidth: CGFloat = 0.2, borderColor: CGColor = UIColor.lightGray.cgColor){
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor
    }

    func addShadowToView(shadowRadius: CGFloat = 2, alphaComponent: CGFloat = 0.6) {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: alphaComponent).cgColor
        self.layer.shadowOffset = CGSize(width: -1, height: 2)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 1
    }
}
class DropDownTableViewCell: UITableViewCell {
    @IBOutlet weak var listNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func configureCell(title: String) {
        let image = (UIImage(systemName: "doc.text.image")?.withTintColor(.white))!
        self.listNameLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        if title == "ind_aadhaar" {
            self.listNameLabel.addLeading(image: image, text: "  Aadhar Card")
        } else if title == "ind_voter_id" {
            self.listNameLabel.addLeading(image: image, text: "  Voter Card")
        } else if title == "ind_driving_license" {
            self.listNameLabel.addLeading(image: image, text: "  Driving License")
        } else if title == "ind_passport" {
            self.listNameLabel.addLeading(image: image, text: "  Passport")
        } else if title == "title" {
            self.listNameLabel.text = "Select Document Type"
            self.listNameLabel.textAlignment = .center
        } else {
        self.listNameLabel.text = title
        }
    }
}
extension UILabel {

    func addTrailing(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image

        let attachmentString = NSAttributedString(attachment: attachment)
        let string = NSMutableAttributedString(string: text, attributes: [:])

        string.append(attachmentString)
        self.attributedText = string
    }

    func addLeading(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image

        let attachmentString = NSAttributedString(attachment: attachment)
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentString)

        let string = NSMutableAttributedString(string: text, attributes: [:])
        mutableAttributedString.append(string)
        self.attributedText = mutableAttributedString
    }
}
