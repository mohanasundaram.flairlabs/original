//
//  DropDown.swift
//  Hostapp
//
//  Created by Admin on 17/03/22.
//

import Foundation
import UIKit
import core

public class DropDown: UIView, DropDownDataSourceProtocol,UIGestureRecognizerDelegate {


    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var selectedNameLabel:UITextField!
    @IBOutlet weak var dropDownImageView:UIImageView!


    let kCONTENT_XIB_NAME           =   "DropDown"

    private var dropDown = DropDownList()

    private var dropDownRowHeight: CGFloat = 50

    private var artifactKey:String = ""

    private var isDropDownPresent:Bool = false

    private var optionList: [String] = []

    private var dalCapture = DALCapture()
    private var themeConfig:ThemeConfig? = nil
    private var templateObject:NSObject?

    private var fullScreenView = UIView()

    private var taskKey = ""
    private var taskType = ""
    private var label = ""


    public override init(frame: CGRect) {

        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        dropDownImageView.image?.withTintColor(.white)

    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager) {
        setConfig(dalCapture: dalCapture, templateId:templateId)
        if taskType == CaptureConstant.TASK_DATA_CAPTURE.rawValue {
            taskManager.registerToArtifactKey(artifactKey: artifactKey)
        } else {
            taskManager.registerToTaskKey(taskKey: taskKey)
        }
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager,themeConfig:ThemeConfig) {
        self.themeConfig = themeConfig
        setConfig(dalCapture: dalCapture, templateId:templateId)
        if taskType == CaptureConstant.TASK_DATA_CAPTURE.rawValue {
            taskManager.registerToArtifactKey(artifactKey: artifactKey)
        } else {
            taskManager.registerToTaskKey(taskKey: taskKey)
        }
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,formTaskManager:FormTaskManager) {
        setConfig(dalCapture: dalCapture, templateId:templateId)
        if taskType == CaptureConstant.TASK_DATA_CAPTURE.rawValue {
            formTaskManager.registerToArtifactKey(artifactKey: artifactKey)
        } else {
            formTaskManager.registerToTaskKey(taskKey: taskKey)
        }
    }
    private func setConfig(dalCapture:DALCapture,templateId:Int) {
        self.dalCapture = dalCapture
        if themeConfig == nil {
            themeConfig = dalCapture.getThemeConfig()
        }
        self.templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")
        label = templateObject?.value(forKey: "label") as? String ?? ""
        let mandatory = templateObject?.value(forKey: "mandatory") as? Bool ?? false
        optionList = templateObject?.value(forKey: "options") as? [String] ?? []
        if optionList.count > 0 {
            dalCapture.saveTask(taskKey: taskKey, value: [optionList[0]])
        }
        taskKey = templateObject?.value(forKey: "task_key") as? String ?? ""
        taskType = templateObject?.value(forKey: "task_type") as? String ?? ""
        dropDownImageView.image = dropDownImageView.image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        dropDownImageView.tintColor = themeConfig?.getSecondaryContrastColor()
        titleLabel.textColor = themeConfig?.getSecondaryContrastColor()
        selectedNameLabel.textColor = themeConfig?.getSecondaryContrastColor()
        if mandatory {
            self.titleLabel.setAttributedTextWithSubscripts(text:"\(label)*")
        } else {
            self.titleLabel.text = label
        }
        if taskType == CaptureConstant.TASK_DATA_CAPTURE.rawValue {
            artifactKey = dalCapture.getArtifactKey(object: templateObject ?? [] as NSObject)
            let selectedValue = dalCapture.getValueforArtifactObject(artifactKey: artifactKey)
            if selectedValue != "" {
                self.selectedNameLabel.text = selectedValue
            } else {
                self.selectedNameLabel.text = optionList[0]
            }
        } else if taskType == CaptureConstant.TASK_VERIFY.rawValue {
            let taskObject = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey)
            guard let manualObject = taskObject?.value(forKey: "manual_response") as? NSObject else { return }
            let selectedValue = manualObject.value(forKey: "value") as? String
            if selectedValue != "" {
                self.selectedNameLabel.text = selectedValue
            } else {
                self.selectedNameLabel.text = optionList[0]
            }
        }
    }
    @IBAction func didTapButton(sender:UIButton) {
        if !isDropDownPresent {
        dropDown = DropDownList()
        setUpDropDown()
        fullScreenView = UIView(frame: CGRect(x: 0, y: 0, width: (self.window?.rootViewController?.view.frame.width)!, height: (self.window?.rootViewController?.view.frame.height)!))
        fullScreenView.backgroundColor = .clear
        fullScreenView.addSubview(dropDown)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self
        fullScreenView.addGestureRecognizer(tap)
        self.window?.rootViewController?.view.addSubview(fullScreenView)
        self.window?.rootViewController?.view.bringSubviewToFront(fullScreenView)
            if optionList.count > 5 {
                self.dropDown.showDropDown(height:250)
            } else {
                self.dropDown.showDropDown(height:self.dropDownRowHeight * CGFloat(optionList.count))
            }
        }  else {
            self.dropDown.hideDropDown()
            isDropDownPresent = false
        }
        UIView.animate(withDuration: 0.5) {
            self.dropDownImageView.transform = CGAffineTransform(rotationAngle: self.isDropDownPresent ? CGFloat(Double.pi) : CGFloat(0))
        }
    }
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard let view = touch.view else { return true }
        if view.isDescendant(of: dropDown) {
            return false
        }
        return true
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        removeGesture()
    }
    private func setUpDropDown() {
        dropDown.dropDownDataSourceProtocol = self
        guard let keyWindow = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) else { return }
        let frame = self.convert(self.bounds, to: keyWindow)
        dropDown.setUpDropDown(viewPositionReference: (frame), offset: 2, optionlist: optionList, backgroundColor: themeConfig?.getSecondaryMainColor() ?? .white, havingTitle: false)
        dropDown.setRowHeight(height: self.dropDownRowHeight)
    }
    internal func selectItemInDropDown(indexPos: Int) {
        removeGesture()
        self.selectedNameLabel.text = optionList[indexPos]
        titleLabel.textColor = themeConfig?.getPrimaryMainColor()

        dalCapture.saveTask(taskKey: taskKey, value: [optionList[indexPos]])
    }
    private func removeGesture() {
        self.dropDown.hideDropDown()
        isDropDownPresent = false
        if fullScreenView.isDescendant(of: (self.window?.rootViewController?.view)!) {
            fullScreenView.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.5) {
            self.dropDownImageView.transform = CGAffineTransform(rotationAngle: self.isDropDownPresent ? CGFloat(Double.pi) : CGFloat(0))
        }
    }
}
