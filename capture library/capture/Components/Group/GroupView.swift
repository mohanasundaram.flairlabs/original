//
//  GroupView.swift
//  Hostapp
//
//  Created by Admin on 28/02/22.
//

import Foundation
import UIKit
import core

public class GroupView:ArtifactComponent,DropDownDataSourceProtocol,UIGestureRecognizerDelegate, ImageDelegate,UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var dummyButtonView:UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var titleView:UIView!
    @IBOutlet weak var clickToCapture:UILabel!
    @IBOutlet weak var imageSetView:UIView!
    @IBOutlet weak var imageView:UIImageView!
    private var imageCapture:CustomCamera?

    let kCONTENT_XIB_NAME           =   "GroupView"

    private var imagePicker = UIImagePickerController()
    
    private var dalCapture = DALCapture()
    private var taskManager:TaskManager?
    private var formTaskManager:FormTaskManager?
    private var iTaskFactory:ITaskFactory?
    private var templateObject:NSObject?
    private var taskArray:NSArray?
    private var taskKey:String = ""
    private var artifactKeyArray:[String] = []
    private var viewHashMap:[String:Image] = [:]

    private var dropDown = DropDownList()
    private var optionList: [String] = []
    private var documentList:[String] = []
    private var images:[UIImage] = []
    private var artifactHashMap:[String:Any] = [:]

    private var tasksList:[Tasks] = []
    private var templateId = 0

    private var taskFinishedCont = 0

    private var document_type_key = ""
    private var allowUpload = false
    private var fullScreenView = UIView()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,iTaskFactory:ITaskFactory,taskManager:TaskManager) {
        self.taskManager = taskManager
        setConfig(dalCapture: dalCapture, templateId: templateId, iTaskFactory: iTaskFactory)
//        taskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,iTaskFactory:ITaskFactory,formTaskManager:FormTaskManager) {
        self.formTaskManager = formTaskManager
        setConfig(dalCapture: dalCapture, templateId: templateId, iTaskFactory: iTaskFactory)
//        formTaskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    private func setConfig(dalCapture:DALCapture,templateId:Int,iTaskFactory:ITaskFactory) {
        self.dalCapture = dalCapture
        self.iTaskFactory = iTaskFactory
        self.templateId = templateId
        self.taskArray = dalCapture.getCaptureItem(templateId: templateId)

        templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")

        clickToCapture.textColor = dalCapture.getThemeConfig().getPrimaryMainColor()
        imageView.tintColor = dalCapture.getThemeConfig().getPrimaryMainColor()
        imageSetView.setCornerBorder(color: dalCapture.getThemeConfig().getPrimaryMainColor(), cornerRadius: 5, borderWidth: 1)

        document_type_key = templateObject?.value(forKey: "document_type_key") as? String ?? ""
        optionList = templateObject?.value(forKey: "options") as? [String] ?? []
        dummyButtonView.isHidden = false
        titleView.isHidden = true
        stackView.isHidden = true

        guard let jsonArrrays = taskArray else { return }
        var template_id = 0

        for(index, _) in jsonArrrays.enumerated() {
            let taskUI = jsonArrrays[index] as? NSObject
            template_id = taskUI?.value(forKey: "template_id") as! Int
            guard let taskIdObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(template_id)") else { return }
            let taskType = taskIdObject.value(forKey: "type") as? String ?? ""
            let side = taskIdObject.value(forKey: "side") as? String ?? ""
            allowUpload = taskIdObject.value(forKey: "allow_upload") as? Bool ?? false
            optionList = taskIdObject.value(forKey: "options") as? [String] ?? []
            let artifactKey = dalCapture.getArtifactKey(object: taskIdObject)
            artifactHashMap.updateValue(side, forKey: artifactKey)
            title.text = getTitle(document_type_key: self.document_type_key, connectedtext: "")
            self.artifactKeyArray.append(artifactKey)
            if taskType == "image" {
                let task = Tasks()
                task.setTaskKey(taskKey: taskIdObject.value(forKey: "task_key") as? String ?? "")
                task.setArtifactKey(artifactKey: artifactKey)
                task.setSide(side: side)
                if tasksList.isEmpty {
                    tasksList = [task]
                } else {
                    tasksList.append(task)
                }
                if taskManager != nil {
                    guard let imageView = self.iTaskFactory?.getImageComponent(view:self,templateId: template_id, taskManager: taskManager!) else { return }
                    stackView.addArrangedSubview(imageView)
                    viewHashMap.updateValue(imageView, forKey: artifactKey)
                } else {
                    guard let imageView = self.iTaskFactory?.getImageComponent(view:self,templateId: template_id, formTaskManager: formTaskManager!) else { return }
                    stackView.addArrangedSubview(imageView)
                    viewHashMap.updateValue(imageView, forKey: artifactKey)
                }
                initialise(dalCapture: dalCapture, templateId: templateId, artifactKey: artifactKey)
            }
        }
        if jsonArrrays.count == 1 {
            stackView.addArrangedSubview(UIView())
        }
    }
    @IBAction func didTapCapture(sender:UIButton) {
            processDocumentOption()
    }
    private func processDocumentOption() {

        if !document_type_key.isEmpty {
            updateArtifactObject(documentType: document_type_key)
            self.selectOption(title: getTitle(document_type_key: document_type_key, connectedtext: ""))
        } else if !optionList.isEmpty {
            if optionList.count == 1 {
                document_type_key = optionList[0]
                updateArtifactObject(documentType: document_type_key)
                self.selectOption(title: getTitle(document_type_key: document_type_key, connectedtext: ""))
            } else {
                showOptions()
            }
        } else {
            let categoryType = templateObject?.value(forKey: "category_type") as! String
            if categoryType.contains("poa") {
                optionList = dalCapture.getPoaOptionList()
            } else if categoryType.contains("poi") {
                optionList = dalCapture.getPoiOptionList()
            } else {
//                document_type_key = categoryType
                self.selectOption(title: getTitle(document_type_key: categoryType, connectedtext: ""))
            }
            if optionList.count == 1 {
                document_type_key = optionList[0]
                updateArtifactObject(documentType: document_type_key)
                self.selectOption(title: getTitle(document_type_key: document_type_key, connectedtext: ""))
            } else {
                showOptions()
            }
        }
    }
    private func updateArtifactObject(documentType:String) {
        for k in artifactHashMap.keys {
            var artifactObject = dalCapture.getArtifactsObjectByKey(key: k) as! [String:Any]
            artifactObject.updateValue(documentType, forKey: "document_type_key")
            dalCapture.updateArtifactObject(updateObject: artifactObject as NSObject, artifactKey: k)
        }
    }
    private func showOptions() {
        dropDown = DropDownList()
        setUpDropDown()
        fullScreenView = UIView(frame: CGRect(x: 0, y: 0, width: (self.window?.rootViewController?.view.frame.width)!, height: (self.window?.rootViewController?.view.frame.height)!))
        fullScreenView.backgroundColor = .clear
        fullScreenView.addSubview(dropDown)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self
        fullScreenView.addGestureRecognizer(tap)
        self.window?.rootViewController?.view.addSubview(fullScreenView)
        self.window?.rootViewController?.view.bringSubviewToFront(fullScreenView)
        self.dropDown.showDropDown(height: 50 * CGFloat(optionList.count))
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        hidePopup()
    }
    private func hidePopup() {
        self.dropDown.hideDropDown()
        if fullScreenView.isDescendant(of: (self.window?.rootViewController?.view)!) {
            fullScreenView.removeFromSuperview()
        }
    }
    private func setUpDropDown() {
        dropDown.dropDownDataSourceProtocol = self
        dropDown.setUpDropDown(viewPositionReference: (CGRect(x: 50, y: 300, width: 300, height: 0)), offset: 2, optionlist: optionList, backgroundColor:ThemeConfig.shared.getSecondaryMainColor(), havingTitle: true)
        dropDown.setRowHeight(height: 50)
    }
    internal func selectItemInDropDown(indexPos: Int) {
        self.document_type_key = optionList[indexPos - 1]
        updateArtifactObject(documentType: document_type_key)
        title.text = getTitle(document_type_key: self.document_type_key, connectedtext: "")
        self.dropDown.hideDropDown()
        self.window?.rootViewController?.checkforCameraAccess(completion: { success in
            self.selectOption(title: getTitle(document_type_key: self.document_type_key, connectedtext: "Capture/Upload Front of"))
            self.hidePopup()
        })
    }
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.dropDown) == true {
            return false
        }
        return true
    }
    private func clearRequest() {

    }
    public func didSelect(image: UIImage?) {
    }
    public override func setArtifact(resultObject: NSObject) {
        let present = resultObject.value(forKey: "present") as! Bool
        let uploading = resultObject.value(forKey: "uploading") as? Bool ?? false
        let artifactKey = resultObject.value(forKey: "key") as! String
        var error = ""
        if let result = resultObject.value(forKey: "error") as? String {
            if error == "" {
            error = result
            }
        }
        DispatchQueue.main.async { [self] in
            if uploading || present {
                hideDummyImage()
                let image = viewHashMap[artifactKey]
                if image != nil {
                    image?.isHidden = false
                }
                for tasksList in tasksList {
                    if error != "" {
                        if tasksList.getArtifactKey().contains("front") {
                            let image = viewHashMap[tasksList.getArtifactKey()]
                            if image != nil {
                                image?.enableRecaptureButton()
                            }
                        }
                    }
                }
            } else {
                showDummyImage()
                for tasksList in tasksList {
                    dalCapture.resetTask(taskKey: tasksList.getTaskKey())
                    let image = viewHashMap[tasksList.getArtifactKey()]
                    if image != nil {
                        image?.isHidden = true
                        image?.removeImage()
                    }
                }
            }
        }
    }
    private func showDummyImage() {
        stackView.isHidden = true
        dummyButtonView.isHidden = false
    }
    private func hideDummyImage() {
        stackView.isHidden = false
        dummyButtonView.isHidden = true
    }
}
extension GroupView  {

    private func selectOption(title:String) {
        if allowUpload {
            images = []
            let alertController = UIAlertController(title: "Capture Option", message: title, preferredStyle: .alert)

            let actionOne = UIAlertAction(title: "Take photo", style: .default) { (action) in
                self.openCamera()
            }
            alertController.addAction(actionOne)
            let actionTwo = UIAlertAction(title: "Photo library", style: .default) { (action) in
                self.selectImageFromLocal()
            }
            alertController.addAction(actionTwo)

            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                self.document_type_key = ""
            })
            if UIDevice.current.userInterfaceIdiom == .pad {
                alertController.popoverPresentationController?.sourceView = self
                alertController.popoverPresentationController?.sourceRect = self.bounds
                alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
            }
            self.window?.rootViewController?.present(alertController, animated: true)
        } else {
            openCamera()
        }
    }
    private func openCamera() {
        
        self.window?.rootViewController?.checkforCameraAccess(completion: { [self] success in
            DispatchQueue.main.async { [self] in
                if success ?? false {
                    DispatchQueue.main.async { [self] in
                        imageCapture = CustomCamera(frame: window!.frame, delegate: self,tasksList:tasksList,documentKey: document_type_key, dalCapture: dalCapture, allowUpload: allowUpload)
                        imageCapture?.tag = 100
                        UIView.transition(with: imageCapture!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                            self.window?.rootViewController?.view.addSubview(self.imageCapture!)
                        })
                    }
                } else {
                    self.window?.rootViewController?.showAlert(title: "Unable to open Camera", message: "Please enable Camera features in settings to proceed", actionTitle: "Ok", completion: { success in

                    })
                }
            }
        })

    }
    private func selectImageFromLocal() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            DispatchQueue.main.async {
            self.window?.rootViewController?.present(self.imagePicker, animated: true)
            }
        }
    }
    public func didCloseCustomCamera(openGallery: Bool) {
        if ((imageCapture?.isDescendant(of: (window?.rootViewController?.view)!)) != nil) {
            imageCapture?.removeFromSuperview()
        }
        if openGallery {
            self.selectImageFromLocal()
            return
        } else {
            document_type_key = ""
        }
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            taskFinishedCont = taskFinishedCont + 1
            if images.isEmpty {
                images = [image]
            } else {
                images.append(image)
            }
            if taskFinishedCont == tasksList.count {
                taskFinishedCont = 0
                for (index,image) in images.enumerated() {
                    let resizedImage = resizeImage(image: image, targetSize: CGSize(width: 480, height: 640))
                    guard let imageData = resizedImage.jpegData(compressionQuality: 1.0) else { return }
                    dalCapture.saveTask(view:self,taskKey: tasksList[index].getTaskKey(), contentType: "image/jpeg", documentType: document_type_key, data: imageData, artifactCallback: nil)
                }
            } else {
                self.selectImageFromLocal()
            }
            if taskFinishedCont == 0 {
                document_type_key = ""
            }

        }
    }
}
