//
//  RadioBox.swift
//  Hostapp
//
//  Created by Admin on 17/03/22.
//

import Foundation
import UIKit
import core

public class RadioBox: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var tableViewHeightConstraint:NSLayoutConstraint!

    let kCONTENT_XIB_NAME           =   "RadioBox"

    private var optionList: [String] = []
    private var selectedOption: [String] = []

    private var artifactKey:String = ""

    private var dalCapture = DALCapture()
    private var themeConfig:ThemeConfig? = nil
    private var templateObject:NSObject?

    private var taskKey = ""
    private var taskType = ""
    private var label = ""


    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        tableView.register(UINib(nibName: "RadioBoxCell", bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: "RadioBoxCell")

        tableView.separatorColor = .clear

    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager) {
        setConfig(dalCapture: dalCapture, templateId:templateId)
        if taskType == CaptureConstant.TASK_DATA_CAPTURE.rawValue {
            taskManager.registerToArtifactKey(artifactKey: artifactKey)
        } else {
            taskManager.registerToTaskKey(taskKey: taskKey)
        }
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager,themeConfig:ThemeConfig) {
        self.themeConfig = themeConfig
        setConfig(dalCapture: dalCapture, templateId:templateId)
        if taskType == CaptureConstant.TASK_DATA_CAPTURE.rawValue {
            taskManager.registerToArtifactKey(artifactKey: artifactKey)
        } else {
            taskManager.registerToTaskKey(taskKey: taskKey)
        }
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,formTaskManager:FormTaskManager) {
        setConfig(dalCapture: dalCapture, templateId:templateId)
        if taskType == CaptureConstant.TASK_DATA_CAPTURE.rawValue {
            formTaskManager.registerToArtifactKey(artifactKey: artifactKey)
        } else {
            formTaskManager.registerToTaskKey(taskKey: taskKey)
        }
    }
    private func setConfig(dalCapture:DALCapture,templateId:Int) {
        self.dalCapture = dalCapture
        if themeConfig == nil {
            themeConfig = dalCapture.getThemeConfig()
        }
        self.templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")
        label = templateObject?.value(forKey: "label") as? String ?? ""
        let mandatory = templateObject?.value(forKey: "mandatory") as? Bool ?? false
        optionList = templateObject?.value(forKey: "options") as? [String] ?? []
        taskKey = templateObject?.value(forKey: "task_key") as? String ?? ""
        taskType = templateObject?.value(forKey: "task_type") as? String ?? ""
        titleLabel.textColor = themeConfig?.getSecondaryContrastColor()
        tableViewHeightConstraint.constant = CGFloat(60 * optionList.count)
        if mandatory {
            self.titleLabel.setAttributedTextWithSubscripts(text:"\(label)*")
        } else {
            self.titleLabel.text = label
        }
        if taskType == CaptureConstant.TASK_DATA_CAPTURE.rawValue {
            artifactKey = dalCapture.getArtifactKey(object: templateObject ?? [] as NSObject)
            let selectedValue = dalCapture.getValueforArtifactObject(artifactKey: artifactKey)
            if selectedValue != "" {
                selectedOption = [selectedValue]
            }
        } else if taskType == CaptureConstant.TASK_VERIFY.rawValue {
            let taskObject = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey)
            guard let manualObject = taskObject?.value(forKey: "manual_response") as? NSObject else { return }
            let selectedValue = manualObject.value(forKey: "value") as? String
            if selectedValue != "" {
                selectedOption = [selectedValue ?? ""]
            }
        }
        self.tableView.reloadData()
    }
    private func checkForSelectedOption() {
        if selectedOption.count == 0 {
            titleLabel.textColor = .red
        } else {
            self.titleLabel.textColor = themeConfig?.getPrimaryMainColor()
            titleLabel.textColor = .white
        }
        dalCapture.saveTask(taskKey: taskKey, value: selectedOption)
    }
}
extension RadioBox: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionList.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RadioBoxCell") as? RadioBoxCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none

        cell.configureCell(title: optionList[indexPath.row], textColor: themeConfig?.getSecondaryContrastColor() ?? .white)
        if selectedOption.contains(self.optionList[indexPath.row]) {
            cell.imageAccessory.image = UIImage(named: "Radio_Icon_Selected", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(themeConfig?.getPrimaryMainColor() ?? .white)
        } else {
            cell.imageAccessory.image = UIImage(named: "Radio_Icon_Unselected", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(themeConfig?.getSecondaryContrastColor() ?? .white)
        }
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedOption = [optionList[indexPath.row]]
        self.tableView.reloadData()
        checkForSelectedOption()
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
class RadioBoxCell:UITableViewCell {
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var imageAccessory: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configureCell(title: String,textColor:UIColor) {
        self.tagLabel.textColor = textColor
        self.tagLabel.text = title
    }
}

