//
//  TemplateComponent.swift
//  capture
//
//  Created by FL Mohan on 28/04/22.
//

import Foundation
import core
import UIKit

public protocol SetTemplateResult {
func setTemplate(resultObject:NSObject)
}

public class TemplateComponent:UIView, ITemplateUpdate,SetTemplateResult {

    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    public func initialise(dalCapture:DALCapture,templateId:Int) {
        dalCapture.subscribeToTemplate(templateId: "\(templateId)", iTemplateUpdate: self)
    }
    public func onUpdate(jsonObject: NSObject) {
        setTemplate(resultObject: jsonObject)
    }
    public func setTemplate(resultObject: NSObject) {

    }

}
