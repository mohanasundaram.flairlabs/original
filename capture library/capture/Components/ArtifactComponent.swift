//
//  ArtifactComponent.swift
//  Hostapp
//
//  Created by FL Mohan on 27/04/22.
//

import Foundation
import core
public protocol SetArtifactResult {
func setArtifact(resultObject:NSObject)
}
public class ArtifactComponent:TemplateComponent,IArtifactUpdate,SetArtifactResult {

    public func initialise(dalCapture:DALCapture,templateId:Int,artifactKey:String) {
        initialise(dalCapture: dalCapture, templateId: templateId)
        _ = dalCapture.subscribeToArtifactKey(key: artifactKey, iArtifactUpdate: self)
    }
    public func onUpdate(templateObject:NSObject) {
        self.setArtifact(resultObject: templateObject)
    }
    public func setArtifact(resultObject: NSObject) {

    }
}
