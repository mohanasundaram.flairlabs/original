//
//  FormView.swift
//  Hostapp
//
//  Created by Admin on 04/03/22.
//

import Foundation
import UIKit
import core
import RxSwift

public class FormView:TaskComponents {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var processignView:UIView!
    @IBOutlet weak var messageLabel:UILabel!
    @IBOutlet weak var processingActivityIndicator:UIActivityIndicatorView!
    @IBOutlet weak var processingLabel:UILabel!

    let kCONTENT_XIB_NAME           =   "FormView"

    var dalCapture = DALCapture()
    private var taskManager = TaskManager()
    private var formTaskManager = FormTaskManager()
    private var disposableList:[Disposable] = []

    var iTaskFactory:ITaskFactory?
    var templateObject:NSObject?
    var formArray:NSArray?
    var taskKey:String = ""
    var taskType:String = ""
    var isAutoSubmitEnabled:Bool?
    var button:UIButton?
    var buttonLabel = ""


    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()

    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        processignView.isHidden = true
        messageLabel.isHidden = true
    }
    public func setConfig(dalCapture:DALCapture,iTaskFactory:ITaskFactory,templateId:Int,taskManager:TaskManager) {
        self.taskManager = taskManager
        setConfig(dalCapture: dalCapture, iTaskFactory:iTaskFactory, templateId: templateId)

    }
    public func setConfig(dalCapture:DALCapture,iTaskFactory:ITaskFactory,templateId:Int,formTaskManager:FormTaskManager) {
//        self.formTaskManager = formTaskManager
        setConfig(dalCapture: dalCapture, iTaskFactory:iTaskFactory, templateId: templateId)
    }
    private func setConfig(dalCapture:DALCapture,iTaskFactory:ITaskFactory,templateId:Int) {
        self.setColor()
        self.dalCapture = dalCapture
        self.iTaskFactory = iTaskFactory
        self.templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")
        self.formArray = dalCapture.getCaptureItem(templateId: templateId)
        if let settings = templateObject?.value(forKey: "settings") as? NSObject {
            let form = settings.value(forKey: "form") as? NSObject
            isAutoSubmitEnabled =  form?.value(forKey: "auto_submit") as? Bool ?? true
        }
        taskKey = templateObject?.value(forKey: "task_key") as! String
        taskType = templateObject?.value(forKey: "task_type") as! String
        buttonLabel = templateObject?.value(forKey: "label") as! String
        initialise(dalCapture: dalCapture, templateId: templateId, taskKey: taskKey)
        taskManager.registerToTaskKey(taskKey: taskKey)
        if let formArray = formArray {
            self.formTaskManager = FormTaskManager(dalCapture: dalCapture,formArray: formArray,taskKey: taskKey)
        }
        guard let jsonArray = formArray else { return }
        for(index, _) in jsonArray.enumerated() {
            let taskUI = jsonArray[index] as? NSObject
            let template_id = taskUI?.value(forKey: "template_id") as! Int
            let taskIdObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(template_id)")
            let taskType = taskIdObject?.value(forKey: "type") as? String
            switch taskType {
            case "text":
                guard let textView = self.iTaskFactory?.getTextComponent(view: stackView,templateId: template_id, formTaskManager: formTaskManager) else { return }
                stackView.addArrangedSubview(textView)
                break
            case "image":
                guard let imageView = self.iTaskFactory?.getImageComponent(view:self,templateId: template_id, formTaskManager: formTaskManager) else { return }
                let stackedInfoView = UIStackView(frame: self.frame)
                stackedInfoView.axis = .horizontal
                stackedInfoView.alignment = .leading
                stackedInfoView.translatesAutoresizingMaskIntoConstraints = false
                stackedInfoView.addArrangedSubview(imageView)
                if jsonArray.count == 1 {
                    stackedInfoView.addArrangedSubview(UIView())
                }
                stackView.addArrangedSubview(stackedInfoView)
                break
            case "group":
                guard let groupView = self.iTaskFactory?.getGroupComponent(view: stackView,templateId: template_id, formTaskManager: formTaskManager) else { return }
                stackView.addArrangedSubview(groupView)
                break
            case "document":
                guard let documentView = self.iTaskFactory?.getDocumentComponent(view: stackView,templateId: template_id, formTaskManager: formTaskManager) else { return }
                stackView.addArrangedSubview(documentView)
                break
            case "checkbox":
                guard let checkBoxView = self.iTaskFactory?.getCheckBoxComponent(view: stackView,templateId: template_id, formTaskManager: formTaskManager) else { return }
                stackView.addArrangedSubview(checkBoxView)
                break
            case "radio":
                guard let radioView = self.iTaskFactory?.getRadioComponent(view: stackView,templateId: template_id, formTaskManager: formTaskManager) else { return }
                stackView.addArrangedSubview(radioView)
                break
            case "dropdown":
                guard let dropDownView = self.iTaskFactory?.getDropdownComponent(view: stackView,templateId: template_id, formTaskManager: formTaskManager) else { return }
                stackView.addArrangedSubview(dropDownView)
                break
            case "section":
                guard let sectionView = self.iTaskFactory?.getSectionComponent(view: stackView,templateId: template_id, formTaskManager: formTaskManager) else { return }
                stackView.addArrangedSubview(sectionView)
                break
            case "button":
                guard let buttonView = self.iTaskFactory?.getButtonComponent(view: stackView,templateId: template_id, formTaskManager: formTaskManager) else { return }
                stackView.addArrangedSubview(buttonView)
                break
            case "filepicker":
                guard let filePickerView = self.iTaskFactory?.getFilePickerComponent(view: stackView, templateId: template_id, formTaskManager: formTaskManager) else { return }
                stackView.addArrangedSubview(filePickerView)
                break
            default:
                break
            }
        }
        if !(isAutoSubmitEnabled ?? true) {
            button = UIButton(frame: CGRect(x: 0, y: 0, width: stackView.frame.width, height: 120))
            button?.backgroundColor = .white
            button?.setTitle(buttonLabel, for: .normal)
            button?.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
            button?.isEnabled = false
            button?.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)
            button?.backgroundColor = .darkGray
            self.stackView.addArrangedSubview(button!)
        }
        subscribeTask()

    }
    private func setColor() {
        processingActivityIndicator.color = dalCapture.getThemeConfig().getSecondaryContrastColor()
        processingLabel.textColor =  dalCapture.getThemeConfig().getSecondaryContrastColor()
        messageLabel.textColor =  dalCapture.getThemeConfig().getSecondaryContrastColor()
    }
    @objc func buttonClicked() {
        self.verifyTask()
    }
    public override func setTask(resultObject: NSObject) {
        processignView.isHidden = true
        var error = ""
        if resultObject.value(forKey: "error") is NSNull {
            error = ""
        } else {
            error = resultObject.value(forKey: "error") as? String ?? ""
        }
        if resultObject.value(forKey: "status") as! String == "completed" {
            messageLabel.isHidden = false
            messageLabel.textColor = dalCapture.getThemeConfig().getSuccessMainColor()
            messageLabel.text = "Verification Completed"
        } else if error != "" {
            messageLabel.isHidden = false
            messageLabel.text = error
            messageLabel.textColor = dalCapture.getThemeConfig().getErrorMainColor()
        } else {
            messageLabel.isHidden = true
        }
        if !(isAutoSubmitEnabled ?? true) {
            if resultObject.value(forKey: "status") as! String == "completed" {
                self.updateButton(enable: false, verified: true)
            } else {
                self.updateButton(enable: false, verified: false)
            }
        }
    }
    public func subscribeTask() {
        disposableList.append(formTaskManager.allTaskDone.subscribe { [self] status in
            guard let status = status.element else { return }
            if status ?? false  {
                DALCapture.printf("Capture","Form Task: \(self.taskKey)", "completed")
                if self.isAutoSubmitEnabled ?? false {
                        self.verifyTask()
                } else {
                    if let taskObject = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey) {
                        if taskObject.value(forKey: "status") as! String != "completed" {
                            self.updateButton(enable: true, verified: false)
                        } else {
                            self.updateButton(enable: false, verified: true)
                        }
                    }

                }
            } else if status == false {
                DALCapture.printf("Capture","Form task: \(self.taskKey)", "pending")
                self.updateButton(enable: false, verified: false)
                if messageLabel.isHidden == false {
                    self.messageLabel.isHidden = true
                }
            }
        } )
    }
    private func verifyTask() {
        if let taskObject = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey) {

            if taskObject.value(forKey: "status") as! String == "pending" || taskObject.value(forKey: "status") as! String == "in_progress" {
                updateButton(enable: false, verified: false)
                DALCapture.printf("Capture","Submitting form task: \(self.taskKey)", "")
                processignView.isHidden = false
                dalCapture.initiateTask(taskKey: taskKey)
            }
        }
    }

    private func updateButton(enable:Bool,verified:Bool) {
        button?.setTitle(buttonLabel.uppercased(), for: .normal)
        button?.backgroundColor = dalCapture.getThemeConfig().getPrimaryMainColor()
        if enable {
            button?.isEnabled = true
        } else {
            button?.isEnabled = false
            if verified {
                button?.setTitle("VERIFICATION COMPLETED", for: .normal)
                self.button?.backgroundColor = dalCapture.getThemeConfig().getSuccessMainColor()
            } else {
                self.button?.backgroundColor = .darkGray
            }
        }
    }
}
