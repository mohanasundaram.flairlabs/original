//
//  TextView.swift
//  Hostapp
//
//  Created by Admin on 23/02/22.
//
import Foundation
import UIKit
import core

public class TextView: UIView,UITextFieldDelegate {
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var errorLabel:UILabel!
    @IBOutlet weak var textField:UITextField!

    let kCONTENT_XIB_NAME           =   "TextView"

    private var placeholderText:String = ""
    private var regex:String = ""
    private var textValue:String = ""
    private var artifactKey:String = ""

    private var templateObject:NSObject?

    private var dalCapture = DALCapture()
    private var themeConfig:ThemeConfig? = nil
    private var taskKey = ""
    private var factTypeKey = ""
    var formatter: String = "yyyy-MM-dd"

    public override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        titleLabel.isHidden = true
        errorLabel.isHidden = true

        textField.delegate = self

    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager) {
        setConfig(dalCapture: dalCapture, templateId: templateId)
        taskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager,themeConfig:ThemeConfig) {
        self.themeConfig = themeConfig
        setConfig(dalCapture: dalCapture, templateId: templateId)
        taskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,formTaskManager:FormTaskManager) {
        setConfig(dalCapture: dalCapture, templateId: templateId)
        formTaskManager.registerToArtifactKey(artifactKey: artifactKey)
    }
    private func setConfig(dalCapture:DALCapture,templateId:Int) {
        self.dalCapture = dalCapture
        if themeConfig == nil {
            themeConfig = dalCapture.getThemeConfig()
        }
        self.templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")

        placeholderText = templateObject?.value(forKey: "label") as? String ?? ""
        let mandatory = templateObject?.value(forKey: "mandatory") as? Bool ?? false
        regex = templateObject?.value(forKey: "regex") as? String ?? ""
        taskKey = templateObject?.value(forKey: "task_key") as? String ?? ""
        factTypeKey = templateObject?.value(forKey: "fact_type_key") as? String ?? ""
        let artifactArray = templateObject?.value(forKey: "artifacts") as! NSArray
        if artifactArray.count > 0 {
            artifactKey = artifactArray[0] as! String
        }
        textValue = dalCapture.getValueforArtifactObject(artifactKey: artifactKey)
        textField.setAttributedTextWithSubscripts(text: "\(placeholderText)*")
        textField.textColor = themeConfig?.getSecondaryContrastColor()
        if textValue != "" {
            textField.text = self.textValue
            textField.textColor = themeConfig?.getSecondaryContrastColor()
            if mandatory {
                titleLabel.setAttributedTextWithSubscripts(text: "\(placeholderText)*")
            } else {
                titleLabel.text = placeholderText
            }
            titleLabel.isHidden  = false
        }
        if factTypeKey == "dob" {
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = self.formatter
            let date = dateformatter.date(from: self.textValue)
            textField.setInputViewCustomDateTimePicker(target: self, selector: #selector(pickerSelectionDone),date: date ?? Date())
        }
    }
    @objc func pickerSelectionDone() {
        if let datePicker = self.textField.inputView as? UIDatePicker {
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = self.formatter
            let date = dateformatter.string(from: datePicker.date)
            self.textField.text = date

        }
        self.textField.resignFirstResponder()
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        titleLabel.isHidden = false
        titleLabel.textColor = themeConfig?.getPrimaryMainColor()
        self.textField.textColor = themeConfig?.getSecondaryContrastColor()
        titleLabel.setAttributedTextWithSubscripts(text: "\(capitalizingEachWord(sentenceToCap: placeholderText))*")
        errorLabel.isHidden = true
        if textValue == "" || titleLabel.text == textField.text {
            textField.text = ""
        }
    }
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.setAttributedTextWithSubscripts(text: "\(capitalizingEachWord(sentenceToCap: placeholderText))*")
            textField.textColor = .red
            titleLabel.isHidden = true
            errorLabel.isHidden = false
            errorLabel.text = "Value must be specified."
            titleLabel.textColor = .red
            updateValues(data: "")

        } else {
            textField.textColor = themeConfig?.getSecondaryContrastColor()
            titleLabel.textColor = themeConfig?.getSecondaryContrastColor()
            errorLabel.isHidden = true
            textValue = textField.text ?? ""
            updateValues(data: textValue)
//            if regex != "" {
//                if !isValidInput(regex: regex, Input: textField.text ?? "") {
//                    errorLabel.isHidden = false
//                    errorLabel.text = "Value must be specified."
//                    titleLabel.textColor = .red
//                }
//            }
        }
    }
    private func updateValues(data:String) {
        dalCapture.saveTask(taskKey: taskKey, value:data)
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
