//
//  TextLabel.swift
//  Hostapp
//
//  Created by Admin on 23/02/22.
//

import Foundation
import UIKit
import core


class Header: UILabel {

    func headerLabel(title:String,frameWidthReference:UIView) {
        self.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: frameWidthReference.frame.width, height: 25))
        self.textAlignment = .left
        self.text = title
        self.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        self.font = UIFont.systemFont(ofSize: 22)
        self.numberOfLines = 0

    }
    func subHeaderLabel(title:String,frameWidthReference:UIView) {
        self.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: frameWidthReference.frame.width, height: 25))
        self.textAlignment = .left
        self.text = title
        self.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        self.font = UIFont.systemFont(ofSize: 17)
        self.numberOfLines = 0
    }
    func textwithValues(title:String,value:String,frameWidthReference:UIView) {
        self.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: frameWidthReference.frame.width, height: 25))
        self.textAlignment = .left
        self.numberOfLines = 0
        self.text = "\(capitalizingEachWord(sentenceToCap: title)) : \(capitalizingEachWord(sentenceToCap: value))"
        self.font = UIFont.systemFont(ofSize: 15)
    }
}
