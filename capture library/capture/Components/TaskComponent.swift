//
//  TaskComponent.swift
//  capture
//
//  Created by FL Mohan on 28/04/22.
//

import Foundation
import core
public protocol SetTaskResult {
func setTask(resultObject:NSObject)
}
public class TaskComponents:TemplateComponent,ITaskUpdate,SetTaskResult {
    public func initialise(dalCapture:DALCapture,templateId:Int,taskKey:String) {
        initialise(dalCapture: dalCapture, templateId: templateId)
        dalCapture.subscribeToTaskKey(key: taskKey, iTaskUpdate: self)
    }
    public func onUpdate(taskObject: NSObject) {
        setTask(resultObject: taskObject)
    }
    public func setTask(resultObject: NSObject) {

    }
}
