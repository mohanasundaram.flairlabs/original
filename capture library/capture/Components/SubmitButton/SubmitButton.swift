//
//  SubmitButton.swift
//  capture
//
//  Created by Admin on 19/04/22.
//

import Foundation

import Foundation
import UIKit
import core

public protocol ISubmitButton {

    func onSuccess()
    func onFailure(event:String,payload:NSObject)
}


public class SubmitButton: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var submitButton: UIButton!

    let kCONTENT_XIB_NAME           =   "SubmitButton"

    var dalCapture = DALCapture()

    var iSubmitButton:ISubmitButton?

    public init(frame: CGRect,dalCapture:DALCapture,iSubmitButton:ISubmitButton) {
        self.dalCapture = dalCapture
        self.iSubmitButton = iSubmitButton
        super.init(frame: frame)
        commonInit()

    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
    }
    @IBAction func didPressButton(_ tag: UIButton) {
        dalCapture.submitArtifacts { onResponse in
            let response = onResponse.value(forKey: "response") as? NSObject
            if response?.value(forKey: "success") as! Bool == true {
                if response?.value(forKey: "capture_pre_check?") as! Bool == true {
                    self.iSubmitButton?.onSuccess()
                } else {
                    self.iSubmitButton?.onFailure(event: "missing_artifacts", payload: ["missing_artifacts":(response?.value(forKey: "missing_artifacts") as! NSArray)] as NSObject)
                }
            } else {
                self.iSubmitButton?.onFailure(event: "failure", payload: onResponse)
            }
        }
    }
}

