//
//  SectionView.swift
//  Hostapp
//
//  Created by Admin on 28/02/22.
//

import Foundation
import UIKit
import core

public protocol PluginCallBack {
    func didUpdateValue(title: String, updatedText: String)
}
public class SectionView:TaskComponents, UITextFieldDelegate, PluginCallBack {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var editButton:UIButton!
    @IBOutlet weak var buttonView:UIView!
    
    let kCONTENT_XIB_NAME           =   "SectionView"
    
    private var refsArray = NSArray()

    private var changesArray:[NSObject] = []
    
    private var textDelegate:UITextFieldDelegate?
    
    private var templateObject:NSObject?
    
    private var isEdit:Bool = false
    
    private var dalCapture = DALCapture()

    private var resultObject:NSObject?

    private var artifactKey = ""
    private var taskKey = ""
    private var configKey = ""
    private var defaultKey = ""
    private var pathArray:[String] = []
    private var overridekeyslist:[String] = []

    private var viewHashMap:[String:Any] = [:]
    private var timer = Timer()

    public override init(frame: CGRect) {

        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        stackView.spacing = 15
        buttonView.isHidden = true
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,taskManager:TaskManager) {
        setConfig(dalCapture: dalCapture, templateId: templateId)
        taskManager.registerToTaskKey(taskKey: taskKey)
    }
    public func setConfig(dalCapture:DALCapture,templateId:Int,formTaskManager:FormTaskManager) {
        setConfig(dalCapture: dalCapture, templateId: templateId)
        formTaskManager.registerToTaskKey(taskKey: taskKey)
    }
    private func setConfig(dalCapture:DALCapture,templateId:Int) {
        self.dalCapture = dalCapture
        self.templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")
        artifactKey = dalCapture.getArtifactKey(object: templateObject ?? [] as NSObject)
        if let refs = templateObject?.value(forKey: "refs") as? NSArray {
            self.refsArray = refs
        }
        if dalCapture.getCustomerOcrEdit() {
            buttonView.isHidden = false
        }
        renderUiItem()
        editButton.tintColor = ThemeConfig.shared.getPrimaryMainColor()
        initialise(dalCapture: dalCapture, templateId: templateId, taskKey: taskKey)
    }
    private func renderUiItem() {
        stackView.removeSubviews()
        for(_ , value) in refsArray.enumerated() {
            let refsObject = value as! NSObject
            if let editConfigObject = refsObject.value(forKey: "edit_config") as? NSObject {
                configKey = refsObject.value(forKey: "key") as! String
                let keySplit = configKey.split(separator: ":").map(String.init)
                taskKey = keySplit[0]
                pathArray = keySplit[1].split(separator: ".").map(String.init)
                defaultKey = editConfigObject.value(forKey: "default") as! String
                if let overrideArray = editConfigObject.value(forKey: "override_keys") as? NSArray, overrideArray.count > 0 {
                    for overrideArray in overrideArray {
                        overridekeyslist.append(overrideArray as! String)
                    }
                }
            }
        }
    }
    private func updateUi() {
        guard let result = resultObject?.value(forKey: "result") as? NSObject else { return }
        DispatchQueue.main.async { [self] in
            stackView.removeSubviews()
            let object = getAutomatedResponse(object: result)
            if refsArray.count == 1 {
                let newRefArray = convertToRefArray(extractedObject: object)
                iterateToRef(refsArray: newRefArray)
            } else {
                iterateToRef(refsArray: refsArray as! [NSObject])
            }
        }
    }
    private func getAutomatedResponse(object:NSObject) -> NSObject {
        var object = object
        for pathArray in pathArray {
            if let value = object.value(forKey: "\(pathArray)") as? NSObject {
                object = value
            }
        }
        return object
    }
    private func convertToRefArray(extractedObject:NSObject) -> [NSObject] {
        var refArray = [NSObject]()
        let extractedObject = extractedObject as? [String:Any] ?? [:]
        for key in extractedObject.keys {
            var refObject = [String:Any]()
            refObject.updateValue(configKey + "." + key, forKey: "key")
            refObject.updateValue("response_ref", forKey: "type")
            refArray.append(refObject as NSObject)
        }
        return refArray
    }
    private func iterateToRef(refsArray:[NSObject]) {
        for refsArray in refsArray {
            let refsObject = refsArray
            if !(refsObject.value(forKey: "edit_config") != nil) {
                createSection(object: refsObject)
            }
        }
    }
    private func createSection(object:NSObject) {
        var label = ""
        let key = object.value(forKey: "key") as! String
        let keySplit = key.split(separator: ":").map(String.init)
        let path = keySplit[1].split(separator: ".").map(String.init)
        if let labl = object.value(forKey: "label") as? String {
            label = labl
        } else {
            label = path.last ?? ""
        }
        if defaultKey == "disabled" {
            buttonView.isHidden = true
        }
        if isEdit {
            let pluginView = PluginTextView(frame: .zero, textValue: getValue(path:path), placeholder: label, delegate: self)
            if label == "date_of_birth" || label == "date_of_issue" {
                pluginView.setDatePicker(date: getValue(path:path))
            }

            switch defaultKey {
            case "enabled","enable":
                if !overridekeyslist.contains(label) {
                    stackView.addArrangedSubview(pluginView)
                }
                break
            case "disabled":

                break
            case "disable":
                if overridekeyslist.contains(label) {
                    stackView.addArrangedSubview(pluginView)
                }
                break
            default:

                break
            }

        } else {
            let textLabel = Header()
            textLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
            textLabel.textwithValues(title: label, value: getValue(path:path), frameWidthReference: stackView)
            stackView.addArrangedSubview(textLabel)
        }
    }
    private func getValue(path:[String]) -> String {
        var stringValue = ""
        let taskObject = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey)
        var resultObject = taskObject?.value(forKey: "result") as! [String:Any]
        for path in path {
            if let value = resultObject["manual_response"] as? [String:Any] {
                resultObject = value
            } else
            if let value = resultObject[path] as? [String:Any] {
                resultObject = value
            } else {
                stringValue = resultObject[path] as? String ?? ""
            }
        }
        return stringValue
    }
    public func didUpdateValue(title: String, updatedText: String) {
        var updateObject = resultObject as! [String:Any]
        var result = updateObject["result"] as! [String:Any]
        var manual_response = result["manual_response"] as! [String:Any]
        var extraction_output = manual_response["extraction_output"] as! [String:Any]
        extraction_output.updateValue(updatedText, forKey: title)
        if changesArray.isEmpty {
            var value:[String:String] = [:]
            value.updateValue("extraction_output.\(title)", forKey: "key")
            value.updateValue(updatedText, forKey: "value")
            changesArray = [value as NSObject]
        } else {
            for (index,values) in changesArray.enumerated() {
                if values.value(forKey: "key") as! String == "extraction_output.\(title)" {
                    changesArray.remove(at: index)
                }
            }
            var value:[String:String] = [:]
            value.updateValue("extraction_output.\(title)", forKey: "key")
            value.updateValue(updatedText, forKey: "value")
            changesArray.append(value as NSObject)
        }
        manual_response.updateValue(extraction_output as NSObject, forKey: "extraction_output")
        result.updateValue(manual_response as NSObject, forKey: "manual_response")
        result.updateValue(changesArray, forKey: "changes")
        updateObject.updateValue(result as NSObject, forKey: "result")
        updateObject.updateValue("completed", forKey: "status")
        dalCapture.updateTaskObject(taskKey: taskKey, resultObject: updateObject as NSObject)
    }

    public override func setTask(resultObject: NSObject) {
        self.resultObject = resultObject
        isEdit = false
        updateUi()
    }
    @IBAction func editButtonAction(_ sender: UIButton) {
        if editButton.titleLabel?.text == "Edit" {
            isEdit = true
            editButton.setTitle("Save", for: .normal)
        } else {
            isEdit = false
            editButton.setTitle("Edit", for: .normal)
        }
        DispatchQueue.main.async {
            self.updateUi()
        }
    }
    
}
public class PluginTextView: UIView,UITextFieldDelegate {
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var errorLabel:UILabel!
    @IBOutlet weak var textField:UITextField!

    let kCONTENT_XIB_NAME           =   "PluginTextView"
    
    private var placeholderText:String = ""
    private var regex:String = ""
    private var textValue:String = ""
    private var formatter = "yyyy-MM-dd"
    private var pluginDelegate:PluginCallBack?


    public init(frame: CGRect,textValue:String,placeholder:String,delegate:PluginCallBack) {
        self.textValue = textValue
        self.placeholderText = placeholder
        self.pluginDelegate = delegate
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        titleLabel.isHidden = true
        errorLabel.isHidden = true

        textField.delegate = self

        if self.textValue != "" {
            textField.text = self.textValue
            titleLabel.text = capitalizingEachWord(sentenceToCap: placeholderText)
            titleLabel.isHidden  = false
        } else {
            textField.text = capitalizingEachWord(sentenceToCap: placeholderText)
        }

        textField.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        titleLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()

    }
    public func setDatePicker(date:String) {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = self.formatter
        let date = dateformatter.date(from: date)
        textField.setInputViewCustomDateTimePicker(target: self, selector: #selector(pickerSelectionDone), date: date ?? Date())
    }
    @objc func pickerSelectionDone() {
        if let datePicker = self.textField.inputView as? UIDatePicker {
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = self.formatter
            let date = dateformatter.string(from: datePicker.date)
            self.textField.text = date
        }
        self.textField.resignFirstResponder()
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        titleLabel.isHidden = false
        titleLabel.textColor = ThemeConfig.shared.getPrimaryMainColor()
        self.textField.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        titleLabel.setAttributedTextWithSubscripts(text: "\(capitalizingEachWord(sentenceToCap: placeholderText))*")
        errorLabel.isHidden = true
        if textValue == "" || titleLabel.text == textField.text {
            textField.text = ""
        }
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.setAttributedTextWithSubscripts(text: "\(capitalizingEachWord(sentenceToCap: placeholderText))*")
            textField.textColor = .red
            titleLabel.isHidden = true
            errorLabel.isHidden = false
            errorLabel.text = "Value must be specified."
            titleLabel.textColor = .red
            updateValues(data: "")

        } else {
            textField.textColor = ThemeConfig.shared.getSecondaryContrastColor()
            titleLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
            errorLabel.isHidden = true
            textValue = textField.text ?? ""
            updateValues(data: textValue)
            if regex != "" {
                if !isValidInput(regex: regex, Input: textField.text ?? "") {
                    errorLabel.isHidden = false
                    errorLabel.text = "Value must be specified."
                    titleLabel.textColor = .red
                }
            }
        }
    }
    func updateValues(data:String) {
            self.pluginDelegate?.didUpdateValue(title: placeholderText, updatedText: data)
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
}
