//
//  CardView.swift
//  Hostapp
//
//  Created by Admin on 03/03/22.
//

import Foundation
import UIKit
import core
import avkyc

public class CardView:TemplateComponent {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var stackView:UIStackView!

    private var textDelegate:UITextFieldDelegate?

    private var templateId = 0

    private var dalCapture = DALCapture()

    private var taskManager = TaskManager()

    private var taskFactory:TaskFactory?

    let kCONTENT_XIB_NAME           =   "CardView"

    public init(frame: CGRect,dalCapture:DALCapture,templateId:Int,taskFactory:TaskFactory,taskManager:TaskManager) {
        self.dalCapture = dalCapture
        self.taskFactory = taskFactory
        self.templateId = templateId
        self.taskManager = taskManager
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        
        cardView.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
        cardView.layer.cornerRadius = 7.0
        cardView.layer.shadowColor = hexStringToUIColor(hex: "1E1E1E").cgColor
        cardView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        cardView.layer.shadowRadius = 1.5
        cardView.layer.shadowOpacity = 0.7
        cardView.translatesAutoresizingMaskIntoConstraints = false

        createCardView()
    }
    public override func setTemplate(resultObject: NSObject) {
        if (resultObject.value(forKey: "visibility") != nil) {
            let visibility = resultObject.value(forKey: "visibility") as! Bool
            DispatchQueue.main.async {
                if visibility {
                    self.isHidden = false
                } else {
                    self.isHidden = true
                }
            }
        }
    }

    private func createCardView() {

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 10
        stackView.distribution = .fill


        self.initialise(dalCapture: dalCapture, templateId: templateId)

        let idObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")
        let title = idObject?.value(forKey: "title") as! String
        let detail = idObject?.value(forKey: "detail") as? String ?? ""

        if title != "" {
            let label = Header()
            label.headerLabel(title: title, frameWidthReference: stackView)
            stackView.addArrangedSubview(label)
        }
        if detail != "" {
            let label = Header()
            label.subHeaderLabel(title: detail, frameWidthReference: stackView)
            stackView.addArrangedSubview(label)
        }
        guard let jsonArrrays = dalCapture.getCaptureItem(templateId: templateId) else { return }

        var template_id = 0
        for(index, _) in jsonArrrays.enumerated() {
            guard let taskUI = jsonArrrays[index] as? NSObject else { return }
            template_id = taskUI.value(forKey: "template_id") as! Int
            guard let taskIdObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(template_id)") else { return }
            let taskType = taskIdObject.value(forKey: "type") as? String ?? ""
            switch taskType {
            case "text":
                guard let textView = taskFactory?.getTextComponent(view: stackView,templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(textView)
                break
            case "image":
                guard let imageView = taskFactory?.getImageComponent(view:stackView,templateId: template_id, taskManager: taskManager) else { return }
                let stackedInfoView = UIStackView()
                stackedInfoView.axis = .vertical
                stackedInfoView.alignment = .leading
                stackedInfoView.translatesAutoresizingMaskIntoConstraints = false
                stackedInfoView.addArrangedSubview(imageView)
                stackView.addArrangedSubview(stackedInfoView)
                break
            case "group":
                guard let groupView = taskFactory?.getGroupComponent(view: stackView,templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(groupView)
                break
            case "form":
                guard let formView = taskFactory?.getFormComponent(view: stackView,templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(formView)
                break
            case "document":
                guard let documentView = taskFactory?.getDocumentComponent(view: stackView,templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(documentView)
                break
            case "checkbox":
                guard let checkBoxView = taskFactory?.getCheckBoxComponent(view: stackView,templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(checkBoxView)
                break
            case "radio":
                guard let radioView = taskFactory?.getRadioComponent(view: stackView,templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(radioView)
                break
            case "dropdown":
                guard let dropDownView = taskFactory?.getDropdownComponent(view: stackView,templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(dropDownView)
                break
            case "section":
                guard let sectionView = taskFactory?.getSectionComponent(view: stackView,templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(sectionView)
                break
            case "button":
                guard let buttonView = taskFactory?.getButtonComponent(view: stackView,templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(buttonView)
                break
            case "filepicker":
                guard let filePickerView = taskFactory?.getFilePickerComponent(view: stackView, templateId: template_id, taskManager: taskManager) else { return }
                stackView.addArrangedSubview(filePickerView)
                break
            default:
                break
            }
        }
    }

}
