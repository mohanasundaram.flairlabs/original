//
//  CapturePreReqisite.swift
//  Hostapp
//
//  Created by FL Mohan on 03/06/22.
//

import Foundation
import AVFoundation
import UIKit
import core
import avkyc

public class CapturePreRequisiteView: UIView, IRedirectNext {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var subTitle:UILabel!
    @IBOutlet weak var cardTitle:UILabel!
    @IBOutlet weak var contentStackView:UIStackView!
    @IBOutlet weak var contentOfCardView:UIView!
    @IBOutlet weak var cardView:UIView!
    @IBOutlet weak var cardStackView:UIStackView!
    @IBOutlet weak var contentHeightConstraint:NSLayoutConstraint!

    let kCONTENT_XIB_NAME           =   "CapturePreRequisite"

    private var dalCapture = DALCapture()

    private var delegate:ICapture?

    private var object = NSObject()

    private var prerequisiteItems:[String] = []

    private var selectedItems:[String] = []

    private var listTableView: UITableView!

    public init(frame: CGRect,object: NSObject,dalCapture:DALCapture,delegate:ICapture) {
        self.delegate = delegate
        self.object = object
        self.dalCapture = dalCapture
        super.init(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        renderUi()
    }

    private func renderUi() {
        guard let configObject = object.value(forKey: "config") as? NSObject else { return  }
        if let title = configObject.value(forKey: "title") as? String {
            titleLabel.text = title
            titleLabel.textColor =  dalCapture.getThemeConfig().getSecondaryContrastColor()
        }
        let display_as_checklist = configObject.value(forKey: "display_as_checklist") as! Bool
        //        let header = configObject.value(forKey: "header") as! String
        if let sTitle = configObject.value(forKey: "subtitle") as? String {
            subTitle.text = sTitle
            subTitle.textColor =  dalCapture.getThemeConfig().getSecondaryContrastColor()
        }
        prerequisiteItems = configObject.value(forKey: "prerequisite_items") as! [String]

        if display_as_checklist {

            contentOfCardView.isHidden = true
            subTitle.isHidden = false
            listTableView = UITableView()
            listTableView.register(UINib(nibName: "PreRequisiteSelectionCell", bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: "PreRequisiteSelectionCell")
            listTableView.dataSource = self
            listTableView.delegate = self
            listTableView.isScrollEnabled = false
            listTableView.tableFooterView = UIView()
            listTableView.backgroundColor = .clear
            listTableView.separatorColor = .clear
            contentStackView.addArrangedSubview(listTableView)
            listTableView.translatesAutoresizingMaskIntoConstraints = false


            submitButton.isEnabled = false
            submitButton.backgroundColor = .gray
            submitButton.setTitle("I'M READY (\(selectedItems.count)/\(prerequisiteItems.count))", for: .normal)

            contentStackView.addArrangedSubview(listTableView)


        } else {
            contentOfCardView.isHidden = false
            subTitle.isHidden = true
            cardView.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
            cardView.layer.cornerRadius = 7.0
            cardView.layer.shadowColor = hexStringToUIColor(hex: "1E1E1E").cgColor
            cardView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            cardView.layer.shadowRadius = 1.5
            cardView.layer.shadowOpacity = 0.7
            cardView.translatesAutoresizingMaskIntoConstraints = false

            if let sTitle = configObject.value(forKey: "subtitle") as? String {
                cardTitle.text = sTitle
                cardTitle.textColor =  dalCapture.getThemeConfig().getSecondaryContrastColor()
            }
            for prerequisiteItem in prerequisiteItems {
                let label = UILabel()
                label.textColor = .white
                label.text = "\u{2022} \(prerequisiteItem)"
                cardStackView.addArrangedSubview(label)
            }
            submitButton.isEnabled = true
            submitButton.tintColor = ThemeConfig.shared.getPrimaryMainColor()

            contentHeightConstraint.constant = CGFloat(80 + (40 * prerequisiteItems.count))

        }

        submitButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)
    }
    private func checkForSelectedOption() {
        submitButton.setTitle("I'M READY (\(selectedItems.count)/\(prerequisiteItems.count))", for: .normal)
        if selectedItems.count == prerequisiteItems.count {
            submitButton.isEnabled = true
            submitButton.tintColor = ThemeConfig.shared.getPrimaryMainColor()
        } else {
            submitButton.isEnabled = false
            submitButton.backgroundColor = .darkGray
        }
    }
    @IBAction private func didTapButton(sender:UIButton) {
        submitButton.isEnabled = false
        self.delegate?.redirectToNextPage(iRedirectNext: self)

    }
    public func onSuccess() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = false
        }
    }
    public func onFailure() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = true
        }
    }
    public func BackPressed() {
        self.delegate?.onCaptureIntermediate(object: ["event": "USER_BACKPRESS"] as NSObject)
    }
}
extension CapturePreRequisiteView:UITableViewDelegate,UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prerequisiteItems.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PreRequisiteSelectionCell") as? PreRequisiteSelectionCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none

        cell.configureCell(title: prerequisiteItems[indexPath.row], textColor: dalCapture.getThemeConfig().getSecondaryContrastColor())
        if selectedItems.contains(self.prerequisiteItems[indexPath.row]) {
            cell.contentView.setCornerBorder(color: dalCapture.getThemeConfig().getPrimaryMainColor(), cornerRadius: 10, borderWidth: 2)
            cell.imageAccessory.image = UIImage(named: "CheckBox", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(dalCapture.getThemeConfig().getPrimaryMainColor())
        } else {
            cell.contentView.setCornerBorder(color: dalCapture.getThemeConfig().getSecondaryContrastColor(), cornerRadius: 10, borderWidth: 0.2)
            cell.imageAccessory.image = UIImage(named: "UncheckBox", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(dalCapture.getThemeConfig().getSecondaryContrastColor())
        }
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if selectedItems.contains(prerequisiteItems[indexPath.row]) {
            if let index = selectedItems.firstIndex(of: prerequisiteItems[indexPath.row]) {
                selectedItems.remove(at: index)
            }
        } else {
            selectedItems.append(prerequisiteItems[indexPath.row])
        }
        self.listTableView.reloadData()
        checkForSelectedOption()
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
class PreRequisiteSelectionCell: UITableViewCell {
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var imageAccessory: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(title: String,textColor:UIColor) {
        self.tagLabel.textColor = textColor
        self.tagLabel.text = title
    }

}


