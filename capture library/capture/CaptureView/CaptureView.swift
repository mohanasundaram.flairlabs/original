//
//  CaptureView.swift
//  Hostapp
//
//  Created by Admin on 08/03/22.
//

import Foundation
import AVFoundation
import UIKit
import core
import avkyc
import WebKit
import RxSwift
import svc

public protocol ICapture {
    func Header()
    func redirectToNextPage(iRedirectNext:IRedirectNext)
    func showThankYouView()
    func onCaptureIntermediate(object:NSObject)
}
public protocol IRedirectNext {
    func onSuccess()
    func onFailure()
}
public class CaptureView: UIView, WKUIDelegate, IPageDataCallback, AVKYCCallBack, SVCCallBack, IRedirectNext,ICapture {


    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var captureContentView:UIView!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var previousButton:UIButton!
    @IBOutlet weak var nextButton:UIButton!

    let kCONTENT_XIB_NAME           =   "CaptureView"

    private var captureView = UIScrollView()
    private var contentOfScrollView = UIStackView()
    private var loggerWrapper = LoggerWrapper()
    private var dalCapture = DALCapture()
    private var callBack:ICapture?
    private var compFactory:CompFactory?
    private var capturePayload:NSObject?
    private var nextPage:String = ""
    private var loggerStartTime: Double = 0
    private var captureTotalTask = 0
    private var captureCtr = 0
    private var captureArray:NSArray = []
    private var taskKey = ""
    private var taskManager = TaskManager()
    private var pluginhandlerList:[PluginHandler] = []
    private var disposableList:[Disposable] = []

    private var avkycView: AvkycView? = nil
    private var svcView: SvcView? = nil
    private var skillSelectionView:SkillSelectionView? = nil

    private var modelCaptureConfig = ModelCaptureConfig()

    public init(frame: CGRect,dalCapture:DALCapture,componentFactory:CompFactory,modelCaptureConfig:ModelCaptureConfig,taskManager:TaskManager, capturePayload: NSObject,callBack:ICapture) {
        self.dalCapture = dalCapture
        self.compFactory = componentFactory
        self.capturePayload = capturePayload
        self.callBack = callBack
        self.taskManager = taskManager
        self.modelCaptureConfig = modelCaptureConfig
        super.init(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        setupScrollView()

        previousButton.isHidden = true
        nextButton.isHidden = true
        renderCapture()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name:UIResponder.keyboardWillHideNotification, object: nil)
//        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(keyboardWillHide(sender:))))

    }
    private func renderCapture() {

        previousButton.setCornerBorder(color: ThemeConfig.shared.getPrimaryMainColor(), cornerRadius: 10, borderWidth: 0.5)
        previousButton.setTitleColor(ThemeConfig.shared.getSecondaryContrastColor(), for: .normal)
        nextButton.setTitleColor(ThemeConfig.shared.getSecondaryMainColor(), for: .normal)
        nextButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)
        previousButton.backgroundColor  = .clear
        nextButton.backgroundColor = .gray

        let pageSequence = dalCapture.getNextPageName()
        nextPage = pageSequence.getPage()
        switch PageEnum(rawValue: nextPage) {
        case .AVKYC:
            self.submitButton.setTitle(ButtonTitle.VIDEO_KYC.rawValue, for: .normal)
            break
        case .SVC :
            self.submitButton.setTitle(ButtonTitle.SELF_VIDEO.rawValue, for: .normal)
            break
        case .SKILL_SELECT,.AV_PREREQUISITE,.SVC_PREREQUISITE :
            self.submitButton.setTitle(ButtonTitle.PROCEED.rawValue, for: .normal)
            break
        default:
            self.submitButton.setTitle(ButtonTitle.INIT_VERIFICATION.rawValue, for: .normal)
            break
        }
        
        loggerWrapper.logPageRender(loggerStartTime: loggerStartTime, component: "Capture",referenceId: dalCapture.getRequestId() != "" ? dalCapture.getRequestId() : dalCapture.getCaptureId(),referenceType: dalCapture.getReferenceType(), meta: [:])


        loggerWrapper.log(logLevel: LogLevel.shared.Info,logDetails: ModalLogDetails(service_category:"captureSDK", service: "RoutePage", event_name:dalCapture.getTatSince(start: loggerStartTime), component: "IndvCaptureComponent", event_source: "renderCapture"), meta: [:])

        let configObject = capturePayload?.value(forKey: "config") as? NSObject

        if let _ = configObject?.value(forKey: "capture") as? NSObject {
            self.parseCaptureResponse(object: configObject!)
        }
        subscribeTask()
        subscribeButton()

    }
    private func parseCaptureResponse(object: NSObject) {
        self.callBack?.Header()
        captureArray = object.value(forKey: "capture") as! NSArray
        taskManager.setCaptureArray(captureArray: captureArray)
        captureTotalTask = captureArray.count
        captureCtr = 0

        if captureArray.count > 1 {
            submitButton.isHidden = true
            nextButton.isHidden = false
        }
        if captureTotalTask > 0 {
            loadCapturePage(pageNo: captureCtr)
        }
    }
    private func loadCapturePage(pageNo:Int) {

        submitButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)

        guard let jsonArray = captureArray[pageNo] as? NSArray else { return }
        for(j, _) in jsonArray.enumerated() {
            guard let jsonObject = jsonArray[j] as? NSObject else { return }
            let templateId = jsonObject.value(forKey: "template_id") as! Int

            let idObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(templateId)")
            let type = idObject?.value(forKey: "type") as! String
            var cardView:UIView?
            if type == "card" {
                cardView = compFactory?.getCardComponent(view:self,templateId:templateId, taskManager: taskManager)
            }
            if let _ = jsonObject.value(forKey: "plugins") {
                let pluginArray = jsonObject.value(forKey: "plugins") as! NSArray
                let pluginObject = pluginArray[0] as! NSObject
                pluginhandlerList.append(PluginHandler(dalCapture: dalCapture, pluginObject: pluginObject, templateId: templateId))

            }
            if cardView != nil {
                contentOfScrollView.addArrangedSubview(cardView!)
            }
        }
    }
    @IBAction func onNextClick(sender:UIButton) {
        captureCtr = captureCtr + 1
        if captureCtr < captureTotalTask {
            previousButton.isHidden = false
            contentOfScrollView.removeSubviews()
            if captureCtr == captureTotalTask - 1 {
                submitButton.isHidden = false
                submitButton.isEnabled = false
                nextButton.isHidden = true
            }
            taskManager.onNextTask(pageNo: captureCtr)
            loadCapturePage(pageNo: captureCtr)
        }
    }
    @IBAction func onPreviousClick(sender:UIButton) {
        captureCtr = captureCtr - 1
        if captureCtr >= 0 {
            contentOfScrollView.removeSubviews()
            previousButton.isHidden = false
            nextButton.isHidden = false
            nextButton.isEnabled = false
            submitButton.isHidden = true
            if captureCtr == 0 {
                previousButton.isHidden = true
            }
            taskManager.onNextTask(pageNo: captureCtr)
            loadCapturePage(pageNo: captureCtr)
        }
    }
    @IBAction func onInitiateClick(sender:UIButton) {
        submitButton.isEnabled = false
        dalCapture.submitArtifacts { [self] onResponse in
            guard let responseObject = onResponse.value(forKey: "response") as? NSObject else { return }
            let success = responseObject.value(forKey: "success") as? Bool ?? false
            let capture_pre_check = responseObject.value(forKey: "capture_pre_check?") as? Bool ?? false
            DispatchQueue.main.async { [self] in
                switch PageEnum(rawValue: nextPage) {
                case .PAGE_END :
                    if success {
                        callBack?.showThankYouView()
                    }
                    break
                case .AVKYC, .SVC, .SKILL_SELECT,.CAPTURE_PREREQUISITE, .AV_PREREQUISITE, .SVC_PREREQUISITE :
                    if capture_pre_check {
                        callBack?.redirectToNextPage(iRedirectNext: self)
                    }
                    break
                default:
                    break
                }
            }
        }
    }
    public func onSuccess() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = false
        }
    }

    public func onFailure() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = true
        }
    }

    public func subscribeTask() {
        disposableList.append(taskManager.allTaskDone.subscribe { [self] status in
            if (status.element ?? false) ?? false {
                submitButton.isEnabled = true
                nextButton.isEnabled = true
                nextButton.tintColor = ThemeConfig.shared.getPrimaryMainColor()
                submitButton.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
            } else if status.element == false {
                submitButton.isEnabled = false
                nextButton.isEnabled = false
                submitButton.backgroundColor = .gray
                nextButton.backgroundColor = .gray
            }
        } )
    }
    public func subscribeButton() {
        disposableList.append(dalCapture.buttonBehaviorSubject.subscribe { [self] page in
            self.taskKey = (page.element ?? "") ?? ""
            if taskKey == "self_video.capture_video_activity" {
                self.startTask(page: "self_video")
            } else {
                if taskKey != "" {
                self.startTask(page: self.taskKey)
                }
            }

        })
    }
    private func captureCleanUp() {
        for pluginhandlerList in pluginhandlerList {
            pluginhandlerList.onDestroy()
        }
        pluginhandlerList.removeAll()
        for disposableList in disposableList {
            disposableList.dispose()
        }
        disposableList.removeAll()
    }
    private func startTask(page:String) {
        dalCapture.fetchPageData(page: page, iPageDataCallback: self)
    }
    public func onPageDataSuccess(page: String, object: NSObject) {
        DispatchQueue.main.async { [self] in
            switch page {
            case "vkyc.assisted_vkyc":
                avkycView = AssistedVideoKycFactory().startAvkyc(modelAVKYCConfig:modelCaptureConfig.getModelAVKYCConfig())
                avkycView?.initialiseAvkyc(view: self.contentView, data: object, delegate: self)
                loadView(view: avkycView ?? UIView())
                break
            case "self_video", "self_video.capture_video_activity":
                guard let configObject = object.value(forKey: "config") as? NSObject else { return }
                svcView = SelfVideoKycFactory().startSvc(svcConfig: modelCaptureConfig.getModelSvcConfig())
                svcView?.initialiseSvc(view: self.contentView,configObject:configObject, delegate: self)
                loadView(view: svcView ?? UIView())
                break
            case "av_skill_select":
                skillSelectionView = SkillSelectionView(view: self.contentView, dalCapture: dalCapture, skillObject: object, delegate: self)
                loadView(view: skillSelectionView ?? UIView())
                break
            default:
                break
            }
        }
    }
    private func loadView(view:UIView) {
        contentView.addSubview(view)
    }
    public func onPageDataFailure(message: String) {

    }
    public func BackPressed() {
        DispatchQueue.main.async { [self] in
            if ((avkycView?.isDescendant(of: self)) != nil) {
                avkycView?.BackPressed()
            } else if ((svcView?.isDescendant(of: self)) != nil) {
                svcView?.BackPressed()
            } else if ((skillSelectionView?.isDescendant(of: self)) != nil) {
                skillSelectionView?.BackPressed()
            } else {
                self.captureCleanUp()
                self.callBack?.onCaptureIntermediate(object: ["event":"USER_BACKPRESS"] as NSObject)
            }
        }
    }
}
extension CaptureView {
    // callbacks
    public func Header() {

    }

    public func redirectToNextPage(iRedirectNext: IRedirectNext) {

    }

    public func showThankYouView() {

    }

    public func onCaptureIntermediate(object: NSObject) {

    }
    public func onSvcSuccess(object: NSObject) {
        var taskObject = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey) as! [String:Any]
        taskObject.updateValue("completed", forKey: "status")
        dalCapture.updateTaskObject(taskKey: taskKey, resultObject: taskObject as NSObject)
        if ((svcView?.isDescendant(of: contentView)) != nil) {
            svcView?.removeFromSuperview()
        }

    }

    public func onSvcIntermediate(object: NSObject) {

    }

    public func onSvcFailure(object: NSObject) {

    }
    public func onAssistedSuccess(object: NSObject) {
        DispatchQueue.main.async { [self] in
            let value = object.value(forKey: "event") as! String
            switch value {
            case "CALL_END", "USER_END_CALL","AGENT_ENDED_CALL":
                var taskObject = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey) as! [String:Any]
                taskObject.updateValue("completed", forKey: "status")
                dalCapture.updateTaskObject(taskKey: taskKey, resultObject: taskObject as NSObject)
                if ((avkycView?.isDescendant(of: contentView)) != nil) {
                    avkycView?.removeFromSuperview()
                }
                break
            default:
                break
            }
        }
    }

    public func onAssistedIntermediate(object: NSObject) {
        let value = object.value(forKey: "event") as! String
        DispatchQueue.main.async { [self] in
            switch value {
            case "VKYC_INITIATED":
                break
            case "VIDEOCOMPONENT_INITIATED":
                break
            case "USER_END_CALL","USER_BACKPRESS","DISCONNECT","user_partially_exit":
                if ((avkycView?.isDescendant(of: self)) != nil) {
                    avkycView?.removeFromSuperview()
                }
                break
            default:
                break
            }
        }
    }

    public func onAssistedFailure(object: NSObject) {
        let value = object.value(forKey: "event") as! String
        DispatchQueue.main.async { [self] in
            switch value {
            case "RECONNECTS_EXHAUSTED","NETWORK_DISCONNECTED","OPERATOR_EXIT","SOCKET_TIMEOUT","USER_EXIT":
                if ((avkycView?.isDescendant(of: self)) != nil) {
                    avkycView?.removeFromSuperview()
                }
                break
            default:
                break
            }
        }
    }
    // view setup
    private func setupScrollView() {

        captureView.backgroundColor = .clear

        captureContentView.addSubview(captureView)

        captureView.translatesAutoresizingMaskIntoConstraints = false

        captureView.leadingAnchor.constraint(equalTo: captureContentView.leadingAnchor).isActive = true
        captureView.trailingAnchor.constraint(equalTo: captureContentView.trailingAnchor).isActive = true
        captureView.topAnchor.constraint(equalTo: captureContentView.topAnchor).isActive = true
        captureView.bottomAnchor.constraint(equalTo: captureContentView.bottomAnchor).isActive = true

        setUpContentOfScrollView()

    }

    private func setUpContentOfScrollView() {

        contentOfScrollView.axis            = .vertical
        contentOfScrollView.distribution    = .fill
        contentOfScrollView.alignment       = .fill
        contentOfScrollView.spacing         = 15

        captureView.addSubview(contentOfScrollView)

        contentOfScrollView.translatesAutoresizingMaskIntoConstraints = false
        contentOfScrollView.leadingAnchor.constraint(equalTo: captureContentView.leadingAnchor).isActive = true
        contentOfScrollView.trailingAnchor.constraint(equalTo: captureContentView.trailingAnchor).isActive = true
        contentOfScrollView.topAnchor.constraint(equalTo: captureView.topAnchor).isActive = true
        contentOfScrollView.bottomAnchor.constraint(equalTo: captureView.bottomAnchor).isActive = true
    }
    @objc func keyboardWillShow(sender: NSNotification) {
        if let keyboardSize = (sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            captureView.contentInset.bottom = keyboardSize.height
        }
    }

    @objc func keyboardWillHide(sender: NSNotification) {
        captureView.contentInset.bottom = 0
    }
}
