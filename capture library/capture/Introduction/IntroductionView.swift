//
//  IntroductionView.swift
//  Hostapp
//
//  Created by Admin on 08/03/22.
//

import Foundation
import AVFoundation
import UIKit
import core
import avkyc

public class IntroductionView: UIView,IRedirectNext {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var introContentView:UIView!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var textLabel:UILabel!

    let kCONTENT_XIB_NAME           =   "IntroductionView"

    private var delegate:ICapture?

    private var object = NSObject()

    public init(frame: CGRect,object: NSObject,delegate:ICapture) {
        self.delegate = delegate
        self.object = object
        super.init(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        renderUi()

    }

    private func renderUi() {
        guard let configObject = object.value(forKey: "config") as? NSObject else { return  }
        if let title = configObject.value(forKey: "title") as? String {
            titleLabel.text = title
            titleLabel.isHidden = false
        } else {
            titleLabel.isHidden = true
        }
        let text = configObject.value(forKey: "text") as? String
        textLabel.attributedText = text?.htmlToAttributedString

        titleLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        textLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        submitButton.tintColor = ThemeConfig.shared.getPrimaryMainColor()
        submitButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)
    }
    @IBAction private func didTapButton(sender:UIButton) {
        submitButton.isEnabled = false
        self.delegate?.redirectToNextPage(iRedirectNext: self)
    }
    public func onSuccess() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = false
        }
    }

    public func onFailure() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = true
        }
    }
    public func BackPressed() {
        self.delegate?.onCaptureIntermediate(object: ["event": "USER_BACKPRESS"] as NSObject)
    }
}

