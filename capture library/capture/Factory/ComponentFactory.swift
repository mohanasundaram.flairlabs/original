//
//  ComponentFactory.swift
//  capture
//
//  Created by Admin on 04/04/22.
//

import Foundation
import Cleanse
import core

public struct CompStructure {
    let compFactory: CompFactory
    public func getCompFactory() -> CompFactory {
        return compFactory
    }
}
public struct CompComponent : Cleanse.RootComponent {
    public typealias Root = CompStructure
    public typealias Seed = CompFeed

    public static func configure(binder: UnscopedBinder) {
        binder.install(dependency: DalComponent.self)
        binder.install(dependency: TaskComponent.self)

    }

    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dd:Seed) in
            Root.init(compFactory: CompFactory(dalCapture: dd.dalComponent.getDalCapture(), taskFactory: dd.taskComponent.getTaskFactory()))
        }
    }
}
public struct CompFeed {
    let dalComponent:DalComponent.Root
    let taskComponent:TaskComponent.Root

    public init(dalComponent:DalComponent.Root,taskComponent:TaskComponent.Root) {
        self.dalComponent = dalComponent
        self.taskComponent = taskComponent
    }
}
public class CompFactory {
    var dalCapture: DALCapture
    var taskFactory:TaskFactory

    public init(dalCapture:DALCapture,taskFactory:TaskFactory) {
        self.dalCapture = dalCapture
        self.taskFactory = taskFactory
    }
    public func getCardComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> CardView {
        let card = CardView(frame: view.frame, dalCapture: dalCapture, templateId: templateId,taskFactory: taskFactory, taskManager: taskManager)
        return card
    }
}
