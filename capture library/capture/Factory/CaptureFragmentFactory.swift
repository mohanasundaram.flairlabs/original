//
//  CaptureFactory.swift
//  capture
//
//  Created by Admin on 04/04/22.
//

import Foundation
import Cleanse
import core

public struct CapStructure {
    public let captureFragmentFactory: CaptureFragmentFactory
    public func getCaptureFactory() -> CaptureFragmentFactory {
        return captureFragmentFactory
    }
}
public struct CapComponent : Cleanse.RootComponent {
    public typealias Root = CapStructure
    public typealias Seed = CapFeed

    public static func configure(binder: UnscopedBinder) {
        binder.install(dependency: DalComponent.self)
    }

    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dd:Seed) in
            Root.init(captureFragmentFactory: CaptureFragmentFactory(dalCapture: dd.dalComponent.getDalCapture(), componentFactory: dd.compComponent.getCompFactory()))
        }
    }
}
public struct CapFeed {
    let dalComponent:DalComponent.Root
    let compComponent:CompComponent.Root

    public init(dalComponent:DalComponent.Root,compCompoent:CompComponent.Root,taskManager:TaskManager) {
        self.dalComponent = dalComponent
        self.compComponent = compCompoent
    }
}
public class CaptureFragmentFactory {
    var dalCapture:DALCapture
    var componentFactory:CompFactory
    
    public init(dalCapture:DALCapture,componentFactory:CompFactory) {
        self.dalCapture = dalCapture
        self.componentFactory = componentFactory
    }
    public func getCaptureFragment(view:UIView,UiObject:NSObject,delegate:ICapture,modelCaptureConfig:ModelCaptureConfig) -> CaptureView {
        let capture = CaptureView(frame: view.frame, dalCapture: dalCapture, componentFactory: componentFactory, modelCaptureConfig: modelCaptureConfig, taskManager: TaskManager(dalCapture: dalCapture), capturePayload: UiObject, callBack: delegate)
        return capture
    }

    public func getSkillSelectionView(view:UIView,skillObject:NSObject,delegate:ICapture) -> SkillSelectionView {
        let skillSelection = SkillSelectionView(view: view, dalCapture: dalCapture, skillObject: skillObject, delegate: delegate)
        return skillSelection
    }
}
