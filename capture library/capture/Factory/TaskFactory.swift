//
//  TaskFactory.swift
//  capture
//
//  Created by Admin on 04/04/22.
//

import Foundation
import Cleanse
import core


public struct TaskStructure {
    let taskFactory: TaskFactory
    public func getTaskFactory() -> TaskFactory {
        return taskFactory
    }
}
public struct TaskComponent : Cleanse.RootComponent {
    public typealias Root = TaskStructure
    public typealias Seed = TaskFeed

    public static func configure(binder: UnscopedBinder) {
        binder.install(dependency: DalComponent.self)

    }

    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dd:Seed) in
            Root.init(taskFactory: TaskFactory(dalCapture: dd.dalComponent.getDalCapture()))
        }
    }
}
public struct TaskFeed {
    let dalComponent:DalComponent.Root

    public init(dalComponent:DalComponent.Root) {
        self.dalComponent = dalComponent
    }
}
public class TaskFactory:ITaskFactory {

    var dalCapture: DALCapture

    public init(dalCapture:DALCapture) {
        self.dalCapture = dalCapture
    }

    public func getGroupComponent(view:UIView,templateId: Int,taskManager:TaskManager) -> GroupView {
        let group = GroupView(frame: view.frame)
        group.setConfig(dalCapture: dalCapture, templateId: templateId, iTaskFactory: self,taskManager: taskManager)
        return group
    }

    public func getGroupComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> GroupView {
        let group = GroupView(frame: view.frame)
        group.setConfig(dalCapture: dalCapture, templateId: templateId, iTaskFactory: self,formTaskManager:formTaskManager)
        return group
    }

    public func getTextComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> TextView {
        let textView = TextView(frame: view.frame)
        textView.setConfig(dalCapture: dalCapture, templateId: templateId, taskManager: taskManager)
        return textView
    }

    public func getTextComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> TextView {
        let textView = TextView(frame: view.frame)
        textView.setConfig(dalCapture: dalCapture, templateId: templateId, formTaskManager: formTaskManager)
        return textView
    }

    public func getFormComponent(view:UIView,templateId: Int,taskManager:TaskManager) -> FormView {
        let form = FormView(frame: view.frame)
        form.setConfig(dalCapture: dalCapture, iTaskFactory: self, templateId: templateId,taskManager: taskManager)
        return form
    }

//    public func getFormComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> FormView {
//        let form = FormView(frame: view.frame)
//        form.setConfig(dalCapture: dalCapture, iTaskFactory: self, templateId: templateId,formTaskManager: formTaskManager)
//        return form
//    }

    public func getImageComponent(view:UIView,templateId: Int,taskManager:TaskManager) -> Image {
        let image = Image(frame: view.frame)
        image.setConfig(dalCapture: dalCapture, templateId: templateId,taskManager: taskManager)
        return image
    }

    public func getImageComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> Image {
        let image = Image(frame: view.frame)
        image.setConfig(dalCapture: dalCapture, templateId: templateId,formTaskManager: formTaskManager)
        return image
    }

    public func getButtonComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> Button {
        let button = Button(frame: view.frame)
        button.setConfig(dalCapture: dalCapture, templateId: templateId,taskManager: taskManager)
        return button
    }

    public func getButtonComponent(view:UIView,templateId:Int,formTaskManager:FormTaskManager) -> Button {
        let button = Button(frame: view.frame)
        button.setConfig(dalCapture: dalCapture, templateId: templateId,formTaskManager: formTaskManager)
        return button
    }

    public func getSectionComponent(view:UIView,templateId: Int,taskManager:TaskManager) -> SectionView {
        let section = SectionView(frame: view.frame)
        section.setConfig(dalCapture: dalCapture, templateId: templateId,taskManager: taskManager)
        return section
    }

    public func getSectionComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> SectionView {
        let section = SectionView(frame: view.frame)
        section.setConfig(dalCapture: dalCapture, templateId: templateId,formTaskManager: formTaskManager)
        return section
    }

    public func getDocumentComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> Document {
        let document = Document(frame: view.frame)
        document.setConfig(dalCapture: dalCapture, templateId: templateId,taskManager: taskManager)
        return document
    }

    public func getDocumentComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> Document {
        let document = Document(frame: view.frame)
        document.setConfig(dalCapture: dalCapture, templateId: templateId,formTaskManager: formTaskManager)
        return document
    }

    public func getCheckBoxComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> CheckBox {
        let checkBox = CheckBox(frame: view.frame)
        checkBox.setConfig(dalCapture: dalCapture, templateId: templateId,taskManager: taskManager)
        return checkBox
    }

    public func getCheckBoxComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> CheckBox {
        let checkBox = CheckBox(frame: view.frame)
        checkBox.setConfig(dalCapture: dalCapture, templateId: templateId,formTaskManager: formTaskManager)
        return checkBox
    }

    public func getRadioComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> RadioBox {
        let radioBox = RadioBox(frame: view.frame)
        radioBox.setConfig(dalCapture: dalCapture, templateId: templateId,taskManager: taskManager)
        return radioBox
    }

    public func getRadioComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> RadioBox {
        let radioBox = RadioBox(frame: view.frame)
        radioBox.setConfig(dalCapture: dalCapture, templateId: templateId,formTaskManager: formTaskManager)
        return radioBox
    }

    public func getDropdownComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> DropDown {
        let dropDown = DropDown(frame: view.frame)
        dropDown.setConfig(dalCapture: dalCapture,templateId: templateId,taskManager: taskManager)
        return dropDown
    }

    public func getDropdownComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> DropDown {
        let dropDown = DropDown(frame: view.frame)
        dropDown.setConfig(dalCapture: dalCapture,templateId: templateId,formTaskManager: formTaskManager)
        return dropDown
    }

    public func getFilePickerComponent(view:UIView,templateId:Int,taskManager:TaskManager) -> FilePicker {
        let filePicker = FilePicker(frame: view.frame)
        filePicker.setConfig(dalCapture: dalCapture, templateId: templateId, taskManager: taskManager)
        return filePicker
    }

    public func getFilePickerComponent(view: UIView, templateId: Int, formTaskManager: FormTaskManager) -> FilePicker {
        let filePicker = FilePicker(frame: view.frame)
        filePicker.setConfig(dalCapture: dalCapture, templateId: templateId, formTaskManager: formTaskManager)
        return filePicker
    }
}
