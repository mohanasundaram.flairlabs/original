//
//  OCR.swift
//  capture
//
//  Created by FL Mohan on 28/04/22.
//

import Foundation
import core

public class OCR: Plugin, ITaskUpdate {

    private var task_key = ""
    private var dalCapture = DALCapture()
    private var templateId = 0

    public func initialise(dalCapture: DALCapture, pluginObject: NSObject, templateId: Int) {
        self.dalCapture = dalCapture
        self.templateId = templateId
        let paramsObject = pluginObject.value(forKey: "params") as! [String:Any]
        for k in paramsObject {
            let keyObject = paramsObject[k.key] as! NSObject
            self.task_key = keyObject.value(forKey: "task_key") as! String
        }
        _ = dalCapture.subscribeToTaskKey(key: task_key, iTaskUpdate: self)
    }
    public func onUpdate(taskObject: NSObject) {
        let resultObject = taskObject.value(forKey: "result") as! NSObject
        guard let automatedObject = resultObject.value(forKey: "automated_response") as? NSObject else { return }
        if automatedObject.isEqual(NSNull()) { return }
        let status = taskObject.value(forKey: "status") as! String
        dalCapture.setResetPlugin(templateId: templateId, isSet: status == "completed" ? true : false)
    }
    public func destroy() {
        
    }


}
