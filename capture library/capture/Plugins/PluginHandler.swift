//
//  PluginHandler.swift
//  capture
//
//  Created by FL Mohan on 28/04/22.
//

import Foundation
import core


public class PluginHandler {
    private var pluginList:[Plugin] = []
    private var dalCapture = DALCapture()
    private var pluginObject = NSObject()
    private var templateId = 0

    public init(dalCapture:DALCapture,pluginObject:NSObject,templateId:Int) {
        self.dalCapture = dalCapture
        self.pluginObject = pluginObject
        self.templateId = templateId
        initialise()
    }
    private func initialise() {
        let script = pluginObject.value(forKey: "script_src") as! String
        if script == "ocr.js" {
            OCR().initialise(dalCapture: dalCapture, pluginObject: pluginObject, templateId: templateId)
        }
    }
    public func onDestroy() {
        for pluginList in pluginList {
            pluginList.destroy()
        }
        pluginList.removeAll()
        pluginList = []
    }
}
