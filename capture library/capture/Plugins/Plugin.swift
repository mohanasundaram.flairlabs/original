//
//  Plugin.swift
//  capture
//
//  Created by FL Mohan on 28/04/22.
//

import Foundation
import core

public protocol Plugin {
    func initialise(dalCapture:DALCapture,pluginObject:NSObject,templateId:Int)
    func destroy()
}

