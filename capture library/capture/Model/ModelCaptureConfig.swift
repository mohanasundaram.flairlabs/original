//
//  ModelCaptureConfig.swift
//  capture
//
//  Created by Admin on 25/01/22.
//

import Foundation
import core
import avkyc
import svc

public class ModelCaptureConfig {

    private var dalConfig = DalConfig()
    private var modelSvcConfig = ModelSVCConfig()
    private var modelAVKYCConfig = ModelAVKYCConfig()

    public init() {}
    private init(captureBuilder: CaptureBuilder) {
        self.dalConfig = captureBuilder.dalConfig
        self.modelSvcConfig = captureBuilder.modelSvcConfig
        self.modelAVKYCConfig = captureBuilder.modelAVKYCConfig
    }

    public func getDalConfig() -> DalConfig {
        return dalConfig
    }
    public func getModelAVKYCConfig() -> ModelAVKYCConfig {
        return modelAVKYCConfig
    }
    public func getModelSvcConfig() -> ModelSVCConfig {
        return modelSvcConfig
    }
    public class CaptureBuilder {
        // common parameters
        private(set) var logger_url:String = ""
        private(set) var enableLogging:Bool = false

        // Dal parameters
        private(set) var token:String = ""
        private(set) var serverUrl:String = ""
        private(set) var captureSocketUrl:String = ""
        private(set) var isStorageService:Bool = false
        private(set) var storageAuthService:String = ""
        private(set) var storageAuthType:String = ""
        private(set) var isAuthParam:Bool = false


        private(set) var networkCheckBaseUrl:String = ""

        // Avkyc parameters
        private(set) var avkycBackendBaseUrl:String = ""
        private(set) var avkycSocketUrl:String = ""

        // SVC Parameters
        private(set) var svcServiceBaseUrl:String = ""
        private(set) var svcSseBaseUrl:String = ""

        // Video component parameters
        private(set) var mediaServerUrl:String = ""
        private(set) var frameWidth:Int = 0
        private(set) var frameHeight:Int = 0
        private(set) var framePerSec:Int = 0


        private(set) var dalConfig = DalConfig()
        private(set) var modelSvcConfig = ModelSVCConfig()
        private(set) var modelAVKYCConfig = ModelAVKYCConfig()

        public init() {}

        public func setToken(token: String) -> CaptureBuilder {
            self.token = token
            return self
        }
        public func setLoggerEndpoint(logger_url: String) -> CaptureBuilder {
            self.logger_url = logger_url
            return self
        }
        public func enableLogging(enableLogging: Bool) -> CaptureBuilder {
            self.enableLogging = enableLogging
            return self
        }
        public func setCaptureSocketUrl(captureSocketUrl: String) -> CaptureBuilder {
            self.captureSocketUrl = captureSocketUrl
            return self
        }
        public func setStorageService(isStorageService: Bool) -> CaptureBuilder {
            self.isStorageService = isStorageService
            return self
        }
        public func setIsAuthParam(isAuthParam: Bool) -> CaptureBuilder {
            self.isAuthParam = isAuthParam
            return self
        }
        public func setStorageAuthService(storageAuthService: String) -> CaptureBuilder {
            self.storageAuthService = storageAuthService
            return self
        }
        public func setStorageAuthType(storageAuthType: String) -> CaptureBuilder {
            self.storageAuthType = storageAuthType
            return self
        }
        // Avkyc config
        public func setCaptureServerUrl(serverUrl: String) -> CaptureBuilder {
            self.serverUrl = serverUrl
            return self
        }
        public func setNetworkCheckBaseUrl(networkCheckBaseUrl: String) -> CaptureBuilder {
            self.networkCheckBaseUrl = networkCheckBaseUrl
            return self
        }
        public func setAvkycBackendBaseUrl(avkycBackendBaseUrl: String) -> CaptureBuilder {
            self.avkycBackendBaseUrl = avkycBackendBaseUrl
            return self
        }

        // vc config
        public func setAvkycSocketUrl(avkycSocketUrl: String) -> CaptureBuilder {
            self.avkycSocketUrl = avkycSocketUrl
            return self
        }
        public func setSvcServiceBaseUrl(svcServiceBaseUrl: String) -> CaptureBuilder {
            self.svcServiceBaseUrl = svcServiceBaseUrl
            return self
        }
        public func setSvcSseBaseUrl(svcSseBaseUrl: String) -> CaptureBuilder {
            self.svcSseBaseUrl = svcSseBaseUrl
            return self
        }
        public func setMediaServerUrl(mediaServerUrl: String) -> CaptureBuilder {
            self.mediaServerUrl = mediaServerUrl
            return self
        }
        public func setFrameWidth(frameWidth: Int) -> CaptureBuilder {
            self.frameWidth = frameWidth
            return self
        }
        public func setFrameHeight(frameHeight: Int) -> CaptureBuilder {
            self.frameHeight = frameHeight
            return self
        }
        public func setFramePerSec(framePerSec: Int) -> CaptureBuilder {
            self.framePerSec = framePerSec
            return self
        }

        private func setDalConfig() -> CaptureBuilder {
            self.dalConfig = DalConfig.DalConfigBuilder()
                .setToken(token: token)
                .setLoggerUrl(loggerUrl: logger_url)
                .enableLogging(enableLogging: enableLogging)
                .enableStorageService(enableStorageService: isStorageService)
                .isAuthParam(isAuthParam: isAuthParam)
                .setStorageAuthService(storageAuthService: storageAuthService)
                .setStorageAuthType(storageAuthType: storageAuthType)
                .setCaptureServerUrl(serverUrl: serverUrl)
                .setCaptureSocketUrl(captureSocketUrl: captureSocketUrl)
                .build()
            return self
        }
        private func setAVKYCConfig() -> CaptureBuilder {

            self.modelAVKYCConfig = ModelAVKYCConfig.AvkycBuilder()
                .loggerUrl(avkycLoggerUrl: logger_url)
                .enableLogging(enableLogging: enableLogging)
                .avkycNetworkCheckBaseUrl(avkycNetworkCheckBaseUrl: networkCheckBaseUrl)
                .avkycBackendBaseUrl(avkycBackendBaseUrl: avkycBackendBaseUrl)
                .avkycSocketUrl(avkycSocketUrl: avkycSocketUrl)
                .modelVcConfig(mediaServerUrl: mediaServerUrl, loggerUrl: logger_url, frameWidth: frameWidth, frameHeight: frameHeight, framePerSec: framePerSec)
                .build()
            return self
        }
        private func setSvcConfig() -> CaptureBuilder {
            self.modelSvcConfig = ModelSVCConfig.SvcBuilder()
                .setLoggerUrl(loggerUrl: logger_url)
                .enableLogging(enableLogging: enableLogging)
                .setNetworkCheckBaseUrl(avkycNetworkCheckBaseUrl: networkCheckBaseUrl)
                .setSvcServiceBaseUrl(svcServiceBaseUrl: svcServiceBaseUrl)
                .setSvcSseBaseUrl(svcSseBaseUrl: svcSseBaseUrl)
                .modelVcConfig(mediaServerUrl: mediaServerUrl, loggerUrl: logger_url, frameWidth: frameWidth, frameHeight: frameHeight, framePerSec: framePerSec)
                .build()
            return self
        }
        public func build() -> ModelCaptureConfig {
            _ = setDalConfig()
            _ = setAVKYCConfig()
            _ = setSvcConfig()
            let modelCaptureConfig = ModelCaptureConfig(captureBuilder: self)
            return modelCaptureConfig

        }
    }
}
