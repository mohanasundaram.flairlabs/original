//
//  CaptureConfig.swift
//  captureapp
//
//  Created by Admin on 13/04/22.
//

import Foundation
import core
import avkyc
import svc

public class CaptureConfig {
    // default values
    private var token: String = ""
    private var baseUrl: String = ""
    private var loggerUrl: String = ""
    private var enableLogging: Bool = false
    private var captureServerUrl: String = ""
    private var captureSocketUrl: String = ""

    private var networkCheckBaseUrl: String = ""

    // Avkyc parameters
    private var avkycBackendBaseUrl:String = ""
    private var avkycSocketUrl:String = ""

    private var svcServiceBaseUrl:String = ""
    private var svcSseBaseUrl:String = ""

    private var mediaServerUrl:String = ""
    private var frameWidth:Int = 0
    private var frameHeight:Int = 0
    private var framePerSec:Int = 0

    func getToken() -> String {
        return token
    }
    func getBaseUrl() -> String {
        return baseUrl
    }
    func getLoggerUrl() -> String {
        return loggerUrl
    }
    func getEnableLogging() -> Bool {
        return enableLogging
    }
    func getCaptureServerUrl() -> String {
        return captureServerUrl
    }
    func getCaptureSocketUrl() -> String {
        return captureSocketUrl
    }
    func getNetworkCheckBaseUrl() -> String {
        return networkCheckBaseUrl
    }
    func getavkycBackendBaseUrl() -> String {
        return avkycBackendBaseUrl
    }
    func getavkycSocketUrl() -> String {
        return avkycSocketUrl
    }
    func getSvcServiceBaseUrl() -> String {
        return svcServiceBaseUrl
    }
    func getSvcSseBaseUrl() -> String {
        return svcSseBaseUrl
    }
    func getmediaServerUrl() -> String {
        return mediaServerUrl
    }
    func getFrameWidth() -> Int {
        return frameWidth
    }
    func getFrameHeight() -> Int {
        return frameHeight
    }
    func getFramePerSec() -> Int {
        return framePerSec
    }
    public init() {}
    private init(captureBuilder: CaptureBuilder) {
        // default values
        self.token = captureBuilder.token
        self.baseUrl = captureBuilder.base_url
        self.loggerUrl = captureBuilder.logger_url
        self.enableLogging = captureBuilder.enableLogging

        self.captureServerUrl = captureBuilder.captureServerUrl
        self.captureSocketUrl = captureBuilder.captureSocketUrl

        self.networkCheckBaseUrl = captureBuilder.networkCheckBaseUrl

        // avkyc parameters
        self.avkycBackendBaseUrl = captureBuilder.avkycBackendBaseUrl
        self.avkycSocketUrl = captureBuilder.avkycSocketUrl

        // svc
        self.svcServiceBaseUrl = captureBuilder.svcServiceBaseUrl
        self.svcSseBaseUrl = captureBuilder.svcSseBaseUrl

        // VC
        self.mediaServerUrl = captureBuilder.mediaServerUrl
        self.frameWidth = captureBuilder.frameWidth
        self.frameHeight = captureBuilder.frameHeight
        self.framePerSec = captureBuilder.framePerSec

    }
    public class CaptureBuilder {
        private(set) var token:String = ""
        private(set) var base_url:String = ""
        private(set) var logger_url:String = ""
        private(set) var enableLogging:Bool = false

        private(set) var captureServerUrl:String = ""
        private(set) var captureSocketUrl:String = ""

        private(set) var networkCheckBaseUrl:String = ""

        private(set) var avkycBackendBaseUrl:String = ""
        private(set) var avkycSocketUrl:String = ""

        private(set) var svcServiceBaseUrl:String = ""
        private(set) var svcSseBaseUrl:String = ""

        private(set) var mediaServerUrl:String = ""
        private(set) var frameWidth:Int = 0
        private(set) var frameHeight:Int = 0
        private(set) var framePerSec:Int = 0

        public init(base_url: String) {
            self.base_url = base_url
        }
        public func setToken(token: String) -> CaptureBuilder {
            self.token = token
            return self
        }
        public func setLoggerEndpoint(logger_url: String) -> CaptureBuilder {
            self.logger_url = logger_url
            return self
        }
        public func enableLogging(enableLogging: Bool) -> CaptureBuilder {
            self.enableLogging = enableLogging
            return self
        }
        public func setCaptureServerUrl(captureServerUrl: String) -> CaptureBuilder {
            self.captureServerUrl = captureServerUrl
            return self
        }
        public func setCaptureSocketUrl(captureSocketUrl: String) -> CaptureBuilder {
            self.captureSocketUrl = captureSocketUrl
            return self
        }
        public func setNetworkCheckBaseUrl(networkCheckBaseUrl: String) -> CaptureBuilder {
            self.networkCheckBaseUrl = networkCheckBaseUrl
            return self
        }
        public func setAvkycBackendBaseUrl(avkycBackendBaseUrl: String) -> CaptureBuilder {
            self.avkycBackendBaseUrl = avkycBackendBaseUrl
            return self
        }
        public func setAvkycSocketUrl(avkycSocketUrl: String) -> CaptureBuilder {
            self.avkycSocketUrl = avkycSocketUrl
            return self
        }
        public func setSvcServiceBaseUrl(svcServiceBaseUrl: String) -> CaptureBuilder {
            self.svcServiceBaseUrl = svcServiceBaseUrl
            return self
        }
        public func setSvcSseBaseUrl(svcSseBaseUrl: String) -> CaptureBuilder {
            self.svcSseBaseUrl = svcSseBaseUrl
            return self
        }
        public func setMediaServerUrl(mediaServerUrl: String) -> CaptureBuilder {
            self.mediaServerUrl = mediaServerUrl
            return self
        }
        public func setFrameWidth(frameWidth: Int) -> CaptureBuilder {
            self.frameWidth = frameWidth
            return self
        }
        public func setFrameHeight(frameHeight: Int) -> CaptureBuilder {
            self.frameHeight = frameHeight
            return self
        }
        public func setFramePerSec(framePerSec: Int) -> CaptureBuilder {
            self.framePerSec = framePerSec
            return self
        }
        public func build() -> CaptureConfig {
            let captureConfig = CaptureConfig(captureBuilder: self)
            return captureConfig
        }
    }
}

public enum CaptureConstant:String {
case TASK_DATA_CAPTURE = "data_capture.data_capture"
case TASK_EXTRACT_POI = "extract.poi"
case TASK_EXTRACT_POA = "extract.poa"
case TASK_VERIFY = "verify.qa"
}

