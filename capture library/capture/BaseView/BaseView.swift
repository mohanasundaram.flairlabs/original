//
//  BaseView.swift
//  app
//
//  Created by Admin on 16/11/21.
//

import Foundation
import AVFoundation
import UIKit
import core
import avkyc
import svc
import Cleanse

public protocol HostAppCallBack {
    func onCaptureSuccess(object: NSObject)
    func onCaptureFailure(object: NSObject)
}
//public protocol ComponentCallBack {
//    func didPressButton(_ tag: UIButton)
//}

public class BaseView: UIView, InstructionActionDelegate, AVKYCCallBack, SVCCallBack,ICapture, IPageSequenceCallback, IPageDataCallback, IStatusCallback {

    @IBOutlet weak var contentCaptureView: UIView!
    @IBOutlet weak var headerBackButton: UIButton!
    @IBOutlet weak var headerTextLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var footerImageView: UIImageView!
    @IBOutlet weak var footerTextLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    let kCONTENT_XIB_NAME           =   "BaseView"

    private var dalCapture = DALCapture()

    private var introductionView:IntroductionView? = nil

    private var prerequestView:CapturePreRequisiteView? = nil

    private var permissionView:PermissionView? = nil
    
    private var infoView = InstructionView()

    private var captureView:CaptureView? = nil

    private var avkycView: AvkycView? = nil

    private var svcView: SvcView? = nil

    private var skillSelectionView:SkillSelectionView? = nil
    
    private var loggerStartTime: Double = 0
    
    private var headerTitle: String = ""

    private var loggerWrapper = LoggerWrapper()

    private var loggerCapture:Logger?

    private var iRedirectNext:IRedirectNext?
    
    private var delegate: HostAppCallBack?

    private var modelCaptureConfig = ModelCaptureConfig()

    private var captureFragmentFactory:CaptureFragmentFactory?

    public init(frame: CGRect,dalCapture:DALCapture,modelCaptureConfig:ModelCaptureConfig,captureFragmentFactory:CaptureFragmentFactory, loggerWrapper:LoggerWrapper,delegate:HostAppCallBack) {
        self.dalCapture = dalCapture
        self.modelCaptureConfig = modelCaptureConfig
        self.captureFragmentFactory = captureFragmentFactory
        self.loggerWrapper = loggerWrapper
        self.delegate = delegate
        super.init(frame: getFrameSize())
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentCaptureView.fixInView(self)

        renderUi()
    }
    private func renderUi() {
        ProgressHUD.show()

        // Create instance of logger
        loggerCapture = loggerWrapper.createLogger(key: EnumLogger.CAPTURE.rawValue, token: dalCapture.getToken() , loggerUrl: modelCaptureConfig.getDalConfig().getLoggerUrl())

        loggerCapture?.log(logLevel: LogLevel.shared.Info,logDetails: ModalLogDetails(service_category:"captureSDK", service: "LogUserAgent",event_type: "Profile.UA", event_name: "iOS app " + ApplicationController.deviceModel, component: "loggerWrapper", event_source: "initLogger"), meta: [:])

        loggerCapture?.log(logLevel: LogLevel.shared.Info,logDetails: ModalLogDetails(service_category:"captureSDK", service: "TokenAuth",event_type: "Authenticate", event_name: "Initialise", component: "DataService Core", event_source: "initialiseCaptureView"), meta: [:])
        loggerStartTime = Double(Date.currentTimeStamp)

        getCaptureStatusDetails(sessionOverride: false)
    }
    func getCaptureStatusDetails(sessionOverride: Bool) {
        dalCapture.getCaptureStatusDetails(sessionOverride: sessionOverride, callback: self)
    }
    public func onStatusSuccess(object:NSObject) {
        DispatchQueue.main.async {
        self.renderCaptureUi(object: object)
        }
    }
    public func onStatusFailure(message:String) {
        ProgressHUD.dismiss()
        if message == "You don't have sufficient permissions to perform this action." {
            loggerCapture?.log(logLevel: LogLevel.shared.Info,logDetails: ModalLogDetails(service_category:"captureSDK", service: "TokenAuth",event_type: "", event_name: "MULTIPLE_SESSIONS", component: "DataService Core", event_source: "getCaptureStatusDetails"), meta: [:])
            DispatchQueue.main.async {
                self.window?.rootViewController?.showAlert(title: "Multiple Sessions Available", message: "You have more than one active sessions. Do you wish to close all other active sessions?", actionTitle1: "Yes", actionTitle2: "No") { success in
                    if success == true {
                        self.getCaptureStatusDetails(sessionOverride: true)
                    }
                }
            }
        } else if message == "error" {
            self.window?.rootViewController?.showAlert(title: "Unable to connect server", message: "Kindly check your internet connection.", actionTitle: "Close", completion: { _ in
                self.onDestroy()
                ProgressHUD.dismiss()
                self.delegate?.onCaptureFailure(object: [:] as NSObject)
            })
        } else {
            DispatchQueue.main.async {

                self.window?.rootViewController?.showAlert(title: "Invalid Token", message: "", actionTitle: "Close", completion: { _ in
                    self.onDestroy()
                    ProgressHUD.dismiss()
                    self.delegate?.onCaptureFailure(object: [:] as NSObject)
                })
            }
        }
    }
    private func renderCaptureUi(object: NSObject) {

        //            if let value = success?.value(forKey: "body") as? NSObject {
        if  dalCapture.getThemeConfig().getDisplayFooter() {
            self.footerView.isHidden = false
            self.footerView.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
        }
        if let headerObject = object.value(forKey: "header") as? NSObject {
            headerTitle = headerObject.value(forKey: "title") as? String ?? ""
            headerTextLabel.text =  headerTitle
        }
        // set defaults
        var logDefaults = ModalLogDetails()
        logDefaults.reference_id = dalCapture.getCaptureId()
        logDefaults.reference_type = "CaptureID"
        loggerWrapper.setDefault(key: "capture", defaultLog: logDefaults)

        // set default meta
        var metaDefaults: [String: Any] = [:]
        metaDefaults.updateValue(dalCapture.getCaptureId() , forKey: "capture_id")
        metaDefaults.updateValue(dalCapture.getCallCompletedStatus() , forKey: "profile_status")
        metaDefaults.updateValue(dalCapture.getCaptureSessionId() , forKey: "session_id")
        loggerWrapper.setMetaDefaults(key: "capture", metaDefaults: metaDefaults)

        // set default and meta healthcheck
        loggerWrapper.getLoggers(key: "healthCheck")?.setDefaults(defaultLogDetails: logDefaults)
        loggerWrapper.getLoggers(key: "healthCheck")?.setMetaDefaults(metaDefaults: metaDefaults)

        // set default and meta vs
        loggerWrapper.getLoggers(key: "vs")?.setDefaults(defaultLogDetails: logDefaults)
        loggerWrapper.getLoggers(key: "vs")?.setMetaDefaults(metaDefaults: metaDefaults)

        // send logs: after apiCaptureStatusDetails load time
        loggerCapture?.log(logLevel: LogLevel.shared.Info,logDetails: ModalLogDetails(service_category:"captureSDK", service: "TokenAuth",event_type: "Auth.TAT", event_name: dalCapture.getTatSince(start: loggerStartTime) , component: "DataService Core", event_source: "renderToolBar"), meta: [:])

        headerTextLabel.textColor = dalCapture.getThemeConfig().getHeaderTextColor()
        headerView.backgroundColor = dalCapture.getThemeConfig().getHeaderBackgroundColor()
        footerTextLabel.textColor = dalCapture.getThemeConfig().getHeaderTextColor()
        footerView.backgroundColor = dalCapture.getThemeConfig().getHeaderBackgroundColor()

        headerView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 50)

        headerImageView.isHidden = false
        headerTextLabel.isHidden = false
        headerImageView.image = ThemeConfig.shared.getLogo()
        headerBackButton.tintColor = dalCapture.getThemeConfig().getPrimaryMainColor()


        footerImageView.image = ThemeConfig.shared.getLogo()

        contentCaptureView.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()

        infoView.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        infoView.delegate = self

        // send logs: after apiCaptureStatusDetails Authenticated
        loggerCapture?.log(logLevel: LogLevel.shared.Info,logDetails: ModalLogDetails(service_category:"captureSDK", service: "TokenAuth",event_type: "Authenticated", event_name: "", component: "DataService Core", event_source: "renderToolBar"), meta: [:])
        if let value = object.value(forKey: "body") as? NSObject {
            if let outOfOffice = value.value(forKey: "out_of_office") as? NSObject {
                if outOfOffice.value(forKey: "active") as! Bool == true {
                    let title = outOfOffice.value(forKey: "title") as? String
                    let message = outOfOffice.value(forKey: "text") as? String
                    self.infoView.loadKycStatusView(title: title ?? "", message: message ?? "")
                    self.contentView.addSubview(self.infoView)
                    ProgressHUD.dismiss()
                } else {
                    processCapture()
                }
            } else {
                processCapture()
            }
        }
    }
    private func processCapture() {
        let status_string = Status(rawValue: dalCapture.getStatus() )

        switch status_string {
        case .Status_Approved, .Status_Processed,.Status_Initiated, .Status_In_Progress, .Status_Review, .Status_Review_Required, .Status_Cancelled, .Status_Capture_Expired, .Status_Failed, .Status_Rejected:
            renderStatusUi(status:status_string?.rawValue ?? "")
            break
        case .Status_Capture_Pending, .Status_Recapture_Pending:
            getPageData()
            break
        default:
            renderStatusUi(status:status_string?.rawValue ?? "")
            break
        }
    }
    private func renderStatusUi(status:String) {
        DispatchQueue.main.async {
            ProgressHUD.dismiss()
            self.headerTextLabel.text = ""
            self.headerImageView.isHidden = true
            self.infoView.loadKycStatusView(title: getStatus(status: status).title, message: getStatus(status: status).message)
            self.contentView.addSubview(self.infoView)
        }
    }
    private func getPageData() {
        // send logs: after apiCaptureStatusDetails Authenticated
        var meta: [String: Any] = [:]
        var param: [String: Any] = [:]
        param.updateValue(dalCapture.getToken() , forKey: "t" )
        param.updateValue(dalCapture.getCaptureId() , forKey: "capture_id")
        param.updateValue(dalCapture.getCaptureSessionId() , forKey: "session_token")
        meta.updateValue(param, forKey: "param")

        loggerCapture?.log(logLevel: LogLevel.shared.Info,logDetails: ModalLogDetails(service_category:"captureSDK", service: "ConnectProfileGateway",event_type: "SocketState", event_name: "Connect", component: "DataService Core", event_source: "getPageData",reference_id: dalCapture.getCaptureId(),reference_type: "AV.TaskID"), meta: meta)

        dalCapture.getPageSequence(iPSCallback: self)

        loggerCapture?.log(logLevel: LogLevel.shared.Info,logDetails: ModalLogDetails(service_category:"captureSDK", service: "ConnectProfileGateway",event_type: "Initiate", event_name: "Invoked", component: "DataService Core", event_source: "getPageData",reference_id: dalCapture.getCaptureId(),reference_type: "AV.TaskID"), meta: [:])

    }
    public func onPageSequenceSuccess(object: NSObject?) {
        if object != nil {
            loggerCapture?.log(logLevel: LogLevel.shared.Info,logDetails: ModalLogDetails(service_category:"captureSDK", service: "ConnectProfileGateway",event_type: "ChannelJoined", event_name: "Session", component: "DataService Core", event_source: "getPageData",reference_id: dalCapture.getCaptureId(),reference_type: "AV.TaskID"), meta: [:])
            fetchPageConfig(page: dalCapture.getFirstPage().getPage() )
        }
    }
    public func onPageSequenceIntermediate() {
        self.loadNetworkDisconnectedView()
    }
    public func onPageSequenceFailure(message: String?) {
        if message == dalCapture.SOCKET_DISCONNECT {
            self.loadNetworkDisconnectedView()
        }
    }
    private func loadNetworkDisconnectedView() {

        dalCapture.disconnectDalSocket()
        DispatchQueue.main.async { [self] in
            contentView.removeSubviews()
            ProgressHUD.dismiss()
            if let viewWithTag = self.window?.rootViewController?.view.viewWithTag(100) {
                viewWithTag.removeFromSuperview()
            }
            if !infoView.isDescendant(of: contentView) {
            infoView.loadNetworkDisconnectedStatusView()
            contentView.addSubview(infoView)
            }
        }
    }
    func fetchPageConfig(page: String) {

        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "fetch page config", timestamp: "", event_type: "send", event_name: page, component: "DataService Core", event_source: "fetchPageConfig", logger_session_id: ""), meta: [:])

        dalCapture.fetchPageData(page: page, iPageDataCallback: self)
    }
    public func onPageDataSuccess(page: String, object: NSObject) {
        if let iRedirectNext = iRedirectNext {
            iRedirectNext.onSuccess()
        }
        DispatchQueue.main.async { [self] in
            ProgressHUD.dismiss()
            headerTextLabel.text = headerTitle
            switch page {
            case "introduction", "consent":
                introductionView = IntroductionView(frame: contentView.frame, object: object,delegate: self)
                loadView(view: introductionView ?? UIView())
                break
            case "capture_prerequisite","av_prerequisite","sv_prerequisite":
                prerequestView = CapturePreRequisiteView(frame: contentView.frame, object: object, dalCapture: dalCapture,delegate: self)
                loadView(view: prerequestView ?? UIView())
                break
            case "capture":
                captureView = captureFragmentFactory?.getCaptureFragment(view: self.contentView, UiObject: object, delegate: self, modelCaptureConfig: modelCaptureConfig)
                loadView(view:captureView ?? UIView())
                break
            case "permissions":
                headerTextLabel.text = "Ask Permissions"
                permissionView = PermissionView(object: object, view: contentView, delegate: self)
                loadView(view: permissionView ?? UIView())
                break
            case "vkyc.assisted_vkyc":
                avkycView = AssistedVideoKycFactory().startAvkyc(modelAVKYCConfig:modelCaptureConfig.getModelAVKYCConfig())
                avkycView?.initialiseAvkyc(view: self.contentView, data: object, delegate: self)
                loadView(view: avkycView ?? UIView())
                break
            case "self_video", "self_video.capture_video_activity":
                guard let configObject = object.value(forKey: "config") as? NSObject else { return }
                let tasks = configObject.value(forKey: "task") as? NSObject
                let status = tasks?.value(forKey: "status") as? String
                if status == "processing" {
                    self.window?.rootViewController?.showAlert(title: "Your details have already been submitted", message: "", actionTitle: "OKAY", completion: { success in
                        self.showThankYouView()
                    })
                } else {
                    svcView = SelfVideoKycFactory().startSvc(svcConfig: modelCaptureConfig.getModelSvcConfig())
                    svcView?.initialiseSvc(view: self.contentView,configObject:configObject, delegate: self)
                    loadView(view: svcView ?? UIView())
                }
                break
            case "av_skill_select":
                skillSelectionView = SkillSelectionView(view: self.contentView, dalCapture: dalCapture, skillObject: object, delegate: self)
                loadView(view: skillSelectionView ?? UIView())
                break
            default:
                break
            }
        }
    }
    public func onPageDataFailure(message: String) {
        iRedirectNext?.onFailure()
    }
    private func loadView(view:UIView) {
        contentView.removeSubviews()
        contentView.addSubview(view)
    }
}

// CALLBACK METHODS
extension BaseView {

    @IBAction func didTapButton(sender:UIButton) {
        self.buttonPressed(sender: sender)
    }
    //TODO
    @objc func buttonPressed(sender: UIButton!) {
        DispatchQueue.main.async { [self] in
            let pageSequence = dalCapture.onNextPage()
            if pageSequence.getPage() != "PAGE_END" {
                fetchPageConfig(page: pageSequence.getPage() )
            } else {
                self.showThankYouView()
            }
        }
    }
    public func didPressButton(_ tag: UIButton) {
        buttonPressed(sender: tag)
    }
    public func didSetRetryButton(screen: RetryOptions) {
        DispatchQueue.main.async { [self] in
            if !Connectivity.isConnectedToInternet() { self.window?.rootViewController?.showAlertonNoInternet(); return }
            if screen == .SessionOverride {
                self.window?.rootViewController?.showAlert(title: "Multiple Sessions Available", message: "You have more than one active sessions. Do you wish to close all other active sessions?", actionTitle1: "Yes", actionTitle2: "No") { [self] success in
                    if success == true {
                        contentView.removeSubviews()
                        updateCaptureView()
                    }
                }
            } else {
                contentView.removeSubviews()
                updateCaptureView()
            }
        }
    }
    @IBAction private func btnBackPressed(_ sender: UIButton) {
        BackPressed()
    }
    func BackPressed() {
        if captureView?.isDescendant(of: contentView) == true {
            captureView?.BackPressed()
        } else if svcView?.isDescendant(of: contentView) == true {
            svcView?.BackPressed()
        } else if avkycView?.isDescendant(of: contentView) == true {
            avkycView?.BackPressed()
        } else if skillSelectionView?.isDescendant(of: contentView) == true {
            skillSelectionView?.BackPressed()
        } else if introductionView?.isDescendant(of: contentView) == true {
            introductionView?.BackPressed()
        } else if prerequestView?.isDescendant(of: contentView) == true {
            prerequestView?.BackPressed()
        } else if permissionView?.isDescendant(of: contentView) == true {
            permissionView?.BackPressed()
        } else {
            onDestroy()
            ProgressHUD.dismiss()
            self.delegate?.onCaptureSuccess(object: ["event": "BackPressed"] as NSObject)
        }
    }
    public func onDestroy() {
        dalCapture.onDestroy()
        dalCapture.disconnectDalSocket()
        dalCapture = DALCapture()
        captureFragmentFactory  = nil
        CaptureComponentFactory().captureComponentDestroy()
        DalComponentFactory.destroyInstance()
        loggerWrapper.clearLoggers()
    }
    func updateHeader() {
        headerImageView.isHidden = false
        headerTextLabel.text = "Capture Details"
    }
    public func onAssistedSuccess(object: NSObject) {
        DispatchQueue.main.async { [self] in
            contentView.removeSubviews()
            let value = object.value(forKey: "event") as! String
//            let status = object.value(forKey: "status") as? String ?? ""
            switch value {
            case "CALL_END", "USER_END_CALL","AGENT_ENDED_CALL":
//                if status == "verified" {
                    showThankYouView()
//                } else if status == "failed" {
//
//                } else if status == "rejected" {
//
//                }
                break
            default:
                break
            }
        }
    }
    public func onAssistedIntermediate(object: NSObject) {
        let value = object.value(forKey: "event") as! String
        DispatchQueue.main.async { [self] in
            switch value {
            case "VKYC_INITIATED":
                headerImageView.isHidden = false
                headerTextLabel.text = "Initiate Verification"
                break
            case "VIDEOCOMPONENT_INITIATED":
                break
            case "USER_END_CALL":
                break
            case "USER_RECONNECT":
                break
            case "DISCONNECT":
                break
            case "USER_BACKPRESS":
                loadPreviousPage()
                break
            case "update_header":
                if !contentView.isDescendant(of: contentView) {
                    headerBackButton.isHidden.toggle()
                    headerImageView.isHidden.toggle()
                }
                break
            case "LOCATION_MISMATCH":
                showOutOfLocationView()
                break
            default:
                break
            }
        }
    }
    public func onAssistedFailure(object: NSObject) {
        DispatchQueue.main.async { [self] in
            contentView.removeSubviews()
            let value = object.value(forKey: "event") as! String
            switch value {
            case "RECONNECTS_EXHAUSTED","NETWORK_DISCONNECTED","OPERATOR_EXIT","SOCKET_TIMEOUT":
                loadNetworkDisconnectedView()
                break
            case "USER_EXIT":
                loadPreviousPage()
                break
            default:
                break
            }
        }
    }
    public func onSvcSuccess(object: NSObject) {
        DispatchQueue.main.async { [self] in
            contentView.removeSubviews()
            let value = object.value(forKey: "event") as! String
            switch value {
            case "CALL_END", "USER_END_CALL", "AGENT_ENDED_CALL":
                showThankYouView()
                break
            default:
                break
            }
        }
    }
    public func onSvcIntermediate(object: NSObject) {
        let value = object.value(forKey: "event") as! String
        DispatchQueue.main.async { [self] in
            switch value {
            case "VKYC_INITIATED":
                break
            case "header_title":
                if object.value(forKey: "title") as? String != "" {
                    headerTextLabel.text = object.value(forKey: "title") as? String
                } else {
                    updateHeader()
                }
//                if object.value(forKey: "title") as? String == "Capture Details" {
//                    headerImageView.isHidden = false
//                }
                break
            case "VIDEOCOMPONENT_INITIATED":
                break
            case "USER_END_CALL":
                break
            case "USER_RECONNECT":
                break
            case "DISCONNECT":
                break
            case "USER_BACKPRESS":
                loadPreviousPage()
                break
            case "update_header":
                if !contentView.isDescendant(of: contentView) {
                    headerBackButton.isHidden.toggle()
                    headerImageView.isHidden.toggle()
                }
                break
            default:
                break
            }
        }
    }
    public func onSvcFailure(object: NSObject) {
        DispatchQueue.main.async { [self] in
            contentView.removeSubviews()
            let value = object.value(forKey: "event") as! String
            switch value {
            case "RECONNECTS_EXHAUSTED","NETWORK_DISCONNECTED","OPERATOR_EXIT", "SOCKET_TIMEOUT":
                loadNetworkDisconnectedView()
                break
            case "USER_EXIT":
                if contentView.isDescendant(of: contentView) {
                    updateCaptureView()
                } else {
                    ProgressHUD.dismiss()
                    self.delegate?.onCaptureSuccess(object: ["event": "BackPressed"] as NSObject)
                }
                break
            default:
                break
            }
        }
    }
    private func loadPreviousPage() {
        let pagesequence = dalCapture.onPreviousPage()
        if pagesequence.getPage() != "PAGE_END" {
            fetchPageConfig(page: pagesequence.getPage() )
            return
        } else {
            onDestroy()
            ProgressHUD.dismiss()
            self.delegate?.onCaptureSuccess(object: ["event": "BackPressed"] as NSObject)
        }
    }
    public func Header() {
        updateHeader()
    }
    public func redirectToNextPage(iRedirectNext: IRedirectNext) {
        self.iRedirectNext = iRedirectNext
        DispatchQueue.main.async { [self] in
            let pageSequence = dalCapture.onNextPage()
            if pageSequence.getPage() != "PAGE_END" {
                fetchPageConfig(page: pageSequence.getPage() )
            } else {
                self.showThankYouView()
            }
        }
    }
    public func showThankYouView() {
        DispatchQueue.main.async { [self] in
            contentView.removeSubviews()
            infoView.loadKycStatusView(title: "Thank You", message: "")
            contentView.addSubview(self.infoView)
            onDestroy()
        }
    }
    public func showOutOfLocationView() {
        DispatchQueue.main.async { [self] in
            contentView.removeSubviews()
            infoView.loadLocationFailureView()
            contentView.addSubview(self.infoView)
            onDestroy()
        }
    }
    public func onCaptureIntermediate(object: NSObject) {
        onAssistedIntermediate(object: object)
    }
    func updateCaptureView() {
        ProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [self] in
            getCaptureStatusDetails(sessionOverride: true)
        }
    }
    private func logGrantedPermission(eventName: String, eventSource: String) {
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "PermissionsCheck", timestamp: "", event_type: "Granted", event_name: eventName, component: "PermissionsService", event_source: eventSource, publish_to_dlk: true), meta: [:])
    }
    private func logDeniedPermission(eventName: String, eventSource: String, errorMessage: String) {
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "PermissionsCheck", timestamp: "", event_type: "Denied", event_name: eventName, component: "PermissionsService", event_source: eventSource, publish_to_dlk: true), meta: ["errorMessage":errorMessage])
    }
}
