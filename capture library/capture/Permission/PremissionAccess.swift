//
//  PremissionAccess.swift
//  Hostapp
//
//  Created by Admin on 08/03/22.
//

import Foundation
import AVFoundation
import UIKit
import core
import avkyc

public class PermissionView: UIView,IRedirectNext {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var locationTextLabel:UILabel!
    @IBOutlet weak var locationSubtitleLabel:UILabel!
    @IBOutlet weak var cameraTextLabel:UILabel!
    @IBOutlet weak var cameraSubtitleLabel:UILabel!
    @IBOutlet weak var microphoneTextLabel:UILabel!
    @IBOutlet weak var microphoneSubtitleLabel:UILabel!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var locationIcon:UIImageView!
    @IBOutlet weak var cameraIcon:UIImageView!
    @IBOutlet weak var micIcon:UIImageView!
    @IBOutlet weak var locationAccessView:UIView!
    @IBOutlet weak var cameraAccessView:UIView!
    @IBOutlet weak var microphoneAccessView:UIView!

    let kCONTENT_XIB_NAME           =   "PermissionView"

    private var object = NSObject()
    private var objectArray = NSArray()
    private var validation:[String:Bool] = [:]

    private var delegate:ICapture?
    
    public init(object: NSObject,view: UIView,delegate:ICapture) {
        self.delegate = delegate
        self.object = object
        super.init(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        submitButton.tintColor = ThemeConfig.shared.getPrimaryMainColor()
        submitButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)

        renderUi()
    }
    private func renderUi() {
        setColors()
        locationAccessView.isHidden = true
        cameraAccessView.isHidden = true
        microphoneAccessView.isHidden = true
        guard let configObject = object.value(forKey: "config") as? NSObject else { return  }
        objectArray = configObject.value(forKey: "capture") as! NSArray

        for(_,value) in objectArray.enumerated() {
            let jsonObject = value as! NSObject
            let category = jsonObject.value(forKey: "attr") as! String
            let messageObject = (value as! NSObject).value(forKey: "template") as! NSObject
            let message = messageObject.value(forKey: "label") as! String
            let helpText = messageObject.value(forKey: "help_text") as! String
            validation.updateValue(messageObject.value(forKey: "mandatory") as! Bool, forKey: category)
            if category == "location" {
                locationAccessView.isHidden = false
                locationTextLabel.text = message
                if helpText != "" {
                    locationSubtitleLabel.text = helpText
                } else {
                    locationSubtitleLabel.isHidden = true
                }
            } else if category == "camera" {
                cameraAccessView.isHidden = false
                cameraTextLabel.text = message
                if helpText != "" {
                    cameraSubtitleLabel.text = helpText
                } else {
                    cameraSubtitleLabel.isHidden = true
                }
            } else if category == "microphone" {
                microphoneAccessView.isHidden = false
                microphoneTextLabel.text = message
                if helpText != "" {
                    microphoneSubtitleLabel.text = helpText
                } else {
                    microphoneSubtitleLabel.isHidden = true
                }
            }
        }
    }
    private func setColors() {
        locationIcon.tintColor = ThemeConfig.shared.getPrimaryMainColor()
        cameraIcon.tintColor = ThemeConfig.shared.getPrimaryMainColor()
        micIcon.tintColor = ThemeConfig.shared.getPrimaryMainColor()
        locationTextLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        cameraTextLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        microphoneTextLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        locationSubtitleLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        microphoneSubtitleLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        cameraSubtitleLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
    }
    @IBAction func didTapButton(sender:UIButton) {
        checkForAccess()
        submitButton.isEnabled = false
    }
    private func checkForAccess() {
        for validation in validation {
            if validation.key == "location" && validation.value == true {
                checkforLocationAccess()
            } else if validation.key == "camera" && validation.value == true {
                checkforCameraAccess()
            } else if validation.key == "microphone" && validation.value == true {
                checkforMicrophoneAccess()
            }
        }
        if validation.values.allSatisfy({$0 == false}) {
            self.delegate?.redirectToNextPage(iRedirectNext: self)
            delegate = nil
        }
    }
    public func onSuccess() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = false
        }
    }

    public func onFailure() {
        DispatchQueue.main.async {
            self.submitButton.isEnabled = true
        }
    }
    public func BackPressed() {
        self.delegate?.onCaptureIntermediate(object: ["event": "USER_BACKPRESS"] as NSObject)
    }
    private func checkforCameraAccess() {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            validation.updateValue(false, forKey: "camera")
            checkForAccess()
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                if granted == true {
                    self.checkforMicrophoneAccess()
                } else {
                    DispatchQueue.main.async {
                        self.window?.rootViewController?.showAlertForCameraAccess(completion: { success in
                            if success == true {
                                self.validation.updateValue(false, forKey: "camera")
                                self.checkForAccess()
                            } else {
                                self.submitButton.isEnabled = true
                            }
                        })
                    }
                }
            })
        }
    }
    private func checkforMicrophoneAccess() {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.audio) ==  AVAuthorizationStatus.authorized {
            validation.updateValue(false, forKey: "microphone")
            checkForAccess()
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.audio, completionHandler: { (granted: Bool) -> Void in
                if granted == true {
                    self.checkforLocationAccess()
                } else {
                    DispatchQueue.main.async {
                        self.window?.rootViewController?.showAlertForMicroPhoneAccess(completion: { [self] success in
                            if success == true {
                                self.validation.updateValue(false, forKey: "microphone")
                                self.checkForAccess()
                            }  else {
                                self.submitButton.isEnabled = true
                            }
                        })
                    }
                }
            })
        }
    }
    private func checkforLocationAccess() {
        if let _ = LocationManager.sharedInstance.currentLocation {
            validation.updateValue(false, forKey: "location")
            checkForAccess()
        } else {
            DispatchQueue.main.async {
                self.window?.rootViewController?.showAlertForLocationAccess(completion: { success in
                    if success == true {
                        if let _ = LocationManager.sharedInstance.currentLocation {
                            self.validation.updateValue(false, forKey: "location")
                            self.checkForAccess()
                        } else {
                            self.checkforLocationAccess()
                        }
                    }  else {
                        self.submitButton.isEnabled = true
                    }
                })
            }
        }
    }
}

