//
//  Capture.swift
//  captureapp
//
//  Created by Admin on 29/03/22.
//

import Foundation
import Cleanse
import core
import avkyc
import healthcheck
import videocomponent
import svc
import UIKit

public class CaptureFactory {

    public class func startCapture(view:UIView,captureModule:CaptureConfig,delegate:HostAppCallBack) -> BaseView? {

        var modelCaptureConfig = ModelCaptureConfig()

        if let envConfig = ApiController.getEnvConfig() {
            modelCaptureConfig = processConfig(envConfig: envConfig, captureConfig: captureModule)
            return startCaptureComponent(view: view, captureModule:modelCaptureConfig,delegate:delegate)
        } else {
            return nil
        }

    }
    public class func startCaptureComponent(view:UIView,captureModule:ModelCaptureConfig,delegate:HostAppCallBack) -> BaseView {
        let captureComponent = CaptureComponentFactory().getCaptureComponent(view: view, modelCaptureConfig: captureModule, delegate: delegate)
        return captureComponent.getBaseView()
    }

    private class func processConfig(envConfig:EnvConfig,captureConfig:CaptureConfig) -> ModelCaptureConfig {
        
        let modCaptureConfig = ModelCaptureConfig.CaptureBuilder()

        _ = modCaptureConfig.setToken(token: captureConfig.getToken())

        if captureConfig.getLoggerUrl() == "" || captureConfig.getLoggerUrl().isEmpty {
            _ = modCaptureConfig.setLoggerEndpoint(logger_url: envConfig.getREACT_APP_LOGGER_URL())
        } else {
            _ = modCaptureConfig.setLoggerEndpoint(logger_url:captureConfig.getLoggerUrl())
        }

        if captureConfig.getEnableLogging() == true {
            _ = modCaptureConfig.enableLogging(enableLogging: true)
        }
        _ = modCaptureConfig.setStorageService(isStorageService: envConfig.getREACT_APP_USE_STORAGE_SERVICE() == "true")

        if captureConfig.getCaptureServerUrl() == "" || captureConfig.getCaptureServerUrl().isEmpty {
            _ = modCaptureConfig.setCaptureServerUrl(serverUrl: envConfig.getREACT_APP_API_SERVER_URI())
        } else {
            _ = modCaptureConfig.setCaptureServerUrl(serverUrl: captureConfig.getCaptureServerUrl())
        }

        if captureConfig.getCaptureSocketUrl() == "" || captureConfig.getCaptureSocketUrl().isEmpty {
            _ = modCaptureConfig.setCaptureSocketUrl(captureSocketUrl: envConfig.getREACT_APP_CAPTURE_SOCKET_URI())
        } else {
            _ = modCaptureConfig.setCaptureSocketUrl(captureSocketUrl: captureConfig.getCaptureSocketUrl())
        }

        if captureConfig.getNetworkCheckBaseUrl() == "" || captureConfig.getNetworkCheckBaseUrl().isEmpty {
            _ = modCaptureConfig.setNetworkCheckBaseUrl(networkCheckBaseUrl: envConfig.getREACT_APP_NETWORK_CHECK_SERVICE_BASE_URI())
        } else {
            _ = modCaptureConfig.setNetworkCheckBaseUrl(networkCheckBaseUrl: captureConfig.getNetworkCheckBaseUrl())
        }

        if captureConfig.getavkycBackendBaseUrl() == "" || captureConfig.getavkycBackendBaseUrl().isEmpty {
            _ = modCaptureConfig.setAvkycBackendBaseUrl(avkycBackendBaseUrl: envConfig.getREACT_APP_VKYC_BACKEND_BASE_URI())
        } else {
            _ = modCaptureConfig.setAvkycBackendBaseUrl(avkycBackendBaseUrl: captureConfig.getavkycBackendBaseUrl())
        }

        if captureConfig.getavkycSocketUrl() == "" || captureConfig.getavkycSocketUrl().isEmpty {
            _ = modCaptureConfig.setAvkycSocketUrl(avkycSocketUrl: envConfig.getREACT_APP_VKYC_SOCKET_URI())
        } else {
            _ = modCaptureConfig.setAvkycSocketUrl(avkycSocketUrl: captureConfig.getavkycSocketUrl())
        }

        if captureConfig.getSvcServiceBaseUrl() == "" || captureConfig.getSvcServiceBaseUrl().isEmpty {
            _ = modCaptureConfig.setSvcServiceBaseUrl(svcServiceBaseUrl: envConfig.getREACT_APP_SELF_VIDEO_SERVICE_BASE_URI())
        } else {
            _ = modCaptureConfig.setSvcServiceBaseUrl(svcServiceBaseUrl: captureConfig.getSvcServiceBaseUrl())
        }

        if captureConfig.getSvcSseBaseUrl() == "" || captureConfig.getSvcSseBaseUrl().isEmpty {
            _ = modCaptureConfig.setSvcSseBaseUrl(svcSseBaseUrl: envConfig.getREACT_APP_SELF_VIDEO_SSE_BASE_URI())
        } else {
            _ = modCaptureConfig.setSvcSseBaseUrl(svcSseBaseUrl: captureConfig.getSvcSseBaseUrl())
        }

        if captureConfig.getmediaServerUrl() == "" || captureConfig.getmediaServerUrl().isEmpty {
            _ = modCaptureConfig.setMediaServerUrl(mediaServerUrl: envConfig.getREACT_APP_MEDIA_SERVER_URI())
        } else {
            _ = modCaptureConfig.setMediaServerUrl(mediaServerUrl: captureConfig.getmediaServerUrl())
        }

        if captureConfig.getFrameWidth() >= 0 {
            _ = modCaptureConfig.setFrameWidth(frameWidth: captureConfig.getFrameWidth())
        }
        if captureConfig.getFrameHeight() >= 0 {
            _ = modCaptureConfig.setFrameHeight(frameHeight: captureConfig.getFrameHeight())
        }
        if captureConfig.getFramePerSec() >= 0 {
            _ = modCaptureConfig.setFramePerSec(framePerSec: captureConfig.getFramePerSec())
        }
        return modCaptureConfig.build()
    }
}
