//
//  CaptureModule.swift
//  captureapp
//
//  Created by Admin on 30/03/22.
//

import Foundation
import Cleanse

public protocol CaptureModuleProtocol {
    mutating func captureModule(modelCaptureConfig:ModelCaptureConfig)
    func provideModelCaptureConfig() -> ModelCaptureConfig
}
public struct CaptureModuleStructure:CaptureModuleProtocol {
    var modelCaptureConfig = ModelCaptureConfig()
    public mutating func captureModule(modelCaptureConfig: ModelCaptureConfig) {
        self.modelCaptureConfig = modelCaptureConfig
    }

    public func provideModelCaptureConfig() -> ModelCaptureConfig {
        return modelCaptureConfig
    }


}
