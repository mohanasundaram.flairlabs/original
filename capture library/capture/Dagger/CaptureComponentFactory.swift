//
//  CaptureComponentFactory.swift
//  capture
//
//  Created by FL Mohan on 05/05/22.
//

import Foundation
import Cleanse
import core

public class CaptureComponentFactory {

    public var captureComponent:CaptureComponent.Root?

    public func getCaptureComponent(view:UIView,modelCaptureConfig:ModelCaptureConfig,delegate:HostAppCallBack) -> CaptureComponent.Root {
        if captureComponent == nil {
            let dalComponent = DalComponentFactory.sharedInstance.getDalComponent(dalConfig: modelCaptureConfig.getDalConfig())

            captureComponent = try! ComponentFactory.of(CaptureComponent.self).build(Feed(view: view, dalComponent: dalComponent, modelCaptureConfig: modelCaptureConfig, delegate: delegate))

        }
        return captureComponent!
    }
    public func captureComponentDestroy() {
        captureComponent = nil
    }

}
