//
//  CaptureComponent.swift
//  captureapp
//
//  Created by Admin on 29/03/22.
//

import Foundation
import Cleanse
import core
import avkyc
import svc
import UIKit
public protocol CaptureProtocol {
    func getBaseView() -> BaseView
}

public struct CaptureStructure:CaptureProtocol {
    public let baseView: BaseView
    public func getBaseView() -> BaseView {
        return baseView
    }
}
public struct CaptureComponent : Cleanse.RootComponent {
    public typealias Root = CaptureStructure
    public typealias Seed = Feed

    public static func configure(binder: UnscopedBinder) {
        binder.install(dependency: DalComponent.self)
        binder.install(dependency: CompComponent.self)
        binder.install(dependency: TaskComponent.self)
    }
    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dd:Seed) in
            Root.init(baseView: BaseView(frame: dd.view.frame, dalCapture: dd.dalComponent.getDalCapture(), modelCaptureConfig: dd.modelCaptureConfig , captureFragmentFactory: dd.captureFragment.getCaptureFactory(), loggerWrapper:dd.dalComponent.getLoggerWrapper(), delegate: dd.delegate))
        }
    }
                      }
public struct Feed {
    let dalComponent:DalComponent.Root
    let captureFragment:CapComponent.Root
    var modelCaptureConfig = ModelCaptureConfig()
    var delegate:HostAppCallBack
    var view:UIView

    public init(view:UIView,dalComponent:DalComponent.Root,modelCaptureConfig:ModelCaptureConfig,delegate:HostAppCallBack) {
        self.dalComponent = dalComponent
        self.captureFragment = CapComponent.Root(captureFragmentFactory: CaptureFragmentFactory(dalCapture: dalComponent.getDalCapture(), componentFactory: CompFactory(dalCapture: dalComponent.getDalCapture(), taskFactory: TaskFactory(dalCapture: dalComponent.getDalCapture()))))
        self.modelCaptureConfig = modelCaptureConfig
        self.delegate = delegate
        self.view = view
    }
}
