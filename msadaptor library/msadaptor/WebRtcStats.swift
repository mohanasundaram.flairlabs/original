//
//  WebRtcStats.swift
//  app
//
//  Created by Admin on 30/09/21.
//

import Foundation
import core
import WebRTC

class WebRtcStats: NSObject {
    
    private var msAdapter: MsAdapter {
        MsAdapter.shared
    }
    var webRtcStat =  WebrtcStatsModelClass()
    var codecModel = [CodecModelList]()
    var typeOfStats: String = ""
    var localCandidateId: String = ""
    var remoteCandidateId: String = ""
    var kind: String = ""
    var audioCodecId: String = ""
    var videoCodecId: String = ""
    var dataClass = DataClass()
    var local = Local()
    var remote = Local()
    var connectionType = ConnectionType()
    var audio = DataAudio()
    var video = DataAudio()
    var trackAudio = TrackAudio()
    var trackVideo = TrackAudio()
    var resolution = Resolutions()
    var bandwidth = Bandwidth()
    
    var previousTimeStamp: Double = 0
    var firstTimeStamp: Int64 = 0
    var publishAudioPreviousBytes: Int = 0
    var publishAudioFirstBytes: Int = 0
    var publishVideoPreviousBytes: Int = 0
    var publishVideoFirstBytes = 0
    var isFirstStats: Bool = false
    var publishAudioPreviousPacketLost: Int = 0
    var publishVideoPreviousPacketLost: Int = 0
    var playAudioPreviousBytes: Int = 0
    var playAudioFirstBytes: Int = 0
    var playVideoPreviousBytes: Int = 0
    var playVideoFirstBytes: Int = 0
    var playAudioPreviousPacketLost: Int = 0
    var playVideoPreviousPacketLost: Int = 0
    var playPreviousTimeStamp: Double = 0
    var playFirstTimeStamp: Int64 = 0
    var isFirstPlayStats: Bool = false
    
    override init() {
    }
    
    public func getStats(reports: RTCStatisticsReport, isAudioEnabled: Bool, isMuted: Bool, audioState: String, isVideoEnabled: Bool, isVideoMuted: Bool, videoState: String, type: String) {
        trackVideo.muted = isVideoMuted
        trackVideo.enabled = isVideoEnabled
        trackVideo.readyState = videoState
        trackAudio.enabled = isAudioEnabled
        trackAudio.muted = isMuted
        trackAudio.readyState = audioState
        
        self.typeOfStats = type
        
        dataClass.currentTimestamp = Date.currentTimeStamp
        let data = reports.statistics
        if !isFirstPlayStats {
            playFirstTimeStamp = dataClass.currentTimestamp ?? 0
        }
        if !isFirstStats {
            firstTimeStamp = dataClass.currentTimestamp ?? 0
        }
        if typeOfStats == "publish" {
            trackVideo.label = "Internal Camera"
            trackAudio.label = "Intrenal Mic"
            dataClass.track?.audio = trackAudio
            dataClass.track?.video = trackVideo
        }
        for key in  data.keys {
            
            let type = data[key]?.type
            let id = data[key]?.id
            if type == "candidate-pair" {
                let state = data[key]?.values["state"] as? String
                let nominated = data[key]?.values["nominated"] as? Int
                if state == "succeeded" && nominated == 1 {
                    connectionType.rtt = data[key]?.values["currentRoundTripTime"] as? String ?? ""
                    localCandidateId = data[key]?.values["localCandidateId"] as? String ?? ""
                    remoteCandidateId = data[key]?.values["remoteCandidateId"] as? String ?? ""
                }
                if  data[key]?.values.contains(key: "availableOutgoingBitrate") == true {
                    bandwidth.availableBandwidth =  data[key]?.values["availableOutgoingBitrate"] as? Int
                }
                if data[key]?.values.contains(key: "availableOutgoingBitrate") == true {
                    bandwidth.availableBandwidth =  data[key]?.values["availableOutgoingBitrate"] as? Int
                }
                
            }
            
            if localCandidateId == id {
                local.candidateType = data[key]?.values["candidateType"] as? String ?? ""
                local.transport = data[key]?.values["protocol"] as? String ?? ""
                local.ipAddress = data[key]?.values["ip"] as? String ?? ""
                local.networkType = data[key]?.values["networkType"] as? String ?? ""
                local.portNumber = data[key]?.values["port"] as? Int ?? 0
            }
            connectionType.local = local
            if remoteCandidateId == id {
                remote.candidateType = data[key]?.values["candidateType"] as? String ?? ""
                remote.transport = data[key]?.values["protocol"] as? String ?? ""
                remote.ipAddress = data[key]?.values["ip"] as? String ?? ""
                remote.portNumber = data[key]?.values["port"] as? Int ?? 0
            }
            connectionType.remote = remote
            
            if type == "codec" {
                if codecModel.isEmpty {
                    codecModel = [CodecModelList(type: data[key]?.type ?? "", id: data[key]?.id ?? "", mimeType: data[key]?.values["mimeType"] as? String ?? "" )]
                } else {
                    codecModel.append(CodecModelList(type: data[key]?.type ?? "", id: data[key]?.id ?? "", mimeType: data[key]?.values["mimeType"] as? String ?? "" ))
                }
                
            }
            if let _ = data[key]?.values["kind"] {
                kind = data[key]?.values["kind"] as? String ?? ""
            }
            if typeOfStats == "Publish" {
                
                if type == "media-source" && kind == "audio" {
                    audio.audioLevel = data[key]?.values["audioLevel"] as? Double ?? 0
                    audio.totalAudioEnergy = data[key]?.values["totalAudioEnergy"] as? Double ?? 0
                }
                if type == "outbound-rtp" && kind == "audio" {
                    audio.bytes = data[key]?.values["bytesSent"] as? Int ?? 0
                    audio.bandwidth = Int(calculateBandwidth(currentBytes: Int(audio.bytes ?? 0), prevBytes: publishAudioPreviousBytes, prevTimeStamp: Int64(previousTimeStamp), currentTimeStamp: dataClass.currentTimestamp ?? 0 ))
                    publishAudioPreviousBytes = Int(audio.bytes ?? 0)
                    if !isFirstStats {
                        publishAudioFirstBytes = Int(audio.bytes ?? 0)
                    }
                    audio.averageBandwidth = Int(calculateAverageBandwidth(currentBytes: Int(audio.bytes ?? 0), firstBytes: publishAudioFirstBytes, firstTimeStamp: firstTimeStamp, currentTimeStamp: dataClass.currentTimestamp ?? 0 ))
                    audioCodecId = data[key]?.values["codecId"] as? String ?? ""
                    for value in codecModel {
                        if value.id == audioCodecId {
                            let mimeType = value.mimeType
                            let newStringArray = mimeType.components(separatedBy: "/")
                            audio.codec = newStringArray[1]
                        }
                    }
                    if data[key]?.values.contains(key: "packetsLost") == true {
                        audio.packetsLost = calculatePacketLoss(currentPacketLoss: data[key]?.values["packetsLost"] as? Int ?? 0, previousPacketLoss: publishAudioPreviousPacketLost)
                    }
                    publishAudioPreviousPacketLost = audio.packetsLost ?? 0
                }
                if type == "outbound-rtp" && kind == "video" {
                    
                    video.bytes = data[key]?.values["bytesSent"] as? Int ?? 0
                    video.codecImplementationName = data[key]?.values["encoderImplementation"] as? String ?? ""
                    resolution.width = data[key]?.values["frameWidth"] as? Double
                    resolution.height = data[key]?.values["frameHeight"] as? Double
                    let qualityLimitationReason = data[key]?.values["qualityLimitationReason"] as? String ?? ""
                    if qualityLimitationReason == "bandwidth" {
                        resolution.bandwidthLimitedResolution = "true"
                    }
                    if qualityLimitationReason == "cpu" {
                        resolution.cpuLimitedResolution = "true"
                    }
                    if !isFirstStats {
                        publishVideoFirstBytes = video.bytes ?? 0
                    }
                    video.bandwidth = Int(calculateBandwidth(currentBytes: Int(video.bytes ?? 0), prevBytes: publishVideoPreviousBytes, prevTimeStamp: Int64(previousTimeStamp), currentTimeStamp: dataClass.currentTimestamp ?? 0 ))
                    publishVideoPreviousBytes = video.bytes ?? 0
                    video.averageBandwidth = Int(calculateAverageBandwidth(currentBytes: Int(video.bytes ?? 0), firstBytes: publishVideoFirstBytes, firstTimeStamp: firstTimeStamp, currentTimeStamp: dataClass.currentTimestamp ?? 0))
                    videoCodecId = data[key]?.values["codecId"] as? String ?? ""
                    for value in codecModel {
                        if value.id == videoCodecId {
                            let mimeType = value.mimeType
                            let newStringArray = mimeType.components(separatedBy: "/")
                            video.codec = newStringArray[1]
                        }
                    }
                }
                bandwidth.averageBandwidth = Int(audio.averageBandwidth ?? 0) + Int(video.averageBandwidth ?? 0)
                if data[key]?.values.contains(key: "packetsLost") == true {
                    video.packetsLost = calculatePacketLoss(currentPacketLoss: data[key]?.values["packetsLost"] as? Int ?? 0, previousPacketLoss: publishVideoPreviousPacketLost)
                }
                publishVideoPreviousPacketLost = video.packetsLost ?? 0
                
            } else {
                if type == "track" {
                    let remoteSource = data[key]?.values["remoteSource"] as? String
                    if kind == "audio" && remoteSource == "true" {
                        audio.audioLevel = data[key]?.values["audioLevel"] as? Double
                        audio.totalAudioEnergy = data[key]?.values["totalAudioEnergy"] as? Double
                        audio.jitterBufferDelay = data[key]?.values["jitterBufferDelay"] as? Double
                    }
                }
                if type == "inbound-rtp" && kind == "audio" {
                    audio.jitter = data[key]?.values["jitter"] as? Double
                    audio.bytes = data[key]?.values["bytesReceived"] as? Int
                    audio.bandwidth = Int(calculateBandwidth(currentBytes: Int(audio.bytes ?? 0), prevBytes: playAudioPreviousBytes, prevTimeStamp: Int64(playPreviousTimeStamp), currentTimeStamp: dataClass.currentTimestamp ?? 0))
                    playAudioPreviousBytes = audio.bytes ?? 0
                    if !isFirstPlayStats {
                        playAudioFirstBytes = audio.bytes ?? 0
                    }
                    audio.averageBandwidth = Int(calculateAverageBandwidth(currentBytes: Int(audio.bytes ?? 0), firstBytes: playAudioFirstBytes, firstTimeStamp: playFirstTimeStamp, currentTimeStamp: dataClass.currentTimestamp ?? 0))
                    audioCodecId = data[key]?.values["codecId"] as? String ?? ""
                    for value in codecModel {
                        if value.id == audioCodecId {
                            let mimeType = value.mimeType
                            let newStringArray = mimeType.components(separatedBy: "/")
                            video.codec = newStringArray[1]
                        }
                    }
                    
                }
                if type == "inbound-rtp" && kind == "video" {
                    video.bytes = data[key]?.values["bytesReceived"] as? Int
                    if !isFirstPlayStats {
                        playVideoFirstBytes = video.bytes ?? 0
                    }
                    video.bandwidth = Int(calculateBandwidth(currentBytes: Int(video.bytes ?? 0), prevBytes: playVideoPreviousBytes, prevTimeStamp: Int64(playPreviousTimeStamp), currentTimeStamp: dataClass.currentTimestamp ?? 0))
                    playVideoPreviousBytes = video.bytes ?? 0
                    video.averageBandwidth = Int(calculateAverageBandwidth(currentBytes: Int(video.bytes ?? 0), firstBytes: playVideoFirstBytes, firstTimeStamp: playFirstTimeStamp, currentTimeStamp: dataClass.currentTimestamp ?? 0))
                    video.jitter = data[key]?.values["jitter"] as? Double
                    video.codecImplementationName = data[key]?.values["encoderImplementation"] as? String
                    resolution.width = data[key]?.values["frameWidth"] as? Double
                    resolution.height = data[key]?.values["frameHeight"] as? Double
                    videoCodecId = data[key]?.values["codecId"] as? String ?? ""
                    for value in codecModel {
                        if value.id == videoCodecId {
                            let mimeType = value.mimeType
                            let newStringArray = mimeType.components(separatedBy: "/")
                            video.codec = newStringArray[1]
                        }
                    }
                    if data[key]?.values.contains(key: "packetsLost") == true {
                        video.packetsLost = calculatePacketLoss(currentPacketLoss: data[key]?.values["packetsLost"] as? Int ?? 0, previousPacketLoss: playVideoPreviousPacketLost)
                    }
                }
                bandwidth.averageBandwidth = Int(audio.averageBandwidth ?? 0) + Int(video.averageBandwidth ?? 0)
                
            }
            
        }
        if typeOfStats == "Publish" {
            previousTimeStamp = Double(dataClass.currentTimestamp ?? 0)
            isFirstStats = true
        } else {
            playPreviousTimeStamp = Double(dataClass.currentTimestamp ?? 0)
            isFirstPlayStats = true
        }
        dataClass.audio = audio
        dataClass.video = video
        dataClass.connectionType = connectionType
        
        dataClass.type = typeOfStats
        dataClass.isChrome = false
        dataClass.currentTimestampISO = currentDate()
        dataClass.resolutions = resolution
        dataClass.msRoomID = msAdapter.reconnectingId
        dataClass.msRoomReferenceID = msAdapter.roomId
        if typeOfStats == "Publish" {
            dataClass.streamID = msAdapter.streamReferenceId
        } else {
            dataClass.streamID = msAdapter.agentStreamId
        }
        dataClass.source = "capture"
        bandwidth.bandwidth = Int(audio.bandwidth ?? 0) + Int(video.bandwidth ?? 0)
        dataClass.bandwidth = bandwidth
        
        webRtcStat.data = dataClass
        msAdapter.onWebrtcStatsReceived(model: webRtcStat)
    }
    func calculateBandwidth(currentBytes: Int, prevBytes: Int, prevTimeStamp: Int64, currentTimeStamp: Int64) -> Double {
        if prevBytes == 0 && prevTimeStamp == 0 {
            return 0
        }
        let bytes = currentBytes - prevBytes
        let kiloBytes = (8 * bytes) / 1024
        let duration = (currentTimeStamp - prevTimeStamp) / 1000
        if duration == 0 {
            return 0
        }
        let convert: Double = Double(kiloBytes / Int(duration))
        return convert
        
    }
    func calculateAverageBandwidth(currentBytes: Int, firstBytes: Int, firstTimeStamp: Int64, currentTimeStamp: Int64) -> Double {
        if firstBytes == 0 && firstTimeStamp == currentTimeStamp {
            return 0
        }
        let bytes = currentBytes - firstBytes
        let kiloBytes = (8 * bytes) / 1024
        let duration = (currentTimeStamp - firstTimeStamp) / 1000
        if duration <= 0 {
            return 0
        }
        return Double(kiloBytes / Int(duration))
        
    }
    func calculatePacketLoss(currentPacketLoss: Int, previousPacketLoss: Int) -> Int? {
        return currentPacketLoss - previousPacketLoss
    }
}
