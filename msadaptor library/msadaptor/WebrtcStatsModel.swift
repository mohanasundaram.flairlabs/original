//
//  WebrtcStatsModel.swift
//  app
//
//  Created by Admin on 16/09/21.
//

import Foundation
import UIKit

// MARK: - WebrtcStatsModelClass
struct WebrtcStatsModelClass: Codable {
    var data: DataClass?
}

// MARK: - DataClass
public struct DataClass: Codable {
    var type: String?
    var isChrome: Bool?
    var currentTimestamp: Int64?
    var currentTimestampISO: String?
    var connectionType: ConnectionType?
    var bandwidth: Bandwidth?
    var track: Track?
    var audio, video: DataAudio?
    var resolutions: Resolutions?
    var streamID: String?
    var msRoomID: Int?
    var msRoomReferenceID, source: String?

    public enum CodingKeys: String, CodingKey {
        case type, isChrome, currentTimestamp, currentTimestampISO, connectionType, bandwidth, track, audio, video, resolutions
        case streamID = "streamId"
        case msRoomID = "MsRoomID"
        case msRoomReferenceID = "MSRoomReferenceID"
        case source
    }
}

// MARK: - DataAudio
public struct DataAudio: Codable {
    var audioLevel: Double?
    var totalAudioEnergy: Double?
    var codec: String?
    var bytes, bandwidth, averageBandwidth, latency: Int?
    var packetsLost: Int?
    var codecImplementationName: String?
    var jitterBufferDelay: Double?
    var jitter: Double?
}

// MARK: - Bandwidth
public struct Bandwidth: Codable {
    var averageBandwidth, bandwidth, availableBandwidth: Int?
}

// MARK: - ConnectionType
public struct ConnectionType: Codable {
    var local, remote: Local?
    var rtt: String?
}

// MARK: - Local
public struct Local: Codable {
    var candidateType, transport, ipAddress: String?
    var networkType: String?
    var portNumber: Int?
}

// MARK: - Resolutions
public struct Resolutions: Codable {
    var width, height: Double?
    var bandwidthLimitedResolution, cpuLimitedResolution: String?
}

// MARK: - Track
public struct Track: Codable {
    var audio, video: TrackAudio?
}

// MARK: - TrackAudio
public struct TrackAudio: Codable {
    var enabled, muted: Bool?
    var label, readyState: String?
}
public struct Stats: Codable {
    var list: [Datas]
}
public struct Datas: Codable {
    var typeName: String
    var local: String
    var remote: String
}

public struct CodecModelList: Decodable {
    var type: String
    var id: String
    var mimeType: String
}
public struct AudioLevel: Codable {
    let type, streamID: String
    let audioLevel, timestamp: Int
    let currentTimestampISO: String
    let msRoomID: Int
    let msRoomReferenceID, source: String

    enum CodingKeys: String, CodingKey {
        case type
        case streamID = "stream_id"
        case audioLevel = "audio_level"
        case timestamp
        case currentTimestampISO
        case msRoomID = "MsRoomID"
        case msRoomReferenceID = "MSRoomReferenceID"
        case source
    }
}
public struct NetworkSignalModel: Codable {
    var publish: Bool?
    var streamId: String?
    var packetsLost: Int?
    var fractionLost: Int?
    var currentTimestamp: String?
    var audioLevel: Double?
    var averageIncomingBitrate: Double?
    var averageOutgoingBitrate: Double?
    var currentIncomingBitrate: Double?
    var currentOutgoingBitrate: Double?
    var streamType: String?
    var token: String?
    var ragStatus: String?
}
class ReadObjectFromStatsModal {
    static func readSampleWebRtcStatsDataFromFile() -> WebrtcStatsModelClass? {
        let path = Bundle.main.path(forResource: "WebRtcStats", ofType: "json")
        let url = URL(fileURLWithPath: path!)
        let sportsData = try? Data(contentsOf: url)
        guard
            let data = sportsData
        else { return nil  }
        do {
            let result = try JSONDecoder().decode(WebrtcStatsModelClass.self, from: data)
//            print(result)
            return result
        } catch let error {
            print("Failed to Decode Object", error)
            return nil
        }
    }
    static func readSampleAudioLevelStatsDataFromFile() -> AudioLevel? {
        let path = Bundle.main.path(forResource: "AudioLevel", ofType: "json")
        let url = URL(fileURLWithPath: path!)
        let sportsData = try? Data(contentsOf: url)
        guard
            let data = sportsData
        else { return nil  }
        do {
            let result = try JSONDecoder().decode(AudioLevel.self, from: data)
//            print(result)
            return result
        } catch let error {
            print("Failed to Decode Object", error)
            return nil
        }
    }
}
