//
//  MsAdapter.swift
//  app
//
//  Created by Admin on 05/10/21.
//
public enum ProviderName {
    case JanusProvider
    case AntMediaProvider
}
import Foundation
import UIKit
import core
import janus
import antmedia
import WebRTC

public protocol MsAdaptorCallBack {
    func onMSStateCallBack(event: String, object: NSObject)
    func onMSError(code: String, message: String, canReconnect: Bool)
}
/// This class is responsible for handling all the janus and antmedia event
/// Create shared instance of this class in your viewcontroller and call the methods.
public class MsAdapter: NSObject, MSSocketCallBack, JanusStateCallback, AntmediaStateCallback {
    
    public static let shared = MsAdapter()
    private var providerName: String = ""
    private var publishStartTimer: Timer?
    private var playStartTimer: Timer?
    private var audioStartTimer: Timer?
    var streamReferenceId: String = ""
    var agentStreamId: String = ""
    public var reconnectingId: Int = 0
    var roomId: String = ""
    private var bitRate:Int?
    
    var loggerStartTime: Double = 0
    
    var loggerMsAdapter = Logger()
    
    var participantId: String = ""
    
    private var isReconnecting: Bool = false
    public var mediaPublishStarted: Bool = false
    public var mediaPlayStarted: Bool = false
    
    public var delegate: MsAdaptorCallBack?
    
    var stillImageOutput = UIImage()
    
    var cleanupProviderCalled: Bool = false
    
    var isPublish: Bool = false
    
    private let webRTCClient: PeerConnection? = nil
    
    private var janusProvider: JanusProvider {
        JanusProvider.shared
    }
    private var antmediaProvider: AntmediaProvider {
        AntmediaProvider.shared
    }
    var msSocket:MSSocket?

    private var webRtcStats:WebRtcStats? = nil
    var dalCapture = DALCapture()
    
    var hostIp: String = ""
    
    var iceServerArray: NSArray = []
    
    /// Initialse the Ms-Adaptor.
    public override init() {
        
//        loggerMsAdapter = LoggerWrapper.sharedInstance.getLoggers(key: "msAdaptor")
        
        var logDetails = ModalLogDetails()
        logDetails.service = "InitializeState"
        logDetails.event_type = "Invoked"
        logDetails.event_name = ""
        logDetails.event_source = "initializeVariables"
        
        loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
    }
    
    /// This method is used to Connect to the Room ID Sockets
    /// - Parameters:
    ///   - roomId: Id used to initiate the ms socket connection.
    ///   - participantId: Participant id used to connect socket.
    ///   - msSocketURL: Url string used to connect socket.
    public func setUpSocket(url: String, roomId: String, participantId: String, msSocketURL: String,bitRate:Int,dalCapture:DALCapture) {
        if roomId == "" || roomId.isEmpty {
            print("Room Id should not null or empty")
        } else if participantId == "" || participantId.isEmpty {
            print("Participant Id should not null or empty")
        }
        self.dalCapture = dalCapture
        self.roomId = roomId
        self.participantId = participantId
        self.bitRate = bitRate
        msSocket = MSSocket()
        msSocket?.initiate(roomId: roomId, participantId: participantId, msSocketURL: url, loggerMs: loggerMsAdapter, dalCapture: dalCapture, callback: self)
        
        var logDetails = ModalLogDetails()
        logDetails.service = "JoinRoom"
        logDetails.event_type = "Invoked"
        logDetails.event_name = ""
        logDetails.event_source = "setUpSocket"
        
        loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
    }
    /// This method is used to initiase the server connection.
    /// - Parameters:
    ///   - providerName: name of the provider eg. Janus, Antmedia, etc.,
    ///   - hostIp: Url used to initialise the server.
    ///   - roomId: room id used to connect respective server.
    ///   - reconnectingId: reconnecting id
    ///   - streamReferenceId: reference id
    ///   - agentStreamId: agent stream id
    ///   - iceServerArray: server array list to communicate provider.
    private func initServer(providerName: String, hostIp: String, roomId: String, reconnectingId: Int, streamReferenceId: String, agentStreamId: String, iceServerArray: NSArray) {
        self.providerName = providerName
        self.streamReferenceId = streamReferenceId
        self.agentStreamId = agentStreamId
        self.reconnectingId = reconnectingId
        self.roomId = roomId
        self.hostIp = hostIp
        self.iceServerArray = iceServerArray
        switch providerName {
        case "janus":
            janusProvider.JanusProviderInit(iceServerArray: iceServerArray, janusStateCallback: self)
            janusProvider.janusDelegate = self
            print("Stream Reference ID is : \(streamReferenceId)")
            janusProvider.StartConnection(url: hostIp, roomId: roomId, reconnectingRoomId: reconnectingId, streamId: streamReferenceId,bitRate: self.bitRate ?? 100)
            
            var logDetails = ModalLogDetails()
            logDetails.service = "InitializeState"
            logDetails.event_name = ""
            logDetails.event_type = "Invoked"
            logDetails.event_source = "initServer"
            
            loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
            break
        case "antmedia":
            AntMediaClient.setDebug(true)
            antmediaProvider.AntmediaProviderInit(iceServerArray: iceServerArray, antmediaStateCallback: self)
            antmediaProvider.StartConnection(url: hostIp, roomId: roomId, reconnectingRoomId: reconnectingId, streamId: streamReferenceId,bitRate: self.bitRate ?? 100)
            break
        default:
            break
        }
    }
    
    /// This method is used to reconnect the call.
    /// - Parameters:
    ///   - type: Type of connection
    ///   - roomId: room id used to perform connection
    ///   - reason: reason received
    public func onReconnectCall(type: String, roomId: Int, reason: String) {
        msSocket?.reconnect(type: type, reason: reason, ms_room_id: roomId)
        
        var logDetails = ModalLogDetails()
        logDetails.service = "WebRTCAdaptor"
        logDetails.event_name = "onReconnectCall"
        logDetails.event_type = "Callback"
        logDetails.event_source = "onReconnectCall"
        
        loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
    }
    /// This method used to handle all callbacks from servers
    /// - Parameters:
    ///   - event: type of event occured
    ///   - object: event message in NSObject format
    public func stateChangeCallback(event: String, object: NSObject) {
        statusEventCallBack(event: event, object: object)
    }
    /// Method to update the clean up completed status.
    public func onCleanUpCompleted() {
        cleanupProviderCalled = false
    }
    /// This method handle all callbacks.
    /// - Parameters:
    ///   - event: type of event occured
    ///   - object: event message in NSObject format
    public func statusEventCallBack(event: String, object: NSObject) {
        if event == "WebRTCIceStateChanged" {
            let state = object.value(forKey: "state") as? String ?? ""
            if state == "disconnected" || state == "failed" {
                msSocket?.reconnect(type: "auto", reason: "ICEConnectionState" + state, ms_room_id: reconnectingId)
                self.delegate?.onMSStateCallBack(event: "WebRTCIceStateChanged", object: object)
            }
        } else if event == "publish_started" && !mediaPublishStarted {
            mediaPublishStarted = true
            self.delegate?.onMSStateCallBack(event: event, object: ["msRoomId": roomId, "publish": true] as NSObject)
            enablePublishStatsEvent()
            if webRtcStats == nil {
                webRtcStats = WebRtcStats()
            }
            // TODO: Audio stats initialise
            
        } else if event == "publish_finished" && mediaPublishStarted {
            mediaPublishStarted = false
            if webRtcStats != nil {
                webRtcStats = nil
            }
            self.delegate?.onMSStateCallBack(event: event, object: object)
        } else if event == "play_started" && !mediaPlayStarted {
            mediaPlayStarted = true
            self.delegate?.onMSStateCallBack(event: event, object: object)
            enablePlayStatsEvent()
            if webRtcStats == nil {
                webRtcStats = WebRtcStats()
            }
            
        } else if event == "play_finished" && !mediaPlayStarted {
            mediaPlayStarted = false
            if webRtcStats != nil {
                webRtcStats = nil
            }
            self.delegate?.onMSStateCallBack(event: event, object: object)
            
        } else if event == "start" {
            self.delegate?.onMSStateCallBack(event: event, object: object)
        }
    }
    /// This method enabling the Play stats
    func enablePlayStatsEvent() {
        DispatchQueue.main.async { [self] in
            guard playStartTimer == nil else { return }
            playStartTimer = Timer.scheduledTimer(timeInterval: TimeInterval(5), target: self, selector: #selector(self.extractPlayStats), userInfo: nil, repeats: true)
        }
    }
    /// This method enabling the Publish stats
    func enablePublishStatsEvent() {
        DispatchQueue.main.async { [self] in
            guard publishStartTimer == nil else { return }
            publishStartTimer = Timer.scheduledTimer(timeInterval: TimeInterval(5), target: self, selector: #selector(self.extractPublishStats), userInfo: nil, repeats: true)
        }
    }
    /// This method extact the statistics data's from peer connection for play
    @objc func extractPlayStats() {
        if providerName == "janus" {
        let peerConnection = janusProvider.remoteConnection
        peerConnection?.rtcPeerConnection?.statistics(completionHandler: { [self] data in

            webRtcStats?.getStats(reports: data, isAudioEnabled: peerConnection?.rtcClient?.remoteAudioTrack?.isEnabled ?? false, isMuted: peerConnection?.rtcClient?.remoteAudioTrack?.isEnabled ?? false, audioState: "\(peerConnection?.rtcClient?.remoteAudioTrack?.readyState.rawValue ?? 0)", isVideoEnabled: peerConnection?.rtcClient?.remoteVideoTrack?.isEnabled ?? false, isVideoMuted: peerConnection?.rtcClient?.remoteVideoTrack?.isEnabled ?? false, videoState: "\(peerConnection?.rtcClient?.remoteVideoTrack?.readyState.rawValue ?? 0)", type: "Play")
        })
        } else if providerName == "antmedia" {
            antmediaProvider.getstatsforPlay { [self] report in
                webRtcStats?.getStats(reports: report, isAudioEnabled: antmediaProvider.getPlayAudioStatus(), isMuted: antmediaProvider.getPlayAudioStatus(), audioState: "\(antmediaProvider.getPlayAudioStatus())", isVideoEnabled: antmediaProvider.getPlayVideoStatus(), isVideoMuted: antmediaProvider.getPlayVideoStatus(), videoState: "\(antmediaProvider.getPlayVideoStatus())", type: "Play")
            }
        }
    }
    /// This method extact the statistics data's from peer connection for publish
    @objc func extractPublishStats() {
        if providerName == "janus" {
        let peerConnection = janusProvider.localConnection
        peerConnection?.rtcPeerConnection?.statistics(completionHandler: { [self] data in
            webRtcStats?.getStats(reports: data, isAudioEnabled: peerConnection?.rtcClient?.localAudioTrack?.isEnabled ?? false, isMuted: peerConnection?.rtcClient?.localAudioTrack?.isEnabled ?? false, audioState: "\(peerConnection?.rtcClient?.localAudioTrack?.readyState.rawValue ?? 0)", isVideoEnabled: isPublish, isVideoMuted: peerConnection?.rtcClient?.localVideoTrack?.isEnabled ?? false, videoState: "\(peerConnection?.rtcClient?.localVideoTrack?.readyState.rawValue ?? 0)", type: "Publish")
        })
        } else if providerName == "antmedia" {
            antmediaProvider.getstatsforPublish { [self] report in
                webRtcStats?.getStats(reports: report, isAudioEnabled: antmediaProvider.getPlayAudioStatus(), isMuted: antmediaProvider.getPlayAudioStatus(), audioState: "\(antmediaProvider.getPlayAudioStatus())", isVideoEnabled: antmediaProvider.getPlayVideoStatus(), isVideoMuted: antmediaProvider.getPlayVideoStatus(), videoState: "\(antmediaProvider.getPlayVideoStatus())", type: "Publish")
            }
        }
        
    }
    /// This method initialise the room call
    /// - Parameter roomCall: NSObject received from socket.
    public func setRoomCall(roomCall: NSObject) {
        streamReferenceId = ""
        agentStreamId = ""
        
        let instance = roomCall.value(forKey: "instance") as? NSObject
        let config = instance?.value(forKey: "config") as? NSObject
        let peerConfig = config?.value(forKey: "peerconnection_config") as? NSObject
        let iceServers = peerConfig?.value(forKey: "iceServers") as! NSArray
        guard let hostIp = instance?.value(forKey: "link") as? String else { return  }
        guard let providerName = instance?.value(forKey: "provider_name") as? String else { return  }
        roomId = roomCall.value(forKey: "ms_room_reference_id") as? String ?? ""
        reconnectingId = roomCall.value(forKey: "ms_room_id") as? Int ?? 0
        let participants = roomCall.value(forKey: "participants") as! [NSObject]
        for value in participants {
            let tempId = value.value(forKey: "participant_id") as? String
            let data = value.value(forKey: "data") as? NSObject
            if self.participantId == tempId {
                streamReferenceId = data?.value(forKey: "stream_reference_id") as? String ?? ""
            } else {
                agentStreamId = data?.value(forKey: "stream_reference_id") as? String ?? ""
            }
        }
        
        var metaDefaults: [String: Any] = [:]
        metaDefaults.updateValue(reconnectingId, forKey: "MSRoomID")
        metaDefaults.updateValue(roomId, forKey: "MSRoomReferenceID")
        metaDefaults.updateValue(streamReferenceId, forKey: "StreamID")
        loggerMsAdapter.setMetaDefaults(metaDefaults: metaDefaults)
        
        self.initServer(providerName: providerName, hostIp: hostIp, roomId: roomId, reconnectingId: reconnectingId, streamReferenceId: streamReferenceId, agentStreamId: agentStreamId, iceServerArray: iceServers)
        
    }
    /// This method handles all disconnect event.
    /// - Parameter envelope: NSObject
    public func onDisconnect(envelope: NSObject) {
        let code = envelope.value(forKey: "code") as! String
        let participant_Id = envelope.value(forKey: "participant_id") as! String
        let message = envelope.value(forKey: "message") as! String
        if code == "reconnects_exhausted" {
            cleanUpProvider()
            self.delegate?.onMSStateCallBack(event: code, object: [] as NSObject)
            
            var logDetails = ModalLogDetails()
            logDetails.service = "RoomChannelEventReceive"
            logDetails.event_name = "Disconnect: \(code)"
            logDetails.event_type = "Received"
            logDetails.event_source = "onDisconnect"
            
            loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
            
        } else if participant_Id.isEmpty {
            self.delegate?.onMSError(code: code, message: message, canReconnect: true)
            
            var logDetails = ModalLogDetails()
            logDetails.service = "RoomChannelEventReceive"
            logDetails.event_name = code
            logDetails.event_source = "onDisconnect"
            logDetails.exceptionName = code
            logDetails.exceptionDescription = message
            
            loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
            
        } else if code == "server_ended_room" {
            cleanUp()
            self.delegate?.onMSStateCallBack(event: code, object: [] as NSObject)
            
            var logDetails = ModalLogDetails()
            logDetails.service = "RoomChannelEventReceive"
            logDetails.event_name = "Disconnect: \(code)"
            logDetails.event_type = "Received"
            logDetails.event_source = "onDisconnect"
            
            loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
            
        } else if code == "left_room" {
            if participant_Id == self.participantId {
                return
            }
            cleanUp()
            self.delegate?.onMSStateCallBack(event: code, object: ["participant_id": participant_Id] as NSObject)
            
            var logDetails = ModalLogDetails()
            logDetails.service = "RoomChannelEventReceive"
            logDetails.event_name = "Disconnect: \(code)"
            logDetails.event_type = "Received"
            logDetails.event_source = "onDisconnect"
            
            loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
            
        } else if code == "exit_room" {
            cleanUp()
            if participant_Id == self.participantId {
                self.delegate?.onMSStateCallBack(event: "USER_EXIT", object: [] as NSObject)
            } else {
                self.delegate?.onMSStateCallBack(event: "operator_exit", object: [] as NSObject)
            }
            
            var logDetails = ModalLogDetails()
            logDetails.service = "RoomChannelEventReceive"
            logDetails.event_name = "Disconnect: \(code)"
            logDetails.event_type = "Received"
            logDetails.event_source = "onDisconnect"
            
            loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
            
        } else {
            cleanUp()
            self.delegate?.onMSError(code: code, message: message, canReconnect: false)
            
            var logDetails = ModalLogDetails()
            logDetails.service = "RoomChannelEventReceive"
            logDetails.event_name = code
            logDetails.event_source = "onDisconnect"
            logDetails.exceptionName = code
            logDetails.exceptionDescription = message
            
            loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        }
    }
    /// This method clean up all connection related part
    public func cleanUp() {
        var logDetails = ModalLogDetails()
        logDetails.service = "cleanUp"
        logDetails.event_name = ""
        logDetails.event_type = "cleanUp"
        logDetails.event_source = "cleanUp"
        
        loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
        msSocket?.disconnectSocket()
        cleanUpProvider()
    }
    /// This method clean all connection and reset.
    public func cleanUpProvider() {
        
        var logDetails = ModalLogDetails()
        logDetails.service = "cleanUpProvider"
        logDetails.event_name = ""
        logDetails.event_type = "cleanUpProvider"
        logDetails.event_source = "cleanUpProvider"
        
        loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
        if !cleanupProviderCalled {
            cleanupProviderCalled = true
            mediaPublishStarted = false
            mediaPlayStarted = false
            // Stop the audio stats
            janusProvider.leaveCurrentRoom()
            janusProvider.reset()
            janusProvider.disconnect()
            cleanupProviderCalled = false
        } else {
            cleanupProviderCalled = false
        }
    }
    /// This method invoked media callback
    /// - Parameter obj: NSObject
    public func invokeMediaFinishedCallback(obj: NSObject) {
        statusEventCallBack(event: "publish_finished", object: obj)
        statusEventCallBack(event: "play_finished", object: obj)
    }
    /// This method handles error callbacks
    /// - Parameters:
    ///   - error: error as string
    ///   - message: error message as string
    ///   - canReconnect: Boolean
    public func msSocketStateErrorCallback(error: String, message: String, canReconnect: Bool) {
        self.delegate?.onMSError(code: error, message: message, canReconnect: canReconnect)
    }
    public func onSocketDisconnected() {
        self.delegate?.onMSError(code: "NETWORK_DISCONNECTED", message: "", canReconnect: false)
    }

    public func onSocketOpen() {

    }

    /// This method attaching the remove view to respective provider
    /// - Parameter renderer: renderer view
    public func attachRemoteView(renderer: RTCVideoRenderer) {
        if providerName == "janus" {
            janusProvider.attachRemoteView(renderer: renderer)
        } else {
            antmediaProvider.attachRemoteView(renderer: renderer)
        }
    }
    /// This method rendering the local video from video component
    /// - Parameters:
    ///   - bufferImage: image received from video component
    ///   - buffers: buffered image
    ///   - status: publish status
    public func renderVideo(bufferImage: UIImage, buffers: CMSampleBuffer, status: Bool) {
        isPublish = status
        let finalPixel: CVPixelBuffer
        let image = bufferImage
        if isPublish {
            stillImageOutput = image
            if dalCapture.getVideoOverlayEnabled() {
            guard let finalImage = generateImageWithText(text: "\(currentDate())", image: image) else { return  }
                finalPixel = buffer(from: finalImage)!
            } else {
                finalPixel = buffer(from: image)!
            }
            
        } else {
            stillImageOutput =  UIImage(named: "Unpublish", in: Bundle(for: type(of: self)), compatibleWith: nil)!
            finalPixel = buffer(from: UIImage(named: "Unpublish", in: Bundle(for: type(of: self)), compatibleWith: nil)!)!
        }
        let timeStamp = Double(buffers.presentationTimeStamp.seconds * Double(NSEC_PER_SEC))
        let rtcPixelBuffer = RTCCVPixelBuffer(pixelBuffer: finalPixel)
        let videoFrame = RTCVideoFrame(buffer: rtcPixelBuffer, rotation: ._0, timeStampNs: Int64(timeStamp))
        if providerName == "janus" {
            janusProvider.didCaptureLocalFrame(videoFrame)
        } else if providerName == "antmedia" {
            antmediaProvider.didCaptureLocalFrame(videoFrame)
        }
    }
    
    /// This method control audio of local media
    /// - Parameter event: Boolean
    public func muteUnMute(event: Bool) {
        if providerName == "janus" {
            event == true ? janusProvider.muteAudio() : janusProvider.unmuteAudio()
        } else if providerName == "antmedia" {
            antmediaProvider.muteUnmuteAudio()
        }
    }
    /// This mothod is used to send Play & Publish webrtc stats events.
    /// - Parameter model: Webrtc Model
    func onWebrtcStatsReceived(model: WebrtcStatsModelClass?) {
        let publish: Bool = model?.data?.type == "Publish"
        let streamId = model?.data?.streamID ?? ""
        let packetLost = (model?.data?.audio?.packetsLost ?? 0) + (model?.data?.video?.packetsLost ?? 0)
        let fractionLost: Int = 0
        let currentTimestamp = String(model?.data?.currentTimestamp ?? 0)
        let audioLevel = model?.data?.audio?.audioLevel ?? 0
        let averageIncomingBitrate = publish ? 0 : Double(model?.data?.bandwidth?.averageBandwidth ?? 0)
        let averageOutgoingBitrate = publish ? Double(model?.data?.bandwidth?.averageBandwidth ?? 0) : 0
        let currentIncomingBitrate = publish ? 0 : Double(model?.data?.bandwidth?.bandwidth ?? 0)
        let currentOutgoingBitrate = publish ? Double(model?.data?.bandwidth?.bandwidth ?? 0) : 0
        
        let data: NetworkSignalModel = NetworkSignalModel(publish: publish, streamId: streamId, packetsLost: packetLost, fractionLost: fractionLost, currentTimestamp: currentTimestamp, audioLevel: audioLevel, averageIncomingBitrate: averageIncomingBitrate, averageOutgoingBitrate: averageOutgoingBitrate, currentIncomingBitrate: currentIncomingBitrate, currentOutgoingBitrate: currentOutgoingBitrate, streamType: "", token: dalCapture.getToken(), ragStatus: "")
        guard let userDict = data.dictionary else { return }
        self.delegate?.onMSStateCallBack(event: "webrtc_stats", object: userDict as! NSObject)
        msSocket?.sendStatsDataPayload(stats: serialiseStats(object: model!))
    }
    /// This method returning screenshot image
    /// - Returns: image of captured frame
    public func screenShot() -> UIImage? {
        return stillImageOutput
    }
    
}
extension MsAdapter: ProviderDelegate {
    
    public func signalingStateChangeNote(state: SignalingConnectionState) {
        
        switch state {
        case .connected:
            print("State : Connected")
        case .cancelled:
            ProgressHUD.dismiss()
            print("State : Cancelled")
        case .disconnected(let reason, let code):
            ProgressHUD.dismiss()
            print("State : Disconnected with: \(reason), code: \(code)")
        case .error(let err):
            ProgressHUD.dismiss()
            print("State : Error \(err?.localizedDescription ?? "No Reason")")
        }
    }
    
}
extension MsAdapter {
    /// This method handle speaker on
    public func speakerOn() {
        if providerName == "janus" {
            janusProvider.speakerOn()
        } else if providerName == "antmedia" {
            antmediaProvider.speakerOn()
        }
    }
    /// This method handle speaker off
    public func speakerOff() {
        if providerName == "janus" {
            janusProvider.speakerOff()
        } else if providerName == "antmedia" {
            antmediaProvider.speakerOff()
        }
    }
    /// This method handle publish local video
    public func showVideo() {
        if providerName == "janus" {
            janusProvider.showVideo()
        } else if providerName == "antmedia" {
            antmediaProvider.showVideo()
        }
    }
    /// This method handle unpublish local video
    public func hideVideo() {
        if providerName == "janus" {
            janusProvider.hideVideo()
        } else if providerName == "antmedia" {
            antmediaProvider.hideVideo()
        }
    }
    /// This method reset the provider
    public func reset() {
        if providerName == "janus" {
            janusProvider.reset()
        } else if providerName == "antmedia" {
            antmediaProvider.reset()
        }
        
    }
//    public func hangUp(user:Bool) {
//        invalidateWebStats()
//        janusProvider.leaveCurrentRoom()
//        janusProvider.reset()
//        if user {
//            janusProvider.disconnect()
//        }
//
//        let logs = ModalLogDetails(service_category: "msAdaptor", service: "HangUp", timestamp: "", event_type: "HangUp", event_name: "", component: "", event_source: "HangUp", logger_session_id: "")
//        loggerMsAdapter.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
    //    }
    /// This method invalidating the webrtc timer's.
    public func invalidateWebStats() {
        playStartTimer?.invalidate()
        playStartTimer = nil
        publishStartTimer?.invalidate()
        publishStartTimer = nil
    }
}
