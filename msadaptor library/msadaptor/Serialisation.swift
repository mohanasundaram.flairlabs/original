//
//  Serialisation.swift
//  app
//
//  Created by Admin on 22/12/21.
//

import Foundation

func serialiseStats(object: WebrtcStatsModelClass) -> [String: Any] {
    var result: [String: Any] = [:]
    let data = object.data
    let connectionTypeLocal = data?.connectionType?.local
    let connectionTypeRemote = data?.connectionType?.remote
    let track = data?.track
    let local: [String: Any] = [
        "candidateType": "\(connectionTypeLocal?.candidateType ?? "")",
        "transport": "\(connectionTypeLocal?.transport ?? "")",
        "ipAddress": "\(connectionTypeLocal?.ipAddress ?? "")",
        "networkType": "\(connectionTypeLocal?.networkType ?? "")",
        "portNumber": "\(connectionTypeLocal?.portNumber ?? 0)"
    ]
    let remote: [String: Any] = [
        "candidateType": "\(connectionTypeRemote?.candidateType ?? "")",
        "transport": "\(connectionTypeRemote?.transport ?? "")",
        "ipAddress": "\(connectionTypeRemote?.ipAddress ?? "")",
        "portNumber": "\(connectionTypeRemote?.portNumber ?? 0)"
    ]
    let bandwidth: [String: Any] = [
        "averageBandwidth": Int(data?.bandwidth?.averageBandwidth ?? 0),
        "bandwidth": Int(data?.bandwidth?.bandwidth ?? 0),
        "availableBandwidth": Int(data?.bandwidth?.availableBandwidth ?? 0)
    ]
    let audio: [String: Any] = [
        "enabled": Bool(track?.audio?.enabled ?? false),
        "muted": Bool(track?.audio?.muted ?? false),
        "label": "\(track?.audio?.label ?? "")",
        "readyState": "\(track?.audio?.readyState ?? "")"
    ]
    let video: [String: Any] = [
        "enabled": Bool(track?.video?.enabled ?? false),
        "muted": Bool(track?.video?.muted ?? false),
        "label": "\(track?.video?.label ?? "")",
        "readyState": "\(track?.video?.readyState ?? "")"
    ]
    let overallAudio: [String: Any] = [
        "audioLevel": Int(data?.audio?.audioLevel ?? 0),
        "totalAudioEnergy": Int(data?.audio?.totalAudioEnergy ?? 0),
        "codec": "\(data?.audio?.codec ?? "")",
        "bytes": Int(data?.audio?.bytes ?? 0),
        "bandwidth": Int(data?.audio?.bandwidth ?? 0),
        "averageBandwidth": Int(data?.audio?.averageBandwidth ?? 0),
        "latency": Int(data?.audio?.latency ?? 0),
        "packetsLost": Int(data?.audio?.packetsLost ?? 0)
    ]
    let overallVideo: [String: Any] = [
        "codec": "\(data?.video?.codec ?? "")",
        "bytes": Int(data?.video?.bytes ?? 0),
        "bandwidth": Int(data?.video?.bandwidth ?? 0),
        "averageBandwidth": Int(data?.video?.averageBandwidth ?? 0),
        "latency": Int(data?.video?.latency ?? 0),
        "packetsLost": Int(data?.video?.packetsLost ?? 0),
        "codecImplementationName": "\(data?.video?.codecImplementationName ?? "")"
    ]
    let resolution: [String: Any] = [
        "width": Int(data?.resolutions?.width ?? 0),
        "height": Int(data?.resolutions?.height ?? 0),
        "bandwidthLimitedResolution": "\(data?.resolutions?.bandwidthLimitedResolution ?? "false")",
        "cpuLimitedResolution": "\(data?.resolutions?.cpuLimitedResolution ?? "false")"
    ]
    let connectionType: [String: Any] = [
        "local": local,
        "remote": remote,
        "rtt": "\(data?.connectionType?.rtt ?? "")"
    ]
    let trackFinal: [String: Any] = [
        "audio": audio,
        "video": video
    ]
    let dataFinal: [String: Any] = [
        "type": "\(data?.type ?? "")",
        "isChrome": Bool(data?.isChrome ?? false),
        "currentTimestamp": Int(data?.currentTimestamp ?? 0),
        "currentTimestampISO": "\(data?.currentTimestampISO ?? "")",
        "connectionType": connectionType,
        "bandwidth": bandwidth,
        "track": trackFinal,
        "audio": overallAudio,
        "video": overallVideo,
        "resolutions": resolution,
        "streamId": "\(data?.streamID ?? "")",
        "MsRoomID": Int(data?.msRoomID ?? 0),
        "MSRoomReferenceID": "\(data?.msRoomReferenceID ?? "")",
        "source": "\(data?.source ?? "")"]

    result = ["data": dataFinal]

    return result
   
    }
