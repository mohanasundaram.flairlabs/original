//
//  LoggerDatabase+CoreDataProperties.swift
//  
//
//  Created by Admin on 04/02/22.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

extension LoggerDatabase {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LoggerDatabase> {
        return NSFetchRequest<LoggerDatabase>(entityName: "LoggerDatabase")
    }

    @NSManaged public var logger: NSObject?

}

extension LoggerDatabase: Identifiable {

}
