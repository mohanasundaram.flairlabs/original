//
//  ViewController.swift
//  captureapp
//
//  Created by Admin on 31/01/22.
//

import UIKit
import capture
import core
import Cleanse
class ViewController: UIViewController,UITextFieldDelegate, HostAppCallBack {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tokenTextField: UITextField!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var submitButton:UIButton!

    var baseView: BaseView?
    private var optionList: [String] = ["Staging","Production"]
    private var selectedOption: [String] = ["Staging"]

    override func viewDidLoad() {
        super.viewDidLoad()

        tokenTextField.text =   "z5uqObAtp1jS"

        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()

        tableView.register(UINib(nibName: "RadioBoxCell", bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: "RadioBoxCell")

        tableView.separatorColor = .clear
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isScrollEnabled = false

        self.tableView.reloadData()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
//        self.contentView.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        contentView.endEditing(true)
    }
    @IBAction func didTapTokenButton(_ sender: UIButton) {
        view.endEditing(true)
        self.view.gestureRecognizers?.forEach(view.removeGestureRecognizer)
        guard tokenTextField.text != "" else { return ProgressHUD.show("Please enter a valid token", icon: .question, interaction: true) }
        DispatchQueue.main.async { [self] in

            let config1 = CaptureConfig.CaptureBuilder(base_url: "https://capture.kyc.idfystaging.com/")
                .setToken(token: tokenTextField.text ?? "")
                .build()

            let config2 = CaptureConfig.CaptureBuilder(base_url: "https://capture.kyc.idfystaging.com/")
                .setToken(token: tokenTextField.text ?? "")
                .enableLogging(enableLogging: true)
                .setLoggerEndpoint(logger_url: "https://capture.kyc.idfystaging.com/js-logger/publish")
                .setCaptureServerUrl(captureServerUrl: "https://capture.kyc.idfystaging.com/backend")
                .setCaptureSocketUrl(captureSocketUrl: "wss://capture.kyc.idfystaging.com/backend")
                .setNetworkCheckBaseUrl(networkCheckBaseUrl: "https://capture.kyc.idfystaging.com/ms-connection-check")
                .setAvkycBackendBaseUrl(avkycBackendBaseUrl: "https://capture.kyc.idfystaging.com/video-kyc-backend")
                .setAvkycSocketUrl(avkycSocketUrl: "wss://capture.kyc.idfystaging.com/video-kyc-backend/socket/task")
                .setSvcServiceBaseUrl(svcServiceBaseUrl: "https://capture.kyc.idfystaging.com/self-video-backend")
                .setSvcSseBaseUrl(svcSseBaseUrl: "https://capture.kyc.idfystaging.com/self-video-sse")
                .setMediaServerUrl(mediaServerUrl: "wss://ms.idfystaging.com/controller/socket")
                .setFrameWidth(frameWidth: 480)
                .setFrameHeight(frameHeight: 640)
                .setFramePerSec(framePerSec: 10)

                .build()

            let production = CaptureConfig.CaptureBuilder(base_url: "https://capture.kyc.idfy.com/")
                .setToken(token: tokenTextField.text ?? "")
                .enableLogging(enableLogging: true)
                .setLoggerEndpoint(logger_url: "https://capture.kyc.idfy.com/js-logger/publish")
                .setCaptureServerUrl(captureServerUrl: "https://capture.kyc.idfy.com/backend")
                .setCaptureSocketUrl(captureSocketUrl: "wss://capture.kyc.idfy.com/backend")
                .setNetworkCheckBaseUrl(networkCheckBaseUrl: "https://capture.kyc.idfy.com/ms-connection-check")
                .setAvkycBackendBaseUrl(avkycBackendBaseUrl: "https://capture.kyc.idfy.com/video-kyc-backend")
                .setAvkycSocketUrl(avkycSocketUrl: "wss://capture.kyc.idfy.com/video-kyc-backend/socket/task")
                .setSvcServiceBaseUrl(svcServiceBaseUrl: "https://capture.kyc.idfy.com/self-video-backend")
                .setSvcSseBaseUrl(svcSseBaseUrl: "https://capture.kyc.idfy.com/self-video-sse")
                .setMediaServerUrl(mediaServerUrl: "wss://ms.idfy.com/controller/socket")
                .setFrameWidth(frameWidth: 480)
                .setFrameHeight(frameHeight: 640)
                .setFramePerSec(framePerSec: 10)

                .build()
            if selectedOption[0] == "Production" {
                baseView = CaptureFactory.startCapture(view:self.view,captureModule: production, delegate: self)
                self.view.addSubview(baseView!)
            } else {
                baseView = CaptureFactory.startCapture(view:self.view,captureModule: config2, delegate: self)
                self.view.addSubview(baseView!)
            }

        }
    }
    func onCaptureSuccess(object: NSObject) {

        if object.value(forKey: "event") as! String == "BackPressed" {
            if self.view.subviews.contains(baseView!) {
                baseView?.removeFromSuperview()
            }
        }
    }
    func onCaptureFailure(object: NSObject) {
        if self.view.subviews.contains(baseView!) {
            baseView?.removeFromSuperview()
        }
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tokenTextField.resignFirstResponder()
        return true;
    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionList.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RadioBoxCell") as? RadioBoxCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none

        cell.configureCell(title: optionList[indexPath.row])
//        cell.contentView.setCornerBorder(color: .black, cornerRadius: 10, borderWidth: 2)
        if selectedOption.contains(self.optionList[indexPath.row]) {
            cell.imageAccessory.image = UIImage(named: "Radio_Icon_Selected", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(.red)
        } else {
            cell.imageAccessory.image = UIImage(named: "Radio_Icon_Unselected", in: Bundle(for: type(of: self)), compatibleWith: .current)
        }
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedOption = [optionList[indexPath.row]]
        self.tableView.reloadData()
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
class RadioBoxCell:UITableViewCell {
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var imageAccessory: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configureCell(title: String) {
        self.tagLabel.text = title
    }
}
