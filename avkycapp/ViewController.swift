//
//  ViewController.swift
//  avkycapp
//
//  Created by Admin on 31/01/22.
//

import Foundation
import AVFoundation
import UIKit
import core
import avkyc

class ViewController: UIViewController, InstructionActionDelegate, AVKYCCallBack {
    @IBOutlet weak var tokenView: UIView!
    @IBOutlet weak var tokenTextField: UITextField!
    @IBOutlet weak var contentCaptureView: UIView!
    @IBOutlet weak var headerBackButton: UIButton!
    @IBOutlet weak var headerTextLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var footerImageView: UIImageView!
    @IBOutlet weak var footerTextLabel: UILabel!
    @IBOutlet weak var contentView: UIView!

    var initiateButton = UIButton()

    var cardButton = UIButton()

    var stackView = UIStackView()

    var captureView = UIView()

    var cardView = UIView()

    var infoView = InstructionView()

    var avkycComponent = AvkycComponent()

    var listPageSeq = [PageSequence]()

    var pageIndex: Int = 0

    var loggerStartTime: Double = 0

    var headerTitle: String = ""

    var loggerWrapper = LoggerWrapper.sharedInstance

    var modelAvkycConfig = ModelAVKYCConfig()

    private var dalCapture: DALCapture {
        DALCapture.shared
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tokenTextField.text = "1OEkuZE_NXXe"
    }
    @IBAction func didTapTokenButton(_ sender: UIButton) {
        view.endEditing(true)
        self.view.gestureRecognizers?.forEach(view.removeGestureRecognizer)
        guard let token = tokenTextField.text else { return ProgressHUD.show("Please enter a valid token", icon: .question, interaction: true)
        checkforCameraAccess()
        self.tokenView.isHidden = true
    }
    }
        func checkforCameraAccess() {
            if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
                checkforMicrophoneAccess()
            } else {
                AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                  if granted == true {
                          self.checkforMicrophoneAccess()
                  } else {
                      self.accessCameraPermission()
                  }
               })
            }
        }
        func checkforMicrophoneAccess() {
            if AVCaptureDevice.authorizationStatus(for: AVMediaType.audio) ==  AVAuthorizationStatus.authorized {
                DispatchQueue.main.async {
                    self.checkforLocationAccess()
                }
            } else {
                AVCaptureDevice.requestAccess(for: AVMediaType.audio, completionHandler: { (granted: Bool) -> Void in
                  if granted == true {
                      DispatchQueue.main.async {
                          self.initialiseCaptureView()
                      }
                  } else {
                      self.accessMicroPhonePermission()
                  }
               })
            }
        }
        func checkforLocationAccess() {
            if let _ = LocationManager.sharedInstance.currentLocation {
                self.initialiseCaptureView()
            } else {
                self.accessLocationPermission()
            }
        }
    func initialiseCaptureView() {

        ProgressHUD.show()

        infoViewInit()

        initLogger()

        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "TokenAuth"
        logDetails.event_type = "Authenticate"
        logDetails.event_name = ""
        logDetails.event_source = "apiCaptureStatusDetails"
        logDetails.component = "DataService Core"

        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        loggerStartTime = Double(Date.currentTimeStamp)

        dalCapture.getCaptureStatusDetails(baseUrl: modelAvkycConfig.getAvkyc_socket_url(), sessionOverride: false) { [self] success, _ in

            if let data = success {
                ProgressHUD.dismiss()
                self.renderCaptureUi(object: data)
            } else {
                DispatchQueue.main.async { [self] in
                    ProgressHUD.dismiss()
                    infoView.loadNetworkDisconnectedStatusView()
                    contentView.addSubview(infoView)
                }
            }
        }
    }
    func infoViewInit() {
        captureView.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        infoView.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        infoView.delegate = self
    }
    func initLogger() {

        // Create instance of logger
        loggerWrapper.createLogger(key: "capture", token: tokenTextField.text ?? "", loggerUrl: modelAvkycConfig.getAvkyc_logger_url())
        loggerWrapper.createLogger(key: "pg", token: tokenTextField.text ?? "", loggerUrl: modelAvkycConfig.getAvkyc_logger_url())
        loggerWrapper.createLogger(key: "vkyc", token: tokenTextField.text ?? "", loggerUrl: modelAvkycConfig.getAvkyc_logger_url())
        loggerWrapper.createLogger(key: "healthCheck", token: tokenTextField.text ?? "", loggerUrl: modelAvkycConfig.getAvkyc_logger_url())

        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "LogUserAgent"
        logDetails.event_type = "Profile.UA"
        logDetails.event_name = "iOS app " + ApplicationController.deviceModel
        logDetails.event_source = "init"
        logDetails.component = "loggerWrapper"

        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])

    }
    func renderCaptureUi(object: NSObject) {
        let headerTitle = object.value(forKey: "title") as? String
        if  ThemeConfig.shared.getDisplayFooter() {
            footerView.isHidden = false
        }
//        contentCaptureView.backgroundColor = hexStringToUIColor(hex: "\( ThemeConfig.shared.getSecondaryMainColor())")
        self.footerView.backgroundColor = hexStringToUIColor(hex: "\( ThemeConfig.shared.getSecondaryMainColor())")

        renderToolBar(headerTitle: headerTitle ?? "")
    }
    func renderToolBar(headerTitle: String) {

        // send logs: after apiCaptureStatusDetails load time
        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "TokenAuth"
        logDetails.event_name = dalCapture.getTatSince(start: loggerStartTime)
        logDetails.event_type = "Auth.TAT"
        logDetails.event_source = "apiCaptureStatusDetails"
        logDetails.component = "DataService Core"

        // set defaults
        var logDefaults = ModalLogDetails()
        logDefaults.reference_id = dalCapture.getCaptureId()
        logDefaults.reference_type = "CaptureID"
        loggerWrapper.setDefault(key: "capture", defaultLog: logDefaults)

        // set default meta
        var metaDefaults: [String: Any] = [:]
        metaDefaults.updateValue(dalCapture.getCaptureId(), forKey: "capture_id")
        metaDefaults.updateValue(dalCapture.getStatus() ?? "", forKey: "profile_status")
        metaDefaults.updateValue(dalCapture.getCaptureSessionId(), forKey: "session_id")
        loggerWrapper.setMetaDefaults(key: "capture", metaDefaults: metaDefaults)

        // set default and meta healthcheck
        loggerWrapper.getLoggers(key: "healthCheck")?.setDefaults(defaultLogDetails: logDefaults)
        loggerWrapper.getLoggers(key: "healthCheck")?.setMetaDefaults(metaDefaults: metaDefaults)

        // set default and meta vs
        loggerWrapper.getLoggers(key: "vs")?.setDefaults(defaultLogDetails: logDefaults)
        loggerWrapper.getLoggers(key: "vs")?.setMetaDefaults(metaDefaults: metaDefaults)

        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])

        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        headerBackButton.isHidden = true
        headerImageView.isHidden = false
        headerImageView.SetLogoImage( ThemeConfig.shared.getLogo())
        headerTextLabel.text = "Capture Details"
        self.headerTitle = headerTitle

        footerImageView.SetLogoImage( ThemeConfig.shared.getLogo())

        contentView.backgroundColor = hexStringToUIColor(hex: "\(  ThemeConfig.shared.getSecondaryMainColor())")

        // send logs: after apiCaptureStatusDetails Authenticated
        var logDetail = ModalLogDetails()
        logDetail.service_category = "CaptureSDK"
        logDetail.service = "TokenAuth"
        logDetail.event_name = ""
        logDetail.event_type = "Authenticated"
        logDetail.event_source = "apiCaptureStatusDetails"
        logDetail.component = "DataService Core"
        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetail, meta: [:])

        processCapture()

    }
    func processCapture() {
        let status_string = Status(rawValue: dalCapture.getStatus() ?? "")
        switch status_string {
        case .Status_Approved, .Status_Processed:
            statusViewAssign(type: .Status_Success, status: status_string?.rawValue ?? "")
            break
        case .Status_Initiated, .Status_In_Progress, .Status_Review, .Status_Review_Required:
            statusViewAssign(type: .Status_Pending, status: status_string?.rawValue ?? "")
            break
        case .Status_Cancelled, .Status_Capture_Expired, .Status_Failed, .Status_Rejected:
            statusViewAssign(type: .Status_Failed, status: status_string?.rawValue ?? "")
            break
        case .Status_Capture_Pending, .Status_Recapture_Pending:
            getPageData()
            break
        default:
            statusViewAssign(type: .Default, status: status_string?.rawValue ?? "")
            break
        }
    }

    func statusViewAssign(type: StatusType, status: String) {
        switch type {
        case .Status_Failed:
            self.renderSecondaryUI(fontColor: .red, status: status)
            break
        case .Status_Pending:
            self.renderSecondaryUI(fontColor: .orange, status: status)
            break
        case .Status_Success:
            self.renderSecondaryUI(fontColor: .green, status: status)
            break
        case .Default:
            self.renderSecondaryUI(fontColor: .white, status: status)
            break
        }
    }
    internal func renderSecondaryUI(fontColor: UIColor, status: String) {
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 50, width: 200, height: 20))
        headerLabel.text = "Status"
        headerLabel.textAlignment = .left
        headerLabel.textColor = .lightGray
        headerLabel.font = UIFont(name: headerLabel.font.fontName, size: 20)
        self.contentView.addSubview(headerLabel)
        let statusName = UILabel(frame: CGRect(x: 30, y: 80, width: 200, height: 20))
        statusName.text = status.startcased()
        statusName.textAlignment = .left
        statusName.textColor = fontColor
        statusName.font = UIFont(name: headerLabel.font.fontName, size: 20)
        self.contentView.addSubview(statusName)
        ProgressHUD.dismiss()
    }
    internal func getPageData() {

        // send logs: after apiCaptureStatusDetails Authenticated
        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "ConnectProfileGateway"
        logDetails.event_name = "Connect"
        logDetails.event_type = "SocketState"
        logDetails.event_source = "setupCaptureSocket"
        logDetails.component = "DataService Core"
        logDetails.reference_id = dalCapture.getCaptureId()
        logDetails.reference_type = "AV.TaskID"

        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])

        var logDetail = ModalLogDetails()
        logDetail.service_category = "CaptureSDK"
        logDetail.service = "ConnectProfileGateway"
        logDetail.event_name = "Invoked"
        logDetail.event_type = "Initiate"
        logDetail.event_source = "setupVKycSocket"
        logDetail.component = "DataService Core"
        logDetail.reference_id = dalCapture.getCaptureId()
        logDetail.reference_type = "AV.TaskID"

        var meta: [String: Any] = [:]
        var param: [String: Any] = [:]
        param.updateValue(dalCapture.getToken(), forKey: "t" )
        param.updateValue(dalCapture.getCaptureId(), forKey: "capture_id")
        param.updateValue(dalCapture.getCaptureSessionId(), forKey: "session_token")
        meta.updateValue(param, forKey: "param")
        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)

        dalCapture.getPageSequence(socketUrl: modelAvkycConfig.getAvkyc_socket_url()) { [self] onSessionSuccess, _, failureCallBack in

            if let success = onSessionSuccess {
                if listPageSeq.count == 0 {
                    parsePageSequence(object: success)

                    var logDetails = ModalLogDetails()
                    logDetails.service_category = "captureSDK"
                    logDetails.service = "ConnectProfileGateway"
                    logDetails.event_name = "Session"
                    logDetails.event_type = "ChannelJoined"
                    logDetails.event_source = "setupCaptureSocket"
                    logDetails.component = "DataService Core"
                    logDetails.reference_id = dalCapture.getCaptureId()
                    logDetails.reference_type = "AV.TaskID"
                    loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])

                    let page = listPageSeq[pageIndex].page
                    var payloadObject = NSObject()
                    payloadObject = ["page": page ?? "", "payload": []] as NSObject
                    fetchPageConfig(page: page ?? "0", payloadObject: payloadObject)
                }
            } else if failureCallBack != nil {

                var logDetails = ModalLogDetails()
                logDetails.service_category = "captureSDK"
                logDetails.service = "TokenAuth"
                logDetails.event_type = ""
                logDetails.event_name = "'MULTIPLE_SESSIONS'"
                logDetails.event_source = "apiCaptureStatusDetails"
                logDetails.component = "DataService Core"
                loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])

                //                DispatchQueue.main.async { [self] in
                //                ProgressHUD.dismiss()
                //                infoView.loadNetworkDisconnectedStatusView()
                //                contentView.addSubview(infoView)
                //                }
            }
        }
    }
    func parsePageSequence(object: NSObject?) {
        do {
            listPageSeq.removeAll()
            pageIndex = 0
            let pageSequence = object?.value(forKey: "page_sequence") as! NSArray
            for(_, value) in pageSequence.enumerated() {
                let jsonObject = value as! NSObject
                let page = jsonObject.value(forKey: "page") as? String
                let validations = jsonObject.value(forKey: "validations") as! NSObject
                listPageSeq.append(PageSequence(page: page ?? "", validations: validations))
            }
        }
    }
    func fetchPageConfig(page: String, payloadObject: NSObject) {

        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "FetchConfiguration"
        logDetails.event_name = page
        logDetails.event_type = "Fetch"
        logDetails.event_source = "FetchPageConfigFromBackend"
        logDetails.component = "DataService Core"
        var metaData: [String: Any] = [:]
        metaData.updateValue(JsonToMap(value: payloadObject), forKey: "data")
        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: metaData)

        dalCapture.fetchPageConfig(page: page, event: "session:fetch_config", jsonObject: payloadObject) { success, _, _ in
            if let object = success {

                var logDetails = ModalLogDetails()
                logDetails.service_category = "captureSDK"
                logDetails.service = "ValidationsCheck"
                logDetails.event_name = page
                logDetails.event_type = "Received"
                logDetails.event_source = "FetchPageConfigFromBackend"
                logDetails.component = "DataService Core"
                metaData.updateValue(metaData["data"]!, forKey: "inputs")
                metaData.removeValue(forKey: "data")

                self.loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: metaData)

                switch page {
                case "introduction":
                    self.renderCustomUI(object: object)
                    break
                case "consent":
                    self.renderCustomUI(object: object)
                    break
                case "capture":
                    self.renderCapture(object: object)
                    break
                case "permissions":
                    break
                case "vkyc.assisted_vkyc":
                    self.startAvkyc(object: object)
                    break
                default:
                    break
                }
            }
        }
    }
    func renderCustomUI(object: NSObject) {
        let jsonData = object.value(forKey: "response") as? NSObject
        let jsonObject = jsonData?.value(forKey: "data") as? NSObject
        let page = jsonObject?.value(forKey: "page") as? String ?? ""
        guard let configObject = jsonObject?.value(forKey: "config") as? NSObject else { return  }

        DispatchQueue.main.async {
        self.parseMasterIntroductionPage(object: configObject, pageName: page)
        }

    }
    func  parseMasterIntroductionPage(object: NSObject, pageName: String) {

        let titleLabel = UILabel(frame: CGRect(x: 20.0, y: 60.0, width: contentView.frame.width - 20, height: 30.0))
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        titleLabel.textColor = UIColor.white
        titleLabel.text = object.value(forKey: "title") as? String
        contentView.addSubview(titleLabel)

        let textLabel = UILabel(frame: CGRect(x: 20.0, y: 60.0, width: contentView.frame.width - 20, height: 200.0))
        textLabel.font = UIFont.systemFont(ofSize: 17)
        textLabel.textColor = UIColor.white
        let text = object.value(forKey: "text") as? String
        textLabel.text = text?.htmlToString
        textLabel.numberOfLines = 0
        contentView.addSubview(textLabel)
        initiateButton.titleLabel?.alpha = 0.5
        initiateButton.setTitleColor(.white, for: .normal)
        initiateButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)
        initiateButton.setTitle("PROCEED".uppercased(), for: .normal)
        initiateButton.backgroundColor = hexStringToUIColor(hex: "#4a599b")
        contentView.addSubview(initiateButton)
        initiateButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        initiateButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addConstraints(generateConstraintsInviteButton(buttonView: initiateButton, mainView: contentView))
    }
    @objc func buttonPressed(sender: UIButton!) {
        let page = onNextPage()
        print(page)

        var payloadObject = NSObject()
        payloadObject = ["page": page, "payload": []] as NSObject
        fetchPageConfig(page: page, payloadObject: payloadObject)
    }
    func renderCapture(object: NSObject) {
        DispatchQueue.main.async {
            self.contentView.removeSubviews()
        }
        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "RoutePage"
        logDetails.event_name = ""
        logDetails.component = "IndvCaptureComponent"
        logDetails.event_source = "setCurrentPage"

        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])

        let jsonData = object.value(forKey: "response") as? NSObject
        let jsonObject = jsonData?.value(forKey: "data") as? NSObject
        let configObject = jsonObject?.value(forKey: "config") as? NSObject
        if let value = configObject?.value(forKey: "capture") as? NSObject {
            print(value)
            self.parseCaptureResponse(object: configObject!)
        }

    }
    func parseCaptureResponse(object: NSObject) {
        print(object)
        let captureArray = object.value(forKey: "capture") as! NSArray
        for(i, _) in captureArray.enumerated() {
            let jsonArray = captureArray[i] as! NSArray
            for(j, _) in jsonArray.enumerated() {
                let jsonObject = jsonArray[j] as! NSObject
                let templateId = jsonObject.value(forKey: "template_id") as! Int
                let templateObject = object.value(forKey: "templates") as! NSObject

                let idObject = templateObject.value(forKey: "\(templateId)") as? NSObject

                let type = idObject?.value(forKey: "type") as! String
                let title = idObject?.value(forKey: "title") as! String
                let detail = "" // idObject?.value(forKey: "detail") as! String

                if type == "card" {
                    DispatchQueue.main.async {
                        self.renderCardUI(title: title, detail: detail, i: j, secondaryMainColor: ThemeConfig.shared.getSecondaryMainColor(), secondaryContrastColor: ThemeConfig.shared.getSecondaryContrastColor())
                    }
                    let jsonArrrays = jsonObject.value(forKey: "tasks") as! NSArray
                    var template_id = 0
                    for(index, _) in jsonArrrays.enumerated() {
                        let taskUI = jsonArrrays[index] as? NSObject
                        template_id = taskUI?.value(forKey: "template_id") as! Int
                        let taskIdObject = templateObject.value(forKey: "\(template_id)") as? NSObject
                        let taskType = taskIdObject?.value(forKey: "type") as? String
                        let taskKey = taskIdObject?.value(forKey: "task_key") as? String
                        let taskLabel = taskIdObject?.value(forKey: "label") as? String
                        if taskType == "button" {
                            DispatchQueue.main.async {
                                self.renderCaptureButton(taskLabel: taskLabel ?? "", taskKey: taskKey ?? "", mainColor: ThemeConfig.shared.getPrimaryMainColor(), textColor: ThemeConfig.shared.getPrimaryContrastColor(), id: index)
                            }
                        } else if taskType == "text" {

                        } else if taskType == "image" {

                        } else if taskType == "group" {

                        } else if taskType == "form" {

                        }

                    }
                }

            }

        }

    }

    func renderCardUI(title: String, detail: String, i: Int, secondaryMainColor: String, secondaryContrastColor: String) {

        cardView.backgroundColor =   hexStringToUIColor(hex: "\(secondaryMainColor)")
        cardView.layer.cornerRadius = 20.0
        cardView.layer.shadowColor = UIColor.black.cgColor
        cardView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cardView.layer.shadowRadius = 6.0
        cardView.layer.shadowOpacity = 0.7
        cardView.tag = 1
        captureView.addSubview(cardView)
        cardView.translatesAutoresizingMaskIntoConstraints = false
        captureView.addConstraints(generateConstraintsCardView(cardView: cardView, mainView: captureView))

        let cardLabel = UILabel()
        cardLabel.text = title
        cardLabel.textAlignment = .center
        cardLabel.textColor = hexStringToUIColor(hex: "\(secondaryContrastColor)")
        cardLabel.font = UIFont(name: cardLabel.font.fontName, size: 20)
        cardView.addSubview(cardLabel)
        cardLabel.translatesAutoresizingMaskIntoConstraints = false
        cardView.addConstraints(generateConstraintsCardViewLabel(labelView: cardLabel, mainView: cardView))

        initiateButton.titleLabel?.alpha = 0.5
        initiateButton.setTitleColor(.white, for: .normal)
        initiateButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)
        initiateButton.setTitle("Initiate Verification".uppercased(), for: .normal)
        initiateButton.isEnabled = dalCapture.getCallCompletedStatus()
        if initiateButton.isEnabled {
            initiateButton.backgroundColor = hexStringToUIColor(hex: "#4a599b")
        } else {
            initiateButton.backgroundColor = .darkGray
        }
        captureView.addSubview(initiateButton)
        initiateButton.addTarget(self, action: #selector(initiateButtonPressed), for: .touchUpInside)
        initiateButton.translatesAutoresizingMaskIntoConstraints = false
        captureView.addConstraints(generateConstraintsInviteButton(buttonView: initiateButton, mainView: captureView))
        ProgressHUD.dismiss()

        contentView.addSubview(captureView)

    }
    @objc func initiateButtonPressed(sender: UIButton!) {
        sender.isEnabled = dalCapture.getCallCompletedStatus() ? true : false
        if sender.isEnabled == true {
            dalCapture.setCallCompletedStatus(status: false)
            dalCapture.submitArtifacts { onResponse in
                self.infoView.loadKycStatusView(title: "Thank You", message: "")
                contentView.addSubview(infoView)
            }

        }
    }
    func renderCaptureButton(taskLabel: String, taskKey: String, mainColor: String, textColor: String, id: Int) {
        cardButton.isEnabled = dalCapture.getCallCompletedStatus()
        if !cardButton.isEnabled {
            cardButton.isEnabled = true
            cardButton.backgroundColor = hexStringToUIColor(hex: "#4a599b")
        } else {
            cardButton.backgroundColor = .darkGray
        }
        cardButton.setCornerBorder(color: .white, cornerRadius: 10, borderWidth: 0)
        cardButton.setTitle(taskLabel.uppercased(), for: .normal)
        cardButton.setTitleColor(.black, for: .normal)
        cardView.addSubview(cardButton)
        if taskKey == "vkyc.assisted_vkyc" {
            cardButton.addTarget(self, action: #selector(cardButtonPressed), for: .touchUpInside)
        }
        cardButton.translatesAutoresizingMaskIntoConstraints = false
        cardView.addConstraints(generateConstraintsCardButton(buttonView: cardButton, mainView: cardView))
    }
    @objc func cardButtonPressed(sender: UIButton!) {

        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "InitiateAVKYC"
        logDetails.event_type = "Clicked"
        logDetails.event_name = "StartVKYCcoupled"
        logDetails.component = "ButtonComponent"
        logDetails.event_source = "handleClick"
        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])

        if onNextPage() == "PAGE_END" {
            var payloadObject = NSObject()
            payloadObject = ["page": "vkyc.assisted_vkyc", "payload": []] as NSObject
            self.fetchPageConfig(page: "vkyc.assisted_vkyc", payloadObject: payloadObject)
        }

    }

    func didSetRetryButton(screen: RetryOptions) {
        DispatchQueue.main.async { [self] in
            if contentView.subviews.contains(infoView) {
                infoView.removeFromSuperview() // Remove it
            }
        }

    }
    @IBAction private func btnBackPressed(_ sender: UIButton) {
        BackPressed()
    }
    func BackPressed() {
        if avkycComponent.isDescendant(of: contentView) {
            avkycComponent.BackPressed()
        } else {
            self.tokenView.isHidden = false
            loggerWrapper.clearLoggers()
        }
    }
    func updateHeader() {
        headerBackButton.isHidden = true
        headerImageView.isHidden = false
    }
    func startAvkyc(object: NSObject) {
        DispatchQueue.main.async { [self] in
            headerTextLabel.text = headerTitle
            avkycComponent = AvkycComponent()
            avkycComponent.initialiseAvkyc(modelAvkycConfig: modelAvkycConfig, view: self.contentView, data: object, delegate: self)
            contentView.addSubview(avkycComponent)
            headerBackButton.isHidden = false
            headerImageView.isHidden = true
        }
    }
    func onAssistedSuccess(object: NSObject) {
        let value = object.value(forKey: "event") as! String
        switch value {
        case "CALL_END", "USER_END_CALL", "AGENT_ENDED_CALL":
            if captureView.isDescendant(of: contentView) {
                updateCaptureView()
            } else {
                removeallViews()
                infoView.loadKycStatusView(title: "Thank You", message: "")
                contentView.addSubview(infoView)
                dalCapture.disconnectCaptureSocket()
            }
            break
        default:
            break
        }
    }
    func removeallViews() {
        DispatchQueue.main.async { [self] in
            if avkycComponent.isDescendant(of: contentView) {
                avkycComponent.removeFromSuperview() // Remove it
            }
            if captureView.isDescendant(of: contentView) {
                captureView.removeFromSuperview() // Remove it
            }
            self.updateHeader()
        }
    }
    func onAssistedIntermediate(object: NSObject) {
        let value = object.value(forKey: "event") as! String
        if value == "VKYC_INITIATED" {
        } else if value == "VIDEOCOMPONENT_INITIATED" {
        } else if value == "USER_END_CALL" {
        } else if value == "USER_RECONNECT" {
        } else if value == "DISCONNECT" {
        } else if value == "USER_BACKPRESS" {
            if captureView.isDescendant(of: contentView) {
                captureView.removeFromSuperview()
            }
            if avkycComponent.isDescendant(of: contentView) {
                avkycComponent.removeFromSuperview()
            }
            dalCapture.disconnectCaptureSocket()
            self.tokenView.isHidden = false
        }
    }

    func onAssistedFailure(object: NSObject) {
        DispatchQueue.main.async { [self] in
            if avkycComponent.isDescendant(of: contentView) {
                avkycComponent.removeFromSuperview()
            }
        }
        let value = object.value(forKey: "event") as! String
        if value == "RECONNECTS_EXHAUSTED" {
            updateCaptureView()
            self.infoView.loadNetworkDisconnectedStatusView()
            self.contentView.addSubview(infoView)
        } else if value == "NETWORK_DISCONNECTED" {
            updateCaptureView()
            self.infoView.loadNetworkDisconnectedStatusView()
            self.contentView.addSubview(infoView)
        } else if value == "USER_EXIT" {
            if captureView.isDescendant(of: contentView) {
            updateCaptureView()
            } else {
                self.tokenView.isHidden = false
            }
        } else if value == "OPERATOR_EXIT" {
            updateCaptureView()
            self.infoView.loadNetworkDisconnectedStatusView()
            self.contentView.addSubview(infoView)
        }
    }
    func updateCaptureView() {
        removeallViews()
        let page = listPageSeq[0].page
        var payloadObject = NSObject()
        payloadObject = ["page": page ?? "", "payload": []] as NSObject
        fetchPageConfig(page: page ?? "0", payloadObject: payloadObject)
    }
    private func logGrantedPermission(eventName: String, eventSource: String) {

        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "PermissionsCheck"
        logDetails.event_name = eventName
        logDetails.event_type = "Granted"
        logDetails.event_source = eventSource
        logDetails.component = "PermissionsService"
        logDetails.publish_to_dlk = true

        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
    }
    private func logDeniedPermission(eventName: String, eventSource: String, errorMessage: String) {

        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "PermissionsCheck"
        logDetails.event_name = eventName
        logDetails.event_type = "Denied"
        logDetails.event_source = eventSource
        logDetails.component = "PermissionsService"
        logDetails.publish_to_dlk = true

        var meta: [String: Any] = [:]
        meta.updateValue(errorMessage, forKey: "error_message")

        loggerWrapper.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)

    }

}
extension ViewController {
    func onNextPage() -> String {
        pageIndex = pageIndex + 1
        if pageIndex < listPageSeq.count {
            return listPageSeq[pageIndex].page ?? "0"
        }
        return "PAGE_END"
    }
    func accessCameraPermission() {
        DispatchQueue.main.async {
            self.showAlertForCameraAccess(completion: { success in
                if success == true {
                    AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                        if granted == true {
                            self.checkforMicrophoneAccess()
                            self.logGrantedPermission(eventName: "Camera", eventSource: "getCameraPermission")
                        } else {
                            DispatchQueue.main.async {
                                self.showAlert(title: "", message: "Without camera access can't proceed.", actionTitle: "Ok", completion: { _ in
                                    self.logDeniedPermission(eventName: "Camera", eventSource: "accessCameraPermission", errorMessage: "userDeniedPermission")
                                    // callback
                                })
                            }
                        }
                    })
                } else {
                    // callback
                }
            })
        }
    }
    func accessMicroPhonePermission() {
        DispatchQueue.main.async {
            self.showAlertForMicroPhoneAccess(completion: { [self] success in
                if success == true {
                    AVCaptureDevice.requestAccess(for: AVMediaType.audio, completionHandler: { (granted: Bool) -> Void in
                        if granted == true {
                            checkforLocationAccess()
                            logGrantedPermission(eventName: "Microphone", eventSource: "getMicrophonePermission")
                        } else {
                            DispatchQueue.main.async {
                                self.showAlert(title: "", message: "Without microphone access can't proceed.", actionTitle: "Ok", completion: { _ in
                                    logDeniedPermission(eventName: "Microphone", eventSource: "accessMicroPhonePermission", errorMessage: "userDeniedPermission")
                                    // callback
                                })
                            }
                        }
                    })
                } else {
                   // callback
                }
            })
        }
    }
    func accessLocationPermission() {
        DispatchQueue.main.async {
            self.showAlertForLocationAccess(completion: { success in
                if success == true {
                    if let _ = LocationManager.sharedInstance.currentLocation {
                        self.initialiseCaptureView()
                        self.logGrantedPermission(eventName: "Location", eventSource: "getLocationPermission")
                    } else {
                        DispatchQueue.main.async {
                            self.showAlert(title: "", message: "Without location access can't proceed.", actionTitle: "Ok", completion: { _ in
                                self.logDeniedPermission(eventName: "Location", eventSource: "accessLocationPermission", errorMessage: "userDeniedPermission")
                                // callback
                            })
                        }
                    }
                } else {
                    // callback
                }
            })
        }
    }
}

func generateConstraintsCardView(cardView: UIView, mainView: UIView) -> [NSLayoutConstraint] {
    let constraintTop = NSLayoutConstraint(item: cardView,
                                           attribute: .top,
                                           relatedBy: .equal,
                                           toItem: mainView,
                                           attribute: .top,
                                           multiplier: 1.0,
                                           constant: 80)
    let constraintRight = NSLayoutConstraint(item: cardView,
                                             attribute: .trailing,
                                             relatedBy: .equal,
                                             toItem: mainView,
                                             attribute: .trailing,
                                             multiplier: 1.0,
                                             constant: -20.0)
    let constraintLeft = NSLayoutConstraint(item: cardView,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: mainView,
                                            attribute: .leading,
                                            multiplier: 1.0,
                                            constant: 20.0)
    let constraintHeight = NSLayoutConstraint(item: cardView,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 150)

    return [constraintTop, constraintHeight, constraintRight, constraintLeft]
}
 func generateConstraintsCardViewLabel(labelView: UILabel, mainView: UIView) -> [NSLayoutConstraint] {

    let constraintRight = NSLayoutConstraint(item: labelView,
                                             attribute: .trailing,
                                             relatedBy: .equal,
                                             toItem: mainView,
                                             attribute: .trailing,
                                             multiplier: 1.0,
                                             constant: -40.0)
    let constraintLeft = NSLayoutConstraint(item: labelView,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: mainView,
                                            attribute: .leading,
                                            multiplier: 1.0,
                                            constant: 40.0)
    let constraintHeight = NSLayoutConstraint(item: labelView,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 40)
    let constraintBottom = NSLayoutConstraint(item: labelView,
                                              attribute: .top,
                                              relatedBy: .equal,
                                              toItem: mainView,
                                              attribute: .top,
                                              multiplier: 1.0,
                                              constant: 30)

    return [constraintHeight, constraintRight, constraintLeft, constraintBottom]
}
 func generateConstraintsInviteButton(buttonView: UIButton, mainView: UIView) -> [NSLayoutConstraint] {

    let constraintRight = NSLayoutConstraint(item: buttonView,
                                             attribute: .trailing,
                                             relatedBy: .equal,
                                             toItem: mainView,
                                             attribute: .trailing,
                                             multiplier: 1.0,
                                             constant: -40.0)
    let constraintLeft = NSLayoutConstraint(item: buttonView,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: mainView,
                                            attribute: .leading,
                                            multiplier: 1.0,
                                            constant: 40.0)
    let constraintHeight = NSLayoutConstraint(item: buttonView,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 40)
    let constraintBottom = NSLayoutConstraint(item: buttonView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: mainView,
                                              attribute: .bottom,
                                              multiplier: 1.0,
                                              constant: -40)

    return [constraintHeight, constraintRight, constraintLeft, constraintBottom]
}

 func generateConstraintsCardButton(buttonView: UIButton, mainView: UIView) -> [NSLayoutConstraint] {

    let constraintRight = NSLayoutConstraint(item: buttonView,
                                             attribute: .trailing,
                                             relatedBy: .equal,
                                             toItem: mainView,
                                             attribute: .trailing,
                                             multiplier: 1.0,
                                             constant: -40.0)
    let constraintLeft = NSLayoutConstraint(item: buttonView,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: mainView,
                                            attribute: .leading,
                                            multiplier: 1.0,
                                            constant: 40.0)
    let constraintHeight = NSLayoutConstraint(item: buttonView,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 40)
    let constraintBottom = NSLayoutConstraint(item: buttonView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: mainView,
                                              attribute: .bottom,
                                              multiplier: 1.0,
                                              constant: -30)

    return [constraintHeight, constraintRight, constraintLeft, constraintBottom]
}

private func createConfig() -> [String: Any] {
    var configObj: [String: Any] = [:]
    configObj.updateValue("https://capture.kyc.idfystaging.com/js-logger/publish", forKey: "APP_LOGGER_URL")
    configObj.updateValue(true, forKey: "APP_ENABLE_LOGGING")
    configObj.updateValue("wss://capture.kyc.idfystaging.com/backend/socket/capture/websocket?", forKey: "APP_CAPTURE_SOCKET_URI")
    configObj.updateValue("wss://capture.kyc.idfystaging.com/video-kyc-backend/socket/task/websocket?", forKey: "APP_VKYC_SOCKET_URI")
    configObj.updateValue("wss://ms.idfystaging.com/controller/socket/websocket?", forKey: "APP_MEDIA_SERVER_URI")
    configObj.updateValue("https://capture.kyc.idfystaging.com/", forKey: "APP_API_SERVER_URI")
    configObj.updateValue(30000, forKey: "APP_SOCKET_RECONNECT_TIMEOUT")
    configObj.updateValue(480, forKey: "FRAME_WIDTH")
    configObj.updateValue(640, forKey: "FRAME_HEIGHT")
    configObj.updateValue(10, forKey: "FRAME_PER_SEC")

    return configObj
}
