//
//  WebRTCClient.swift
//  AntMediaSDK
//
//  Created by Oğulcan on 6.06.2018.
//  Copyright © 2018 AntMedia. All rights reserved.
//

import Foundation
import AVFoundation
import WebRTC
import ReplayKit

class WebRTCClient: NSObject {
    
    let VIDEO_TRACK_ID = "VIDEO"
    let AUDIO_TRACK_ID = "AUDIO"
    let LOCAL_MEDIA_STREAM_ID = "STREAM"
    
    private static let factory: RTCPeerConnectionFactory = {
        RTCInitializeSSL()
        let videoEncoderFactory = RTCDefaultVideoEncoderFactory()
        let videoDecoderFactory = RTCDefaultVideoDecoderFactory()
        return RTCPeerConnectionFactory(encoderFactory: videoEncoderFactory, decoderFactory: videoDecoderFactory)
    }()
    
    var delegate: WebRTCClientDelegate?
    var peerConnection: RTCPeerConnection?
    
    private var videoCapturer: RTCVideoCapturer?
    var localVideoTrack: RTCVideoTrack!
    var localAudioTrack: RTCAudioTrack!
    var remoteVideoTrack: RTCVideoTrack!
    var remoteAudioTrack: RTCAudioTrack!
    var remoteVideoView: RTCVideoRenderer?
    var localVideoView: RTCVideoRenderer?
    var videoSender: RTCRtpSender?
    var dataChannel: RTCDataChannel?
    private let rtcAudioSession =  RTCAudioSession.sharedInstance()
    private let audioQueue = DispatchQueue(label: "audio")
    
    private var localVideoSource: RTCVideoSource?
    
    private var token: String!
    private var streamId: String!

    private var audioEnabled: Bool = true
    private var videoEnabled: Bool = true
    private var captureScreenEnabled: Bool = false
    private var mode: AntMediaClientMode = AntMediaClientMode.join
    
    private var enableDataChannel: Bool = false
    
    private var cameraPosition: AVCaptureDevice.Position = .front
    
    private var targetWidth: Int = 480
    private var targetHeight: Int = 360
    
    public init(remoteVideoView: RTCVideoRenderer?, localVideoView: RTCVideoRenderer?, delegate: WebRTCClientDelegate) {
        super.init()
        
        self.remoteVideoView = remoteVideoView
        self.localVideoView = localVideoView
        self.delegate = delegate
        
        RTCPeerConnectionFactory.initialize()
        
        let stunServer = Config.defaultStunServer()
        let defaultConstraint = Config.createDefaultConstraint()
        let configuration = Config.createConfiguration()
        
        self.peerConnection = WebRTCClient.factory.peerConnection(with: configuration, constraints: defaultConstraint, delegate: self)
    }
    
    public convenience init(remoteVideoView: RTCVideoRenderer?, localVideoView: RTCVideoRenderer?, delegate: WebRTCClientDelegate, mode: AntMediaClientMode, cameraPosition: AVCaptureDevice.Position, targetWidth: Int, targetHeight: Int) {
        self.init(remoteVideoView: remoteVideoView, localVideoView: localVideoView, delegate: delegate,
                  mode: mode, cameraPosition: cameraPosition, targetWidth: targetWidth, targetHeight: targetHeight, videoEnabled: true, multiPeerActive: false, enableDataChannel: false)
    }
    public convenience init(remoteVideoView: RTCVideoRenderer?, localVideoView: RTCVideoRenderer?, delegate: WebRTCClientDelegate, mode: AntMediaClientMode, cameraPosition: AVCaptureDevice.Position, targetWidth: Int, targetHeight: Int, videoEnabled: Bool, multiPeerActive: Bool, enableDataChannel: Bool) {
        self.init(remoteVideoView: remoteVideoView, localVideoView: localVideoView, delegate: delegate,
                  mode: mode, cameraPosition: cameraPosition, targetWidth: targetWidth, targetHeight: targetHeight, videoEnabled: true, multiPeerActive: false, enableDataChannel: false, captureScreen: false)
    }
    
    public convenience init(remoteVideoView: RTCVideoRenderer?, localVideoView: RTCVideoRenderer?, delegate: WebRTCClientDelegate, mode: AntMediaClientMode, cameraPosition: AVCaptureDevice.Position, targetWidth: Int, targetHeight: Int, videoEnabled: Bool, multiPeerActive: Bool, enableDataChannel: Bool, captureScreen: Bool) {
        self.init(remoteVideoView: remoteVideoView, localVideoView: localVideoView, delegate: delegate)
        self.mode = mode
        self.cameraPosition = cameraPosition
        self.targetWidth = targetWidth
        self.targetHeight = targetHeight
        self.videoEnabled = videoEnabled
        self.captureScreenEnabled = captureScreen
        self.enableDataChannel = enableDataChannel
        
        if self.mode != .play && !multiPeerActive {
            self.addLocalMediaStream()
            self.configureAudioSession()
        }
    }
    
    public func setMaxVideoBps(maxVideoBps: NSNumber) {
        AntMediaClient.printf("In setMaxVideoBps:\(maxVideoBps)")
        if maxVideoBps.intValue > 0 {
            AntMediaClient.printf("setMaxVideoBps:\(maxVideoBps)")
            self.peerConnection?.setBweMinBitrateBps(nil, currentBitrateBps: nil, maxBitrateBps: maxVideoBps)
        }
    }
    
    public func getStats(handler: @escaping (RTCStatisticsReport) -> Void) {
        self.peerConnection?.statistics(completionHandler: handler)
    }
    public func setStreamId(_ streamId: String) {
        self.streamId = streamId
    }
    
    public func setToken(_ token: String) {
        self.token = token
    }
    
    public func setRemoteDescription(_ description: RTCSessionDescription) {
        self.peerConnection?.setRemoteDescription(description, completionHandler: {
            (error) in
            if error != nil {
                AntMediaClient.printf("Error (setRemoteDescription): " + error!.localizedDescription + " debug description: " + error.debugDescription)
                
            }
        })
    }
    
    public func addCandidate(_ candidate: RTCIceCandidate) {
        self.peerConnection?.add(candidate)
    }
    
    public func sendData(data: Data, binary: Bool = false) {
        if self.dataChannel?.readyState == .open {
            let dataBuffer = RTCDataBuffer.init(data: data, isBinary: binary)
            self.dataChannel?.sendData(dataBuffer)
        } else {
            AntMediaClient.printf("Data channel is nil or state is not open. State is \(String(describing: self.dataChannel?.readyState)) Please check that data channel is enabled in server side ")
        }
    }
    
    public func isDataChannelActive() -> Bool {
        return self.dataChannel?.readyState == .open
    }
    
    public func sendAnswer() {
        let constraint = Config.createAudioVideoConstraints()
        self.peerConnection?.answer(for: constraint, completionHandler: { (sdp, error) in
            if error != nil {
                AntMediaClient.printf("Error (sendAnswer): " + error!.localizedDescription)
            } else {
                AntMediaClient.printf("Got your answer")
                if sdp?.type == RTCSdpType.answer {
                    self.peerConnection?.setLocalDescription(sdp!, completionHandler: {
                        (error) in
                        if error != nil {
                            AntMediaClient.printf("Error (sendAnswer/closure): " + error!.localizedDescription)
                        }
                    })
                    
                    var answerDict = [String: Any]()
                    
                    if self.token.isEmpty {
                        answerDict =  ["type": "answer",
                                       "command": "takeConfiguration",
                                       "sdp": sdp!.sdp,
                                       "streamId": self.streamId!] as [String: Any]
                    } else {
                        answerDict =  ["type": "answer",
                                       "command": "takeConfiguration",
                                       "sdp": sdp!.sdp,
                                       "streamId": self.streamId!,
                                       "token": self.token] as [String: Any]
                    }
                    
                    self.delegate?.sendMessage(answerDict)
                }
            }
        })
    }
    
    public func createOffer() {
        
        // let the one who creates offer also create data channel.
        // by doing that it will work both in publish-play and peer-to-peer mode
        if enableDataChannel {
            self.dataChannel = createDataChannel()
            self.dataChannel?.delegate = self
        }
        
        let constraint = Config.createAudioVideoConstraints()

        self.peerConnection?.offer(for: constraint, completionHandler: { (sdp, error) in
            if sdp?.type == RTCSdpType.offer {
                AntMediaClient.printf("Got your offer")
                
                self.peerConnection?.setLocalDescription(sdp!, completionHandler: {
                    (error) in
                    if error != nil {
                        AntMediaClient.printf("Error (createOffer): " + error!.localizedDescription)
                    }
                })
                
                AntMediaClient.printf("offer sdp: " + sdp!.sdp)
                var offerDict = [String: Any]()
                
                if self.token.isEmpty {
                    offerDict =  ["type": "offer",
                                  "command": "takeConfiguration",
                                  "sdp": sdp!.sdp,
                                  "streamId": self.streamId!] as [String: Any]
                } else {
                    offerDict =  ["type": "offer",
                                      "command": "takeConfiguration",
                                      "sdp": sdp!.sdp,
                                      "streamId": self.streamId!,
                                      "token": self.token] as [String: Any]
                }
                
                self.delegate?.sendMessage(offerDict)
            }
        })
    }
    
    public func stop() {
        disconnect()
        
    }
    
    private func createDataChannel() -> RTCDataChannel? {
        let config = RTCDataChannelConfiguration()
        guard let dataChannel = self.peerConnection?.dataChannel(forLabel: "WebRTCData", configuration: config) else {
            AntMediaClient.printf("Warning: Couldn't create data channel.")
            return nil
        }
        return dataChannel
    }

    public func disconnect() {
        // TODO: how to clear all resources
      //  self.localVideoTrack?.remove(self.localVideoView!)
        self.remoteVideoTrack?.remove(self.remoteVideoView!)
      //  self.localVideoView?.renderFrame(nil)
        self.remoteVideoView?.renderFrame(nil)
        self.localVideoTrack = nil
        self.remoteVideoTrack = nil
        (self.videoCapturer as? RTCCustomFrameCapturer)?.stopCapture()
        self.peerConnection?.close()
    }
    
    public func toggleAudioEnabled() {
        self.audioEnabled = !self.audioEnabled
        if self.localAudioTrack != nil {
            self.localAudioTrack.isEnabled = self.audioEnabled
        }
    }
    
    public func toggleVideoEnabled() {
        self.videoEnabled = !self.videoEnabled
        if self.localVideoTrack != nil {
            self.localVideoTrack.isEnabled = self.videoEnabled
        }
    }
    
    func createAudioTrack() -> RTCAudioTrack {
        let audioConstrains = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        let audioSource = WebRTCClient.factory.audioSource(with: audioConstrains)
        let audioTrack = WebRTCClient.factory.audioTrack(with: audioSource, trackId: "audio0")
        return audioTrack
    }
    private func createVideoTrack() -> RTCVideoTrack? {
        let videoSource = WebRTCClient.factory.videoSource()
        localVideoSource = videoSource
        #if TARGET_IS_EXTENSION
        let videoCapturer = ScreenSampleCapturer(delegate: videoSource)
        self.videoCapturer = videoCapturer
        delegate?.webRTCClient(self, didCreate: videoCapturer)
        #else
        let videoCapturer = RTCCameraVideoCapturer(delegate: videoSource)
        //        videoCapturer.rotationDelegate = self
        self.videoCapturer = videoCapturer
        #endif
        
        let videoTrack = WebRTCClient.factory.videoTrack(with: videoSource, trackId: "video0")
        return videoTrack
    }
    private func configureAudioSession() {
        rtcAudioSession.lockForConfiguration()
        do {
            try rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
            try rtcAudioSession.setMode(AVAudioSession.Mode.voiceChat.rawValue)
        } catch let error {
            debugPrint("Error changeing AVAudioSession category: \(error)")
        }
        rtcAudioSession.unlockForConfiguration()
    }
    private func addLocalMediaStream() -> Bool {
        
        AntMediaClient.printf("Add local media streams")
        self.localVideoTrack = createVideoTrack()
        self.localAudioTrack = createAudioTrack()
        
        self.videoSender = self.peerConnection?.add(self.localVideoTrack, streamIds: [LOCAL_MEDIA_STREAM_ID])
        self.videoSender = self.peerConnection?.add(self.localAudioTrack, streamIds: [LOCAL_MEDIA_STREAM_ID])
        self.delegate?.addLocalStream()
        return true
    }
    public func didCaptureLocalFrame(_ videoFrame: RTCVideoFrame) {
        guard let videoSource = localVideoSource,
            let videoCapturer = videoCapturer else { return }
        
        videoSource.capturer(videoCapturer, didCapture: videoFrame)
    }
    public func renderRemoteVideo(to renderer: RTCVideoRenderer) {
        remoteVideoTrack?.add(renderer)
    }
}

extension WebRTCClient: RTCDataChannelDelegate {
    func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        self.delegate?.dataReceivedFromDataChannel(didReceiveData: buffer)
    }
    
    func dataChannelDidChangeState(_ parametersdataChannel: RTCDataChannel) {
        if parametersdataChannel.readyState == .open {
            AntMediaClient.printf("Data channel state is open")
        } else if  parametersdataChannel.readyState == .connecting {
            AntMediaClient.printf("Data channel state is connecting")
        } else if  parametersdataChannel.readyState == .closing {
            AntMediaClient.printf("Data channel state is closing")
        } else if  parametersdataChannel.readyState == .closed {
            AntMediaClient.printf("Data channel state is closed")
        }
    }
    
    func dataChannel(_ dataChannel: RTCDataChannel, didChangeBufferedAmount amount: UInt64) {
        
    }
    
}

extension WebRTCClient: RTCPeerConnectionDelegate {
    
    // signalingStateChanged
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        // AntMediaClient.printf("---> StateChanged:\(stateChanged.rawValue)")
    }
    
    // addedStream
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        AntMediaClient.printf("AddedStream")
        
        if stream.audioTracks.count > 1 || stream.videoTracks.count > 1 {
            return
        }
        
        if stream.videoTracks.count == 1 {
            AntMediaClient.printf("stream has video track")
                remoteVideoTrack = stream.videoTracks[0]

                AntMediaClient.printf("Has delegate??? (signalingStateChanged): \(String(describing: self.delegate))")
            delegate?.addRemoteStream()
        } else {
            
        }
    }
    
    // removedStream
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        AntMediaClient.printf("RemovedStream")
        remoteVideoTrack = nil
        remoteAudioTrack = nil
    }
    
    // GotICECandidate
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        let candidateJson = ["command": "takeCandidate",
                             "type": "candidate",
                             "streamId": self.streamId,
                             "candidate": candidate.sdp,
                             "label": candidate.sdpMLineIndex,
                             "id": candidate.sdpMid] as [String: Any]
        self.delegate?.sendMessage(candidateJson)
    }
    
    // iceConnectionChanged
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        AntMediaClient.printf("---> iceConnectionChanged: \(newState.rawValue) for stream: \(self.streamId)")
        self.delegate?.connectionStateChanged(newState: newState)
    }
    
    // iceGatheringChanged
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        // AntMediaClient.printf("---> iceGatheringChanged")
    }
    
    // didOpen dataChannel
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        AntMediaClient.printf("---> dataChannel opened")
        self.dataChannel = dataChannel
        self.dataChannel?.delegate = self
        
    }
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        // AntMediaClient.printf("---> peerConnectionShouldNegotiate")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        // AntMediaClient.printf("---> didRemove")
    }
}
