//
//  AntmediaProvider.swift
//  antmedia
//
//  Created by Admin on 05/01/22.
//

import Foundation
import WebRTC
import core

public protocol AntmediaStateCallback {
    func onReconnectCall(type: String, roomId: Int, reason: String)
    func stateChangeCallback(event: String, object: NSObject)
    func onCleanUpCompleted()
}

public final class AntmediaProvider {
    
    public static let shared = AntmediaProvider()
    
    var roomId: String = ""
    
    var reconnectingRoomId: Int = 0
    
    var streamId: String = ""
    
    var url: String = ""
    
    var antmedia: ConferenceClient!
    
    var isRemoteStreamReceived: Bool = false
    
    var iceServers: [RTCIceServer] = []
    
    private var iceServerArray: NSArray = []
    
    public var delegate: AntmediaStateCallback?
    
    var publisherClient: AntMediaClient?
    
    var playerClients: [AntMediaClientConference] = []
    
    class AntMediaClientConference {
        var playerClient: AntMediaClient
        var viewIndex: Int
        
        init(player: AntMediaClient, index: Int) {
            self.playerClient = player
            self.viewIndex = index
        }
    }
    private init() {
        }
    
    public func AntmediaProviderInit(iceServerArray: NSArray, antmediaStateCallback: AntmediaStateCallback) {
        self.delegate = antmediaStateCallback
        self.iceServerArray = iceServerArray
        
        for value in iceServerArray {
            let urls = (value as AnyObject).value(forKey: "urls") as! NSArray
            if (value as AnyObject).value(forKey: "credential") != nil && (value as AnyObject).value(forKey: "username") != nil {
                iceServers.append(RTCIceServer(urlStrings: (urls as NSObject) as! [String], username: ((value as AnyObject).value(forKey: "username") as? String)!, credential: ((value as AnyObject).value(forKey: "credential") as? String)!))
            } else {
                iceServers.append(RTCIceServer(urlStrings: (urls as NSObject) as! [String]))
            }
        }
    }
    public func StartConnection(url: String, roomId: String, reconnectingRoomId: Int, streamId: String,bitRate:Int) {
        
        self.roomId = roomId
        self.url = url
        self.reconnectingRoomId = reconnectingRoomId
        self.streamId = streamId
        self.isRemoteStreamReceived = false
        
        AntMediaClient.setDebug(true)
        antmedia = ConferenceClient.init(serverURL: url, conferenceClientDelegate: self)
        antmedia.joinRoom(roomId: self.roomId, streamId: "")
    }
    public func didCaptureLocalFrame(_ videoFrame: RTCVideoFrame) {
       publisherClient?.didCaptureLocalFrame(videoFrame)
    }
    public func attachRemoteView(renderer: RTCVideoRenderer) {
        playerClients[0].playerClient.attachRemoteView(renderer: renderer)
    }
    public func getstatsforPlay(completionHandler: @escaping (RTCStatisticsReport) -> Void) {
        playerClients[0].playerClient.getStats(completionHandler: completionHandler)
    }
    public func getstatsforPublish(completionHandler: @escaping (RTCStatisticsReport) -> Void) {
        publisherClient?.getStats(completionHandler: completionHandler)
    }
    public func getPlayAudioStatus() -> Bool {
        if !playerClients.isEmpty {
        return playerClients[0].playerClient.audioEnable
        } else {
        return false
        }
    }
    public func getPublishAudioStatus() -> Bool {
        return publisherClient?.audioEnable ?? false
    }
    public func getPlayVideoStatus() -> Bool {
        if !playerClients.isEmpty {
        return playerClients[0].playerClient.videoEnable
        } else {
            return false
        }
    }
    public func getPublishVideoStatus() -> Bool {
        return publisherClient?.videoEnable ?? false
    }
    public func muteUnmuteAudio() {
        publisherClient?.toggleAudio()
    }
    public func speakerOn() {
        publisherClient?.speakerOn()
    }
    public func speakerOff() {
        publisherClient?.speakerOff()
    }
    public func hideVideo() {
        publisherClient?.videoEnable = false
    }
    public func showVideo() {
        publisherClient?.videoEnable = true
    }
    public func reset() {
        publisherClient?.stop()
        playerClients.forEach({ $0.playerClient.stop() })
    }
}
extension AntmediaProvider: ConferenceClientDelegate {
    public func streamIdToPublish(streamId: String) {
        AntMediaClient.printf("stream id to publish \(streamId)")
        
        self.streamId = streamId
        self.publisherClient?.antMediaProviderInit(iceServerArray: iceServerArray)
        self.publisherClient =  AntMediaClient.init()
        self.publisherClient?.delegate = self
        self.publisherClient?.setOptions(url: url, streamId: streamId, token: "", mode: AntMediaClientMode.publish, enableDataChannel: false)
       
        self.publisherClient?.initPeerConnection()
        self.publisherClient?.start()
  
    }
    
    public func newStreamsJoined(streams: [String]) {
        AntMediaClient.printf("Room current capacity: \(playerClients.count)")
        if playerClients.count == 4 {
            AntMediaClient.printf("Room is full")
            return
        }
            for stream in streams {
                AntMediaClient.printf("stream in the room: \(stream)")
                let playerClient = AntMediaClient.init()
                playerClient.delegate = self
                playerClient.setOptions(url: url, streamId: stream, token: "", mode: AntMediaClientMode.play, enableDataChannel: false)
                playerClient.initPeerConnection()
                playerClient.start()
                
                let playerConferenceClient = AntMediaClientConference.init(player: playerClient, index: 0)
                
                self.playerClients.append(playerConferenceClient)
                
            }
    }
    
    public func streamsLeaved(streams: [String]) {
        
    }
    
}
extension AntmediaProvider: AntMediaClientDelegate {
    public func clientDidConnect(_ client: AntMediaClient) {
        AntMediaClient.printf("Websocket is connected")
    }
    
    public func clientDidDisconnect(_ message: String) {
        
    }
    
    public func clientHasError(_ message: String) {
        
    }
    
    public func remoteStreamStarted(streamId: String) {

    }
    
    public func remoteStreamRemoved(streamId: String) {
        
    }
    
    public func localStreamStarted(streamId: String) {
      
    }
    
    public func playStarted(streamId: String) {
        print("play started")
        delegate?.stateChangeCallback(event: "play_started", object: ["msRoomId": roomId, "publish": true] as NSObject)
    }
    
    public func playFinished(streamId: String) {
        delegate?.stateChangeCallback(event: "play_finished", object: [] as NSObject)
    }
    
    public func publishStarted(streamId: String) {
        delegate?.stateChangeCallback(event: "publish_started", object: ["msRoomId": roomId] as NSObject)
    }
    
    public func publishFinished(streamId: String) {
        delegate?.stateChangeCallback(event: "publish_finished", object: [] as NSObject)
    }
    
    public func disconnected(state: String, streamId: String) {
        delegate?.stateChangeCallback(event: "WebRTCIceStateChanged", object: ["state": state] as NSObject)
    }
    
    public func audioSessionDidStartPlayOrRecord(streamId: String) {
        
    }
    
    public func dataReceivedFromDataChannel(streamId: String, data: Data, binary: Bool) {
        
    }
    
    public func streamInformation(streamInfo: [StreamInformation]) {
        
    }
    
}
