//
//  ViewController.swift
//  videocomponentapp
//
//  Created by Admin on 31/01/22.
//

import UIKit
import core
import videocomponent

class ViewController: UIViewController, VideoComponentCallBack {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var roomIdTextField: UITextField!
    @IBOutlet weak var participantIdTextField: UITextField!

    var videoComponent = VideoComponentView()

    var modelVcConfig = ModelVcConfig()

    override func viewDidLoad() {
        super.viewDidLoad()

        roomIdTextField.text = ""
        participantIdTextField.text = ""
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        self.contentView.addGestureRecognizer(tap)
    }
    @IBAction func didTapTokenButton(_ sender: UIButton) {
        view.endEditing(true)
        self.view.gestureRecognizers?.forEach(view.removeGestureRecognizer)
        guard roomIdTextField.text != "" else { return ProgressHUD.show("Please enter a valid Room Id", icon: .question, interaction: true) }
        guard participantIdTextField.text != "" else { return ProgressHUD.show("Please enter a valid Participant Id", icon: .question, interaction: true) }
        DispatchQueue.main.async { [self] in

            let vcConfig = ModelVcConfig.VCBuilder()
                .vc_socket_url(vc_socket_url: "wss://ms.idfystaging.com/controller/socket/websocket?")
                .vc_logger_url(vc_logger_url: "https://capture.kyc.idfystaging.com/js-logger/publish")
                .frame_width(frame_width: 480)
                .frame_height(frame_height: 640)
                .frame_per_sec(frame_per_sec: 10)
                .build()

            videoComponent.InitialiseCameraView(modelVcConfig: vcConfig, view: contentView, delegate: self)
            self.view.addSubview(videoComponent)
        }
    }
    func onVCStateCallBack(event: String, object: NSObject) {
        if event == "VC_STARTED" {
            videoComponent.setUpSocket(roomId: roomIdTextField.text ?? "", participantId: participantIdTextField.text ?? "", msSocketURL: modelVcConfig.getVc_socket_url(), purpose: "Connecting")
        }
    }

    func onVCError(code: String, message: String, canReconnect: Bool) {

    }
}
