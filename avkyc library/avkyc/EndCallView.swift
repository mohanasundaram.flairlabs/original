//
//  EndCallView.swift
//  app
//
//  Created by Admin on 15/09/21.
//

import Foundation
import UIKit
import core

protocol EndCallActionDelegate {
    func didSetReconnect()
    func didSetDone()
}

class EndCallView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var reconnectButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var timerUpdateLabel: UILabel!
    
    let kCONTENT_XIB_NAME           =   "EndCallView"
    
    var delegate: EndCallActionDelegate?
    
    var loggerStartTime: Double = 0
    
    var loggerCapture: Logger?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        
    }
    func setEndCallView() {
        contentView.backgroundColor = .black
        reconnectButton.setCornerBorder(color: .clear, cornerRadius: 10, borderWidth: 1)
        reconnectButton.backgroundColor = hexStringToUIColor(hex: "#4a599b")
        doneButton.setTitleColor(hexStringToUIColor(hex: "#4a599b"), for: .normal)
    }
    @IBAction func didTapReconnectButton(_ sender: UIButton) {
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "UIInteraction", timestamp: "", event_type: "ButtonClicked", event_name: "reconnect", component: "EndCallOverlay", event_source: "didTapReconnectButton", logger_session_id: "")
      
        let meta: [String: Any] = [:]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        self.delegate?.didSetReconnect()
    }
    @IBAction func didTapImDoneButton(_ sender: UIButton) {
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "UIInteraction", timestamp: "", event_type: "ButtonClicked", event_name: "Decline", component: "EndCallOverlay", event_source: "didTapImDoneButton", logger_session_id: "")
      
        let meta: [String: Any] = [:]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        self.delegate?.didSetDone()
    }
    
}
