//
//  AvkycComponent.swift
//  captureapp
//
//  Created by Admin on 30/03/22.
//

import Foundation
import Cleanse
import core
import healthcheck
import videocomponent
public struct AvkycStructure {

    let healthCheck: HealthCheck
    let videoComponent:VideoComponent
    public func getHealthCheck() -> HealthCheck {
        return healthCheck
    }
    public func getVideoCallVideoComponent() -> VideoComponent {
        return videoComponent
    }
}
public struct AvkycComponent : Cleanse.RootComponent {
    public typealias Root = AvkycStructure
    public typealias Seed = AvkycFeed

    public static func configure(binder: UnscopedBinder) {
        binder.install(dependency: DalComponent.self)
        binder.install(dependency: VcComponent.self)
        binder.install(dependency: HCComponent.self)
    }
    
    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dd:Seed) in
            Root.init(healthCheck: dd.healthCheck.gethealthCheck(), videoComponent: dd.videoComponent.getVideoComponent())
        }
    }
}
public struct AvkycFeed {
    let dalComponent:DalComponent.Root
    let videoComponent:VcComponent.Root
    let healthCheck: HCComponent.Root

    public init(dalComponent:DalComponent.Root,healthCheck:HCComponent.Root,videoComponent:VcComponent.Root) {
        self.dalComponent = dalComponent
        self.healthCheck = healthCheck
        self.videoComponent = videoComponent
    }
}
