//
//  AvkycModule.swift
//  captureapp
//
//  Created by Admin on 30/03/22.
//import Foundation
import Cleanse
public protocol AvkycModuleProtocol {
    mutating func avkycModule(modelAvkycConfig:ModelAVKYCConfig)
    func provideModelAvkycConfig() -> ModelAVKYCConfig
}
public struct AvkycModuleStructure:AvkycModuleProtocol {

    var modelAvkycConfig = ModelAVKYCConfig()
    public mutating func avkycModule(modelAvkycConfig: ModelAVKYCConfig) {
        self.modelAvkycConfig = modelAvkycConfig
    }

    public func provideModelAvkycConfig() -> ModelAVKYCConfig {
        return modelAvkycConfig
    }


}
public struct AvkycModule : Cleanse.Module {
    public static func configure(binder: Binder<Unscoped>) {
        binder
            .bind(AvkycModuleStructure.self)
            .to(factory: AvkycModuleStructure.init)

    }
}

