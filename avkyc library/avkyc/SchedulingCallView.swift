//
//  SchedulingCallView.swift
//  avkyc
//
//  Created by FL Mohan on 23/06/22.
//

import Foundation
import UIKit
import core

public protocol ScheduleTimeCallBack {
    func didCloseScheduleTime()
}


class SchedulingCallView:UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var datePickerView:UIDatePicker!
    @IBOutlet weak var dateView:UIView!
    @IBOutlet weak var sessionSelectionView:UIStackView!
    @IBOutlet weak var slotCollectionView:UICollectionView!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet var sessionView:[UIView]!
    @IBOutlet var callBookedView:UIView!
    @IBOutlet var dateTimeLabel:UILabel!
    @IBOutlet var closeButton:UIButton!
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var subTitleLabel:UILabel!
    

    let kCONTENT_XIB_NAME           =   "SchedulingCallView"

    private var dalCapture = DALCapture()
    private var object = NSObject()
    private var timeSlots:[String] = []
    private var availableSlots:[String] = [""]
    private var duration = 0
    private var selectedSlot:String?
    private var referenceId:String?
    private var delegate:ScheduleTimeCallBack? = nil

    public init(frame: CGRect,dalCapture:DALCapture,referenceId:String,object:NSObject,delegate:ScheduleTimeCallBack) {
        self.dalCapture = dalCapture
        self.object = object
        self.referenceId = referenceId
        self.delegate = delegate
        super.init(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        dateView.isHidden = true
        sessionSelectionView.isHidden = true
        getRequiredDates()
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        setCalenderView()
        callBookedView.isHidden = true
        self.backgroundColor = ThemeConfig.shared.getSecondaryMainColor()
//        dateView.setCornerBorder(color: ThemeConfig.shared.getSecondaryContrastColor(), cornerRadius: 10, borderWidth: 0.5)
        dateView.backgroundColor = ThemeConfig.shared.getHeaderTextColor()
        titleLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        subTitleLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
    }
    @IBAction func didTapSelectDate(sender:UIButton) {
        selectedSlot = nil
        if dateView.isHidden {
            dateView.isHidden = false
            if sessionSelectionView.isHidden == false {
                sessionSelectionView.isHidden = true
            }
        } else {
            dateView.isHidden = true
        }
    }
    private func getRequiredDates() {
        let dates = object.value(forKey: "dates") as! NSArray
        let startDateObject = dates.firstObject as! NSObject
        let startDate = startDateObject.value(forKey: "date") as! String
        let endDateObject = dates.lastObject as! NSObject
        let endDate = endDateObject.value(forKey: "date") as! String
        DispatchQueue.main.async { [self] in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            datePickerView.minimumDate = dateFormatter.date(from: startDate)
            datePickerView.maximumDate = dateFormatter.date(from: endDate)
        }
    }
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        dateView.isHidden = true
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateSelected = dateFormatter.string(from: datePicker.date)
        dateLabel.text = dateSelected
        dalCapture.callApiScheduleCallTime(view: self, date:dateSelected) { onSuccess, onFailure in
            if onSuccess != nil {
                self.timeSlots.removeAll()
                self.duration = onSuccess?.value(forKey: "duration") as! Int
                let slots = onSuccess?.value(forKey: "slots") as! NSArray
                for (_,slot) in slots.enumerated() {
                    let timeSlot  = slot as! NSObject
                    if self.timeSlots.isEmpty {
                        self.timeSlots = [timeSlot.value(forKey: "start_time") as! String]
                    } else {
                        self.timeSlots.append(timeSlot.value(forKey: "start_time") as! String)
                    }
                }
                self.sessionSelectionView.isHidden = false
                DispatchQueue.main.async {
                    self.updateSlots(tag: 1)
                    for view in self.sessionView {
                        if view.tag == 1 {
                            view.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
                        } else {
                            view.backgroundColor = .systemGray2
                        }
                    }
                    self.slotCollectionView.reloadData()
                }
            }
        }
    }
    @IBAction func selectedSlotAction(sender:UIButton) {
        self.updateSlots(tag: sender.tag)
        for view in sessionView {
            if view.tag == sender.tag {
                view.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
            } else {
                view.backgroundColor = .systemGray2
            }
        }
    }
    @IBAction func sumbitDate(sender:UIButton) {
        if selectedSlot == "" || selectedSlot == nil {
            self.window?.rootViewController?.showAlert(title: "Please select a slot to proceed", message: "", actionTitle: "RETRY") { success in
                return
            }
        }
        let parameters: [String: Any] = ["reference_id": referenceId ?? "", "slot_details": ["start_time": selectedSlot] as NSObject]
        dalCapture.callApiBookCall(view: self, parameters: parameters) { onSuccess, onFailure in
            if onSuccess ?? false {
                DispatchQueue.main.async {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "EEEE,d MMMM yyyy"
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.dateFormat = "HH:mm"
                    self.dateTimeLabel.text = self.getlabelWithDate(date: self.selectedSlot ?? "")
                    self.callBookedView.isHidden = false
                }
            }
        }
    }
    @IBAction func closeButton(sender:UIButton) {
        delegate?.didCloseScheduleTime()
    }
    private func updateSlots(tag:Int) {
        availableSlots.removeAll()
        for timeSlot in timeSlots {
            let calendar = NSCalendar.current
            let date = getDate(date: timeSlot)
            let currentHour = calendar.component(.hour, from: date)
            let hourInt = Int(currentHour.description)!
            print(hourInt)
            if hourInt >= 0 && hourInt <= 11 && tag == 1 {
                availableSlots.append(timeSlot)
            }
            else if hourInt >= 12 && hourInt <= 15 && tag == 2 {
                availableSlots.append(timeSlot)
            }
            else if hourInt >= 16 && hourInt <= 19 && tag == 3 {
                availableSlots.append(timeSlot)
            } else if hourInt >= 20 && hourInt <= 24 && tag == 4 {
                availableSlots.append(timeSlot)
            }
        }
        slotCollectionView.reloadData()

    }
    private func setCalenderView() {
        dateView.backgroundColor = .white
        dateView.layer.cornerRadius = 7.0
        dateView.layer.shadowColor = hexStringToUIColor(hex: "1E1E1E").cgColor
        dateView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        dateView.layer.shadowRadius = 1.5
        dateView.layer.shadowOpacity = 0.7
        dateView.translatesAutoresizingMaskIntoConstraints = false

        submitButton.setCornerBorder(color: ThemeConfig.shared.getPrimaryMainColor(), cornerRadius: 10, borderWidth: 0.5)
        submitButton.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
        closeButton.setCornerBorder(color: ThemeConfig.shared.getPrimaryMainColor(), cornerRadius: 10, borderWidth: 0.5)
        closeButton.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
        callBookedView.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()

        slotCollectionView.delegate = self
        slotCollectionView.dataSource = self
        slotCollectionView.register(UINib(nibName: "SlotCollectionCell", bundle: Bundle(for: type(of: self))), forCellWithReuseIdentifier: "SlotCollectionCell")
    }
    private func getlabelWithDate(date:String) -> String {
        let date = getDate(date: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, d MMMM yyyy"
        let dayWithDateYear = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "HH:mm"
        let time = dateFormatter.string(from: date)
        let label = "Your appointment is scheduled for \(dayWithDateYear) at \(time) hours"
        return label
    }
    private func getDate(date:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = dateFormatter.date(from: date) else { return Date() }
        return date
    }
}
extension SchedulingCallView:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfSections section: Int) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if availableSlots.count == 0 {
            self.slotCollectionView.setEmptyMessage("There is no slots available for selected time period.")
        } else {
            self.slotCollectionView.restore()
        }
        return availableSlots.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = slotCollectionView.dequeueReusableCell(withReuseIdentifier: "SlotCollectionCell", for: indexPath) as! SlotCollectionCell
        cell.setCornerBorder(color: ThemeConfig.shared.getPrimaryMainColor(), cornerRadius: 10, borderWidth: 0.5)
        let dateFormatter = DateFormatter()
        let calendar = NSCalendar.current
        let startDate = getDate(date: availableSlots[indexPath.row])
        guard let endDate = calendar.date(byAdding: .minute, value: duration, to: startDate) else { return cell }
        dateFormatter.dateFormat = "HH:mm"
        cell.setLabel(time: "\(dateFormatter.string(from: startDate)) - \(dateFormatter.string(from: endDate))", textColor: ThemeConfig.shared.getSecondaryContrastColor())
        if selectedSlot == availableSlots[indexPath.row] {
            cell.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
        } else {
            cell.backgroundColor = .systemGray2
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:150, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 5.0,left: 5.0,bottom: 5.0,right: 5.0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedSlot = availableSlots[indexPath.row]
        slotCollectionView.reloadData()
    }

}
class SlotCollectionCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    func setLabel(time:String,textColor:UIColor) {
        self.label.text = time
        self.label.textColor = textColor
    }
}
extension UICollectionView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel;
    }

    func restore() {
        self.backgroundView = nil
    }
}
