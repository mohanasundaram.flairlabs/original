//
//  Avkyc.swift
//  captureapp
//
//  Created by Admin on 30/03/22.
//

import Foundation
import core
import Cleanse
import healthcheck
import videocomponent
public class AssistedVideoKycFactory {

    var assistedVideoKycFragment:AvkycView?

    public init() {
    }
    public func startAvkyc(modelAVKYCConfig:ModelAVKYCConfig) -> AvkycView {

        let dalComponent:DalComponent.Root?

        dalComponent = DalComponentFactory.sharedInstance.getDalComponent()

        let avkycComponent = AVKYCComponentFactory.getAvkycComponent(dalComponent: dalComponent!)

        assistedVideoKycFragment = AvkycView(frame: .zero, dalCapture: (dalComponent?.getDalCapture())!, modelAVKYCConfig: modelAVKYCConfig, healthCheck: avkycComponent.getHealthCheck(), videoComponent:avkycComponent.getVideoCallVideoComponent(), loggerWrapper: (dalComponent?.getLoggerWrapper())!)

        return assistedVideoKycFragment!
    }
}


