//
//  AvkycView.swift
//  app
//
//  Created by Admin on 16/11/21.
//

import Foundation
import UIKit
import Lottie
import core
import healthcheck
import videocomponent

public protocol AVKYCCallBack {
    func onAssistedSuccess(object: NSObject)
    func onAssistedIntermediate(object: NSObject)
    func onAssistedFailure(object: NSObject)
}

/// This class do the overall AVKYC process.
/// Create a instance of this class in your class or view.
/// Check

public class AvkycView: UIView, HealthCheckCallback, KYCCallBack, ODSocketCallBack, EndCallActionDelegate, InstructionActionDelegate,ScheduleTimeCallBack {
   
    var dataInfo: NSObject?
    
    private var roomId: String = ""
    
    private var participantId: String = ""
    
    var kycComponent = KycView()
    
    var healthCheck = HealthCheckView()
    
    var timer: Timer?
    
    var countTimer: Timer?
    
    var time = 30
    
    var loggerStartTime: Double = 0
    
    var loggerCapture,loggerHC,loggerVC: Logger?
    
    var requestionTableView: UITableView!
    
    var infoView = InstructionView()
    
    var avkycpage = NetworkCheckView()
    
    var endCallView = EndCallView()

    var schedulingCallView:SchedulingCallView? = nil
    
    var delegate: AVKYCCallBack?

    var videoComponent:VideoComponent?
    var healthcheck:HealthCheck?

    var loggerWrapper = LoggerWrapper()
    
    var dalCapture = DALCapture()
    var odSocket = ODSocket()


    
    var checkSurrounding: Int = 0
    var networkConfigObject:NSObject?
    var unavailableAgentConfig:NSObject?
    var schedulingObject:NSObject?
    var requestionArray: NSArray? = []
    var optionList: [String] = []
    var selectedOption: [String] = []
    var transactionId = ""
   
    var title = ""

    var modelAvkycConfig = ModelAVKYCConfig()

    public init(frame: CGRect,dalCapture:DALCapture,modelAVKYCConfig:ModelAVKYCConfig,healthCheck:HealthCheck,videoComponent:VideoComponent,loggerWrapper:LoggerWrapper) {
        self.dalCapture = dalCapture
        self.modelAvkycConfig = modelAVKYCConfig
        self.videoComponent = videoComponent
        self.healthcheck = healthCheck
        self.loggerWrapper = loggerWrapper
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    /// This Method should be called first to initiate the AVKYC process. Pass the below params.
    /// - Parameters:
    ///   - view: View which is used to add this component
    ///   - data: Payload to load the Avkyc screen
    ///   - delegate: callback for success, intermediate and failure.
    public func initialiseAvkyc(view: UIView, data: NSObject, delegate: AVKYCCallBack) {
        self.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.delegate = delegate
        dataInfo = data
        initLogger()
        self.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
        parseAVKycDataUi()
        
    }
    func initLogger() {
        loggerCapture = loggerWrapper.getLoggers(key: EnumLogger.CAPTURE.rawValue)
        if loggerCapture == nil {
            loggerCapture = loggerWrapper.createLogger(key: EnumLogger.CAPTURE.rawValue, token: dalCapture.getToken(), loggerUrl: modelAvkycConfig.getAvkycLoggerUrl())
        }
        loggerCapture?.setDefaults(defaultLogDetails: ModalLogDetails(reference_id: dalCapture.getRequestId(), reference_type:dalCapture.getReferenceType() ))
        loggerVC = loggerWrapper.createLogger(key: EnumLogger.VC.rawValue, token: dalCapture.getToken(), loggerUrl: modelAvkycConfig.getModelVcConfig().getVc_logger_url())
        loggerVC?.setDefaults(defaultLogDetails: ModalLogDetails(reference_id: dalCapture.getRequestId(), reference_type:dalCapture.getReferenceType() ))
    }
    func parseAVKycDataUi() {
        
        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "RoutePage"
        logDetails.event_type = "vkyc.assisted_vkyc"
        logDetails.event_name = ""
        logDetails.component = "Button"
        logDetails.event_source = "routeToAssistedVKyc"
        
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
        loggerCapture?.logPageRender(loggerStartTime: loggerStartTime, component: "AssistedVideoKYC",referenceId: dalCapture.getRequestId() != "" ? dalCapture.getRequestId() : dalCapture.getCaptureId(),referenceType: dalCapture.getReferenceType(), meta: [:])
        
        self.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
        
        infoView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        endCallView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        infoView.delegate = self
        endCallView.delegate = self
        endCallView.loggerCapture = loggerCapture
        
       
        let configObject = dataInfo?.value(forKey: "config") as? NSObject
//        let tasks = configObject?.value(forKey: "task") as? NSObject
        networkConfigObject = configObject?.value(forKey: "network_check_config") as? NSObject
        unavailableAgentConfig = configObject?.value(forKey: "unavailable_agent_config") as? NSObject
        let bandwidth = configObject?.value(forKey: "bandwidth") as? NSObject
        let bitrate = bandwidth?.value(forKey: "bitrate") as? NSObject
        let user = bitrate?.value(forKey: "user") as? NSObject
        let max_outgoing = (user?.value(forKey: "max_outgoing") as? Int)!
        modelAvkycConfig.getModelVcConfig().setBitrate(bitrate: max_outgoing)
        schedulingObject = dataInfo?.value(forKey: "scheduling") as? NSObject
        let introObject = configObject?.value(forKey: "introduction") as? NSObject
        title = introObject?.value(forKey: "title") as? String ?? ""
//        headerView.didsetTitle = title
//        let prerequisiteCard = introObject?.value(forKey: "prerequisite_card") as? NSObject
//        let subTitle = prerequisiteCard?.value(forKey: "title") as? String
//        requestionArray = prerequisiteCard?.value(forKey: "prerequisite_items") as? NSArray
//        if let swiftArray = requestionArray! as NSArray as? [String] {
//            optionList = swiftArray
//        }
        //        let box = prerequisiteCard?.value(forKey: "box") as? Bool
//        let displayChecklist = prerequisiteCard?.value(forKey: "display_as_checklist") as? Bool
        
        let object = ["event": "VKYC_INITIATED", "title": title, "icon": "BackButton"] as NSObject
        self.delegate?.onAssistedIntermediate(object: object)


        onInitialiseVideoCall()
                
//        let label = UILabel()
//        label.text = "Are you ready for the video call?"
//        label.textAlignment = .center
//        label.font = UIFont(name: label.font.fontName, size: 25)
//        label.textColor = dalCapture.getThemeConfig().getSecondaryContrastColor()
//        self.addSubview(label)
//        label.translatesAutoresizingMaskIntoConstraints = false
//        self.addConstraints(generateConstraintsStatePageTitle(label: label, mainView: self))
//
//        let subTitles = UILabel()
//        subTitles.text = subTitle
//        subTitles.textAlignment = .left
//        subTitles.font = UIFont(name: label.font.fontName, size: 15)
//        subTitles.textColor = dalCapture.getThemeConfig().getSecondaryContrastColor()
//        self.addSubview(subTitles)
//        subTitles.translatesAutoresizingMaskIntoConstraints = false
//        self.addConstraints(generateConstraintsStatePageSubTitle(label: subTitles, mainView: self))
//
//        if displayChecklist ?? false {
//            requestionTableView = UITableView()
//            requestionTableView.register(UINib(nibName: "MultiselectionCell", bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: "MultiselectionCell")
//            requestionTableView.dataSource = self
//            requestionTableView.delegate = self
//            requestionTableView.tableFooterView = UIView()
//            requestionTableView.backgroundColor = .clear
//            requestionTableView.separatorColor = .clear
//            self.addSubview(requestionTableView)
//            requestionTableView.translatesAutoresizingMaskIntoConstraints = false
//            self.addConstraints(generateConstraintsTableView(label: requestionTableView, mainView: self))
//        } else {
//            DispatchQueue.main.async { [self] in
//                onInitialiseVideoCall()
//            }
//        }
//        setInitiateButton()
    }
//    func setInitiateButton() {
//        let initiateButton = UIButton()
//
//        if optionList.count == selectedOption.count {
//            initiateButton.setTitleColor(.white, for: .normal)
//            initiateButton.backgroundColor = dalCapture.getThemeConfig().getPrimaryMainColor()
//        } else {
//            initiateButton.setTitleColor(.white, for: .normal)
//            initiateButton.backgroundColor = .darkGray
//            initiateButton.titleLabel?.alpha = 0.5
//        }
//        initiateButton.setCornerBorder(color: .lightGray, cornerRadius: 10, borderWidth: 0)
//        initiateButton.setTitle("I'M READY (\(checkSurrounding)/\(optionList.count))", for: .normal)
//        self.addSubview(initiateButton)
//        initiateButton.addTarget(self, action: #selector(initiateButtonPressed), for: .touchUpInside)
//        initiateButton.translatesAutoresizingMaskIntoConstraints = false
//        self.addConstraints(generateConstraintsReadyButton(buttonView: initiateButton, mainView: self))
//    }
//    @objc func initiateButtonPressed() {
//        if optionList.count == selectedOption.count {
//
//            var logDetails = ModalLogDetails()
//            logDetails.service_category = "captureSDK"
//            logDetails.service = "ConnectVSGateway"
//            logDetails.event_type = "Clicked"
//            logDetails.event_name = "Ready"
//            logDetails.component = "StartVideoComponent"
//            logDetails.event_source = "startClicked"
//            logDetails.reference_id = dalCapture.getRequestId()
//            logDetails.reference_type = "AV.TaskID"
//            var meta: [String: Any] = [:]
//            meta.updateValue(dalCapture.getRequestId(), forKey: "request_uid")
//
//            loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)
//
//            DispatchQueue.main.async { [self] in
//                avkycpage = NetworkCheckView()
//                avkycpage.initialiseView(view: self, dalCapture: dalCapture, delegate: self)
//                self.addSubview(avkycpage)
//                self.delegate?.onAssistedIntermediate(object: ["event": "update_header"] as NSObject)
//            }
//        }
//    }
    func cleanUp() {
        odSocket.sendEndCallEvent(code: "user_ended", reason: "exitRoom", manual: false)
        odSocket.disconnectOdSocket()
        kycComponent.cleanUp()
    }
//    public func didTapInitiateButton() {
//        onInitialiseVideoCall()
//    }
    
}
//extension AvkycView: UITableViewDelegate, UITableViewDataSource {
//    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return optionList.count
//    }
//
//    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MultiselectionCell") as? MultiselectionCell else {
//            return UITableViewCell()
//        }
//        cell.backgroundColor = .clear
//        cell.selectionStyle = .none
//
//        cell.configureCell(title: optionList[indexPath.row], textColor: dalCapture.getThemeConfig().getSecondaryContrastColor())
//        if selectedOption.contains(self.optionList[indexPath.row]) {
//            cell.contentView.setCornerBorder(color: dalCapture.getThemeConfig().getPrimaryMainColor(), cornerRadius: 10, borderWidth: 2)
//            cell.imageAccessory.image = UIImage(named: "CheckBox", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(dalCapture.getThemeConfig().getPrimaryMainColor())
//        } else {
//            cell.contentView.setCornerBorder(color:dalCapture.getThemeConfig().getSecondaryContrastColor(), cornerRadius: 10, borderWidth: 0.2)
//            cell.imageAccessory.image = UIImage(named: "UncheckBox", in: Bundle(for: type(of: self)), compatibleWith: .current)?.withTintColor(dalCapture.getThemeConfig().getSecondaryContrastColor())
//        }
//        return cell
//    }
//    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        if selectedOption.contains(optionList[indexPath.row]) {
//            if let index = selectedOption.firstIndex(of: optionList[indexPath.row]) {
//                selectedOption.remove(at: index)
//            }
//        } else {
//            selectedOption.append(optionList[indexPath.row])
//        }
//        checkSurrounding = selectedOption.count
//        self.setInitiateButton()
//        self.requestionTableView.reloadData()
//    }
//    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60
//    }
//}
extension AvkycView {
    func onInitialiseVideoCall() {
        
        let object = ["event": "VIDEOCOMPONENT_INITIATED"] as NSObject
        self.delegate?.onAssistedIntermediate(object: object)

        let attempt = dataInfo?.value(forKey: "attempts") as? NSObject
        let attempts_exhausted = attempt?.value(forKey: "attempts_exhausted") as? Bool
        let last_attempt = attempt?.value(forKey: "last_attempt") as? Bool
        
        if attempts_exhausted == false {
            if last_attempt == true {
                self.window?.rootViewController?.showAlert(title: "This is your last call attempt for today", message: "Ensure you are on a good network & Ensure you are carrying the required documents", actionTitle: "OK") { _ in
                    self.checkForHealthCheckNeeded()
                }
            } else {
                checkForHealthCheckNeeded()
            }
        } else {
            self.window?.rootViewController?.showAlert(title: "You have reached the maximum call attempts limit for the day. Please try again tomorrow", message: "Ensure you are on a good network & Ensure you are carrying the required documents", actionTitle: "OK") { [self] _ in
                self.delegate?.onAssistedSuccess(object: ["event": "USER_END_CALL"] as NSObject)
            }
        }
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "VideoCallState", timestamp: "", event_type: "Clicked", event_name: last_attempt ?? false ? "LastAttempt" : "AttemptsExhausted", component: "StartVideoComponent", event_source: "onInitialiseVideoCall", logger_session_id: "", reference_id: dalCapture.getRequestId(), reference_type: "AV.TaskID")
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)
        
    }
    func checkForHealthCheckNeeded() {
        
        let config = dataInfo?.value(forKey: "config") as? NSObject
        let tasks = config?.value(forKey: "task") as? NSObject
        //        let vkyc_assisted_vkyc = tasks?.value(forKey: "vkyc.assisted_vkyc") as? NSObject
        dalCapture.setRequestId(id: tasks?.value(forKey: "request_uid") as? String ?? "")
        let checkObject = networkConfigObject?.value(forKey: "checks") as? NSObject
        let isRoomJoineeded = checkObject?.value(forKey: "room_join") as! Bool
        let isNetworkCheckNeeded = networkConfigObject?.value(forKey: "enabled") as? Bool ?? false
        let timeout = networkConfigObject?.value(forKey: "timeout") as! Int
        let videoOverlayObject = config?.value(forKey: "video_overlay") as? NSObject
        dalCapture.setVideoOverlayEnabled(status: videoOverlayObject?.value(forKey: "enabled") as? Bool ?? false)
        if isNetworkCheckNeeded && isRoomJoineeded {
            let modelHealthCheck = ModelHealthCheck(modelVcConfig: modelAvkycConfig.getModelVcConfig(), init_url: modelAvkycConfig.getAvkycInitHealthCheckUrl(), submit_url: modelAvkycConfig.getAvkycSubmitHealthUrl(), requestId: dalCapture.getRequestId(), isRoomJoinNeeded: isRoomJoineeded, timeOut: timeout)
            DispatchQueue.main.async { [self] in
                healthCheck = (healthcheck?.startHealthCheck(modelHealthCheck: modelHealthCheck))!
                healthCheck.initialiseHealthCheck(view: self, delegate: self)
                self.addSubview(healthCheck)
            }
        } else {
            getSessionToken()
        }
    }
    /// Callback of healthcheck success
    public func onSuccessCallback() {
        DispatchQueue.main.async { [self] in
            self.removeSubviews()
        }
        getSessionToken()
    }
    /// Callback of faliure kyc component
    /// - Parameter code: Type of event occured as string
    public func onFailureCallback(code: String) {
        switch code {
        case "USER_EXIT":
            odSocket.sendEndCallEvent(code: "user_ended", reason: "exitRoom", manual: false)
            odSocket.disconnectOdSocket()
            kycComponent.cleanUp()
            healthCheck.cleanUp()
            if self.healthCheck.isDescendant(of: self) {
                self.healthCheck.removeFromSuperview()
            }
            let object = ["event": "USER_EXIT"] as NSObject
            self.delegate?.onAssistedFailure(object: object)
            break
        case "SOCKET_ERROR","init_failure","submit_failure","failure":
            DispatchQueue.main.async {
                if self.healthCheck.isDescendant(of: self) {
                    self.healthCheck.removeFromSuperview()
                }
                self.infoView.loadHealthCheckFailureView()
                self.infoView.delegate = self
                self.addSubview(self.infoView)
            }
            break
        default:
            break
        }
    }
    /// Back press event
    public func BackPressed() {
        DispatchQueue.main.async { [self] in
            if kycComponent.isDescendant(of: self) {
                kycComponent.BackPressed()
            } else if healthCheck.isDescendant(of: self) {
                healthCheck.BackPressed()
            } else {
                odSocket.disconnectOdSocket()
                kycComponent.cleanUp()
                self.delegate?.onAssistedIntermediate(object: ["event": "USER_BACKPRESS"] as NSObject)
        }
    }
}
    func getSessionToken() {
        DispatchQueue.main.async { [self] in
            if !self.subviews.contains(kycComponent) {
                kycComponent.initialiseKyc(modelVcConfig: modelAvkycConfig.getModelVcConfig(), config: dataInfo ?? NSObject(), view: self, dalCapture: dalCapture, videoComponent: videoComponent!, delegate: self)
                self.addSubview(kycComponent)
            }
        }
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "AvKYC.GetAVSessionToken", timestamp: "", event_type: "Initiate", event_name: "GetSessionToken", component: "AssistedVideoKYC", event_source: "getSessionToken", logger_session_id: "")
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
       
        dalCapture.callApi(view: self, url: modelAvkycConfig.getAvkycSessionTokenUrl(), completion: { [self] onSuccess, _ in
            if onSuccess != nil {
                
                let logDetails =  ModalLogDetails(service_category: "CaptureSDK", service: "AvKYC.GetAVSessionToken", timestamp: "", event_type: "GetAVSessionToken.Success.TAT", event_name: dalCapture.getTatSince(start: loggerStartTime), component: "AssistedVideoKYC", event_source: "getSessionToken", logger_session_id: "")
                loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
                
                let logDetail = ModalLogDetails(service_category: "CaptureSDK", service: "AvKYC.GetAVSessionToken", timestamp: "", event_type: "Success", event_name: "GetSessionToken", component: "AssistedVideoKYC", event_source: "subscribeToVideoKYCHandler", logger_session_id: "")
                loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: logDetail, meta: [:])
                
                if let sessionToken = onSuccess?.value(forKey: "session_token") as? String {
                    DispatchQueue.global().asyncAfter(deadline: .now() + 2) { [self] in
                        self.odSocket = ODSocket(delegate: self)
                        self.odSocket.initiate(sessionToken: sessionToken, socketUrl: modelAvkycConfig.getAvkycSocketUrl(), dalCapture: dalCapture, loggerWrapper: loggerWrapper)
                        
                        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "ConnectVSGateway", timestamp: "", event_type: "Connecting", event_name: dalCapture.getTatSince(start: loggerStartTime), component: "DataService core", event_source: "getSessionToken", logger_session_id: "", reference_id: dalCapture.getRequestId(), reference_type: "AV.TaskID")
                        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
                    }
                } else if onSuccess?.value(forKey: "error") as? String == "unauthorized" {
                    let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                    self.delegate?.onAssistedFailure(object: object)
            }
        } else {
                let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                self.delegate?.onAssistedFailure(object: object)
            }
        })
    }
    /// This method give callback from od socket
    /// - Parameters:
    ///   - roomId: Room Id need to connect call as String
    ///   - participantId: Participant Id for call as String
    public func onConnected(roomId: String, participantId: String) {
        self.roomId = roomId
        self.participantId = participantId
        DispatchQueue.main.async { [self] in
            if self.subviews.contains(infoView) {
                self.infoView.removeFromSuperview()
            }
            kycComponent.setUpKyc(roomId: roomId, participantId: participantId, msSocketURL: "", purpose: "Connecting")
        }
    }
    /// This method used to upload the
    /// - Parameters:
    ///   - artifact: Key used to upload screenshot image
    ///   - link: Url for uploading the screenshot taken
    public func setScreenShotUpload(artifact: String, link: String,transactionId:String,storageProvider:String) {
        self.transactionId = transactionId
        self.infoView.loadScreenShotRequestView(title: "Please Wait", message: "Screenshot Upload in progress.. Your video will resume soon.")
        self.addSubview(self.infoView)
        self.bringSubviewToFront(infoView)
        guard let screenshotImage =  kycComponent.screenShot() else {
                let data = ModalLogDetails(service_category: "CaptureSDK", service: "TakeScreenshot", timestamp: "", event_type: "CaptureFailed", event_name: artifact, event_source: "setScreenShotUpload", logger_session_id: "", publish_to_dlk: true)
                let meta: [String: Any] = ["artifact_key": artifact, "link": link]
                loggerCapture?.log(logLevel: LogLevel.shared.Error, logDetails: data, meta: meta)
            return }
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "TakeScreenshot", timestamp: "", event_type: "Captured", event_name: artifact, event_source: "setScreenShotUpload", logger_session_id: "", publish_to_dlk: true)
        let meta: [String: Any] = ["artifact_key": artifact, "link": link]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)

        
        let orientation = UIDevice.current.orientation
        var resizedImage = UIImage()
        if orientation.isPortrait {
            resizedImage = resizeImage(image: screenshotImage, targetSize: CGSize(width: 480, height: 640))
        } else {
            resizedImage = resizeImage(image: screenshotImage, targetSize: CGSize(width: 640, height: 480))
        }
        dalCapture.submitScreenShot(view: self, url: link, fileName: "", image: resizedImage, storageService: storageProvider) { [self] onSuccess in
            DispatchQueue.main.async {
                if self.subviews.contains(self.infoView) {
                    self.infoView.removeFromSuperview() // Remove it
                }
            }
            DispatchQueue.main.async {
                self.window?.rootViewController?.showAlertOnMessage(title: "Screenshot Uploaded.", message: "", completion: { success in
                })
            }
            if onSuccess == "success" {
                let data = ModalLogDetails(service_category: "", service: "TakeScreenshot", timestamp: "", event_type: "Uploaded", event_name: "Success", component: "", event_source: "setScreenShotUpload", logger_session_id: "", publish_to_dlk: true)
                let meta: [String: Any] = ["artifact_key": artifact, "link": link]
                loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
                
                self.odSocket.sendScreenShotStatus(status: "screenshot_uploaded",event: "Uploaded", artifactKey: artifact,transactionId: transactionId)
            } else {
                let data = ModalLogDetails(service_category: "", service: "TakeScreenshot", timestamp: "", event_type: "UploadFailed", event_name: "Failure", component: "", event_source: "setScreenShotUpload", logger_session_id: "", exceptionDescription: "Falied", publish_to_dlk: true)
                let meta: [String: Any] = ["artifact_key": artifact, "link": link]
                loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
            }
        }
    }
    
    /// This method received callback from od socket
    /// - Parameter code: Type of event occured as string
    public func onDisconnect(code: String) {
        print("On disconnect code -----> \(code)")
        DispatchQueue.main.async { [self] in
            odSocket.disconnectOdSocket()
            if self.subviews.contains(infoView) {
                self.infoView.removeFromSuperview()
            }
            if self.subviews.contains(self.kycComponent) {
                self.kycComponent.removeFromSuperview()
            }
            if code == "agent_not_available" || code == "0" || code == "error" {

                let data = ModalLogDetails(service_category: "CaptureSDK", service: "ConnectVSGateway", timestamp: "", event_type: "NoAgents", event_name: "\(code)", component: "AssistedVideoKYC", event_source: "onDisconnect", logger_session_id: "")
                let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "reason": code, "av_session_id": dalCapture.getCaptureSessionId()]
                loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)

                self.infoView.loadStatusView(title: unavailableAgentConfig?.value(forKey: "title") as? String, message: unavailableAgentConfig?.value(forKey: "text") as? String, enableScheduling: schedulingObject?.value(forKey: "enabled") as? Bool ?? false)
                self.addSubview(infoView)

//                let object = ["event": "DISCONNECT", "reason": "agent_not_available"] as NSObject
//                self.delegate?.onAssistedIntermediate(object: object)

            } else if code == "invalid_task_data" {
                self.infoView.loadLocationFailureView()
                self.addSubview(infoView)
            } else if code == "NETWORK_DISCONNECTED" {
                self.delegate?.onAssistedFailure(object: ["event": "NETWORK_DISCONNECTED"] as NSObject)
            } else if code == "LOCATION_MISMATCH" {
                self.delegate?.onAssistedIntermediate(object: ["event": "LOCATION_MISMATCH"] as NSObject)
            }
        }

    }
    /// This method is button action of netowrk disconnected view
    /// - Parameter screen: Enum type which view is used to display
    public func didSetRetryButton(screen: RetryOptions) {
        if self.subviews.contains(infoView) {
            self.infoView.removeFromSuperview()
        }
        if screen == .SessionContinue {

            let data = ModalLogDetails(service_category: "CaptureSDK", service: "ConnectVSGateway", timestamp: "", event_type: "Clicked", event_name: "Retry", component: "AssistedVideoKYC", event_source: "didSetRetryButton", logger_session_id: "", reference_type: "AV.TaskID")
            let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
            loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
            getSessionToken()

            let object = ["event": "USER_RECONNECT", "reason": "agent_not_available"] as NSObject
            self.delegate?.onAssistedIntermediate(object: object)
        } else if screen == .scheduling {
            
            if schedulingObject?.value(forKey: "slots_available") as? Bool ?? false {
                let booking_details = schedulingObject?.value(forKey: "booking_details") as! NSObject
                let referenceId = booking_details.value(forKey: "reference_id") as? String ?? ""
                dalCapture.callApiScheduleCallDate(view: self, startDate: schedulingObject?.value(forKey: "start_date") as! String, endDate: schedulingObject?.value(forKey: "end_date") as! String) { [self] onSuccess, onFailure in
                    DispatchQueue.main.async { [self] in
                        if onSuccess != nil {
                            schedulingCallView = SchedulingCallView(frame: self.frame, dalCapture:dalCapture,referenceId: referenceId,object: onSuccess ?? NSObject(), delegate: self)
                            self.addSubview(schedulingCallView ?? UIView())
                        } else {
                            self.window?.rootViewController?.showAlert(title: "Something went wrong.", message: "Please try after some time.", actionTitle: "OK") { _ in
                            }
                        }
                    }
                }
            } else {
                self.window?.rootViewController?.showAlert(title: "All slots are currently unavailable.", message: "Please try after some time.", actionTitle: "OK") { _ in
                }
            }
        } else if screen == .SessionOverride {
            let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
            self.delegate?.onAssistedFailure(object: object)
        }
    }
    public func didCloseScheduleTime() {
            let object = ["event": "AGENT_ENDED_CALL"] as NSObject
            self.delegate?.onAssistedSuccess(object: object)
    }
    /// This method gives callback from od socket
    /// - Parameter message: Type of event occured
    public func onCompleted(message: String) {
        DispatchQueue.main.async {
            self.removeSubviews()
        }
        dalCapture.setCallCompletedStatus(status: true)
        odSocket.sendEndCallEvent(code: "agent_ended_call", reason: "ended", manual: true)
        odSocket.disconnectOdSocket()
        kycComponent.cleanUp()
        let object = ["event": "AGENT_ENDED_CALL"] as NSObject
        delegate?.onAssistedSuccess(object: object)
    }
    public func onTouch(x: Int, y: Int) {

    }
    public func fetchWaitTime(time: Int) {
        self.kycComponent.setWaitTime(time: time)
    }
    func sendEndCallToHost() {
        let object =  ["event": "USER_END_CALL"] as NSObject
        self.delegate?.onAssistedSuccess(object: object)
        
    }
    /// This method receives callbacks from the kyc component
    /// - Parameters:
    ///   - event: Type of event occured
    ///   - object: NSObject
    public func KYCStateCallBack(event: String, object: NSObject) {
        switch event {
        case "publish_started":
            break
        case "play_started":
            break
        case "RECONNECTS_EXHAUSTED", "server_ended_room", "left_room", "USER_EXIT", "operator_exit":
            odSocket.sendEndCallEvent(code: event, reason: event, manual: true)
            odSocket.disconnectOdSocket()
            DispatchQueue.main.async {
                self.removeSubviews()
            }
            let object = ["event": "\(event)"] as NSObject
            self.delegate?.onAssistedFailure(object: object)
            break
        case "start":
            break
        case "webrtc_stats":
            odSocket.sendWebStats(model: object)
            break
        case "no_audio":
            odSocket.sendNoAudio_Video(message: event)
            break
        case "no_video":
            odSocket.sendNoAudio_Video(message: event)
            break
        case "user_partially_exit":
            DispatchQueue.main.async {
                self.showEndCallUI()
            }
            break
        case "camera_position":
            break
        case "wait_timeout":
            self.onDisconnect(code: "agent_not_available")
            break
        default:
            break
        }

    }
    
    /// This method receives failure callback from the kyc component
    /// - Parameter event: Type of failure callback as string
    public func KYCFailure(event: String) {
        if event == "SOCKET_ERROR" {
            DispatchQueue.main.async {
                self.odSocket.disconnectOdSocket()
                self.kycComponent.cleanUp()
                if self.healthCheck.isDescendant(of: self) {
                    self.healthCheck.removeFromSuperview()
                }
                self.infoView.loadHealthCheckFailureView()
                self.infoView.delegate = self
                self.addSubview(self.infoView)
            }
        } else {
        
        var endedAs: String = ""
        var reason: String = ""
        var user_ended_call: Bool = false
        if event == "RECONNECTS_EXHAUSTED" {
            endedAs = "mediaserver_disconnect"
            reason = "reconnects_exhausted"
            user_ended_call = false
        } else if event == "browserInBackground" {
            endedAs = "user_in_background"
            reason = "user_aborted"
            user_ended_call = true
        } else {
            endedAs = "mediaserver_disconnect"
            reason = "mediaserver_disconnect"
            user_ended_call = false
        }
        odSocket.sendEndCallEvent(code: endedAs, reason: reason, manual: user_ended_call)
        odSocket.disconnectOdSocket()
        self.delegate?.onAssistedFailure(object: ["event": "RECONNECTS_EXHAUSTED"] as NSObject)
        kycComponent.cleanUp()
        }
        
    }
    func showEndCallUI() {
        
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "UIInteraction", timestamp: "", event_type: "render", event_name: "", component: "videoComponent", event_source: "showEndCallUI", logger_session_id: "", publish_to_dlk: true)
      
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        endCallView.setEndCallView()
        self.addSubview(endCallView)
        startTimer(timeout: 30)
    }
    func startTimer(timeout: Int) {
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(timeout), target: self, selector: #selector(self.callDisconnectionTimeOut), userInfo: nil, repeats: false)
        countTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateCounting), userInfo: timeout, repeats: true)
    }
    @objc func callDisconnectionTimeOut() {
        
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "CustomerManualReconnect", timestamp: "", event_type: "Failed", event_name: "TimedOut", component: "AssistedVideoKYC", event_source: "callDisconnectionTimeOut", logger_session_id: "", reference_type: "AV.TaskID", publish_to_dlk: true)
      
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        resetTimer()
        odSocket.sendEndCallEvent(code: "user_ended_call", reason: "ended", manual: true)
        odSocket.disconnectOdSocket()
        kycComponent.cleanUp()
        if kycComponent.isDescendant(of: self) {
            kycComponent.removeFromSuperview()
        }
        if endCallView.isDescendant(of: self) {
            endCallView.removeFromSuperview()
        }
        delegate?.onAssistedSuccess(object: ["event": "USER_END_CALL"] as NSObject)
    }
    @objc func updateCounting() {
        if self.countTimer == nil {
            endCallView.timerUpdateLabel.text = "You will be auto redirected in 30 seconds"
        } else {
            endCallView.timerUpdateLabel.text = "You will be auto redirected in \(time) seconds"
            time = time - 1
        }
    }
    func didSetReconnect() {
        
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "CustomerManualReconnect", timestamp: "", event_type: "Invoked", event_name: "", component: "AssistedVideoKYC", event_source: "didSetReconnect", logger_session_id: "", reference_type: "AV.TaskID", publish_to_dlk: true)
      
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        resetTimer()
        time = 30
        updateCounting()
        let object = ["event": "USER_RECONNECT", "reason": "manual_call_end"] as NSObject
        delegate?.onAssistedIntermediate(object: object)
        if endCallView.isDescendant(of: self ) {
            endCallView.removeFromSuperview()
        }
        kycComponent.setUpKyc(roomId: roomId, participantId: participantId, msSocketURL: "", purpose: "Reconnecting")
        
        let value = ModalLogDetails(service_category: "CaptureSDK", service: "CustomerManualReconnect", timestamp: "", event_type: "Executed", event_name: "", component: "AssistedVideoKYC", event_source: "didSetReconnect", logger_session_id: "", reference_type: "AV.TaskID", publish_to_dlk: true)
      
        let metadata: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: value, meta: metadata)
    }
    
    func didSetDone() {
        
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "CustomerManualReconnect", timestamp: "", event_type: "Failed", event_name: "Declined", component: "AssistedVideoKYC", event_source: "didSetDone", logger_session_id: "", reference_type: "AV.TaskID", publish_to_dlk: true)
      
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        resetTimer()
        odSocket.sendEndCallEvent(code: "user_ended_call", reason: "ended", manual: true)
        odSocket.disconnectOdSocket()
        kycComponent.cleanUp()
        dalCapture.setCallCompletedStatus(status: true)
        self.sendEndCallToHost()
        
    }
    func resetTimer() {
        timer?.invalidate()
        timer = nil
        countTimer?.invalidate()
        countTimer = nil
    }
    private func logGrantedPermission(eventName: String, eventSource: String) {
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "PermissionsCheck", timestamp: "", event_type: "Granted", event_name: eventName, component: "PermissionsService", event_source: eventSource, logger_session_id: "", publish_to_dlk: true)
        
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
    }
    private func logDeniedPermission(eventName: String, eventSource: String, errorMessage: String) {
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "PermissionsCheck", timestamp: "", event_type: "Denied", event_name: eventName, component: "PermissionsService", event_source: eventSource, logger_session_id: "", publish_to_dlk: true)
        
        var meta: [String: Any] = [:]
        meta.updateValue(errorMessage, forKey: "error_message")
        self.loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)
        
    }
    
}
public protocol ButtonActionDelegate {
    func didTapInitiateButton()
}
class NetworkCheckView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var cardView: UIView!
    @IBOutlet var signalImage: UIImageView!
    @IBOutlet var titleText:UILabel!
    @IBOutlet var infoText:UILabel!
    @IBOutlet var cardTitleText:UILabel!
    @IBOutlet var cardInfoText:UILabel!
    
    var view = UIView()
    
    let kCONTENT_XIB_NAME           =   "NetworkCheckView"
    
    var initiateButton = UIButton()
    
    var delegate: ButtonActionDelegate?
    
    var dalCapture = DALCapture()
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        loadView()
    }
       
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
    }
   
    func initialiseView(view: UIView,dalCapture:DALCapture, delegate: ButtonActionDelegate) {
        self.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.delegate = delegate
        self.dalCapture = dalCapture
        self.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
        titleText.textColor = dalCapture.getThemeConfig().getSecondaryContrastColor()
        infoText.textColor = dalCapture.getThemeConfig().getSecondaryContrastColor()
        cardTitleText.textColor = dalCapture.getThemeConfig().getSecondaryContrastColor()
        cardInfoText.textColor = dalCapture.getThemeConfig().getSecondaryContrastColor()
    }
    func loadView() {
        let animationView = AnimationView(name: "router_wifi", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: signalImage.frame.width, height: signalImage.frame.height)
//        animationView.center = imageView.center
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        signalImage.addSubview(animationView)

        cardView.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
        cardView.layer.cornerRadius = 20.0
        cardView.layer.shadowColor = UIColor.black.cgColor
        cardView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cardView.layer.shadowRadius = 6.0
        cardView.layer.shadowOpacity = 0.7

        initiateButton.setTitleColor(.white, for: .normal)
        initiateButton.backgroundColor = dalCapture.getThemeConfig().getPrimaryMainColor()
        initiateButton.setCornerBorder(color: .lightGray, cornerRadius: 10, borderWidth: 0)
        initiateButton.isUserInteractionEnabled = true
        initiateButton.setTitle("START VIDEO CALL", for: .normal)
        self.addSubview(initiateButton)
        initiateButton.addTarget(self, action: #selector(initiateButtonPressed), for: .touchUpInside)
        initiateButton.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(generateConstraintsInviteButton(buttonView: initiateButton, mainView: self))

    }
    
    @objc func initiateButtonPressed() {
        initiateButton.setTitle("", for: .normal)
        let lay = CAReplicatorLayer()
        lay.frame = CGRect(x: 0, y: 0, width: initiateButton.frame.width - 40, height: initiateButton.frame.height - 20)
        let bar = CALayer()
        bar.frame = CGRect(x: self.frame.width / 3, y: 10, width: 20, height: 20)
        bar.backgroundColor = UIColor.white.cgColor
        bar.cornerRadius = 10
        lay.addSublayer(bar)
        lay.instanceCount = 3
        lay.instanceTransform = CATransform3DMakeTranslation(25, 0, 0)
        let anim = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        anim.fromValue = 1.0
        anim.toValue = 0.2
        anim.duration = 1
        anim.repeatCount = .infinity
        bar.add(anim, forKey: nil)
        lay.instanceDelay = anim.duration / Double(lay.instanceCount)
        initiateButton.isUserInteractionEnabled = false
        initiateButton.layer.addSublayer(lay)
        initiateButton.backgroundColor = .darkGray
        
        self.delegate?.didTapInitiateButton()
    }
}
public func generateConstraintsLogo(imageView: UIImageView, view: UIView) -> [NSLayoutConstraint] {
    let constraintTop = NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 28.0)
    let constraintX = NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: imageView, attribute: .centerY, multiplier: 1.0, constant: 0.0)
    let constraintWidth = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
    let constraintLeft = NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 10.0)
    let constraintHeight = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
    return [constraintTop, constraintX, constraintWidth, constraintHeight, constraintLeft]
}
public func generateConstraintsTitle(label: UILabel, view: UIImageView, mainView: UIView) -> [NSLayoutConstraint] {
    
    let constraintY = NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: mainView,
                                         attribute: .centerY,
                                         multiplier: 1.0,
                                         constant: 0.0)
    let constraintRight = NSLayoutConstraint(item: label,
                                             attribute: .trailing,
                                             relatedBy: .equal,
                                             toItem: mainView,
                                             attribute: .trailing,
                                             multiplier: 1.0,
                                             constant: -20.0)
    let constraintLeft = NSLayoutConstraint(item: label,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: view,
                                            attribute: .trailing,
                                            multiplier: 1.0,
                                            constant: 20.0)
    let constraintHeight = NSLayoutConstraint(item: label,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 60)
    
    return [constraintY, constraintHeight, constraintRight, constraintLeft]
}

public func generateConstraintsStatePageTitle(label: UILabel, mainView: UIView) -> [NSLayoutConstraint] {
    let constraintTop = NSLayoutConstraint(item: label,
                                           attribute: .top,
                                           relatedBy: .equal,
                                           toItem: mainView,
                                           attribute: .top,
                                           multiplier: 1.0,
                                           constant: 40)
    let constraintRight = NSLayoutConstraint(item: label,
                                             attribute: .trailing,
                                             relatedBy: .equal,
                                             toItem: mainView,
                                             attribute: .trailing,
                                             multiplier: 1.0,
                                             constant: -20.0)
    let constraintLeft = NSLayoutConstraint(item: label,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: mainView,
                                            attribute: .leading,
                                            multiplier: 1.0,
                                            constant: 20.0)
    let constraintHeight = NSLayoutConstraint(item: label,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 60)
    
    return [constraintTop, constraintHeight, constraintRight, constraintLeft]
}
public func generateConstraintsStatePageSubTitle(label: UILabel, mainView: UIView) -> [NSLayoutConstraint] {
    let constraintTop = NSLayoutConstraint(item: label,
                                           attribute: .top,
                                           relatedBy: .equal,
                                           toItem: mainView,
                                           attribute: .top,
                                           multiplier: 1.0,
                                           constant: 80.0)
    let constraintRight = NSLayoutConstraint(item: label,
                                             attribute: .trailing,
                                             relatedBy: .equal,
                                             toItem: mainView,
                                             attribute: .trailing,
                                             multiplier: 1.0,
                                             constant: -20.0)
    let constraintLeft = NSLayoutConstraint(item: label,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: mainView,
                                            attribute: .leading,
                                            multiplier: 1.0,
                                            constant: 20.0)
    let constraintHeight = NSLayoutConstraint(item: label,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 60)
    
    return [constraintTop, constraintHeight, constraintRight, constraintLeft]
}
public func generateConstraintsTableView(label: UITableView, mainView: UIView) -> [NSLayoutConstraint] {
    let constraintTop = NSLayoutConstraint(item: label,
                                           attribute: .top,
                                           relatedBy: .equal,
                                           toItem: mainView,
                                           attribute: .top,
                                           multiplier: 1.0,
                                           constant: 130.0)
    let constraintRight = NSLayoutConstraint(item: label,
                                             attribute: .trailing,
                                             relatedBy: .equal,
                                             toItem: mainView,
                                             attribute: .trailing,
                                             multiplier: 1.0,
                                             constant: -20.0)
    let constraintLeft = NSLayoutConstraint(item: label,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: mainView,
                                            attribute: .leading,
                                            multiplier: 1.0,
                                            constant: 20.0)
    let constraintHeight = NSLayoutConstraint(item: label,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 500)
    
    return [constraintTop, constraintHeight, constraintRight, constraintLeft]
}
public func generateConstraintsCardViewforNetworkCheck(cardView: UIView, mainView: UIView) -> [NSLayoutConstraint] {
    let constraintTop = NSLayoutConstraint(item: cardView,
                                           attribute: .top,
                                           relatedBy: .equal,
                                           toItem: mainView,
                                           attribute: .top,
                                           multiplier: 1.0,
                                           constant: 190.0)
    let constraintX = NSLayoutConstraint(item: cardView,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: mainView,
                                         attribute: .centerX,
                                         multiplier: 1.0,
                                         constant: 0.0)
    
    let constraintRight = NSLayoutConstraint(item: cardView,
                                             attribute: .trailing,
                                             relatedBy: .equal,
                                             toItem: mainView,
                                             attribute: .trailing,
                                             multiplier: 1.0,
                                             constant: -20.0)
    let constraintLeft = NSLayoutConstraint(item: cardView,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: mainView,
                                            attribute: .leading,
                                            multiplier: 1.0,
                                            constant: 20.0)
    let constraintHeight = NSLayoutConstraint(item: cardView,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 150)
    //    let constraintTop = NSLayoutConstraint(item: cardView,
    //                                             attribute: .top,
    //                                             relatedBy: .equal,
    //                                             toItem: mainView,
    //                                             attribute: .bottom,
    //                                             multiplier: 1.0,
    //                                             constant: (mainView.frame.size.height / 5) + 20)
    
    return [constraintTop, constraintX, constraintHeight, constraintRight, constraintLeft]
}
public func generateConstraintsReadyButton(buttonView: UIButton, mainView: UIView) -> [NSLayoutConstraint] {
    
    let constraintRight = NSLayoutConstraint(item: buttonView, attribute: .trailing, relatedBy: .equal, toItem: mainView, attribute: .trailing, multiplier: 1.0, constant: -40.0)
    let constraintLeft = NSLayoutConstraint(item: buttonView, attribute: .leading, relatedBy: .equal, toItem: mainView, attribute: .leading, multiplier: 1.0, constant: 40.0)
    let constraintHeight = NSLayoutConstraint(item: buttonView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
    let constraintBottom = NSLayoutConstraint(item: buttonView, attribute: .bottom, relatedBy: .equal, toItem: mainView, attribute: .bottom, multiplier: 1.0, constant: -10)
    return [constraintHeight, constraintRight, constraintLeft, constraintBottom]
}
public func generateConstraintsInviteButton(buttonView: UIButton, mainView: UIView) -> [NSLayoutConstraint] {
    
    let constraintRight = NSLayoutConstraint(item: buttonView,attribute: .trailing,relatedBy: .equal,toItem: mainView,attribute: .trailing,multiplier: 1.0,constant: -40.0)
    let constraintLeft = NSLayoutConstraint(item: buttonView,attribute: .leading,relatedBy: .equal,toItem: mainView,attribute: .leading,multiplier: 1.0,constant: 40.0)
    let constraintHeight = NSLayoutConstraint(item: buttonView,attribute: .height,relatedBy: .equal,toItem: nil,attribute: .notAnAttribute,multiplier: 1.0,constant: 40)
    let constraintBottom = NSLayoutConstraint(item: buttonView,attribute: .bottom,relatedBy: .equal,toItem: mainView,attribute: .bottom,multiplier: 1.0,constant: -10)
    return [constraintHeight, constraintRight, constraintLeft, constraintBottom]
}
