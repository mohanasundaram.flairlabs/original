//
//  MultiselectionCell.swift
//  app
//
//  Created by Admin on 01/09/21.
//

import UIKit
import core

class MultiselectionCell: UITableViewCell {
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var imageAccessory: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(title: String,textColor:UIColor) {
        self.tagLabel.textColor = textColor
        self.tagLabel.text = title
    }
    
}
