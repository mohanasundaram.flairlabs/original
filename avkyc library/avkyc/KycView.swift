//
//  KycView.swift
//  app
//
//  Created by Admin on 14/12/21.
//
public protocol KYCCallBack {
    func KYCStateCallBack(event: String, object: NSObject)
    func KYCFailure(event: String)
}
import Foundation
import UIKit
import videocomponent
import core

class KycView: UIView, VideoComponentCallBack, InstructionActionDelegate {

    private var roomId: String = ""
    
    private var participantId: String = ""
    
    var msSocketURL: String = ""
    
    var videoComponentView:VideoComponentView?
    
    var infoView = InstructionView()
    
    var delegate: KYCCallBack?

    var modelVcConfig = ModelVcConfig()
    
    var dalCapture = DALCapture()
    var videoComponent: VideoComponent?
    var config:NSObject? = nil
    var waitTime = 0
    var showCountDown = false

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    func initialiseKyc(modelVcConfig: ModelVcConfig,config:NSObject, view: UIView,dalCapture:DALCapture,videoComponent:VideoComponent, delegate: KYCCallBack) {
        self.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.delegate = delegate
        self.dalCapture = dalCapture
        self.videoComponent = videoComponent
        self.modelVcConfig = modelVcConfig
        self.config = config
        InitiateKyc()
        InitUIViews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func InitiateKyc() {
        DispatchQueue.main.async { [self] in

            videoComponentView = videoComponent?.startVcView()
            videoComponentView?.InitialiseCameraView(modelVcConfig: modelVcConfig, view: self, delegate: self)
            videoComponentView?.hideAllOptions()
            self.addSubview(videoComponentView!)
            if self.subviews.contains(infoView) {
                self.infoView.removeFromSuperview()
            }
            let config = config?.value(forKey: "config") as? NSObject
            let waitSceenConfig = config?.value(forKey: "wait_screen_configuration") as? NSObject ?? NSObject()
            showCountDown = waitSceenConfig.value(forKey: "display_wait_screen_countdown") as! Bool
            let schedule_call_button = waitSceenConfig.value(forKey: "schedule_call_button") as! String
            let threshold = waitSceenConfig.value(forKey: "threshold") as! NSObject
            let messageAfter = threshold.value(forKey: "message_after") as! String
            let messageBefore = threshold.value(forKey: "message_before") as! String
            waitTime = (threshold.value(forKey: "time") as! Int) / 1000
            let schedulingObject = self.config?.value(forKey: "scheduling") as? NSObject
            let enableScheduling = schedulingObject?.value(forKey: "enabled") as? Bool ?? false
            var callButtonEnable = false
            if schedule_call_button == "always" && enableScheduling {
                callButtonEnable = true
            }
            self.infoView.loadConnectingToAgentView(messageAfter: messageAfter, messageBefore: messageBefore, callButtonEnable: callButtonEnable)
            self.addSubview(infoView)
        }
    }
    public func setWaitTime(time:Int) {
        if showCountDown {
            if (time/1000) < waitTime {
                self.infoView.startTimer(waitTime: time/1000)
            } else {
                self.infoView.startTimer(waitTime: waitTime)
            }
        }
    }
    func setUpKyc(roomId: String, participantId: String, msSocketURL: String, purpose: String) {
        DispatchQueue.main.async { [self] in
            if self.subviews.contains(infoView) {
                self.infoView.removeFromSuperview() // Remove it
            }
        }
        self.roomId = roomId
        self.participantId = participantId
        self.msSocketURL = msSocketURL
        videoComponentView?.setUpSocket(roomId: roomId, participantId: participantId, msSocketURL: msSocketURL, purpose: purpose)
    }
    func screenShot() -> UIImage? {
        return videoComponentView?.screenShot()
    }
    func InitUIViews() {
        infoView = InstructionView()
        infoView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        infoView.delegate = self
    }
    func didSetRetryButton(screen: RetryOptions) {
        if screen == .scheduling || screen == .timeOut {
            self.delegate?.KYCStateCallBack(event: "wait_timeout", object: [] as NSObject)
        }
    }
    func onVCStateCallBack(event: String, object: NSObject) {
        DispatchQueue.main.async { [self] in
            switch event {
            case "publish_started", "publish_finished", "play_finished", "start", "webrtc_stats", "WebRTCIceStateChanged", "screenshot", "no_audio", "no_video", "user_partially_exit", "camera_position":
                self.delegate?.KYCStateCallBack(event: event, object: object)
                break
            case "play_started":
                self.infoView.stopRunningTimer()
                self.delegate?.KYCStateCallBack(event: event, object: object)
                break
            case "RECONNECTS_EXHAUSTED", "server_ended_room", "left_room", "USER_EXIT", "operator_exit":
                videoComponentView?.cleanUp()
                videoComponentView?.removeFromSuperview()
                self.delegate?.KYCStateCallBack(event: event, object: object)
                break
            default:
                break
            }
        }
    }
    
    private var cTimer: Timer?
    private var maxRetryTimeout: Int = 20
    private var currentMaxRetryTimer: Int = 1
    private var currentTimeout: Int = 0
    
    func onVCError(code: String, message: String, canReconnect: Bool) {
        if canReconnect && code != "RECONNECTS_EXHAUSTED" && currentMaxRetryTimer < maxRetryTimeout {
            
            currentTimeout = currentMaxRetryTimer + 2
            currentMaxRetryTimer = currentTimeout
            
            if cTimer != nil {
                cTimer?.invalidate()
                cTimer = nil
            }
            cTimer = Timer.scheduledTimer(timeInterval: TimeInterval(currentTimeout), target: self, selector: #selector(self.onFinish), userInfo: nil, repeats: false)
           
        } else if code == "RECONNECTS_EXHAUSTED" {
            // endedAs = 'mediaserver_disconnect'
            // user_ended_call = false
            // reason = reconnects_exhausted
            self.delegate?.KYCFailure(event: code)
        } else if code == "browserInBackground" {
            
            // endedAs = 'user_in_background'
            // user_ended_call = true
            // reason = user_aborted
            self.delegate?.KYCFailure(event: code)
        } else {
            // endedAs = 'mediaserver_disconnect'
            // user_ended_call = false
            // reason = mediaserver_disconnect
            self.delegate?.KYCFailure(event: code)
        }
    }
    @objc func onFinish() {
        videoComponentView?.cleanUp()
        setUpKyc(roomId: roomId, participantId: participantId, msSocketURL: "", purpose: "Reconnecting")
    }
    func BackPressed() {
        if videoComponentView?.isDescendant(of: self) == true {
            videoComponentView?.BackPressed()
        } else {
           // print("In kyc page")
        }
    }
    func cleanUp() {
        videoComponentView?.cleanUp()
    }
}
