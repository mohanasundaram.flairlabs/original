//
//  AVKYCComponentFactory.swift
//  captureapp
//
//  Created by Admin on 30/03/22.
//

import Foundation
import Cleanse
import core
import videocomponent
import healthcheck

public class AVKYCComponentFactory {
    public static func getAvkycComponent(dalComponent:DalComponent.Root) -> AvkycComponent.Root {

        let vcComponent = try! ComponentFactory.of(VcComponent.self).build(dalComponent)
        let hcComponent = try! ComponentFactory.of(HCComponent.self).build(HCFeed(dalComponent: dalComponent, videoComponent: vcComponent))
        let avkycComponent = try! ComponentFactory.of(AvkycComponent.self)
            .build(AvkycFeed(dalComponent: dalComponent, healthCheck: hcComponent, videoComponent: vcComponent))
        return avkycComponent
    }
}

