//
//  ModelAVKYCConfig.swift
//  avkyc
//
//  Created by Admin on 27/01/22.
//

import Foundation
import videocomponent

public class ModelAVKYCConfig {
    private var avkycLoggerUrl: String = ""
    private var enableLogging: Bool = false
    private var avkycInitHealthCheckUrl: String = ""
    private var avkycSubmitHealthUrl: String = ""
    private var avkycSessionTokenUrl: String = ""
    private var avkycSocketUrl: String = ""
    private var modelVcConfig = ModelVcConfig()

    public init() {}
    
    private init(avkycBuilder: AvkycBuilder) {
        self.avkycLoggerUrl = avkycBuilder.avkycLoggerUrl
        self.enableLogging = avkycBuilder.enableLogging
        self.avkycInitHealthCheckUrl = avkycBuilder.avkycInitHealthCheckUrl
        self.avkycSubmitHealthUrl = avkycBuilder.avkycSubmitHealthUrl
        self.avkycSessionTokenUrl = avkycBuilder.avkycSessionTokenUrl
        self.avkycSocketUrl = avkycBuilder.avkycSocketUrl
        self.modelVcConfig = avkycBuilder.modelVcConfig
    }
    public func getAvkycLoggerUrl() -> String {
        return avkycLoggerUrl
    }
    public func isLoggerEnabled() -> Bool {
        return enableLogging
    }
    public func getAvkycSessionTokenUrl() -> String {
        return avkycSessionTokenUrl
    }
    public func getAvkycSocketUrl() -> String {
        return avkycSocketUrl
    }
    public func getAvkycInitHealthCheckUrl() -> String {
        return avkycInitHealthCheckUrl
    }
    public func getAvkycSubmitHealthUrl() -> String {
        return avkycSubmitHealthUrl
    }
    public func getModelVcConfig() -> ModelVcConfig {
        return modelVcConfig
    }
    public class AvkycBuilder {
        private(set) var avkycLoggerUrl: String = ""
        private(set) var enableLogging: Bool = true
        private(set) var avkycInitHealthCheckUrl: String = "/initiate_check"
        private(set) var avkycSubmitHealthUrl: String = "/submit_connection_stats"
        private(set) var avkycSessionTokenUrl: String = "/token"
        private(set) var avkycSocketUrl: String = "/websocket?"
        private(set) var modelVcConfig = ModelVcConfig()


        public init() {}

        public func loggerUrl(avkycLoggerUrl: String) -> AvkycBuilder {
            self.avkycLoggerUrl = avkycLoggerUrl
            return self
        }
        public func enableLogging(enableLogging: Bool) -> AvkycBuilder {
            self.enableLogging = enableLogging
            return self
        }
        public func avkycBackendBaseUrl(avkycBackendBaseUrl: String) -> AvkycBuilder {
            self.avkycSessionTokenUrl = avkycBackendBaseUrl + self.avkycSessionTokenUrl
            return self
        }
        public func avkycSocketUrl(avkycSocketUrl: String) -> AvkycBuilder {
            self.avkycSocketUrl = avkycSocketUrl + self.avkycSocketUrl
            return self
        }
        public func avkycNetworkCheckBaseUrl(avkycNetworkCheckBaseUrl: String) -> AvkycBuilder {
            self.avkycInitHealthCheckUrl = avkycNetworkCheckBaseUrl + self.avkycInitHealthCheckUrl
            self.avkycSubmitHealthUrl = avkycNetworkCheckBaseUrl + self.avkycSubmitHealthUrl
            return self
        }
        public func modelVcConfig(mediaServerUrl:String,loggerUrl:String,frameWidth:Int,frameHeight:Int,framePerSec:Int) -> AvkycBuilder {
            var loggerUrl = loggerUrl
            if loggerUrl == "" || loggerUrl.isEmpty {
                loggerUrl = avkycLoggerUrl
            }
            self.modelVcConfig = ModelVcConfig.VCBuilder()
                .mediaSocketUrl(mediaSocketUrl: mediaServerUrl)
                .vc_logger_url(vc_logger_url: loggerUrl)
                .frame_width(frame_width: frameWidth)
                .frame_height(frame_height: frameHeight)
                .frame_per_sec(frame_per_sec: framePerSec)
                .build()

            return self

        }
        public func build() -> ModelAVKYCConfig {
            let modelAVKYCConfig = ModelAVKYCConfig(avkycBuilder: self)
            return modelAVKYCConfig
        }
    }
}
