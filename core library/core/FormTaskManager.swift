//
//  FormTaskManager.swift
//  capture
//
//  Created by FL Mohan on 27/05/22.
//

import Foundation
import RxSwift

public class FormTaskManager:IArtifactUpdate,ITaskUpdate,ITaskManager {

    private var dalCapture = DALCapture()
    private var capturePageIndex = 0
    private var formArray:NSArray = []
    public var allTaskDone = BehaviorSubject<Bool?>(value: nil)
    private var disposableList:[Disposable] = []
    public var taskKey = ""
    public init() {
        
    }
    public init(dalCapture:DALCapture,formArray:NSArray,taskKey:String) {
        self.dalCapture = dalCapture
        self.formArray = formArray
        self.taskKey = taskKey
        disposableList.append(allTaskDone.subscribe { [self] status in
            guard let status = status.element else { return }
            if !(status ?? true) {
                DALCapture.printf("Core","Form Task Manager: \(self.taskKey)", "pending")
                resetTask()
            }
        } )
    }
    private func resetTask() {
        DALCapture.printf("Core", "Form Task Manager -- Resetting Task", "\(taskKey)")
        guard let taskObj = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey) as? [String:Any] else { return }
        var taskObject = taskObj
        if taskObject["status"] as! String != "pending" {
            taskObject.updateValue("pending", forKey: "status")
            dalCapture.updateTaskObject(taskKey: taskKey, resultObject: taskObject as NSObject)
        }
    }
    public func isAllTaskDone() -> Bool? {
        return artifactHasValue() && areAllVerifyQATasksComplete()
    }
    public func onUpdate(templateObject: NSObject) {
        if artifactHasValue() && areAllVerifyQATasksComplete() {
            allTaskDone.onNext(true)
        } else {
            allTaskDone.onNext(false)
        }
    }
    public func onUpdate(taskObject: NSObject) {
        if artifactHasValue() && areAllVerifyQATasksComplete() {
            allTaskDone.onNext(true)
        } else {
            allTaskDone.onNext(false)
        }
    }
    public func onNextTask(pageNo:Int) {
        destroy()
        capturePageIndex = pageNo
    }
    public func destroy() {
        for disposableList in disposableList {
            disposableList.dispose()
        }
        disposableList.removeAll()
    }
    public func registerToArtifactKey(artifactKey:String) {
        guard let disposable = dalCapture.subscribeToArtifactKey(key: artifactKey, iArtifactUpdate: self) else { return }
        disposableList.append(disposable)
    }
    public func registerToTaskKey(taskKey:String) {
        guard let disposable = dalCapture.subscribeToTaskKey(key: taskKey, iTaskUpdate: self) else { return }
        disposableList.append(disposable)
    }
    private func artifactHasValue() -> Bool {
        for taskObject in formArray {
            var templateObject:NSObject?
            if let subTask = taskObject as? NSObject {
                let type = subTask.value(forKey: "task_type") as! String
                if type == "card.group" && (subTask.value(forKey: "tasks") != nil) {
                    let tasks = subTask.value(forKey: "tasks") as! NSArray
                    for tsk in tasks {
                        if let taskElement = tsk as? NSObject {
                            let taskTemplateId = taskElement.value(forKey: "template_id") as? Int
                            templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
                            let artifactKey = dalCapture.getArtifactKey(object: taskElement)
                            if let artifactObject = dalCapture.getArtifactsObjectByKey(key: artifactKey) {
                                if !areAllArtifactsComplete(templateObject: templateObject!, artifactObject: artifactObject) {
                                    return false
                                }
                            }
                        }
                    }
                } else {
                    let taskTemplateId = subTask.value(forKey: "template_id") as? Int
                    templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
                    let artifactKey = dalCapture.getArtifactKey(object: subTask)
                    if let artifactObject = dalCapture.getArtifactsObjectByKey(key: artifactKey) {
                        if !areAllArtifactsComplete(templateObject: templateObject!, artifactObject: artifactObject) {
                            return false
                        }
                    }
                }
            }
        }
        return true
    }
    private func areAllArtifactsComplete(templateObject:NSObject,artifactObject:NSObject) -> Bool {
        let templateType = templateObject.value(forKey: "type") as? String
        var hashValue = true
        if templateType == "filePicker" || templateType == "text" || templateType == "dropdown" || templateType == "radio" || templateType == "checkbox" || templateType == "image"  {
            let isMandatory = templateObject.value(forKey: "mandatory") as! Bool
            var error:String? = ""
            if let result = artifactObject.value(forKey: "error") as? String {
                error = result
            } else if let _ = artifactObject.value(forKey: "error") as? NSNull {
                error = ""
            }
            if isMandatory {
                hashValue = hashValue && artifactObject.value(forKey: "present") as! Bool && error == ""
            } else {
                hashValue = error == ""
            }
        }
        return hashValue
    }
    private func areAllVerifyQATasksComplete() -> Bool {
        for task in formArray {
            if let subTask = task as? NSObject {
                let taskType = subTask.value(forKey: "task_type") as? String
                let taskKey = subTask.value(forKey: "task_key") as? String
                let taskTemplateId = subTask.value(forKey: "template_id") as? Int
                if taskType == "card.group" && (subTask.value(forKey: "tasks") != nil) {
                    let tasks = subTask.value(forKey: "tasks") as! NSArray
                    for task in tasks {
                        if let tsk = task as? NSObject {
                            let taskTemplateId = tsk.value(forKey: "template_id") as? Int
                            let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
                            if taskType == "card.group" && templateObject?.value(forKey: "type") as? String == "image" {
                                let isMandatory = templateObject?.value(forKey: "mandatory") as! Bool
                                if isMandatory {
                                    if !(!isDivPending(taskObject: tsk) && !isPanTamperingPending(taskObject: tsk)) {
                                        return false
                                    }
                                }
                            }
                        }
                    }
                } else {
                    let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
                    let type = templateObject?.value(forKey: "type") as? String
                    let isMandatory = templateObject?.value(forKey: "mandatory") as! Bool
                    if taskType == "verify.qa" && (type == "radio" || type == "dropdown" || type == "checkbox") {
                        if isMandatory {
                            let taskObject = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey ?? "")
                            if !(((taskObject?.value(forKey: "question")) != nil) && ((taskObject?.value(forKey: "value")) != nil)) {
                                return false
                            }
                        }
                    } else {
                        if isMandatory {
                            if !(!isDivPending(taskObject: subTask) && !isPanTamperingPending(taskObject: subTask)) {
                                return false
                            }
                        }
                    }
                }
            }
        }
        return true
    }
    private func isDivPending(taskObject: NSObject) -> Bool {
        let taskTemplateId = taskObject.value(forKey: "template_id") as? Int
        let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
        let type = templateObject?.value(forKey: "type") as! String
        let artifactKey = dalCapture.getArtifactKey(object: templateObject ?? [] as NSObject)
        let artifactObject = dalCapture.getArtifactsObjectByKey(key: artifactKey)
        var divStat = "div_done"
        if type == "image"  {
                divStat = artifactObject?.value(forKey: "div_status") as? String ?? "div_done"
        }
        return divStat == "div_pending"
    }
    private func isPanTamperingPending(taskObject:NSObject) -> Bool {
        let taskTemplateId = taskObject.value(forKey: "template_id") as? Int
        let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
        let type = templateObject?.value(forKey: "type") as! String
        let artifactKey = dalCapture.getArtifactKey(object: templateObject ?? [] as NSObject)
        let artifactObject = dalCapture.getArtifactsObjectByKey(key: artifactKey)
        var divStat = "pan_tampering_done"
        if type == "image"  {
                divStat = artifactObject?.value(forKey: "pan_tampering_status") as? String ?? "pan_tampering_done"
        }
        return divStat == "pan_tampering_pending"
    }
}
