//
//  DalConfig.swift
//  svc library
//
//  Created by Admin on 01/04/22.
//

import Foundation

public class DalConfig {

    private var token:String = ""
    private var loggerUrl:String = ""
    private var enableLogging:Bool = false
    private var storageAuthService:String = ""
    private var storageAuthType:String = ""
    private var isAuthParam:Bool = false
    private var enableStorageService: Bool = true
    private var captureStatusUrl:String = ""
    private var captureSocketUrl:String = ""
    
    public init() {}
    private init(configBuilder:DalConfigBuilder) {
        self.token = configBuilder.token
        self.loggerUrl = configBuilder.loggerUrl
        self.enableLogging = configBuilder.enableLogging
        self.captureStatusUrl = configBuilder.captureStatusUrl
        self.captureSocketUrl = configBuilder.captureSocketUrl
    }
    public func getToken() -> String {
        return self.token
    }
    public func getLoggerUrl() -> String {
        return self.loggerUrl
    }
    public func getEnableLogging() -> Bool {
        return self.enableLogging
    }
    public func getStorageAuthService() -> String {
        return self.storageAuthService
    }
    public func getStorageAuthType() -> String {
        return self.storageAuthType
    }
    public func getEnableStorageService() -> Bool {
        return self.enableStorageService
    }
    public func getIsAuthParam() -> Bool {
        return self.isAuthParam
    }
    public func getCaptureStatusUrl() -> String {
        return self.captureStatusUrl
    }
    public func getCaptureSocketUrl() -> String {
        return self.captureSocketUrl
    }
    public class DalConfigBuilder {
        private(set) var token: String = ""
        private(set) var loggerUrl: String = ""
        private(set) var enableLogging: Bool = true
        private(set) var storageAuthService:String = ""
        private(set) var storageAuthType:String = ""
        private(set) var isAuthParam:Bool = false
        private(set) var enableStorageService: Bool = true
        private(set) var captureStatusUrl: String = "/captures/status"
        private(set) var captureSocketUrl: String = "/socket/capture/websocket?"

        public init() {}

        public func setToken(token: String) -> DalConfigBuilder {
            self.token = token
            return self
        }
        public func setLoggerUrl(loggerUrl: String) -> DalConfigBuilder {
            self.loggerUrl = loggerUrl
            return self
        }
        public func enableLogging(enableLogging: Bool) -> DalConfigBuilder {
            self.enableLogging = enableLogging
            return self
        }
        public func setStorageAuthService(storageAuthService: String) -> DalConfigBuilder {
            self.storageAuthService = storageAuthService
            return self
        }
        public func setStorageAuthType(storageAuthType: String) -> DalConfigBuilder {
            self.storageAuthType = loggerUrl
            return self
        }
        public func isAuthParam(isAuthParam: Bool) -> DalConfigBuilder {
            self.isAuthParam = isAuthParam
            return self
        }
        public func enableStorageService(enableStorageService: Bool) -> DalConfigBuilder {
            self.enableStorageService = enableStorageService
            return self
        }
        public func setCaptureSocketUrl(captureSocketUrl: String) -> DalConfigBuilder {
            self.captureSocketUrl = captureSocketUrl + self.captureSocketUrl
            return self
        }
        public func setCaptureServerUrl(serverUrl: String) -> DalConfigBuilder {
            self.captureStatusUrl = serverUrl + self.captureStatusUrl
            return self
        }

        public func build() -> DalConfig {
            let dalConfig = DalConfig(configBuilder: self)
            return dalConfig
        }
    }
}
