//
//  EnumIdentifier.swift
//  core
//
//  Created by FL Mohan on 16/06/22.
//

import Foundation
public enum EnumLogger:String {
    case CAPTURE = "capture"
    case HEALTHCHECK = "healthcheck"
    case VC = "vc"
    case PG = "pg"
    case MSADAPTOR = "msadaptor"
}
public enum DocumentKey:String {
    case IND_PAN = "ind_pan"
    case IND_AADHAR = "ind_aadhaar"
    case IND_VOTER_ID = "ind_voter_id"
    case IND_DRIVING_LICENSE = "ind_driving_license"
    case IND_PASSPORT = "ind_passport"
    case IND_CHEQUE = "ind_cheque"
    case WET_SIGN = "wet_signature"
    case SELFIE = "selfie"
    case PHL_VOTER_ID = "phl_voter"
    case PHL_DRIVING_LICENSE = "phl_driving_license"
    case PHL_PASSPORT = "phl_passport"
    case IND_GST_CERTIFICATE = "ind_gst_certificate"
    case BANK_ACCOUNT_PROOF = "bank_account_proof"
    case PHL_SSS = "phl_sss"
    case PHL_TIN = "phl_tin"
    case SHOP_EST_LICENSE = "shop_est_license"
    case STORE_FRONT = "store_front"
}
public enum EventName:String {
    case SESSION_FETCH_CONFIG = "session:fetch_config"
    case ARTIFACT_UPDATE = "artifact:update"
    case SESSION_INITIATE = "session:initiate"
    case SESSION_SAVE = "session:save"
    case ARTIFACT_INIT_DIV = "artifact:initiate_div"
    case ARTIFACT_INIT_CHECK = "artifact:initiate_check"
    case ARTIFACT_FETCH_DOC = "artifact:fetch_document"
    case ARTIFACT_INIT_PAN_TAMPERED = "artifact:initiate_pan_tamper_check"
}
public enum Code: String {
    case Reconnect_Exhausted = "RECONNECTS_EXHAUSTED"
    case Server_Ended_Room = "server_ended_room"
    case Left_Room = "left_room"
    case Exit_Room = "exitRoom"
    case Failed = "failed"

}
public enum SessionEnum {
    case SESSION_OK, SESSION_ERROR, SESSION_RECONNECTING, SESSION_CONNECTED, SESSION_DISCONNECTED
}
public enum ArtifactEnum {
    case ARTIFACT_ERROR, ARTIFACT_DIV_RESPONSE, ARTIFACT_CHECK_RESPONSE, ARTIFACT_DOC_RETRIEVAL, ARTIFACT_NOTIFY_DOC_RETRIEVAL
}
public enum PageEnum:String {
    case AVKYC = "vkyc.assisted_vkyc"
    case SVC = "self_video"
    case SKILL_SELECT = "av_skill_select"
    case CAPTURE_PREREQUISITE = "capture_prerequisite"
    case SVC_PREREQUISITE = "sv_prerequisite"
    case AV_PREREQUISITE = "av_prerequisite"
    case PAGE_END = "PAGE_END"
}
public enum ButtonTitle:String {
    case VIDEO_KYC = "START VIDEO KYC"
    case SELF_VIDEO = "START SELF VIDEO"
    case PROCEED = "PROCEED"
    case INIT_VERIFICATION = "INITIATE VERIFICATION"
}
