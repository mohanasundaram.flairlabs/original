//
//  CallBacks.swift
//  captureapp
//
//  Created by FL Mohan on 16/06/22.
//

import Foundation

public protocol DalProtocol {
    func getDalCapture() -> DALCapture
    func getLoggerWrapper() -> LoggerWrapper
    func getTaskHelper() -> TaskHelper
}
public protocol ArtifactsCallback {
    func onFileUploaded(event:String,jsonObject:NSObject)
    func onUpdateDiv(event:String,jsonObject:NSObject)
    func onInitiateDiv(event:String,jsonObject:NSObject)
    func onArtifactFailure(event:String,jsonObject:NSObject)
}
public protocol DocFetcherCallback {
    func onArtifactUpdate(event:String,jsonObject:NSObject)
    func onArtifactFailure(event:String,message:String)
    func onDocumentFetch(event:String,jsonObject:NSObject)
}
public protocol OCRCallBack {
    func onOcrSuccess(event:String,jsonObject:NSObject)
    func onOcrFailure(event:String,jsonObject:NSObject)
}
public protocol IReqDocCallback {
    func onReqDocSuccess(artifactsList:[Tasks])
    func onReqDocFailure(message:String)
}
public protocol iTaskRespCallBack {
    func onOpen()
    func onEvent(type:String,data:[String:Any])
    func onFailure(message:String)
    func onClosed()
}
public protocol IStatusCallback {
    func onStatusSuccess(object:NSObject)
    func onStatusFailure(message:String)
}
public protocol ITemplateUpdate {
    func onUpdate(jsonObject:NSObject)
}
public protocol ITaskUpdate {
    func onUpdate(taskObject:NSObject)
}
public protocol IArtifactUpdate {
    func onUpdate(templateObject:NSObject)
}
public protocol IArtifactSubmitCallback {
    func onSubmitSuccess(object:NSObject)
    func onSubmitFailure(message:String)
}
public protocol ITaskManager {
     func registerToArtifactKey(artifactKey:String)
     func registerToTaskKey(taskKey:String)
}
public protocol IInitiateTask {
    func onInitiateSuccess(response:NSObject)
    func onInitiateFailed(message:String)
    func onOCRSuccess(responseArray:NSArray)
}
public protocol IRequiredTask {
    func onSuccess(tasksList:[Tasks])
    func onFailure(msg:String)
}
public protocol ITaskResultCallback {
    func onSuccess(response:NSObject)
    func onFailure(message:String)
    func onTaskResult(responseArray:NSArray)
}
