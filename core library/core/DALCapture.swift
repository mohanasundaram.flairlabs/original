//
//  DALCapture.swift
//  app
//
//  Created by Admin on 24/08/21.
//

import UIKit
import Foundation
import Alamofire
import SwiftPhoenixClient
import Starscream
import RxSwift

/// DalCapture, the Data access layer for socket communication, api call and all core common functionalities
/// Create a instance of this Class in your view by calling [shared] variable.
/// and with the created instance call  [getCaptureStatusDetails()]
public class DALCapture: NSObject, ICaptureCallback, IPageSequenceCallback {

    private var captureId: String = ""
    private var captureSessionId: String = ""
    private var session_token: String = ""
    private var requestId: String = ""
    private var status: String?
    private var poaOptionList:[String] = []
    private var poiOptionList:[String] = []
    public var SOCKET_DISCONNECT:String = "SOCKET_DISCONNECTED"
    private var referenceType:String = "AV.TaskID"
    
    private var artifactObject:NSObject? = nil
    private var captureArray:NSArray? = nil
    private var taskObject:NSObject? = nil
    private var pgLogger = Logger()
    private var dalConfig = DalConfig()
    private var eventSource:EventSource?
    private var artifactsList:[Artifacts] = []
    private var listPageSeq = [PageSequence]()
    private var pageIndex: Int = 0

    internal static var isDebug: Bool = true
    private var customer_ocr_edit:Bool = false
    private var check_tampered_pan:Bool = false
    private var callCompletedStatus: Bool = false
    private var videoOverlayEnabled: Bool = false

    private var captureChannel: Channel?
    private var artifactsChannel: Channel?
    private var captureSocket: CaptureSocket?

    private var iPSCallback:IPageSequenceCallback?
    private var iPageDataCallback:IPageDataCallback?
    private var iReqDocCallBack:IReqDocCallback?
    private var artifactsCallback:ArtifactsCallback?
    private var eventSourceDelegate:iTaskRespCallBack?
    private var iTaskResultCallback:ITaskResultCallback?

    public var artifactHashMap:[String:NSObject] = [:]
    public var taskHashMap:[String:Any] = [:]
    public var captureItemHashMap:[Int:NSArray] = [:]
    public var optionHashMap:[String:[String]] = [:]
    public var taskDetailHashMap:[String:[String:String]] = [:]
    public var templateIdHashMap:[String:Int] = [:]

    public var alltaskBehaviorSubject = BehaviorSubject<Bool?>(value: nil)
    public var buttonBehaviorSubject = BehaviorSubject<String?>(value: nil)
    public var templateBehaviorSubjectHashMap:[String:BehaviorSubject<NSObject>] = [:]
    public var artifactBehaviourSubjectHashMap:[String:BehaviorSubject<NSObject>] = [:]
    public var taskBehaviorSubjectHashMap:[String:BehaviorSubject<NSObject>] = [:]

    private var tasksList:[Tasks] = []
    private var taskType:String = ""
    private var taskKey:String = ""
    public var divProgressList:[String] = []
    private var isForm = false
    private var artifactList:[String] = []
    private var dependentList:[String] = []
    private var taskTypeArtifactHashMap:[String:Tasks] = [:]
    private var taskKeyAndArtifactHashMap:[String:[String]] = [:]
    private var formTaskManagerHashMap:[String:FormTaskManager] = [:]

    private var documentList = ["ind_aadhaar","ind_voter_id","ind_driving_license","ind_passport","ind_pan"]
    private var tamperedDocumentList = ["ind_pan"]


    /// Default constructor
    public override init() {
    }
    public init(pgLogger:Logger,dalConfig:DalConfig,captureSocket:CaptureSocket) {
        self.dalConfig = dalConfig
        self.pgLogger = pgLogger
        self.captureSocket = captureSocket
    }

    /// This will be 1st method that needs to be called after applying the Configuration and creating the dalcapture instance.
    /// This method calls the status api and provides you with the theme details, session Id, capture Id, token status.
    /// Below params are required.
    /// On Response you will receive theme configuration, headers, footers, colors by which you can set your theme configuration.Next you will be required to call the getPageSequence()
    /// - Parameters:
    ///   - sessionOverride: Override the session (true / false)
    ///   - callback: which provides the call back of api in onSessionSuccess(), intermediateCallBack(), onSessionFailure().
    public func getCaptureStatusDetails(sessionOverride: Bool,callback:IStatusCallback) {

        pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "get capture status", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "getCaptureStatusDetails", logger_session_id: ""), meta: [:])

        ApiController.getCaptureStatusDetails(controller: UIViewController(), url: dalConfig.getCaptureStatusUrl(), sessionOverride: sessionOverride, id: dalConfig.getToken()) { [self] success, failure in
            if success != nil {

                DALCapture.printf("Core","getCaptureStatusDetails","Success")

                pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "get capture status success", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "getCaptureStatusDetails", logger_session_id: ""), meta: [:])

                if let value = success?.value(forKey: "body") as? NSObject {
                    captureId = value.value(forKey: "capture_id") as! String
                    captureSessionId = value.value(forKey: "session_id") as! String
                    status = value.value(forKey: "status") as? String
                    let headerObject = success?.value(forKey: "header") as! NSObject
                    let themeObject = headerObject.value(forKey: "theme_config") as! NSObject
                    let logo = headerObject.value(forKey: "logo") as? String ?? ""
                    getThemeConfig().setLogo(logo: logo)
                    getThemeConfig().setThemeConfig(themeObject: themeObject)
                    callback.onStatusSuccess(object: success!)
                } else if (success?.value(forKey: "error") as? NSObject) != nil {
                    let message = success?.value(forKey: "message") as? String
                    callback.onStatusFailure(message: message ?? "")
                    DALCapture.printf("Core", "getCaptureStatusDetails","failed")
                } else {
                    callback.onStatusFailure(message: "")
                }
            } else {
                if failure != nil {
                    DALCapture.printf("Core", "getCaptureStatusDetails","failed")

                    if let message = failure?.value(forKey: "message") as? String {
                        callback.onStatusFailure(message: message)
                    } else {
                        let message = failure?.value(forKey: "error") as? String
                        callback.onStatusFailure(message: message ?? "")
                    }
                }
                
            }
        }
    }
    /// This will be 2nd method that needs to be called after getting the status details.
    /// getPageData Connects to the socket connection and provides you with page sequences, validation configuration in callback.
    /// On Success response, call getFirstPage() which returns you with the pageSequence().
    /// Fetch the page from PageSequence and call {@link #fetchPageData}  method with required parameters and payloads.
    /// - Parameter iPSCallback: iPSCallback provides page sequence callback
    public func getPageSequence(iPSCallback:IPageSequenceCallback) {
        self.iPSCallback = iPSCallback

        let url: String = dalConfig.getCaptureSocketUrl() +
        "t=" + dalConfig.getToken() + "&capture_id=" + captureId + "&session_token=" + captureSessionId
        captureSocket = CaptureSocket()
        DALCapture.printf("Core", "getPageSequence", url)
        captureSocket?.setUpSocket(webSocketUrl: url, captureId: captureId, pgLogger: pgLogger, iCaptureCallback: self)
    }
    public func onSocketOpen() {
        DALCapture.printf("Core", "SocketOpened", "")
        iPSCallback?.onPageSequenceSuccess(object: nil)
    }

    public func onSocketError() {
        DALCapture.printf("Core", "SocketError", "")
        iPSCallback?.onPageSequenceFailure(message:SOCKET_DISCONNECT)
    }

    public func onSessionReceive(event: SessionEnum, payload: NSObject) {
        self.sessionData(event: event, payload: payload)
    }

    public func onArtifactReceive(event: ArtifactEnum, payload: NSObject) {
        self.artifactData(event: event, payload: payload)
    }
    /// Use this method to get the page related data. This method will be called, when you get a success response
    /// - Parameters:
    ///   - page: page to be fetch.
    ///   - event:  Name of the event eg: "session:fetch_config"
    ///   - jsonObject:  payload of the event eg: {page: "pageName", payload: []}
    ///   - completion:  ApiCallBack instance which provides the call back of fetPageConfig in onSessionSuccess(), intermediateCallBack(), onSessionFailure().
    public func fetchPageData(page:String,iPageDataCallback:IPageDataCallback) {
        self.iPageDataCallback = iPageDataCallback

        var payloadArray:[String:Any] = [:]
        var payloadObject:[String:Any] = [:]
        payloadObject.updateValue(page, forKey: "page")
        for sequence in listPageSeq {
            if page == sequence.getPage() {
                if let validationModelList = sequence.getValidationModel() {
                    if validationModelList.count > 0 {
                        for validationModelList in validationModelList {
                            payloadArray = createValidationPayLoad(validation: validationModelList.getValidations())
                        }
                    }
                }
            }
        }
        let payload: NSObject = payloadArray as NSObject

        DALCapture.printf("Core", "fetchPageData","")

        pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "fetch page data", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "fetchPageData", logger_session_id: ""), meta: ["data":payload])
        captureSocket?.fetchSessionChannel(event: EventName.SESSION_FETCH_CONFIG.rawValue, payload: ["page": page, "payload": [payload]], completion: { onSessionSuccess, onSessionFailure in
            if onSessionSuccess != nil {
                var page:String = ""

                let responseObject = onSessionSuccess?.value(forKey: "response") as! NSObject
                let dataObject = responseObject.value(forKey: "data") as! NSObject
                if let _ = dataObject.value(forKey: "page") as? String {
                    page = dataObject.value(forKey: "page") as! String
                }

                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "ValidationsCheck", timestamp: "", event_type: "Received", event_name: page, component: "DataService Core", event_source: "fetchPageConfig", logger_session_id: ""), meta: ["inputs":dataObject])

                DALCapture.printf("Core", "fetchSessionChannel","success")

                if page == "capture" {
                    self.processCapturePayload(payload: dataObject)
                }
                iPageDataCallback.onPageDataSuccess(page: page, object: dataObject)

            } else if onSessionFailure == "Session socket time out" {
                iPageDataCallback.onPageDataFailure(message: "SESSION_TIMEOUT")
                DALCapture.printf("Core", "fetchSessionChannel","session timeout")
            } else {
                iPageDataCallback.onPageDataFailure(message: "Enable Location")
                DALCapture.printf("Core", "fetchSessionChannel","failed")
            }
        })
    }

    private func processCapturePayload(payload:NSObject) {

        pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "process all capture data", timestamp: "", event_type: "process", event_name: "", component: "DataService Core", event_source: "processCapturePayload", logger_session_id: ""), meta: [:])

        guard let configObject = payload.value(forKey: "config") as? NSObject else { return }
        if let value = configObject.value(forKey: "customer_ocr_edit") as? Bool {
            customer_ocr_edit = value
        }
        if let value = configObject.value(forKey: "check_tampered_pan") as? Bool {
            check_tampered_pan = value
        }
        if let poaArray = configObject.value(forKey: "allowed_poa_documents") as? [String] {
            if !poaArray.isEmpty {
                poaOptionList.removeAll()
                for poaArray in poaArray {
                    poaOptionList.append(poaArray)
                }
            }
        }
        if let poiArray = configObject.value(forKey: "allowed_poi_documents") as? [String] {
            if !poiArray.isEmpty {
                poiOptionList.removeAll()
                for poiArray in poiArray {
                    poiOptionList.append(poiArray)
                }
            }
        }
        guard let captureArray = configObject.value(forKey: "capture") as? NSArray else { return }
        self.captureArray = captureArray
        artifactObject = configObject.value(forKey: "artifacts") as? NSObject
        taskObject = configObject.value(forKey: "tasks") as? NSObject
        let templateObject = configObject.value(forKey: "templates") as! NSObject
        setTemplates(templatesObject: templateObject)
        setArtifact()
        setTaskBehaviorSubjectHashMap()
        documentOptionsList(templateObject: templateObject)
        setCaptureItem(captureArray: captureArray, taskObject: taskObject!)
        setCaptureItem(captureArray: captureArray)
    }
    private func createValidationPayLoad(validation:String) -> [String:Any] {
        if validation == "location" {
            guard let location = LocationManager.sharedInstance.currentLocation else { return [:] }
            let dataArr: Payload =  ["timestamp": "\(currentDate())", "speed": "\(location.speed)", "longitude": "\(location.coordinate.longitude)", "latitude": "\(location.coordinate.latitude)", "accuracy": "\(location.speedAccuracy)", "heading": "", "altitude": "\(location.altitude)", "altitudeAccuracy": "\(location.horizontalAccuracy)"]
            return ["attr": "location", "data": dataArr as NSObject]

        }
        return [:]
    }
    private func setTemplates(templatesObject:NSObject) {
        templateBehaviorSubjectHashMap.removeAll()
        let tempObj = templatesObject as! [String:Any]
        var optionsList:[String] = []
        for k in tempObj.keys {
            if let _ = templatesObject.value(forKey: k) as? NSObject {
                var object = templatesObject.value(forKey: k) as! [String:Any]
                let artifactArray = object["artifacts"] as! NSArray
                let optionArray = object["options"] as! NSArray
                if artifactArray.count > 0 && optionArray.count > 0 {
                    for optionArray in optionArray {
                        optionsList.append(optionArray as! String)
                    }
                    optionHashMap.updateValue(optionsList,forKey: artifactArray[0] as! String)
                }
                if object["type"] as! String == "card" {
                    let taskType = object["task_type"] as? String ?? ""
                    if taskType.contains("extract") {
                        object.updateValue(false, forKey: "visibility")
                    }
                }
                let templateSubject:BehaviorSubject<NSObject> = BehaviorSubject.init(value: object as NSObject)
                templateBehaviorSubjectHashMap.updateValue(templateSubject, forKey: k)
            }
        }
    }
    private func setArtifact() {
        artifactHashMap.removeAll()
        let artiObj = artifactObject as! [String:Any]
        for k in artiObj.keys {
            if let value = artifactObject?.value(forKey: k) as? NSObject {
                let object = value as! [String:Any]
                artifactHashMap.updateValue(object as NSObject, forKey: k)
            }
        }
    }
    private func setTaskBehaviorSubjectHashMap() {
        let tskObject = taskObject as! [String:Any]
        for k in tskObject.keys {
            taskHashMap.updateValue(taskObject?.value(forKey: k) as! NSObject, forKey: k)
            let taskSubject:BehaviorSubject<NSObject> = BehaviorSubject.init(value:taskObject?.value(forKey: k) as! NSObject)
            taskBehaviorSubjectHashMap.updateValue(taskSubject, forKey: k)
        }
    }
    private func documentOptionsList(templateObject:NSObject) {
        let tempObject = templateObject as! [String:Any]
        for key in tempObject.keys {
            var optionsList:[String] = []
            let data = templateObject.value(forKey: key) as! NSObject
            let artifactArray = data.value(forKey: "artifacts") as! NSArray
            let optionArray = data.value(forKey: "options") as! NSArray
            if artifactArray.count > 0 && optionArray.count > 0 {
                for(i,_) in optionArray.enumerated() {
                    optionsList.append(optionArray[i] as! String)
                }
                optionHashMap.updateValue(optionsList, forKey: artifactArray[0] as! String)
            }
        }
    }
    private func setCaptureItem(captureArray:NSArray,taskObject:NSObject) {
        var task_key = ""
        var task_type = ""
        for(index,_) in captureArray.enumerated() {
            let jsonArray = captureArray[index] as! NSArray
            for(i,_) in jsonArray.enumerated() {
                let jsonObject = jsonArray[i] as! NSObject
                if let _ = jsonObject.value(forKey: "plugins") {
                    let pluginArray = jsonObject.value(forKey: "plugins") as! NSArray
                    let pluginObject = pluginArray[0] as! NSObject
                    let paramsObject = pluginObject.value(forKey: "params") as! NSObject
                    let paramObj = paramsObject as! [String:Any]
                    for k in paramObj.keys {
                        let keyObject = paramsObject.value(forKey: k) as! NSObject
                        task_key = keyObject.value(forKey: "task_key") as! String
                        task_type = keyObject.value(forKey: "task_type_key") as! String
                        let taskSubject:BehaviorSubject<NSObject> = BehaviorSubject.init(value: taskObject.value(forKey: task_key) as! NSObject)
                        taskBehaviorSubjectHashMap.updateValue(taskSubject, forKey: k)
                    }
                }
                getCaptureItem(task_key: task_key, task_type: task_type, jsonObject: jsonObject)
                task_key = ""
                task_type = ""
            }
        }
    }

    private func setCaptureItem(captureArray:NSArray) {
        for (_,value) in captureArray.enumerated() {
            guard let jsonArray = value as? NSArray else { return }
            for (_,val) in jsonArray.enumerated() {
                guard let jsonObject = val as? NSObject else { return }
                iterateAllCaptureItem(jsonObject: jsonObject)
            }
        }
    }
    private func iterateAllCaptureItem(jsonObject:NSObject) {
        guard let templateId = jsonObject.value(forKey: "template_id") as? Int else { return }
        if (jsonObject.value(forKey: "tasks") != nil) {
            var optionList:[String] = []
            var artifactKey = ""
            let taskArray = jsonObject.value(forKey: "tasks") as! NSArray
            captureItemHashMap.updateValue(taskArray, forKey: templateId)
            for taskArray in taskArray {
                guard let taskObject = taskArray as? NSObject else { return }
                let taskTemplateId = taskObject.value(forKey: "template_id") as? Int
                let task_key = taskObject.value(forKey: "task_key") as? String
                let task_type = taskObject.value(forKey: "task_type") as? String
                guard let templateObject = getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)") else { return }
                let type = templateObject.value(forKey: "type") as? String
                var side = templateObject.value(forKey: "side") as? String
                let categoryType = templateObject.value(forKey: "category_type") as? String
                let mandatory = templateObject.value(forKey: "mandatory") as? Bool
                var document_type_key = templateObject.value(forKey: "document_type_key") as? String
                if type == "form" {
                    dependentList = []
                    taskKey = task_key ?? ""
                    taskType = task_type ?? ""
                    isForm = true
                }
                if type == "image" || type == "text" || type == "filepicker" || type == "radio" || type == "dropdown" || type == "checkbox" {
                    if task_type == "data_capture.data_capture" {
                        artifactKey = getArtifactKey(object: taskObject)
                        if dependentList != [] {
                            dependentList.append(task_key ?? "")
                        }
                        guard let artifactObject = getArtifactsObjectByKey(key: artifactKey) else { return }
                        if document_type_key == "" {
                            document_type_key = artifactObject.value(forKey: "document_type_key") as? String
                        }
                        let value = artifactObject.value(forKey: "value") as? String
                        let present = artifactObject.value(forKey: "present") as? Bool
                        if side == "" {
                            side = artifactObject.value(forKey: "side") as? String
                        }
                        optionList = getOptionHashMap(artifactKey: artifactKey) ?? []
                        if ((categoryType?.contains("poa")) != nil) && optionList == [] {
                            optionList = getPoaOptionList()
                        } else if ((categoryType?.contains("poi")) != nil) && optionList == [] {
                            optionList = getPoiOptionList()
                        }
                        taskTypeArtifactHashMap.updateValue(Tasks(artifactKey: artifactKey, taskType: task_type ?? ""), forKey: task_key ?? "")
                        tasksList.append(Tasks(type: type ?? "", artifactKey: artifactKey, taskKey: task_key ?? "", taskType: task_type ?? "", templateId: taskTemplateId ?? 0, documentType: document_type_key ?? "", mandatory: mandatory ?? false, side: side ?? "", value: value ?? "", present: present ?? false, optionList: optionList))
                    } else {
                        taskTypeArtifactHashMap.updateValue(Tasks(taskType: task_type ?? ""), forKey: task_key ?? "")
                        tasksList.append(Tasks(type: type ?? "", taskKey: task_key ?? "", taskType: task_type ?? "", mandatory: mandatory ?? false, templateId: taskTemplateId ?? 0))
                    }
                }
                iterateAllCaptureItem(jsonObject: taskObject)
                updateTaskKeyAndArtifactHashMap()
            }
        }
    }
    private func updateTaskKeyAndArtifactHashMap() {
        if isForm {
            if taskKeyAndArtifactHashMap.isEmpty {
                taskKeyAndArtifactHashMap = [taskKey:artifactList]
            } else {
                taskKeyAndArtifactHashMap.updateValue(artifactList, forKey: taskKey)
            }
            artifactList = []
            taskKey = ""
            taskType = ""
            isForm = false
        }
    }
    /// Using this method one can reset the capture component by passing the artifact key.
    /// This method will be used when building the custom capture.
    /// - Parameter artifactKey: Artifact key received in capture payload or in the list of required documents
    public func resetComponent(artifactKey:String) {

        let taskKey = getTaskKey(artifactKey: artifactKey) ?? ""
        if !taskKey.isEmpty {
            guard let taskBehaviorSubject:BehaviorSubject<NSObject> = taskBehaviorSubjectHashMap[taskKey] else { return }
            var obj:[String:Any] = [:]
            do {
                obj = try taskBehaviorSubject.value() as! [String:Any]
            }
            catch {
            }
            obj.updateValue("pending", forKey: "status")
            updateTaskObject(taskKey: taskKey, resultObject: obj as NSObject)
            taskBehaviorSubject.onNext(obj as NSObject)
        }
        var artifactObject:[String:Any]? = nil
        guard let subject:BehaviorSubject<NSObject> = artifactBehaviourSubjectHashMap[artifactKey] else { return }
        artifactObject = getArtifactsObjectByKey(key: artifactKey) as? [String : Any]
        if artifactObject?["present"] as! Bool {
            artifactObject?.updateValue(false, forKey: "present")
            artifactObject?.updateValue(false, forKey: "uploading")
            artifactObject?.updateValue("", forKey: "error")
            artifactObject?.updateValue("div_pending", forKey: "div_status")
            if check_tampered_pan {
                artifactObject?.updateValue("pan_tampering_pending", forKey: "pan_tampering_status")
            }
            updateArtifactObject(updateObject: artifactObject! as NSObject, artifactKey: artifactKey)
            subject.on(.next(artifactObject! as NSObject))
            updateArtifacts(artifactKey: artifactKey, updateObject: artifactObject! as NSObject, isFromupload: false)
        }
    }

    /// Using this method one can reset the capture component by passing the task key.
    /// This method will be used when building the custom capture.
    /// - Parameter artifactKey: Artifact key received in capture payload or in the list of required documents
    public func resetTask(taskKey:String) {
        if let tasks = getTaskTypeAndArtifactKey(taskKey: taskKey) {
            let artifactKey = tasks.getArtifactKey()
            var artifactObject = getArtifactsObjectByKey(key: artifactKey) as? [String : Any]
            if artifactObject?["present"] as! Bool {
                artifactObject?.updateValue(false, forKey: "present")
                artifactObject?.updateValue(false, forKey: "uploading")
                artifactObject?.updateValue("", forKey: "error")
                artifactObject?.updateValue("div_pending", forKey: "div_status")
                if divProgressList.contains(artifactKey) {
                    divProgressList.removeAll{$0 == "\(artifactKey)"}
                }
                if tamperedDocumentList.contains(artifactObject?["document_type_key"] as! String) {
                    if check_tampered_pan {
                        artifactObject?.updateValue("pan_tampering_pending", forKey: "pan_tampering_status")
                    }
                }
                updateArtifactObject(updateObject: artifactObject! as NSObject, artifactKey: artifactKey)
                updateArtifacts(artifactKey: artifactKey, updateObject: artifactObject! as NSObject, isFromupload: false)
            }
        }
    }
    
    /// This will mark the status of the task as completed based on the artifact key.
    /// Basically this method will or can be used in the custom capture where the complete UI is build by the customer.
    /// - Parameters:
    ///   - key: Artifact Key
    ///   - value: Value for respective key
    ///   - state: Boolean value
    public func getTaskObjectByTaskKey(taskKey:String) -> NSObject? {
        guard let taskObject = taskHashMap[taskKey] as? NSObject else { return nil }
        return taskObject
    }
    /// This method is used to upload the image document as per the artifact key.
    /// You need to pass the below mentioned parameters for uploading
    /// - Parameters:
    ///   - artifactKey: Artifact key.
    ///   - documentKey: Document key
    ///   - image: Image
//    public func uploadArtifacts(artifactKey:String,documentKey:String,image: UIImage) {
//        uploadArtifact(artifactKey: artifactKey, documentKey: documentKey, image: image)
//    }
    /// This method is used to upload the image document as per the artifact key. To receive the artifact and ocr callback you can use this method.
    /// You need to pass the below mentioned parameters for uploading

    /// - Parameters:
    ///   - artifactKey: Artifact key
    ///   - documentKey: Document key
    ///   - image: Image
    ///   - artifactsCallback: Artifact callback
    ///   - ocrCallback: OCR callback
//    public func uploadArtifact(artifactKey:String,documentKey:String,image: UIImage,artifactsCallback:ArtifactsCallback? = nil,ocrCallback:OCRCallBack? = nil) {
//        var url:String = ""
//        var contentType = ""
//        guard let artifacts = getArtifactsObjectByKey(key: artifactKey) else { return }
//        var artifact = artifacts as! [String:Any]
//        if let obj = artifact["upload_urls"] as? NSObject {
//            let image = obj.value(forKey: "image/jpeg") as? NSObject
//            let objectId = image?.value(forKey: "object_id") as? String
//            artifact.updateValue(objectId ?? "", forKey: "object_id")
//            url = image?.value(forKey: "url") as? String ?? ""
//            contentType = "image/jpeg"
//        } else {
//            let value = artifact["upload_url"] as! String
//            url = value
//            contentType = ""
//        }
//        artifact.updateValue(true, forKey: "uploading")
//        artifact.updateValue("div_pending", forKey: "div_status")
//        updateArtifactObject(updateObject: artifact as NSObject, artifactKey: artifactKey)
//
//        var obj:[String:Any] = [:]
//        obj.updateValue(artifactKey, forKey: "artifactKey")
//        obj.updateValue("Image Uploading In Progress", forKey: "message")
//        self.artifactsCallback?.onFileUploaded(event: "UPLOAD_INPROGRESS", jsonObject: obj as NSObject)
//        DispatchQueue.main.asyncAfter(deadline: .now()) {
//            guard let imageData = image.jpegData(compressionQuality: 0.5) else { return }
//            DALCapture.printf("core", "Uploading image url",url)
//            var request = URLRequest(url: URL(string: url)!, timeoutInterval: Double.infinity)
//            request.addValue(contentType, forHTTPHeaderField: "Content-Type")
//            request.httpMethod = "PUT"
//            request.httpBody = imageData
//            ApiController.uploadImage(request:request,controller: UIViewController()) { success, _ in
//                if success == true {
//                    DALCapture.printf("Core", "Image Upload", "Success")
//                    var finalArtifacts = artifact
//                    finalArtifacts.updateValue(false, forKey: "uploading")
//                    finalArtifacts.updateValue(true, forKey: "present")
//                    finalArtifacts.updateValue(documentKey, forKey: "document_type_key")
//                    finalArtifacts.updateValue("image/jpeg", forKey: "content_type")
//                    if let metaData = finalArtifacts["metadata"] as? NSObject {
//                        var metaData = metaData as! [String:Any]
//                        metaData.updateValue("camera", forKey: "capture_mechanism")
//                        finalArtifacts.updateValue(metaData as NSObject, forKey: "metadata")
//                    }
//                    self.updateArtifactObject(updateObject: finalArtifacts as NSObject, artifactKey: artifactKey)
//                    var obj:[String:Any] = [:]
//                    obj.updateValue(artifactKey, forKey: "artifactKey")
//                    obj.updateValue("Image Uploaded Successful", forKey: "message")
//                    artifactsCallback?.onFileUploaded(event: "UPLOAD_SUCCESS", jsonObject: obj as NSObject)
//                    self.updateArtifacts(artifactKey: artifactKey, updateObject: finalArtifacts as NSObject, isFromupload: true)
//                } else {
//                }
//            }
//        }
//    }
    /// This method is used for uploading document like digi locker etc
    /// - Parameters:
    ///   - artifactKey: Artifact key
    ///   - uploading: Boolean
    ///   - error: Error description
    ///   - docFetcherCallback: Callback of onDocumentFetch() and onArtifactFailure().
    public func documentFetcher(artifactKey:String,uploading:Bool,error:String,details:NSObject? = nil,docFetcherCallback:DocFetcherCallback) {
        var finalArtifacts = getArtifactsObjectByKey(key: artifactKey) as! [String:Any]
        finalArtifacts.updateValue(uploading, forKey: "uploading")
        if uploading {
            finalArtifacts.updateValue(false, forKey: "present")
            if error != "" {
                finalArtifacts.updateValue(error, forKey: "error")
            }
        }
        captureSocket?.fetchArtifactChannel(event: EventName.ARTIFACT_UPDATE.rawValue, payload: finalArtifacts, completion: { onArtifactSuccess, onArtifactFailure in
            if onArtifactSuccess != nil {
                let response = onArtifactSuccess?.value(forKey: "response") as! NSObject
                if response.value(forKey: "success") as! Bool == true {
                    docFetcherCallback.onArtifactUpdate(event: "ARTIFACT_UPDATE", jsonObject: response)
                    if uploading && error == "" {
                        var pushObject = Payload()
                        let documentTypeArray = artifactKey.components(separatedBy: ".") as [String]
                        pushObject.updateValue(artifactKey, forKey: "artifact_key")
                        pushObject.updateValue(documentTypeArray[1], forKey: "document_type_key")
                        if details != nil {
                        pushObject.updateValue(details! as NSObject, forKey: "details")
                        }
                        self.captureSocket?.fetchArtifactChannel(event: EventName.ARTIFACT_FETCH_DOC.rawValue, payload: pushObject, completion: { onArtifactSuccess, onArtifactFailure in
                            if onArtifactSuccess != nil {
                                let response = onArtifactSuccess?.value(forKey: "response") as! NSObject
                                docFetcherCallback.onDocumentFetch(event: "DOCUMENT_FETCH", jsonObject: response)
                            } else {
                                docFetcherCallback.onArtifactFailure(event: "ARTIFACT_FAILURE", message: onArtifactFailure ?? "")
                            }
                        })
                    }

                } else {
                    docFetcherCallback.onArtifactFailure(event: "ARTIFACT_FAILURE", message: onArtifactFailure ?? "")
                }
            }
        })
    }
//    public func uploadFile(view:UIView,taskKey:String,fileUrl:URL) {
//        let tasks = getTaskTypeAndArtifactKey(taskKey: taskKey)!
//        let artifactKey = tasks.getArtifactKey()
//        var url:String = ""
//        guard let artifacts = getArtifactsObjectByKey(key: artifactKey) else { return }
//        var artifact = artifacts as! [String:Any]
//        if let obj = artifact["upload_urls"] as? NSObject {
//            let image = obj.value(forKey: "image/jpeg") as? NSObject
//            let objectId = image?.value(forKey: "object_id") as? String
//            artifact.updateValue(objectId ?? "", forKey: "object_id")
//            url = image?.value(forKey: "url") as? String ?? ""
//        } else {
//            let value = artifact["upload_url"] as! String
//            url = value
//        }
//        ApiController.uploadFile(url: url, fileUrl: fileUrl, controller:  view.window?.rootViewController ?? UIViewController()) { [self] status, error in
//            if status == true {
//                artifact.updateValue(true, forKey: "present")
//                updateArtifactObject(updateObject: artifact as NSObject, artifactKey: artifactKey)
//                captureSocket?.fetchArtifactChannel(event: EventName.ARTIFACT_UPDATE.rawValue, payload: artifact, completion: { onArtifactSuccess, onArtifactFailure in
//                    if onArtifactSuccess != nil {
//                        self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "updating the artifact", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "updateArtifacts", logger_session_id: ""), meta: [:])
//                        let response = onArtifactSuccess?.value(forKey: "response") as! NSObject
//                        if response.value(forKey: "success") as! Bool == true {
//
//                        }
//                    }
//                })
//
//            } else {
//                artifact.updateValue(false, forKey: "present")
//                updateArtifactObject(updateObject: artifact as NSObject, artifactKey: artifactKey)
//            }
//        }
//    }
    /// This method is used to upload the artifacts on artifact channel. Is is used while developing the custom capture.
    /// It submits the artifacts details for the documents
    /// - Parameters:
    ///   - artifactKey: Artifact Key
    ///   - updateObject: Artifact object of a particular object key
    ///   - isFromupload: Boolean decides who is calling this method.
    public func updateArtifacts(artifactKey:String,updateObject:NSObject,isFromupload:Bool) {

        var finalUpdateObject = updateObject as! [String:Any]
        if isFromupload {
            var object:[String:Any] = [:]
            object.updateValue(false, forKey: "success")
            object.updateValue(artifactKey, forKey: "artifactKey")
            artifactsCallback?.onUpdateDiv(event: "UPDATE_ARTIFACT_INITIATE", jsonObject: object as NSObject)
        }

        pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "updating the artifact", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "updateArtifacts", logger_session_id: ""), meta: [:])

        captureSocket?.fetchArtifactChannel(event: EventName.ARTIFACT_UPDATE.rawValue, payload: finalUpdateObject, completion: { onArtifactSuccess, onArtifactFailure in
            if onArtifactSuccess != nil {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "updating the artifact", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "updateArtifacts", logger_session_id: ""), meta: [:])

                let response = onArtifactSuccess?.value(forKey: "response") as! NSObject
                if isFromupload {
                    var object:[String:Any] = [:]
                    if response.value(forKey: "success") as! Bool == true {
                        var pushObject:[String:Any] = [:]
                        pushObject.updateValue(updateObject, forKey: artifactKey)
                        self.updateArtifactObject(updateObject: finalUpdateObject as NSObject, artifactKey: artifactKey)
                        object.updateValue(true, forKey: "success")
                        object.updateValue(artifactKey, forKey: "artifactKey")
                        self.artifactsCallback?.onUpdateDiv(event: "UPDATE_ARTIFACT_SUCCESS", jsonObject: object as NSObject)
                        if self.documentList.contains(updateObject.value(forKey: "document_type_key") as! String) {
                            self.sendArtifactDivResponse(artifactKey: artifactKey, updateObject: pushObject)
                        } else {
                            finalUpdateObject.updateValue("div_done", forKey: "div_status")
                            self.divProgressList.removeAll { $0 == "\(artifactKey)"}
                            self.updateArtifactObject(updateObject: finalUpdateObject as NSObject, artifactKey: artifactKey)
                        }
                    } else {
                        finalUpdateObject.updateValue(response.value(forKey: "error") as! String, forKey: "error")
                        self.updateArtifactObject(updateObject: finalUpdateObject as NSObject, artifactKey: artifactKey)
                        self.artifactsCallback?.onUpdateDiv(event: "UPDATE_ARTIFACT_FAILURE", jsonObject: object as NSObject)
                    }
                }
            } else {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "updating the artifact failure", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "updateArtifacts", logger_session_id: ""), meta: [:])
            }
        })
    }
    /// Call this method to hide / show the plugin when performing the OCR
    /// - Parameters:
    ///   - templateId: TemplateId of the plugin.
    ///   - isSet: Boolean defines show & hide plugin.
    public func setResetPlugin(templateId:Int,isSet:Bool) {
        guard let templateSubject:BehaviorSubject<NSObject> = templateBehaviorSubjectHashMap["\(templateId)"] else { return }
        var obj:[String:Any] = [:]
        do {
            obj = try templateSubject.value() as! [String:Any]
        }
        catch {
        }
        obj.updateValue(isSet, forKey: "visibility")
        templateSubject.onNext(obj as NSObject)
    }
    /// This method is used to subscribe to the template object changes.
    /// Any change in the template object will generate a broadcast to its subscriber.
    /// For subscribing, you need to send the below mentioned parameters.
    /// - Parameters:
    ///   - templateId: template id of the object.
    ///   - iTemplateUpdate: Updated template object which you want to render.
    public func subscribeToTemplate(templateId:String,iTemplateUpdate:ITemplateUpdate) -> Disposable? {
        var disposable:Disposable? = nil
        guard let templateSubject:BehaviorSubject<NSObject> = templateBehaviorSubjectHashMap[templateId] else { return disposable }
        disposable = templateSubject.subscribe(onNext: { event in
            DispatchQueue.main.async {
                iTemplateUpdate.onUpdate(jsonObject: event)
            }
        })
        return disposable
    }
    /// This method is used to subscribe to the task object changes.
    /// Any change in the task object will generate a broadcast to its subscriber.
    /// For subscribing, you need to send the below mentioned parameters.
    /// - Parameters:
    ///   - key: task Key of the object.
    ///   - iTaskUpdate: Updated task object which you want to observe.
    public func subscribeToTaskKey(key:String,iTaskUpdate:ITaskUpdate) -> Disposable? {
        var disposable:Disposable? = nil
        guard let taskBehaviorSubject:BehaviorSubject<NSObject> = taskBehaviorSubjectHashMap[key] else { return disposable }
        disposable = taskBehaviorSubject.subscribe(onNext: { event in
            DispatchQueue.main.async {
                iTaskUpdate.onUpdate(taskObject: event)
            }
        })
        return disposable
    }
    /// This method is used to subscribe to the artifact object changes.
    /// Any change in the artifact object will generate a broadcast to its subscriber.
    /// For subscribing, you need to send the below mentioned parameters.

    /// - Parameters:
    ///   - key: Artifact key of the object.
    ///   - iArtifactUpdate: Updated artifact object which you want to observe.
    public func subscribeToArtifactKey(key:String,iArtifactUpdate:IArtifactUpdate) -> Disposable? {
        var disposable:Disposable? = nil
        guard let artifactBehaviorSubject:BehaviorSubject<NSObject> = artifactBehaviourSubjectHashMap[key] else { return disposable }
        disposable = artifactBehaviorSubject.subscribe(onNext: { event in
            DispatchQueue.main.async {
                iArtifactUpdate.onUpdate(templateObject: event)
            }
        })
        return disposable
    }

    private func sendArtifactDivResponse(artifactKey:String,updateObject:Payload) {

        self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate the div", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "sendArtifactDivResponse", logger_session_id: ""), meta: [:])

        captureSocket?.fetchArtifactChannel(event:EventName.ARTIFACT_INIT_DIV.rawValue, payload: updateObject, completion: { onArtifactSuccess, onArtifactFailure in
            if onArtifactSuccess != nil {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate the dive success", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "sendArtifactDivResponse", logger_session_id: ""), meta: [:])

                let responseObject = onArtifactSuccess?.value(forKey: "response") as! NSObject
                var error = ""
                if let result = responseObject.value(forKey: "error") as? String {
                    error = result
                } else if let _ = responseObject.value(forKey: "error") as? NSNull {
                    error = ""
                }
                if error == "NOT_ELIGIBLE_OR_LIMIT_REACHED" {
                    var divObject = updateObject[artifactKey] as! [String:Any]
                    divObject.updateValue("div_done", forKey: "div_status")
                    if self.tamperedDocumentList.contains(divObject["document_type_key"] as! String) {
                        if self.check_tampered_pan {
                            divObject.updateValue("pan_tampering_done", forKey: "pan_tampering_status")
                        }
                    }
                    self.divProgressList.removeAll { $0 == "\(artifactKey)"}
                    self.updateArtifactObject(updateObject: divObject as NSObject, artifactKey: artifactKey)
                }
            } else {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate the dive failure", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "sendArtifactDivResponse", logger_session_id: ""), meta: [:])
            }
        })
    }
    public func artifactInitiateCheck(payload:Payload) {

        self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate check", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "artifactInitiateCheck", logger_session_id: ""), meta: [:])
        DALCapture.printf("Core", "Artifact Initiate check", "sent")
        self.captureSocket?.fetchArtifactChannel(event: EventName.ARTIFACT_INIT_CHECK.rawValue, payload: payload, completion: { onArtifactSuccess, onArtifactFailure in
            if onArtifactSuccess != nil {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate check success", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactInitiateCheck", logger_session_id: ""), meta: [:])
            } else {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate check failure", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactInitiateCheck", logger_session_id: ""), meta: [:])
            }
        })
    }
    public func initiateTask(taskKey:String,iTaskResultCallback:ITaskResultCallback? = nil) {
        self.iTaskResultCallback = iTaskResultCallback
        var newArtifactObject:[String:Any] = [:]
        guard let taskObject = getTaskObjectByTaskKey(taskKey: taskKey) else { return }
        let tasks = getTaskTypeAndArtifactKey(taskKey: taskKey)
        guard let artifactList = taskKeyAndArtifactHashMap[taskKey] else { return }
        if artifactList.count > 0 {
            for value in artifactList.enumerated() {
                guard let artifactObject = getArtifactsObjectByKey(key: value.element) else { return }
                newArtifactObject.updateValue(artifactObject, forKey: value.element)
            }
        }
        var payload:[String:Any] = [:]
        payload.updateValue(tasks?.getTaskType() ?? "", forKey: "task_type")
        payload.updateValue(taskKey, forKey: "task_key")
        payload.updateValue(newArtifactObject as NSObject, forKey: "artifacts")
        payload.updateValue(taskObject, forKey: "tasks")

        DALCapture.printf("Core", "Artifact Initiate check", "sent")
        self.captureSocket?.fetchArtifactChannel(event: EventName.ARTIFACT_INIT_CHECK.rawValue, payload: payload, completion: { onArtifactSuccess, onArtifactFailure in
            if onArtifactSuccess != nil {
                let responseObject = onArtifactSuccess?.value(forKey: "response") as! NSObject
                var error = ""
                if let result = responseObject.value(forKey: "error") as? String {
                    error = result
                } else if let _ = responseObject.value(forKey: "error") as? NSNull {
                    error = ""
                }
                if error == "" {
                    iTaskResultCallback?.onFailure(message: onArtifactFailure ?? "")
                    self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate check failure", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactInitiateCheck", logger_session_id: ""), meta: [:])
                } else if error == "NOT_ELIGIBLE_OR_LIMIT_REACHED" {
                    var obj:[String:Any] = [:]
                    guard let object = self.getTaskObjectByTaskKey(taskKey: taskKey) as? [String : Any] else { return }
                    obj = object
                    obj.updateValue("completed", forKey: "status")
                    obj.updateValue(error, forKey: "error")
                DALCapture.printf("Core", "\(taskKey)", "Status completed with error")
                self.updateTaskObject(taskKey: taskKey, resultObject: obj as NSObject)
                }
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate check success", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactInitiateCheck", logger_session_id: ""), meta: [:])
            }
        })

    }
    public func artifactPanTamperedInitiateCheck(payload:Payload) {

        self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate check", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "artifactInitiateCheck", logger_session_id: ""), meta: [:])

        self.captureSocket?.fetchArtifactChannel(event: EventName.ARTIFACT_INIT_PAN_TAMPERED.rawValue, payload: payload, completion: { onArtifactSuccess, onArtifactFailure in
            if onArtifactSuccess != nil {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate check success", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactInitiateCheck", logger_session_id: ""), meta: [:])
            } else {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "initiate check failure", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactInitiateCheck", logger_session_id: ""), meta: [:])
            }
        })
    }
    private func sessionData(event:SessionEnum,payload:NSObject) {
        switch event {
        case .SESSION_OK:

            DALCapture.printf("Core", "SESSION_OK", "")
            pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SESSION_OK", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "sessionData", logger_session_id: ""), meta: [:])

            if listPageSeq.count == 0 {
                let jsonObject = payload.value(forKey:"response") as! NSDictionary as NSObject
                parsePageSequence(object: jsonObject)
                iPSCallback?.onPageSequenceSuccess(object:jsonObject)
            }
            break

        case .SESSION_ERROR:

            DALCapture.printf("Core", "SESSION_ERROR", "")
            pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SESSION_ERROR", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "sessionData", logger_session_id: ""), meta: [:])

            iPSCallback?.onPageSequenceFailure(message: "Channel Error")
            break

        case .SESSION_RECONNECTING:
            DALCapture.printf("Core", "SESSION_RECONNECTING", "")
            pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SESSION_RECONNECTING", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "sessionData", logger_session_id: ""), meta: [:])
            break

        case .SESSION_CONNECTED:
            DALCapture.printf("Core", "SESSION_CONNECTED", "")
            pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SESSION_CONNECTED", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "sessionData", logger_session_id: ""), meta: [:])
            break

        case .SESSION_DISCONNECTED:
            DALCapture.printf("Core", "SESSION_DISCONNECTED", "")
            pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SESSION_DISCONNECTED", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "sessionData", logger_session_id: ""), meta: [:])

            let result = payload.value(forKey: "code") as! String
            if result == "SESSION_OVERRIDE" {
                DALCapture.printf("Core", "SESSION_OVERRIDE", "")
                iPSCallback?.onPageSequenceIntermediate()
            }
            break
        }

    }
    private func artifactData(event:ArtifactEnum,payload:NSObject) {
        switch event {
        case .ARTIFACT_ERROR:
            DALCapture.printf("Core", "ARTIFACT_ERROR", "")
            break
        case .ARTIFACT_DIV_RESPONSE:
            DALCapture.printf("Core", "ARTIFACT_DIV_RESPONSE", "")

            pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "ARTIFACT_DIV_RESPONSE", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactData", logger_session_id: ""), meta: [:])
            let dataArray = payload.value(forKey: "data") as! NSArray
            print(dataArray)
            for(_,value) in dataArray.enumerated() {
                let data = value as! NSObject
                if let _ = data.value(forKey: "value") {
                    guard let key = data.value(forKey: "key") as? String else { return }
                    var error = ""
                    if data.value(forKey: "error") is NSNull {
                        error = ""
                    } else {
                        error = data.value(forKey: "error") as? String ?? ""
                    }
                    var artifactObject = getArtifactsObjectByKey(key: key) as! [String:Any]
                    artifactObject.updateValue(error, forKey: "error")
                    artifactObject.updateValue("div_done", forKey: "div_status")
                    if divProgressList.contains(key) {
                        if tamperedDocumentList.contains(artifactObject["document_type_key"] as! String) {
                            if check_tampered_pan {
                                artifactObject.updateValue("pan_tampering_pending", forKey: "pan_tampering_status")
                                var payload:[String:Any] = [:]
                                payload.updateValue(artifactObject as NSObject, forKey: key)
                                if error == "" {
                                artifactPanTamperedInitiateCheck(payload: payload)
                                } else {
                                    divProgressList.removeAll { $0 == "\(key)"}
                                }
                            } else {
                                divProgressList.removeAll { $0 == "\(key)"}
                            }
                        } else {
                            divProgressList.removeAll { $0 == "\(key)"}
                        }
                    }
                    updateArtifactObject(updateObject: artifactObject as NSObject, artifactKey: key)
                    self.artifactsCallback?.onInitiateDiv(event: "DIV_RESPONSE", jsonObject: data)
                }
            }
            break
        case .ARTIFACT_CHECK_RESPONSE:
            DALCapture.printf("Core", "ARTIFACT_CHECK_RESPONSE", "")
            pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "ARTIFACT_CHECK_RESPONSE", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactData", logger_session_id: ""), meta: [:])
            let array = payload.value(forKey: "data") as! NSArray
            print(array)
            if array.count > 0 {
                for(_,value) in array.enumerated() {
                    guard let value = value as? NSObject else { return }
                    var error = ""
                    if value.value(forKey: "error") is NSNull {
                        error = ""
                    } else {
                        error = value.value(forKey: "error") as? String ?? ""
                    }
                    if value.value(forKey: "type") as? String ?? "" == "artifact" {
                        let artifactKey = value.value(forKey: "key") as? String ?? ""
                        if let object = getArtifactsObjectByKey(key: artifactKey) {
                            var artifactObject = object as! [String:Any]
                            artifactObject.updateValue(error, forKey: "error")
                            if divProgressList.contains(artifactKey) {
                                if tamperedDocumentList.contains(artifactObject["document_type_key"] as! String) {
                                    artifactObject.updateValue("pan_tampering_done", forKey: "pan_tampering_status")
                                    DALCapture.printf("Core", "\(artifactKey)", "Pan tampered done updating")
                                }
                                divProgressList.removeAll { $0 == "\(artifactKey)"}
                            }
                            updateArtifactObject(updateObject: artifactObject as NSObject, artifactKey: artifactKey)
                        }
                    } else {
                        let taskKey = value.value(forKey: "key") as? String ?? ""
                        if let newResultObject = value.value(forKey: "result") as? NSObject {
                            DispatchQueue.main.async {
                                var obj:[String:Any] = [:]
                                guard let object = self.getTaskObjectByTaskKey(taskKey: taskKey) as? [String : Any] else { return }
                                obj = object
                                obj.updateValue(newResultObject, forKey: "result")
                                if error == "" {
                                    obj.updateValue("completed", forKey: "status")
                                } else {
                                    obj.updateValue("failed", forKey: "status")
                                    obj.updateValue(error, forKey: "error")
                                }
                                DALCapture.printf("Core", "\(taskKey)", "Status completed updating")
                                self.updateTaskObject(taskKey: taskKey, resultObject: obj as NSObject)
                            }
                        }

                    }
                }
                if iTaskResultCallback != nil {
                    self.iTaskResultCallback?.onTaskResult(responseArray: array)
                }
            }
            break
        case .ARTIFACT_DOC_RETRIEVAL:
            DALCapture.printf("Core", "ARTIFACT_DOC_RETRIEVAL", "")
            pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "ARTIFACT_DOC_RETRIEVAL", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactData", logger_session_id: ""), meta: [:])


            guard let jsonArray = payload.value(forKey: "data") as? NSArray else { return }
            var requestError = ""
            var key = ""
            for(_,value) in jsonArray.enumerated() {
                let response = value as! NSObject
                if let _ = response.value(forKey: "result") as? NSObject {
                    if let result = response.value(forKey: "request_error") as? String {
                        requestError = result
                    } else if let _ = response.value(forKey: "request_error") as? NSNull {
                        requestError = ""
                    }
                } else {
                    key = response.value(forKey: "key") as! String
                    if let object = getArtifactsObjectByKey(key: key)  {
                        var artifactObject = object as! [String:Any]
                        artifactObject.updateValue(requestError, forKey: "error")
                        artifactObject.updateValue(true, forKey: "present")
                        artifactObject.updateValue("div_done", forKey: "div_status")
                        updateArtifactObject(updateObject: artifactObject as NSObject, artifactKey: key)
                        guard let documentBehaviorSubject:BehaviorSubject<NSObject> = artifactBehaviourSubjectHashMap[key] else { return }
                        documentBehaviorSubject.onNext(artifactObject as NSObject)
                        break
                    }
                }
            }
            break
        case .ARTIFACT_NOTIFY_DOC_RETRIEVAL:
            DALCapture.printf("Core", "ARTIFACT_NOTIFY_DOC_RETRIEVAL", "")
            pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "ARTIFACT_DOC_RETRIEVAL", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "artifactData", logger_session_id: ""), meta: [:])


            guard let jsonArray = payload.value(forKey: "data") as? NSArray else { return }
            var requestError = ""
            var key = ""
            for(_,value) in jsonArray.enumerated() {
                let response = value as! NSObject
                if let _ = response.value(forKey: "result") as? NSObject {
                    if let result = response.value(forKey: "request_error") as? String {
                        requestError = result
                    } else if let _ = response.value(forKey: "request_error") as? NSNull {
                        requestError = ""
                    }
                } else {
                    key = response.value(forKey: "key") as! String
                    if let object = getArtifactsObjectByKey(key: key)  {
                        var artifactObject = object as! [String:Any]
                        artifactObject.updateValue(requestError, forKey: "error")
                        artifactObject.updateValue(true, forKey: "present")
                        var metaData:[String:Any] = [:]
                        metaData.updateValue("document_fetcher", forKey: "capture_mechanism")
                        artifactObject.updateValue(metaData as NSObject, forKey: "metadata")
                        self.updateArtifacts(artifactKey: key, updateObject: artifactObject as NSObject, isFromupload: false)
                        break
                    }
                }
            }
            break
        }
    }
    /// This method gives the template object. Required parameter is Template ID.
    /// - Parameter templateId: Template Id
    /// - Returns: Template Object
    public func getTemplateObjectByTemplateId(templateId:String) -> NSObject? {
        guard let tempObj:BehaviorSubject<NSObject> = templateBehaviorSubjectHashMap[templateId] else { return nil}
        var obj = NSObject()
        do {
            obj = try tempObj.value()
        }
        catch {
        }
        return obj
    }
    /// This method gives the artifact object. Required parameter is artifact Key.
    /// - Parameter key: Artifact Key
    /// - Returns: Artifact Object
    public func getArtifactsObjectByKey(key:String) -> NSObject? {
        guard let artifactObj = artifactHashMap[key] else { return nil }
        return artifactObj
    }
    /// This method gives the Capture page object. Required parameter is Template Id
    /// - Parameter templateId: Template Id
    /// - Returns: Capture Array
    public func getCaptureItem(templateId:Int) -> NSArray? {
        guard let captureItem = captureItemHashMap[templateId] else { return nil}
        return captureItem
    }
    /// This method gives the task key. Required parameter is artifact Key
    /// - Parameter artifactKey: Artifact Key
    /// - Returns: Task Key
    public func getTaskKey(artifactKey:String) -> String? {
        guard let taskObject = taskDetailHashMap[artifactKey] as? NSObject else { return nil}
        let taskKey = taskObject.value(forKey: "task_key") as! String
        return taskKey
    }
    /// This method gives the task type. Required parameter is artifact Key
    /// - Parameter artifactKey: Artifact Key
    /// - Returns: Task Type
    public func getTaskType(artifactKey:String) -> String? {
        guard let taskObject = taskDetailHashMap[artifactKey] as? NSObject else { return nil}
        let taskType = taskObject.value(forKey: "task_type") as! String
        return taskType
    }

    private func getCaptureItem(task_key:String,task_type:String,jsonObject:NSObject) {
        let templateId = jsonObject.value(forKey: "template_id") as! Int
        if let _ = jsonObject.value(forKey: "tasks") {
            let jsonArray = jsonObject.value(forKey: "tasks") as! NSArray
            for(index,_) in jsonArray.enumerated() {
                let taskObject = jsonArray[index] as! NSObject
                var artifactKey = ""
                let taskTemplateId = taskObject.value(forKey: "template_id") as! Int
                if let _ = taskObject.value(forKey: "artifacts") {
                    let artifactArray = taskObject.value(forKey: "artifacts") as! NSArray
                    if artifactArray.count > 0 {
                        for(i,_) in artifactArray.enumerated() {
                            artifactKey = artifactArray[i] as! String
                            if task_key != "" {
                                var hashMap:[String:String] = [:]
                                hashMap.updateValue(task_key, forKey: "task_key")
                                hashMap.updateValue(task_type, forKey: "task_type")
                                taskDetailHashMap.updateValue(hashMap, forKey: artifactKey)
                            }
                            let object = getArtifactsObjectByKey(key: artifactKey) as! [String:Any]
                            let eventBehaviorSubject:BehaviorSubject<NSObject> = BehaviorSubject.init(value: object as NSObject)
                            artifactBehaviourSubjectHashMap.updateValue(eventBehaviorSubject, forKey: artifactKey)
                            templateIdHashMap.updateValue(taskTemplateId, forKey: artifactKey)
                        }
                    }
                }
                captureItemHashMap.updateValue(jsonArray, forKey:templateId)
                getCaptureItem(task_key: task_key, task_type: task_type, jsonObject: taskObject)
            }
        }
    }
    public func getTaskTypeAndArtifactKey(taskKey:String) -> Tasks? {
        return taskTypeArtifactHashMap[taskKey]
    }
    public func saveTask(taskKey:String,value:String) {
        if value != "" {
        self.saveTask(taskKey: taskKey, value: [value])
        }
    }
    public func saveTask(taskKey:String,value:[String]) {
        guard let tasks:Tasks = getTaskTypeAndArtifactKey(taskKey: taskKey) else { return }
        if tasks.getTaskType() == "verify.qa" {
            var taskObject = getTaskObjectByTaskKey(taskKey: taskKey) as! [String:Any]
            var manualObject:[String:Any] = [:]
            manualObject.updateValue(value, forKey: "value")
            taskObject.updateValue(manualObject as NSObject, forKey: "manual_response")
            if value.count > 0 {
                taskObject.updateValue("completed", forKey: "status")
            } else {
                taskObject.updateValue("pending", forKey: "status")
            }
            updateTaskObject(taskKey: taskKey, resultObject: taskObject as NSObject)
        } else if tasks.getTaskType() == "data_capture.data_capture" {
            let artifactKey = tasks.getArtifactKey()
            var artifactObject = getArtifactsObjectByKey(key: artifactKey) as! [String:Any]
            artifactObject.updateValue((value.map{String($0)}).joined(separator: ","), forKey: "value")
            artifactObject.updateValue(value.count > 0, forKey: "present")
            updateArtifactObject(updateObject: artifactObject as NSObject, artifactKey: artifactKey)
        } else {
            var taskObject = getTaskObjectByTaskKey(taskKey: taskKey) as! [String:Any]
            var manualObject:[String:Any] = [:]
            manualObject.updateValue((value.map{String($0)}).joined(separator: ","), forKey: "value")
            taskObject.updateValue(manualObject as NSObject, forKey: "manual_response")
            if value.count > 0 {
                taskObject.updateValue("completed", forKey: "status")
            } else {
                taskObject.updateValue("pending", forKey: "status")
            }
            updateTaskObject(taskKey: taskKey, resultObject: taskObject as NSObject)
        }
    }
    public func saveTask(view:UIView,taskKey:String,contentType:String,documentType:String,data:Data,artifactCallback:ArtifactsCallback? = nil) {
        self.artifactsCallback = artifactCallback
        var url:String = ""
        var contentType = contentType
        var artifacts:NSObject?
        guard let tasks = getTaskTypeAndArtifactKey(taskKey: taskKey) else { return }
        let artifactKey = tasks.getArtifactKey()
        artifacts = getArtifactsObjectByKey(key: artifactKey)
        let storageProvider = artifacts?.value(forKey: "storage_provider") as! String
        if storageProvider != "storage_service" {
            contentType = ""
        }
        var artifact = artifacts as! [String:Any]
        if let obj = artifact["upload_urls"] as? NSObject {
            let image = obj.value(forKey: contentType) as? NSObject
            let objectId = image?.value(forKey: "object_id") as? String
            artifact.updateValue(objectId ?? "", forKey: "object_id")
            url = image?.value(forKey: "url") as? String ?? ""
//            url = url+"&t="+self.dalConfig.getToken()+"%2F"+self.getCaptureSessionId()+"&auth_service=pg&type=session"
        } else {
            let value = artifact["upload_url"] as! String
            url = value
            url = url+"&t="+self.dalConfig.getToken()+"%2F"+self.getCaptureSessionId()+"&auth_service=pg&type=session"
        }
        artifact.updateValue(true, forKey: "uploading")
        artifact.updateValue("div_pending", forKey: "div_status")
        if divProgressList.isEmpty {
            divProgressList = [artifactKey]
        } else {
            divProgressList.append(artifactKey)
        }
        updateArtifactObject(updateObject: artifact as NSObject, artifactKey: artifactKey)

        var obj:[String:Any] = [:]
        obj.updateValue(artifactKey, forKey: "artifactKey")
        self.artifactsCallback?.onFileUploaded(event: "UPLOAD_INPROGRESS", jsonObject: obj as NSObject)
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            DALCapture.printf("core", "Uploading image/file url",url)
            var request = URLRequest(url: URL(string: url)!, timeoutInterval: Double.infinity)
            request.addValue(contentType, forHTTPHeaderField: "Content-Type")
            request.httpMethod = "PUT"
            request.httpBody = data
            request.addValue("application/json, text/plain, */*", forHTTPHeaderField: "Accept")
            ApiController.uploadFile(request:request,controller: UIViewController()) { success, _ in
                var finalArtifacts = artifact
                if success == true {
                    DALCapture.printf("Core", "Image/File Upload", "Success")
                    finalArtifacts.updateValue(false, forKey: "uploading")
                    finalArtifacts.updateValue(true, forKey: "present")
                    finalArtifacts.updateValue(documentType, forKey: "document_type_key")
                    finalArtifacts.updateValue(contentType, forKey: "content_type")
                    if let metaData = finalArtifacts["metadata"] as? NSObject {
                        var metaData = metaData as! [String:Any]
                        metaData.updateValue("camera", forKey: "capture_mechanism")
                        finalArtifacts.updateValue(metaData as NSObject, forKey: "metadata")
                    }
                    self.updateArtifactObject(updateObject: finalArtifacts as NSObject, artifactKey: artifactKey)
                    var obj:[String:Any] = [:]
                    obj.updateValue(artifactKey, forKey: "artifactKey")
                    self.artifactsCallback?.onFileUploaded(event: "UPLOAD_SUCCESS", jsonObject: obj as NSObject)
                    self.updateArtifacts(artifactKey: artifactKey, updateObject: finalArtifacts as NSObject, isFromupload: true)
                } else {
                    finalArtifacts.updateValue(false, forKey: "present")
                    self.updateArtifactObject(updateObject: finalArtifacts as NSObject, artifactKey: artifactKey)
                    artifactCallback?.onArtifactFailure(event: "UPLOAD_FAILURE", jsonObject: [] as NSObject)
                }
            }
        }
    }
    /// This method is use to get the Capture Session Token.
    /// Call this Method when isNetworkCheckNeeded and isRoomJoinNeeded both are false or,
    /// health check process is completed or,
    /// you want to start the kyc call.
    /// Pass the below required params
    /// - Parameter completion: ApiCallBack which provides the call back of api in onSessionSuccess(),onSessionFailure().
    public func callApi(view:UIView,url: String, completion:@escaping(_ onSuccess: NSObject?, _ onFailure: NSObject?) -> Void) {
        self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "session token", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "getSessionToken", logger_session_id: ""), meta: [:])
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            ApiController.callApi(controller: view.window?.rootViewController ?? UIViewController(), url: url, taskId: self.requestId, token: self.dalConfig.getToken()) { success, failure  in
                if success != nil {
                    self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "session token success", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "callApi", logger_session_id: ""), meta: [:])
                    completion(success, nil)
                } else {
                    self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "session token failure", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "callApi", logger_session_id: ""), meta: [:])
                    completion(nil, failure)
                }
            }
        }
    }
    public func callApiScheduleCallDate(view:UIView,startDate:String,endDate:String, completion:@escaping(_ onSuccess: NSObject?, _ onFailure: NSObject?) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            ApiController.callApiScheduleCall(controller: view.window?.rootViewController ?? UIViewController(), url:"https://capture.kyc.idfystaging.com/scheduling/availability/dates?t=\(self.dalConfig.getToken())&start_date=\(startDate)&end_date=\(endDate)", completion: { success, failure in
                if success != nil {
                    completion(success, nil)
                } else {
                    completion(nil, failure)
                }
            })
        }
    }
    public func callApiScheduleCallTime(view:UIView,date:String,completion:@escaping(_ onSuccess: NSObject?, _ onFailure: NSObject?) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            ApiController.callApiScheduleCall(controller: view.window?.rootViewController ?? UIViewController(), url:"https://capture.kyc.idfystaging.com/scheduling/availability/time?t=\(self.dalConfig.getToken())&date=\(date)", completion: { success, failure in
                if success != nil {
                    completion(success, nil)
                } else {
                    completion(nil, failure)
                }
            })
        }
    }
    public func callApiBookCall(view:UIView,parameters: [String: Any],completion:@escaping(_ onSuccess: Bool?, _ onFailure: Bool?) -> Void) {
            let url = URL(string: "https://capture.kyc.idfystaging.com/scheduling/book?t=\(self.dalConfig.getToken())")!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json, text/plain, */*", forHTTPHeaderField: "Accept")
            do {
              request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
              print(error.localizedDescription)
              return
            }
            ApiController.callApiBookCall(request: request, controller: view.window?.rootViewController ?? UIViewController()) { status, error in
                if status {
                    completion(true, nil)
                } else {
                    completion(nil,false)
                }
            }
    }
    /// Generic method for calling any api / request.
    /// Pass the below required parameters
    /// - Parameters:
    ///   - url: Complete Url of the service which you want to hit.
    ///   - data: Body for the request
    ///   - completion: completion provides the success and failure callback
    public func callApi(view:UIView,url: String,data:Data,completion:@escaping(_ onSuccess: NSObject?, _ onFailure: NSObject?) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            ApiController.callApi(controller: view.window?.rootViewController ?? UIViewController(), url: url,dataInfo: data, token: self.dalConfig.getToken()) { success, failure  in
                if success != nil {
                    completion(success, nil)
                } else {
                    completion(nil, failure)
                }
            }
        }
    }
    /// This method is use to Upload any Image to the server with the following params
    /// - Parameters:
    ///   - url: URL to upload the file
    ///   - fileName: Captured image name optional one.
    ///   - image: image data to send over the api.
    ///   - completion: apiCallBack ApiCallBack which provides the call back of api in onSessionSuccess(), intermediateCallBack(), onSessionFailure().
    public func submitScreenShot(view:UIView,url: String, fileName: String, image: UIImage,storageService:String, completion:@escaping(_ onSuccess: String) -> Void) {
        guard let imageData = image.jpegData(compressionQuality: 0.5) else { return }
        DALCapture.printf("core", "Uploading image url",url)
        var request = URLRequest(url: URL(string: url)!, timeoutInterval: Double.infinity)
        if storageService == "storage_service" {
            request.addValue("image/jpeg", forHTTPHeaderField: "Content-Type")
        } else {
            request.addValue("", forHTTPHeaderField: "Content-Type")
        }
        request.httpMethod = "PUT"
        request.httpBody = imageData
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            ApiController.uploadFile(request:request, controller: view.window?.rootViewController ?? UIViewController()) { success, _ in
                if success == true {
                    completion("success")
                } else {
                    completion("failed")
                }
            }
        }
    }
    /// This method is use to Upload audio to the server with the following params
    /// - Parameters:
    ///   - url: URL to upload the file
    ///   - fileName: Captured image name optional one.
    ///   - image: image data to send over the api.
    ///   - completion: apiCallBack ApiCallBack which provides the call back of api in onSessionSuccess(), intermediateCallBack(), onSessionFailure().
    public func submitAudio(view:UIView,url: String, data: Data,completion:@escaping(_ onSuccess: String) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            ApiController.uploadAudio(url: url, data: data, controller: view.window?.rootViewController ?? UIViewController()) { status,error in
                if status == true {
                    completion("success")
                } else {
                    completion("failed")
                }
            }
        }
    }
    /// In Page assisted-vkyc you will receive two boolean variable isNetworkCheckNeeded and isRoomJoinNeeded if both are true
    /// you have to call this method to initiate health check process.
    /// In Success callback you will get network_check_id, participant_id, room_id
    /// - Parameters:
    ///   - info: JsonObject with room_join, isRoomJoinNeeded, timeout
    ///   - completion:  ApiCallBack which provides the call back of api in onSessionSuccess(), intermediateCallBack(), onSessionFailure().
    public func callApi(view:UIView,info: Data, url: String, completion:@escaping(_ onResponse: NSObject?, _ onFailure: NSObject?) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            ApiController.callApi(controller: view.window?.rootViewController ?? UIViewController(), url: "\(url)?t=\(self.dalConfig.getToken())", info: info) { success, failure in
                if success != nil {
                    completion(success, nil)
                } else {
                    completion(nil, failure)
                }
            }
        }
    }
    /// This method will be used to send the logs.
    /// - Parameters:
    ///   - logs: log details
    ///   - url: url on which you need to send the logs
    public func logger(view:UIView,logs: NSObject, url: String, completion:@escaping(_ onResponse: NSObject?, _ onFailure: NSObject?) -> Void) {
        let urls = url + "?t=" + self.dalConfig.getToken()
        DispatchQueue.main.async {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: logs, options: .prettyPrinted)
                ApiController.logger(controller: view.window?.rootViewController ?? UIViewController(), info: jsonData, url: urls) { success, fail in
                    if success != nil {
                        completion(success, nil)
                    } else {
                        completion(nil, fail)
                    }
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    /// This method connects to the server send event service.
    /// Pass the below required parameters
    /// - Parameters:
    ///   - url: Complete Url of the service which you want to hit.
    ///   - svcSessionId: session id for SSE connection
    ///   - delegate: provides the success and failure callback
    public func initEventSource(url:String, svcSessionId:String,delegate:iTaskRespCallBack) {

        self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "callSSE", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "initEventSource", logger_session_id: ""), meta: [:])

        self.eventSourceDelegate = delegate

        let url = "\(url)?t=\(dalConfig.getToken())&session_id=\(svcSessionId)"
        DispatchQueue.main.async { [self] in
            let serverURL = URL(string: url)!
            eventSource = EventSource(url: serverURL)
            eventSource?.connect()
            eventSource?.onComplete({ [self] (statusCode, reconnect, error) in
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "callSSE", timestamp: "", event_type: "received", event_name: "closed", component: "DataService Core", event_source: "initEventSource", logger_session_id: ""), meta: [:])

                self.eventSourceDelegate?.onClosed()
            })
            eventSource?.onOpen {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "callSSE", timestamp: "", event_type: "received", event_name: "open", component: "DataService Core", event_source: "initEventSource", logger_session_id: ""), meta: [:])
                self.eventSourceDelegate?.onOpen()
            }
            eventSource?.onMessage({ [self] (id, event, data) in
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "onEventReceive", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "initEventSource", logger_session_id: ""), meta: [:])

                guard let dataString = data else {return }
                guard let dict = dataString.convertToDictionary(text: dataString) else { return }
                self.eventSourceDelegate?.onEvent(type: event ?? "", data: dict)
            })
        }
    }
    private func parsePageSequence(object: NSObject?) {
        self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "parse the page sequence", timestamp: "", event_type: "process", event_name: "", component: "DataService Core", event_source: "parsePageSequence", logger_session_id: ""), meta: [:])
        do {
            listPageSeq.removeAll()
            pageIndex = 0
            let pageSequence = object?.value(forKey: "page_sequence") as! NSArray
            for(_, value) in pageSequence.enumerated() {
                var validationModelList = [ValidationModel]()
                let jsonObject = value as! NSObject
                let page = jsonObject.value(forKey: "page") as? String
                let validations = jsonObject.value(forKey: "validations") as! NSArray
                if validations.count > 0 {
                    for validation in validations {
                        validationModelList.append(ValidationModel(validations: validation as! String))
                    }
                }
                listPageSeq.append(PageSequence(page: page ?? "", validationModelList: validationModelList))
            }
        }
    }
    /// This method is used for uploading the image and different artifacts. User needs to subscribe its different Behavioural subject for availing its callback.
    /// - Parameters:
    ///   - artifactKey: artifactKey of the document which you want to upload.
    ///   - documentKey: document key of the image which you want to upload.
    ///   - image: Image file which you want to upload.
    ///   - url: uri path of the image which you want to upload.
//    public func uploadHelper(artifactKey:String,documentKey:String,image:UIImage,url:String) {
//        self.uploadArtifacts(artifactKey: artifactKey, documentKey: documentKey, image: image)
//    }
    /// This method is used to submit the page data on session channel.
    /// - Parameter completion:  Call back to get the success failure response
    public func submitArtifacts(completion:@escaping(_ onResponse: NSObject) -> Void) {
        pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "submit artifact", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "submitArtifacts", logger_session_id: ""), meta: [:])

        captureSocket?.fetchSessionChannel(event: EventName.SESSION_INITIATE.rawValue, payload: ["artifacts": artifactHashMap as NSObject, "tasks": taskHashMap as NSObject], completion: { onSessionSuccess, onSessionFailure in
            if onSessionSuccess != nil {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "submit artifact success", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "submitArtifacts", logger_session_id: ""), meta: [:])

                guard let object = onSessionSuccess else { return }
                completion(object)
            } else {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "submit artifact failure", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "submitArtifacts", logger_session_id: ""), meta: [:])

                guard let str = onSessionFailure else { return }
                completion(["message":str] as NSObject)
            }
        })
    }
    /// Use this method to send events to the session channel.
    /// - Parameters:
    ///   - event: Event Name
    ///   - skillObj: Payload object
    ///   - completion: Callback type
    public func sendEvent(event:String,skillObj:Payload,completion:@escaping(_ onResponse: NSObject) -> Void) {
        pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "send all event", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "sendEvent", logger_session_id: ""), meta: [:])
        print(event)
        print(skillObj)
        captureSocket?.fetchSessionChannel(event:event, payload: skillObj, completion: { onSessionSuccess, onSessionFailure in
            if onSessionSuccess != nil {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "send all event", timestamp: "", event_type: "received", event_name: "", component: "DataService Core", event_source: "sendEvent", logger_session_id: ""), meta: [:])

                guard let object = onSessionSuccess else { return }
                completion(object)
            } else {
                guard let str = onSessionFailure else { return }
                completion(["message":str] as NSObject)
            }
        })
    }
    /// Use this method to update the artifact data. provide the artifact key and the updated artifact object.
    /// This method will be used while building the custom capture.
    /// - Parameters:
    ///   - updateObject: Artifact Object
    ///   - artifactKey: Artifact Key
    public func updateArtifactObject(updateObject:NSObject,artifactKey:String) {
        artifactHashMap.updateValue(updateObject, forKey: artifactKey)
        let behaviorSubject = artifactBehaviourSubjectHashMap[artifactKey]
        if behaviorSubject != nil {
            behaviorSubject?.onNext(updateObject)
        }

    }
    public func getValueforArtifactObject(artifactKey:String?) -> String {
        let artifacts = artifactObject as! [String:Any]
        guard let keyValue = (artifacts[artifactKey ?? ""] as? [String:Any])?["value"] as? String else { return "" }
        return keyValue
    }
    /// This method updates the task object based on the artifact key.
    /// - Parameters:
    ///   - artifactKey: Artifact Key
    ///   - resultObject: Update Task Object
    public func updateTaskObject(taskKey:String,resultObject:NSObject) {
        taskHashMap.updateValue(resultObject, forKey: taskKey)
        let behaviorSubject = taskBehaviorSubjectHashMap[taskKey]
        if behaviorSubject != nil {
            behaviorSubject?.onNext(resultObject)
        }
    }
    public func updateOcrTaskObject(taskKey:String,resultObject:NSObject) {
        taskHashMap.updateValue(resultObject, forKey: taskKey)
    }
    public func getArtifactKey(object:NSObject) -> String {
        guard let artifactArray = object.value(forKey: "artifacts") as? NSArray else { return ""}
        var artifactKey = ""
        for(_, _) in artifactArray.enumerated() {
            artifactKey = artifactArray[0] as! String
        }
        return artifactKey
    }
    private func processArtifacts() {
        self.iReqDocCallBack?.onReqDocSuccess(artifactsList: tasksList)
    }

    /// This method is used in custom capture. It provides the list of documents and its details that needs to be uploaded in the capture process.
    /// - Parameter callBack: Provides the result in onReqDocSuccess and onReqDocFailure.
    public func getRequiredDocuments(callBack:IReqDocCallback) {
        pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "ListOfDocs", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "getRequiredDocuments", logger_session_id: ""), meta: [:])
        handleCaptureSocket(callBack: callBack)
    }
    private func handleCaptureSocket(callBack:IReqDocCallback) {
        self.iReqDocCallBack = callBack
        getPageSequence(iPSCallback: self)
    }
    public func onPageSequenceSuccess(object: NSObject?) {
        pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "get page", timestamp: "", event_type: "Received", event_name: "", component: "DataService Core", event_source: "handleCaptureSocket", logger_session_id: ""), meta: [:])
        fetchCapturePageData()
    }
    public func onPageSequenceIntermediate() {

    }

    public func onPageSequenceFailure(message: String?) {

    }
    private func fetchCapturePageData() {

        captureSocket?.fetchSessionChannel(event: EventName.SESSION_FETCH_CONFIG.rawValue, payload: ["page": "capture", "payload": [:]], completion: { onSessionSuccess, onSessionFailure in
            if onSessionSuccess != nil {
                self.pgLogger.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "fetch page data", timestamp: "", event_type: "send", event_name: "", component: "DataService Core", event_source: "fetchPageData", logger_session_id: ""), meta: [:])

                let responseObject = onSessionSuccess?.value(forKey: "response") as! NSObject
                let dataObject = responseObject.value(forKey: "data") as! NSObject
                self.processCapturePayload(payload: dataObject)
                self.processArtifacts()
            }
        })
    }
    /// Use this method to disconnect the pg socket connection
    public func disconnectDalSocket() {
        buttonBehaviorSubject.onNext("")
        DALCapture.printf("Core", "DisconnectDalSocket", "")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            if captureSocket != nil {
                DALCapture.printf("Core", "CaptureSocket", "disconnect")
                captureChannel?.leave()
                captureSocket?.disconnect()
                captureChannel = nil
                captureSocket = nil
                listPageSeq.removeAll()
            }
        }
    }
    /// This method is used to clear the data, subscription etc done in Core.
    public func onDestroy() {
        DALCapture.printf("Core", "DESTROY","success")
        artifactHashMap.removeAll()
        captureItemHashMap.removeAll()
        optionHashMap.removeAll()
        taskDetailHashMap.removeAll()
        templateIdHashMap.removeAll()
        artifactBehaviourSubjectHashMap.removeAll()
        taskBehaviorSubjectHashMap.removeAll()
        templateBehaviorSubjectHashMap.removeAll()
        taskHashMap.removeAll()
        artifactObject = nil
        taskObject = nil
        poaOptionList.removeAll()
        poiOptionList.removeAll()
    }
    /// Disconnect the event source if already created.
    public func disconnectEventSource() {
        if eventSource != nil {
            eventSource?.disconnect()
            eventSource = nil
        }
    }

}

/// This method converts json object to Map object
/// - Parameter value: jsonObject json object
/// - Returns: map object
public func JsonToMap(value: NSObject) -> [String: Any] {
    var map: [String: Any] = [:]
    if let obj = value as? [String: String] {
        for case let (label?, value) in Mirror(reflecting: obj)
            .children.map({ ($0.label, $0.value) }) {
            map.updateValue(value, forKey: label)
        }
    }
    return map
}
extension DALCapture {
    public func getFirstPage() -> PageSequence {
        return listPageSeq[0]
    }
    public func getCurrentPage() -> PageSequence {
        return listPageSeq[pageIndex]
    }
    public func onNextPage() -> PageSequence {
        pageIndex = pageIndex + 1
        if pageIndex < listPageSeq.count {
            return listPageSeq[pageIndex]
        }
        return PageSequence(page: "PAGE_END", validationModelList:nil)
    }
    public func getNextPageName() -> PageSequence {
        var currPg = pageIndex
        currPg = currPg + 1
        if currPg < listPageSeq.count {
            return listPageSeq[currPg]
        }
        return PageSequence(page: "PAGE_END", validationModelList: nil)
    }
    public func onPreviousPage() -> PageSequence {
        pageIndex = pageIndex - 1
        if pageIndex < listPageSeq.count && pageIndex >= 0 {
            return listPageSeq[pageIndex]
        }
        return PageSequence(page: "PAGE_END", validationModelList: nil)
    }
    /// This method calculates the Total turn arround time. To calculate it, send the start time.
    /// - Parameter start: startTime starting time of the process
    /// - Returns: tat total required
    public func getTatSince(start: Double) -> String {
        let tat = NSDate().timeIntervalSince1970 * 1000 - start
        return String(tat)
    }
    public func getThemeConfig() -> ThemeConfig {
        return ThemeConfig.shared
    }
    /// This method will return the Token
    /// - Returns: Token as String
    public func getToken() -> String {
        return self.dalConfig.getToken()
    }
    /// This method will set the Request ID For Further Use
    /// - Returns: requestId value received from avkyc payload
    public func setRequestId(id: String) {
        self.requestId = id
    }

    /// This method will return the Request ID For Further Use
    /// - Returns: requestId returns the value
    public func getRequestId() -> String {
        return self.requestId
    }

    /// This method will return the capture Id
    /// - Returns: capture id as String
    public func getCaptureId() -> String {
        return self.captureId
    }

    /// This method will return the capture session ID
    /// - Returns: session id as String
    public func getCaptureSessionId() -> String {
        return self.captureSessionId
    }
    
    /// This method will set the capture call completed status
    /// - Parameter status: status of the call - true / false
    public func setCallCompletedStatus(status: Bool) {
        self.callCompletedStatus = status
    }
    
    /// This method will return the capture call completed status
    /// - Returns: call completed status as boolean
    public func getCallCompletedStatus() -> Bool {
        return self.callCompletedStatus
    }

    /// This method will return the Status of Capture
    /// - Returns: status as String eg.. Capture_Pending, Review_Required etc
    public func getStatus() -> String {
        return self.status ?? ""
    }
    /// This method will set boolean for overlay enabled
    /// - Parameter status: Boolean
    public func setVideoOverlayEnabled(status: Bool) {
        videoOverlayEnabled = status
    }
    /// This method will return boolean for overlay enabled
    /// - Returns: Boolean
    public func getVideoOverlayEnabled() -> Bool {
        return videoOverlayEnabled
    }
    /// This method will return boolean of ocr status
    /// - Parameter status: Boolean
    public func setCustomerOcrEdit(status: Bool) {
        customer_ocr_edit = status
    }
    /// This method will return boolean for ocr check
    /// - Returns: Boolean
    public func getCustomerOcrEdit() -> Bool {
        return customer_ocr_edit
    }
    public func getCheckPanTempered() -> Bool {
        return check_tampered_pan
    }
    /// This method will set Artifacts array model.
    /// - Parameter list: Artifacts
    public func setArtifactsList(list: [Artifacts]) {
        artifactsList = list
    }
    /// This method will set artifact object.
    /// - Returns: Artifact object.
    public func setArtifactObject(object: NSObject) {
        artifactObject = object
    }
    /// This method will return artifact object.
    /// - Returns: Artifact object
    public func getArtifactObject() -> NSObject {
        return artifactObject!
    }
    /// This method will set task object.
    /// - Parameter object: task object.
    public func setTaskObject(object: NSObject) {
        taskObject = object
    }
    /// This method will return task object
    /// - Returns: Object
    public func getTaskObject() -> NSObject {
        return taskObject!
    }
    /// This method will return reference type.
    /// - Returns: String
    public func getReferenceType() -> String {
        return self.referenceType
    }
    /// This method will set reference type.
    /// - Parameter type: String
    public func setReferenceType(type:String) {
        self.referenceType = type
    }
    /// This method will poa option list array.
    /// - Returns: Array of string
    public func getPoaOptionList() -> [String] {
        return poaOptionList
    }
    /// This method will return poi option list array.
    /// - Returns: Array of string.
    public func getPoiOptionList() -> [String] {
        return poiOptionList
    }
    public func getDocumentList() -> [String] {
        return documentList
    }
    /// This method will return options hashmap array.
    /// - Parameter artifactKey: Artifact Key.
    /// - Returns: Array of string.
    public func getOptionHashMap(artifactKey:String) -> [String]? {
        return optionHashMap[artifactKey]
    }
    /// This method will print the values whenever needed.
    /// - Parameters:
    ///   - lib: In which library it is executing.
    ///   - msg: Type of method & their response.
    ///   - data: Value received from response.
    public static func printf(_ lib:String,_ msg: String,_ data:Any) {
        if DALCapture.isDebug {
            debugPrint("IDFY SDK :" + " " + lib + " " + "---" + " " + msg + " " + "--" + " " + "\(data)")
        }
    }
}
public class StatusModel:Codable {
    public var status = ""
    public var title = ""
    public var message = ""
    public init(status:String,title:String,message:String) {
        self.status = status
        self.title = title
        self.message = message
    }
}
public enum Status: String {
    case Status_Capture_Pending = "capture_pending"
    case Status_Recapture_Pending = "recapture_pending"
    case Status_Review = "review"
    case Status_Failed = "failed"
    case Status_Initiated = "initiated"
    case Status_In_Progress = "in_progress"
    case Status_Review_Required = "review_required"
    case Status_Review_Onhold = "review_onhold"
    case Status_Rejected = "rejected"
    case Status_Approved = "approved"
    case Status_Cancelled = "cancelled"
    case Status_Processed = "processed"
    case Status_Capture_Expired = "capture_expired"
    case Status_Completed = "completed"
    case Status_Purged = "purged"
}
public func getStatus(status:String) -> StatusModel {
    var title = ""
    var message = ""
    switch Status(rawValue: status) {
    case .Status_Approved,.Status_Rejected,.Status_Completed:
        title = "Your application has been completed."
        message = "Thank You for using IDfy."
        break
    case .Status_Capture_Expired, .Status_Failed, .Status_Cancelled,.Status_Purged:
        title = "This link is no longer active."
        message = "Please contact customer support to know more details."
        break
    case .Status_In_Progress,.Status_Initiated,.Status_Processed:
        title = "Your application is in progress."
        message = "updated status of the application will be communicated to you shortly."
        break
    case .Status_Review_Required, .Status_Review_Onhold:
        title = "Your application is currently under review."
        message = "For more details, please reach out to customer support."
        break
    default:
        break
    }
    return StatusModel(status:status, title: title, message:message)
}
