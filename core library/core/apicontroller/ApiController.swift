//
//  AppController.swift
//  app
//
//  Created by Admin on 18/08/21.
//

import Foundation
import UIKit
import Alamofire
import Cleanse
public class ApiController {

    public class func getEnvConfig(controller: UIViewController, baseUrl: String,completion:@escaping(_ success: NSObject?, _ failure: NSObject?) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return }
        let header: HTTPHeaders?  = [
            "Authorization": "",
            "Content-Type": ""

        ]
        var urlRequest = URLRequest(url: URL(string: baseUrl)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "GET"
        urlRequest.headers = header ?? []
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard error == nil else { print(error!.localizedDescription); return }
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode == 200 {
                do {
//                    let responseString =  String(data: data!, encoding: .utf8)
//                    let json:String = "\"{ mohan:fwjkvnkj }\""
//                    let jsonData = json.data(using: .utf8)!
//                    let decoder = JSONDecoder()
//                    if let jsonPetitions = try? decoder.decode(Testing.self, from: jsonData) {
//                        print(jsonPetitions.mohan)
//                        }
//                    let jsons = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
//
//                    if let str = jsons as? Testing {
//                        print(str.mohan)
//                    }
                    completion(nil, nil)
                } catch {
                    print(error)
                }
            } else {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    let object = result as? NSObject
                    let message = object?.value(forKey: "message")
                    completion(nil, ["status": httpResponse.statusCode, "message": message] as? NSObject)
                } catch {
                    print(error)
                }

            }
        }
        task.resume()
    }
    public class func getCaptureStatusDetails(controller: UIViewController, url: String, sessionOverride: Bool, id: String, completion:@escaping(_ success: NSObject?, _ failure: NSObject?  ) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return }
        let url = "\(url)?t=\(id)"
        let headers: HTTPHeaders? = [
                "Authorization": "",
                "Content-Type": "application/x-www-form-urlencoded",
                "override_session": "\(sessionOverride)"
            ]
        var urlRequest = URLRequest(url: URL(string: url)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "GET"
        urlRequest.headers = headers ?? []
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            if error != nil {
                DALCapture.printf("core", "getCaptureSocketInfo error", error!.localizedDescription);
                completion(nil, ["message": "error"] as? NSObject)
            }
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode == 200 {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    completion(result as? NSObject, nil)
                    DALCapture.printf("core", "getCaptureSocketInfo response","success")
                } catch {
                    DALCapture.printf("core", "getCaptureSocketInfo error",error)
                }
            } else {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    let object = result as? NSObject
                    let message = object?.value(forKey: "message")
                    completion(nil, ["status": httpResponse.statusCode, "message": message] as? NSObject)
                    DALCapture.printf("core", "getCaptureSocketInfo response",result)
                } catch {
                    DALCapture.printf("core", "getCaptureSocketInfo error",error)
                }

            }
        }
        task.resume()
    }
    public class func callApiScheduleCall(controller: UIViewController, url: String, completion:@escaping(_ success: NSObject?, _ failure: NSObject?) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return completion(nil,["status": "No Internet"] as? NSObject)}
        let header: HTTPHeaders?  = [
            "Authorization": "",
            "Content-Type": ""

        ]
        var urlRequest = URLRequest(url: URL(string: url)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "GET"
        urlRequest.headers = header ?? []
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard error == nil else { print(error!.localizedDescription); return }
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode == 200 {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    DALCapture.printf("core", "get availability date response", result)
                    completion(result as? NSObject, nil)
                } catch {
                    DALCapture.printf("core", "get availability date response error", error)
                }
            } else {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    let object = result as? NSObject
                    let message = object?.value(forKey: "message")
                    DALCapture.printf("core", "get availability date response", result)
                    completion(nil, ["status": httpResponse.statusCode, "message": message] as? NSObject)
                } catch {
                    DALCapture.printf("core", "get availability date response error", error)
                }

            }
        }
        task.resume()
    }
    public class func callApiBookCall(request:URLRequest, controller: UIViewController, completion:@escaping(_ status: Bool, _ error: String) -> Void) {
        let session = URLSession.shared
        let task = session.dataTask(with: request) { data, response, error in
            if let error = error {
                print("Post Request Error: \(error.localizedDescription)")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode)
            else {
                print("Invalid Response received from the server")
                return
            }

            guard let responseData = data else {
                print("nil Data received from the server")
                return
            }

            do {
                if let _ = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String: Any] {
                    completion(true,"success")
                } else {
                    throw URLError(.badServerResponse)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    public class func callApi(controller: UIViewController, url: String, taskId: String, token: String, completion:@escaping(_ success: NSObject?, _ failure: NSObject?) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return completion(nil,["status": "No Internet"] as? NSObject)}
        let url = "\(url)?task_id=\(taskId)&t=\(token)"
        let header: HTTPHeaders?  = [
            "Authorization": "",
            "Content-Type": ""
            
        ]
        var urlRequest = URLRequest(url: URL(string: url)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "GET"
        urlRequest.headers = header ?? []
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard error == nil else { print(error!.localizedDescription); return }
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode == 200 {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    DALCapture.printf("core", "getSession response", result)
                    completion(result as? NSObject, nil)
                } catch {
                    DALCapture.printf("core", "getSession response error", error)
                }
            } else {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    let object = result as? NSObject
                    let message = object?.value(forKey: "message")
                    DALCapture.printf("core", "getSession response", result)
                    completion(nil, ["status": httpResponse.statusCode, "message": message] as? NSObject)
                } catch {
                    DALCapture.printf("core", "getSession response error", error)
                }

            }
        }
        task.resume()
    }
    public class func callApi(controller: UIViewController, url: String,dataInfo:Data, token: String, completion:@escaping(_ success: NSObject?, _ failure: NSObject?) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return completion(nil,["status": "No Internet"] as? NSObject)}
        let url = "\(url)?t=\(token)"
        let header: HTTPHeaders?  = [
            "Authorization": "",
            "Content-Type": "application/json"

        ]
        DALCapture.printf("core", "Task status url", url)
        var urlRequest = URLRequest(url: URL(string: url)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = dataInfo
        urlRequest.headers = header ?? []
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard error == nil else { print(error!.localizedDescription); return }
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode == 200 {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    completion(result as? NSObject, nil)
                    DALCapture.printf("core", "getTask Status", result)
                } catch {
                    DALCapture.printf("core", "getTask Status error", error)
                }
            } else {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    let object = result as? NSObject
                    let message = object?.value(forKey: "message")
                    completion(nil, ["status": httpResponse.statusCode, "message": message] as? NSObject)
                    DALCapture.printf("core", "getTask Status", result)
                } catch {
                    DALCapture.printf("core", "getTask Status error", error)
                }

            }
        }
        task.resume()
    }
    public class func callApi(controller: UIViewController, url: String, info: Data, completion:@escaping(NSObject?, NSObject?) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return completion(nil,["status": "No Internet"] as? NSObject)}
        let header: HTTPHeaders?  = [
            "Content-Type": "application/json"
        ]
        DALCapture.printf("core", "healthcheck url",url)
        var urlRequest = URLRequest(url: URL(string: url)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
        urlRequest.httpMethod = "POST"
        urlRequest.headers = header ?? []
        
        urlRequest.httpBody = info
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard error == nil else {  DALCapture.printf("core", "healthcheck error",error!.localizedDescription); return }
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode == 200 {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    completion(result as? NSObject, nil)
                    DALCapture.printf("core", "healthcheck response",result)
                } catch {
                    DALCapture.printf("core", "healthcheck error",error)
                }
            } else {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    let object = result as? NSObject
                    let message = object?.value(forKey: "message")
                    completion(nil, ["status": httpResponse.statusCode, "message": message] as? NSObject)
                    DALCapture.printf("core", "healthcheck error",error as Any)
                } catch {
                    DALCapture.printf("core", "healthcheck error",error as Any)
                }

            }
            
        }
        task.resume()
    }
    public class func uploadFile(request:URLRequest, controller: UIViewController, completion:@escaping(_ status: Bool, _ error: String) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return completion(false,"No Internet")}

        let task = URLSession.shared.dataTask(with: request) { data,response, error in
            guard error == nil else { print(error!.localizedDescription); return }
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode == 200 {
                DALCapture.printf("core", "Upload image response","Success")
                completion(true, "")
            } else {
                print(httpResponse)
                DALCapture.printf("core", "Upload image error","Failed")
                completion(false, "")
            }
        }
        task.resume()
    }
    public class func uploadAudio(url:String,data:Data,controller: UIViewController,completion:@escaping(_ status: Bool, _ error: String) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return completion(false,"No Internet")}
        DALCapture.printf("core", "Uploading audio url",url)
        var request = URLRequest(url: URL(string: url)!, timeoutInterval: Double.infinity)
        request.addValue("", forHTTPHeaderField: "Content-Type")  
        request.addValue("application/json, text/plain, */*", forHTTPHeaderField: "Accept")
        request.httpMethod = "PUT"
        request.httpBody = data
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
          guard let data = data else {
              DALCapture.printf("core", "Uploading audio response",error as Any)
            return
          }
            DALCapture.printf("core", "Uploading audio response",String(data: data, encoding: .utf8)!)
            completion(true, "")
        }
        task.resume()
    }
    public class func uploadFile(url:String,fileUrl:URL,controller: UIViewController,completion:@escaping(_ status: Bool, _ error: String) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return completion(false,"No Internet")}
        DALCapture.printf("core", "Uploading file url",url)
        do {
            let fileData = try Data(contentsOf: fileUrl)
            var request = URLRequest(url: URL(string: url)!, timeoutInterval: Double.infinity)
            request.addValue("", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json, text/plain, */*", forHTTPHeaderField: "Accept")
            request.httpMethod = "PUT"
            request.httpBody = fileData
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
              guard let data = data else {
                  DALCapture.printf("core", "Uploading file response",error as Any)
                return
              }
                DALCapture.printf("core", "Uploading file response",String(data: data, encoding: .utf8)!)
                completion(true, "")
            }
            task.resume()
        } catch {

        }
    }
    public class func logger(controller: UIViewController, info: Data, url: String, completion:@escaping(NSObject?, NSObject?) -> Void) {
        if !Connectivity.isConnectedToInternet() { controller.showAlertonNoInternet(); return completion(nil,["status": "No Internet"] as? NSObject)}

        let header: HTTPHeaders?  = [
            "Content-Type": "application/json"
        ]

        var urlRequest = URLRequest(url: URL(string: url)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = info
        urlRequest.headers = header ?? []
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard error == nil else { print(error!.localizedDescription); return }
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode == 200 {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    completion(result as? NSObject, nil)
                } catch {
                    DALCapture.printf("core", "logger error",error)
                }
            } else {
                do {
                    let result  = try JSONSerialization.jsonObject(with: data!)
                    let object = result as? NSObject
                    let message = object?.value(forKey: "message")
                    completion(nil, ["status": httpResponse.statusCode, "message": message] as? NSObject)
                } catch {
                    DALCapture.printf("core", "logger error",error)
                }

            }
        }
        task.resume()
    }
    public class func getEnvConfig() -> EnvConfig? {
        let path = Bundle(for: self).path(forResource: "env_config", ofType: "json")
        let url = URL(fileURLWithPath: path!)
        let sData = try? Data(contentsOf: url)
        guard let data = sData else { return nil  }
        do {
            let result = try JSONDecoder().decode(EnvConfig.self, from: data)
            return result
        } catch let error {
            DALCapture.printf("core", "env response error",error)
            return nil
        }
    }
}
