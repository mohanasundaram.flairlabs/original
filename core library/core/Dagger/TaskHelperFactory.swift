//
//  TaskHelperFactory.swift
//  core
//
//  Created by FL Mohan on 14/06/22.
//

import Foundation
public class TaskHelperFactory {
    public static func getTaskHelper() -> TaskHelper {
        let dalComponent = DalComponentFactory.sharedInstance.getDalComponent()
        return dalComponent.getTaskHelper()
    }
}
