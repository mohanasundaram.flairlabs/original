//
//  DalComponent.swift
//  captureapp
//
//  Created by Admin on 29/03/22.
//

import Foundation


import Foundation
import Cleanse

public struct DalStructure:DalProtocol {

    let dalCapture:DALCapture
    let taskManager:TaskManager
    
    public func getDalCapture() -> DALCapture {
        return dalCapture
    }
    public func getLoggerWrapper() -> LoggerWrapper {
        return LoggerWrapper()
    }
    public func getTaskHelper() -> TaskHelper {
        return TaskHelper(dalCapture: dalCapture, taskManager: taskManager)
    }

}
public struct DalComponent : Cleanse.RootComponent {
    public typealias Root = DalStructure
    public typealias Seed = DalFeed

    public static func configure(binder: SingletonBinder) {
        binder.include(module: DalModule.self)
    }

    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dd:Seed) in
            Root.init(dalCapture:DALCapture(pgLogger: Logger(), dalConfig: dd.dalConfig, captureSocket: CaptureSocket()), taskManager: TaskManager())
        }
    }
}
public struct DalFeed {
    let dalConfig:DalConfig

    public init(dalConfig:DalConfig) {
        self.dalConfig = dalConfig
    }
}
