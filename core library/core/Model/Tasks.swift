//
//  Tasks.swift
//  core
//
//  Created by FL Mohan on 09/06/22.
//

import Foundation

public class Tasks {
    private var artifactKey = ""
    private var type  = ""
    private var taskKey  = ""
    private var taskType  = ""
    private var templateId  = 0
    private var documentType  = ""
    private var mandatory:Bool = false
    private var side  = ""
    private var value  = ""
    private var present:Bool = false
    private var optionList:[String] = []
    private var dependentTask:[String] = []


    public init() {
        
    }
    public init(type:String,artifactKey:String,taskKey:String,taskType:String,templateId:Int,documentType:String,mandatory:Bool, side:String,value:String,present:Bool,optionList:[String]) {
        self.type = type
        self.artifactKey = artifactKey
        self.taskKey = taskKey
        self.taskType = taskType
        self.templateId = templateId
        self.documentType = documentType
        self.mandatory = mandatory
        self.side = side
        self.value = value
        self.present = present
        self.optionList = optionList
    }

    public init(type:String,taskKey:String,taskType:String,mandatory:Bool,templateId:Int) {
        self.type = type
        self.taskKey = taskKey
        self.taskType = taskType
        self.mandatory = mandatory
        self.templateId = templateId
    }

    public init(type:String,taskKey:String,taskType:String,dependentTask:[String]) {
        self.type = type
        self.taskKey = taskKey
        self.taskType = taskType
        self.dependentTask = dependentTask
    }

    public init(artifactKey:String,taskType:String) {
        self.artifactKey = artifactKey
        self.taskType = taskType
    }

    public init(taskType:String) {
        self.taskType = taskType
    }

    public func getType() -> String {
        return type
    }

    public func setType(type:String) {
        self.type = type
    }

    public func getDocumentType() -> String {
        return documentType
    }

    public func setDocumentType(documentType:String) {
        self.documentType = documentType
    }
    public func setValue(value:String) {
        self.value = value
    }

    public func getValue()  -> String {
        return value
    }

    public func getTaskKey() -> String  {
        return taskKey
    }

    public func setTaskKey(taskKey:String) {
        self.taskKey = taskKey
    }

    public func getTaskType() -> String {
        return taskType
    }

    public func setTaskType(taskType:String) {
        self.taskType = taskType
    }

    public func getSide() -> String {
        return side
    }

    public func setSide(side:String) {
        self.side = side
    }

    public func getOptionList() -> [String] {
        return optionList
    }

    public func setOptionList(optionList:[String]) {
        self.optionList = optionList
    }


    public func getTemplateId() -> Int  {
        return templateId
    }

    public func setTemplateId(templateId:Int) {
        self.templateId = templateId
    }

    public func getArtifactKey() -> String {
        return artifactKey
    }

    public func setArtifactKey(artifactKey:String) {
        self.artifactKey = artifactKey
    }

    public func isMandatory() -> Bool {
        return mandatory
    }

    public func setMandatory(mandatory:Bool) {
        self.mandatory = mandatory
    }

    public func isPresent() -> Bool {
        return present
    }

    public func setPresent(present:Bool) {
        self.present = present
    }

    public func getDependentTask() -> [String] {
        return dependentTask
    }

    public func setDependentTask(dependentTask:[String]) {
        self.dependentTask = dependentTask
    }
}
