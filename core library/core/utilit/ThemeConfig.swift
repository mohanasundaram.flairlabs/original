//
//  ThemeConfig.swift
//  app
//
//  Created by Admin on 17/12/21.
//

import Foundation
import UIKit
public class ThemeConfig {
    //TODO: Add default values
    private var headerBackgroundColor = UIColor()
    private var headerTextColor = UIColor()
    private var isDisplayFooter = Bool()
    private var secondaryMainColor = UIColor()
    private var secondaryContrastColor = UIColor()
    private var primaryMainColorString = UIColor()
    private var primaryContrastColorString = UIColor()
    private var successMainColor = UIColor()
    private var successLightColor = UIColor()
    private var successDarkColor = UIColor()
    private var errorMainColor = UIColor()
    private var errorLightColor = UIColor()
    private var errorDarkColor = UIColor()
    private var logo = UIImage()
    private var primaryFont = UIFont()
    private var secondaryFont = UIFont()

    private static var privateSharedInstance: ThemeConfig?

    public static var shared: ThemeConfig = {

        if privateSharedInstance    == nil {
            privateSharedInstance = ThemeConfig()
        }

        return privateSharedInstance!
    }()
    
    public required init() {
        
    }
    
    public func getHeaderBackgroundColor() -> UIColor {
        return headerBackgroundColor
    }
    public func setHeaderBackgroundColor(headerBackgroundColor: String) {
        self.headerBackgroundColor = hexStringToUIColor(hex:headerBackgroundColor)
    }
    public func getHeaderTextColor() -> UIColor {
        return headerTextColor
    }
    public func setHeaderTextColor(headerTextColor: String) {
        self.headerTextColor = hexStringToUIColor(hex:headerTextColor)
    }
    public func getDisplayFooter() -> Bool {
        return self.isDisplayFooter
    }
    public func setDisplayFooter(displayFooter: Bool) {
        self.isDisplayFooter = displayFooter
    }
    public func getSecondaryMainColor() -> UIColor {
        return secondaryMainColor
    }
    public func setSecondaryMainColor(secondaryMainColor: String) {
        self.secondaryMainColor = hexStringToUIColor(hex: secondaryMainColor)
    }
    public func getSecondaryContrastColor() -> UIColor {
        return secondaryContrastColor
    }
    public func setSecondaryContrastColor(secondaryContrastColor: String) {
        self.secondaryContrastColor = hexStringToUIColor(hex: secondaryContrastColor)
    }
    public func getPrimaryMainColor() -> UIColor {
        return primaryMainColorString
    }
    public func setPrimaryMainColor(primaryMainColor: String) {
        self.primaryMainColorString = hexStringToUIColor(hex: primaryMainColor)
    }
    public func getPrimaryContrastColor() -> UIColor {
        return primaryContrastColorString
    }
    public func setPrimaryContrastColor(primaryContrastColor: String) {
        self.primaryContrastColorString = hexStringToUIColor(hex: primaryContrastColor)
    }
    public func setSuccessMainColor(successMainColor: String) {
        self.successMainColor = hexStringToUIColor(hex: successMainColor)
    }
    public func getSuccessMainColor() -> UIColor {
        return successMainColor
    }
    public func setSuccessLightColor(successLightColor: String) {
        self.successLightColor = hexStringToUIColor(hex: successLightColor)
    }
    public func getSuccessLightColor() -> UIColor {
        return successLightColor
    }
    public func setSuccessDarkColor(successDarkColor: String) {
        self.successDarkColor = hexStringToUIColor(hex: successDarkColor)
    }
    public func getSuccessDarkColor() -> UIColor {
        return successDarkColor
    }
    public func setErrorMainColor(errorMainColor: String) {
        self.errorMainColor = hexStringToUIColor(hex: errorMainColor)
    }
    public func getErrorMainColor() -> UIColor {
        return errorMainColor
    }
    public func setErrorLightColor(errorLightColor: String) {
        self.errorLightColor = hexStringToUIColor(hex: errorLightColor)
    }
    public func getErrorLightColor() -> UIColor {
        return errorLightColor
    }
    public func setErrorDarkColor(errorDarkColor: String) {
        self.errorDarkColor = hexStringToUIColor(hex: errorDarkColor)
    }
    public func getErrorDarkColor() -> UIColor {
        return errorDarkColor
    }
    public func getLogo() -> UIImage {
        return logo
    }
    public func setLogo(logo: String) {
        var urlString = logo
        if urlString == "" {
            urlString = "https://blog.idfy.com/content/images/size/w1000/2018/07/idfy_logo_600-600-1.png"
        }
        guard let url = URL(string: urlString) else { return }
        if let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
            self.logo = image
        }
    }
    public func setPrimaryFont(primaryFont:String) {
        self.primaryFont = UIFont(name: primaryFont, size: 16.0) ?? .systemFont(ofSize: 16)
    }
    public func getPrimaryFont() -> UIFont {
        return self.primaryFont
    }
    public func setSecondaryFont(secondaryFont:String) {
        self.secondaryFont = UIFont(name: secondaryFont, size: 14.0) ?? .systemFont(ofSize: 14)
    }
    public func getSecondaryFont() -> UIFont {
        return self.secondaryFont
    }
    public func setThemeConfig(themeObject:NSObject) {
        let customHeader = themeObject.value(forKey: "custom_header") as! NSObject
        self.setHeaderBackgroundColor(headerBackgroundColor: customHeader.value(forKey: "backgroundColor") as? String ?? "")
        if customHeader.value(forKey: "backgroundColor") as? String == "#fff" {
            self.setHeaderBackgroundColor(headerBackgroundColor: "#ffffff")
        }
        self.setHeaderTextColor(headerTextColor: customHeader.value(forKey: "color") as? String ?? "")
        if customHeader.value(forKey: "color") as? String ?? "" == "#000" {
            self.setHeaderTextColor(headerTextColor: "#000000")
        }
        let footerObject = themeObject.value(forKey: "custom_footer") as? NSObject
        if footerObject != nil {
            self.setDisplayFooter(displayFooter: footerObject?.value(forKey: "display") as! Bool)
        }
        let paletteObject = themeObject.value(forKey: "palette") as? NSObject
        if paletteObject != nil {
            let primaryObject = paletteObject?.value(forKey: "secondary") as? NSObject
            if primaryObject != nil {
                self.setSecondaryMainColor(secondaryMainColor: primaryObject?.value(forKey: "main") as? String ?? "")
                if primaryObject?.value(forKey: "main") as? String ?? "" == "#000" {
                    self.setSecondaryMainColor(secondaryMainColor: "#000000")
                } else if primaryObject?.value(forKey: "main") as? String ?? "" == "#fff" {
                    self.setSecondaryMainColor(secondaryMainColor: "#ffffff")
                }
                self.setSecondaryContrastColor(secondaryContrastColor: primaryObject?.value(forKey: "contrastText") as? String ?? "")
                if primaryObject?.value(forKey: "contrastText") as? String ?? "" == "#000" {
                    self.setSecondaryContrastColor(secondaryContrastColor: "#000000")
                } else  if primaryObject?.value(forKey: "contrastText") as? String ?? "" == "#fff" {
                    self.setSecondaryContrastColor(secondaryContrastColor: "#ffffff")
                }
            }
            let primaryPaletteObject = paletteObject?.value(forKey: "primary") as? NSObject
            if primaryPaletteObject != nil {
                self.setPrimaryMainColor(primaryMainColor: primaryPaletteObject?.value(forKey: "dark") as? String ?? "")
                if primaryPaletteObject?.value(forKey: "dark") as? String ?? "" == "#000" {
                    self.setPrimaryMainColor(primaryMainColor: "#000000")
                } else if primaryPaletteObject?.value(forKey: "dark") as? String ?? "" == "#fff" {
                    self.setPrimaryMainColor(primaryMainColor: "#ffffff")
                }
                self.setPrimaryContrastColor(primaryContrastColor: primaryPaletteObject?.value(forKey: "contrastText") as? String ?? "")
                if primaryPaletteObject?.value(forKey: "contrastText") as? String ?? "" == "#000" {
                    self.setPrimaryContrastColor(primaryContrastColor: "#000000")
                } else  if primaryPaletteObject?.value(forKey: "contrastText") as? String ?? "" == "#fff" {
                    self.setPrimaryContrastColor(primaryContrastColor: "#ffffff")
                }
            }
            let successPaletteObject = paletteObject?.value(forKey: "success") as? NSObject
            if successPaletteObject != nil {
                self.setSuccessMainColor(successMainColor: successPaletteObject?.value(forKey: "main") as? String ?? "")
                self.setSuccessLightColor(successLightColor: successPaletteObject?.value(forKey: "light") as? String ?? "")
                self.setSuccessDarkColor(successDarkColor: successPaletteObject?.value(forKey: "dark") as? String ?? "")
            }
            let errorPaletteObject = paletteObject?.value(forKey: "error") as? NSObject
            if errorPaletteObject != nil {
                self.setErrorMainColor(errorMainColor: errorPaletteObject?.value(forKey: "main") as? String ?? "")
                self.setErrorLightColor(errorLightColor: errorPaletteObject?.value(forKey: "light") as? String ?? "")
                self.setErrorDarkColor(errorDarkColor: errorPaletteObject?.value(forKey: "dark") as? String ?? "")
            }
        }
        let typography = themeObject.value(forKey: "typography") as? NSObject
        if typography != nil {
            let fontFamily = typography?.value(forKey: "fontFamily") as? NSObject
            if fontFamily != nil {
                self.setPrimaryFont(primaryFont: fontFamily?.value(forKey: "primary") as? String ?? "")
                self.setSecondaryFont(secondaryFont: fontFamily?.value(forKey: "secondary") as? String ?? "")
            }
        }
    }
}
public func hexStringToUIColor (hex: String) -> UIColor {
    var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

    if cString.hasPrefix("#") {
        cString.remove(at: cString.startIndex)
    }

    if (cString.count) != 6 {
        return UIColor.gray
    }

    var rgbValue: UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
