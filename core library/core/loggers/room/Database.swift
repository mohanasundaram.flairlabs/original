//
//  Database.swift
//  core
//
//  Created by Admin on 03/02/22.
//

import Foundation
import CoreData

class DatabaseManagerSingleton: NSObject {
    private static var privateSharedInstance: DatabaseManagerSingleton?

    static var sharedInstance: DatabaseManagerSingleton {

        if privateSharedInstance == nil {
            privateSharedInstance   =   DatabaseManagerSingleton()
        }

        return privateSharedInstance!
    }

    private(set) var managedObjectContext: NSManagedObjectContext?
    private(set) var managedObjectModel: NSManagedObjectModel?
    private(set) var persistentStoreCoordinator: NSPersistentStoreCoordinator?

    // Returns the managed object context for the application.
    // If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
    var managedObjectContextValue: NSManagedObjectContext {
        if let managedObjectContext = managedObjectContext {
            return managedObjectContext
        }
        let coordinator = persistentStoreCoordinatorValue
        if let coordinator = coordinator {
            managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            managedObjectContext?.persistentStoreCoordinator = coordinator
        }
        return managedObjectContext!
    }

    // Returns the managed object model for the application.
    // If the model doesn't already exist, it is created from the application's model.
    var managedObjectModelValue: NSManagedObjectModel {
        if let managedObjectModel = managedObjectModel {
            return managedObjectModel
        }
        let modelURL = Bundle(for: type(of: self)).url(forResource: "LoggerDatabase", withExtension: "momd")
        if let modelURL = modelURL {
            managedObjectModel = NSManagedObjectModel(contentsOf: modelURL)
        }
        return managedObjectModel!
    }
    // Returns the persistent store coordinator for the application.
    // If the coordinator doesn't already exist, it is created and the application's store added to it.
    var persistentStoreCoordinatorValue: NSPersistentStoreCoordinator? {
        if let persistentStoreCoordinator = persistentStoreCoordinator {
            return persistentStoreCoordinator
        }
        let storeURL = applicationDocumentsDirectory()?.appendingPathComponent("core")
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModelValue)
        let options = [
            NSMigratePersistentStoresAutomaticallyOption: NSNumber(value: true),
            NSInferMappingModelAutomaticallyOption: NSNumber(value: true)
        ]
        do {
            try persistentStoreCoordinator?.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: options)
        } catch {
            abort()
        }
        return persistentStoreCoordinator!
    }

    func applicationDocumentsDirectory() -> URL? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
    }
}
