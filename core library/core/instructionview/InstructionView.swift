//
//  InstructionView.swift
//  app
//
//  Created by Admin on 06/09/21.
//

import UIKit
import Lottie
import Foundation

public enum RetryOptions {
    case SessionContinue
    case SessionOverride
    case scheduling
    case timeOut
}
public protocol InstructionActionDelegate {
    func didSetRetryButton(screen: RetryOptions)
}
public class InstructionView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var messageHeader: UILabel!
    @IBOutlet weak var messageInfo: UILabel!
    @IBOutlet weak var informationOne: UILabel!
    @IBOutlet weak var informationTwo: UILabel!
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var backgroundInfoViewOne: UIView!
    @IBOutlet weak var backgroundInfoViewTwo: UIView!
    @IBOutlet weak var schedulingButton: UIButton!
    @IBOutlet weak var timerLabel:UILabel!
    @IBOutlet weak var alwaysScheduleCallButton:UIButton!
    
    public var delegate: InstructionActionDelegate?

    private var waitingTimer: Timer?
    private var waitTime = 0
    private var message:String?
    private var countDown = 0
    
    let kCONTENT_XIB_NAME           =   "InstructionView"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        
    }
    public func loadNetworkCheckInfoView() {
        self.updateColours()
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "wifi_loading", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        
        imageView.addSubview(animationView)
        
        animationView.play()
        self.messageHeader.text = StringHandler.Wait.rawValue
        self.messageInfo.text = StringHandler.CheckingNetwork.rawValue
        self.informationOne.text = StringHandler.InstructionOne.rawValue
        self.informationTwo.text = StringHandler.InstructionTwo.rawValue
        btnRetry.isHidden = true
        self.informationOne.isHidden = false
        self.informationTwo.isHidden = false
        schedulingButton.isHidden = true
        alwaysScheduleCallButton.isHidden = true
        timerLabel.isHidden = true
    }
    public func loadConnectingToAgentView(messageAfter:String,messageBefore:String,callButtonEnable:Bool) {
        waitingTimer?.invalidate()
        waitingTimer = nil
        self.message = messageAfter
        self.updateColours()
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "agent_connecting", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: 60, height: 40)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        imageView.addSubview(animationView)
        self.messageHeader.text =  messageBefore != "" ? messageBefore : StringHandler.Connecting.rawValue
        if callButtonEnable {
            alwaysScheduleCallButton.isHidden = false
        } else {
            alwaysScheduleCallButton.isHidden = true
        }
        self.messageInfo.text = StringHandler.AcceptingCall.rawValue
        self.informationOne.text = StringHandler.InstructionOne.rawValue
        self.informationTwo.text = StringHandler.InstructionTwo.rawValue
        self.btnRetry.isHidden = true
        self.informationOne.isHidden = false
        self.informationTwo.isHidden = false
        schedulingButton.isHidden = true
    }
    public func startTimer(waitTime:Int) {
        self.waitTime = waitTime
        self.countDown = waitTime
        DispatchQueue.main.async { [self] in
            guard waitingTimer == nil else { return }
            waitingTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(self.stopTimer), userInfo: nil, repeats: true)
        }
        timerLabel.isHidden = false
    }
    @objc func stopTimer() {
        timerLabel.text = "Expected Wait Time: \(countDown) seconds"
        if countDown == 0 {
            countDown = waitTime
            waitingTimer?.invalidate()
            waitingTimer = nil
            timerLabel.isHidden = true
            updateCountDownStatus()
        }
        countDown = countDown - 1
    }
    public func stopRunningTimer() {
        if waitingTimer != nil {
            waitingTimer?.invalidate()
            waitingTimer = nil
        }
    }
    private func updateCountDownStatus() {
        alwaysScheduleCallButton.isHidden = false
        self.messageHeader.text = message
        DispatchQueue.main.async { [self] in
            guard waitingTimer == nil else { return }
            waitingTimer = Timer.scheduledTimer(timeInterval: TimeInterval(waitTime), target: self, selector: #selector(self.stopWaitTimer), userInfo: nil, repeats: false)
        }
    }
    @objc func stopWaitTimer() {
        waitingTimer?.invalidate()
        waitingTimer = nil
        self.delegate?.didSetRetryButton(screen: .timeOut)
        self.delegate = nil
    }
    public func loadConnectingView(message: String) {
        self.updateColours()
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "connecting", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        imageView.addSubview(animationView)
        self.messageHeader.text = message
        self.messageInfo.text = StringHandler.Refresh.rawValue
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        backgroundInfoViewTwo.isHidden = true
        self.btnRetry.isHidden = true
        schedulingButton.isHidden = true
        timerLabel.isHidden = true
        alwaysScheduleCallButton.isHidden = true
    }
    public func loadScreenShotRequestView(title: String,message:String) {
        self.updateColours()
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "connecting", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        imageView.addSubview(animationView)
        self.messageHeader.text = title
        self.messageInfo.text = message
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        backgroundInfoViewTwo.isHidden = true
        self.btnRetry.isHidden = true
        schedulingButton.isHidden = true
        timerLabel.isHidden = true
        alwaysScheduleCallButton.isHidden = true
    }
    public func loadStatusView(title:String?,message:String?,enableScheduling:Bool) {
        self.updateColours()
        contentView.backgroundColor = ThemeConfig.shared.getSecondaryMainColor()
        self.imageView.removeSubviews()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        imageView.image = ThemeConfig.shared.getLogo()
        self.imageView.addSubview(imageView)
        self.messageHeader.text = title ?? StringHandler.Unavailable.rawValue
        self.messageInfo.text = message ?? StringHandler.TryAfterSometimes.rawValue
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = false
        btnRetry.tag = 0
        backgroundInfoViewTwo.isHidden = true
        schedulingButton.isHidden = !enableScheduling
        timerLabel.isHidden = true
        alwaysScheduleCallButton.isHidden = true
    }
    public func loadNetworkDisconnectedStatusView() {
        self.updateColours()
        contentView.backgroundColor = ThemeConfig.shared.getSecondaryMainColor()
        self.imageView.removeSubviews()
        let animationView = AnimationView(name: "no_internet", bundle: Bundle(for: type(of: self)), imageProvider: nil, animationCache: nil)
        animationView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        animationView.play()
        imageView.addSubview(animationView)
        self.messageHeader.text = StringHandler.Disconnected.rawValue
        self.messageInfo.text = StringHandler.GoodNetwork.rawValue
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = false
        btnRetry.tag = 1
        backgroundInfoViewOne.backgroundColor = .none
        backgroundInfoViewTwo.isHidden = true
        schedulingButton.isHidden = true
        timerLabel.isHidden = true
        alwaysScheduleCallButton.isHidden = true
    }
    public func loadLocationFailureView() {
        self.updateColours()
        contentView.backgroundColor = ThemeConfig.shared.getSecondaryMainColor()
        self.imageView.removeSubviews()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        if let myImage = UIImage(named: "NetworkDisconnected") {
            let tintableImage = myImage.withRenderingMode(.alwaysTemplate)
            imageView.image = tintableImage
        }
        imageView.tintColor = .red
        self.imageView.addSubview(imageView)
        self.messageHeader.text = "Location out of bounds"
        self.messageInfo.text = "Location should be within India"
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = false
        btnRetry.tag = 1
        backgroundInfoViewOne.backgroundColor = .none
        backgroundInfoViewTwo.isHidden = true
        schedulingButton.isHidden = true
        timerLabel.isHidden = true
        alwaysScheduleCallButton.isHidden = true
        
    }
    public func loadKycStatusView(title:String,message:String) {
        self.updateColours()
        contentView.backgroundColor = ThemeConfig.shared.getSecondaryMainColor()
        self.imageView.removeSubviews()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        imageView.image = ThemeConfig.shared.getLogo()
        self.imageView.addSubview(imageView)
        self.messageHeader.text = title
        self.messageInfo.text = message
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = true
        backgroundInfoViewOne.backgroundColor = .clear
        backgroundInfoViewTwo.isHidden = true
        schedulingButton.isHidden = true
        timerLabel.isHidden = true
        alwaysScheduleCallButton.isHidden = true

    }
    public func loadHealthCheckFailureView() {
        self.updateColours()
        contentView.backgroundColor = ThemeConfig.shared.getSecondaryMainColor()
        self.imageView.removeSubviews()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        imageView.image = ThemeConfig.shared.getLogo()
        self.imageView.addSubview(imageView)
        self.messageHeader.text = StringHandler.BadNetwork.rawValue
        self.messageInfo.text = StringHandler.InitiateGoodNetwork.rawValue
        self.informationOne.isHidden = true
        self.informationTwo.isHidden = true
        btnRetry.isHidden = false
        btnRetry.tag = 0
        backgroundInfoViewTwo.isHidden = true
        schedulingButton.isHidden = true
        timerLabel.isHidden = true
        alwaysScheduleCallButton.isHidden = true
        
    }
    @IBAction public func didTapButton(_ sender: UIButton) {
        if btnRetry.tag == 0 {
            self.delegate?.didSetRetryButton(screen: .SessionContinue)
        } else {
            self.delegate?.didSetRetryButton(screen: .SessionOverride)
        }
    }
    @IBAction public func didTapSchduleButton(_ sender: UIButton) {
        self.delegate?.didSetRetryButton(screen: .scheduling)
    }
    private func updateColours() {
        alwaysScheduleCallButton.setCornerBorder(color: ThemeConfig.shared.getPrimaryMainColor(), cornerRadius: 10, borderWidth: 0.5)
        alwaysScheduleCallButton.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
        messageHeader.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        messageInfo.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        btnRetry.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
        schedulingButton.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
        backgroundInfoViewOne.backgroundColor = ThemeConfig.shared.getSecondaryMainColor()
    }
}
