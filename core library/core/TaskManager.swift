//
//  TaskManager.swift
//  capture
//
//  Created by FL Mohan on 24/05/22.
//

import Foundation
import RxSwift

typealias callback<T> = ((_ eval:T) -> Bool)
public class TaskManager:IArtifactUpdate,ITaskUpdate,ITaskManager {

    private var dalCapture = DALCapture()
    private var capturePageIndex = 0
    private var captureArray:NSArray = []
    public var allTaskDone = BehaviorSubject<Bool?>(value: nil)
    private var disposableList:[Disposable] = []

    public init() {
    }
    public init(dalCapture:DALCapture) {
        self.dalCapture = dalCapture
    }

    public func registerToArtifactKey(artifactKey:String) {
        guard let disposable = dalCapture.subscribeToArtifactKey(key: artifactKey, iArtifactUpdate: self) else { return }
        disposableList.append(disposable)
    }
    public func registerToTaskKey(taskKey:String) {
        guard let disposable = dalCapture.subscribeToTaskKey(key: taskKey, iTaskUpdate: self) else { return }
        disposableList.append(disposable)
    }
    public func onNextTask(pageNo:Int) {
        destroy()
        capturePageIndex = pageNo

    }
    public func destroy() {
        for disposableList in disposableList {
            disposableList.dispose()
        }
        disposableList.removeAll()
    }

    public func onUpdate(templateObject: NSObject) {
        if checkTaskDone() == -1 {
            allTaskDone.onNext(true)
        } else {
            allTaskDone.onNext(false)
        }
    }

    public func onUpdate(taskObject: NSObject) {
        if checkTaskDone() == -1 {
            allTaskDone.onNext(true)
        } else {
            allTaskDone.onNext(false)
        }
    }
    public func isAllTaskCompleted() -> Bool {
        let isDone = checkTaskDone()
        return isDone == -1
    }
    public func setCaptureArray(captureArray:NSArray) {
        self.captureArray = captureArray
    }

    private func checkTaskDone() -> Int {
        var isValid = -1
        if captureArray.count == 0 {
            return 0
        }
        for (_,_) in captureArray.enumerated() {
            let jsonArray = captureArray[capturePageIndex] as! NSArray
            isValid = findIndex(arr: jsonArray, c: { eval in
                guard let task = eval else { return false }
                return !areAllMandatoryPresent(taskArray: task.value(forKey: "tasks") as? NSArray)
            })
        }
        return isValid
    }
    private func findIndex(arr:NSArray,c:callback<NSObject?>) -> Int {
        for(index,_) in arr.enumerated() {
            if c.self(arr[index] as? NSObject) {
                return index
            }
        }
        return -1
    }


    private func areAllMandatoryPresent(taskArray:NSArray?) -> Bool {
            guard let taskArray = taskArray else { return true }

            return (findIndex(arr: taskArray) { eval in
                guard let task = eval else { return false }
                let artifactArray = task.value(forKey: "artifacts") as? NSArray
                let taskTemplateId = task.value(forKey: "template_id") as? Int
                let taskKey = task.value(forKey: "task_key") as? String
                let taskType = task.value(forKey: "task_type") as? String
                let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
                let type = templateObject?.value(forKey: "type") as! String
                if type == "form" || type == "radio" || type == "dropdown" || type == "checkbox" || (( type == "button" && taskType == "vkyc.assisted_vkyc" || taskType == "self_video.capture_video_activity" || taskType == "self_video.capture_and_verify")) {
                    let objectByTaskKey = dalCapture.getTaskObjectByTaskKey(taskKey: taskKey ?? "")
                    if objectByTaskKey != nil {
                        guard let status = objectByTaskKey?.value(forKey: "status") as? String else { return false }
                        DALCapture.printf("Capture", "Task Manager: \(taskKey ?? "")", "\(status)")
                        return !(status == "completed")
                    }
                }
                return ((artifactArray != nil && artifactArray?.count ?? 0 > 0 && !artifactHashValue(taskObject: task)) || isDivPending(taskObject: task) || isPanTameredPending(taskObject: task) || !areAllMandatoryPresent(taskArray: task.value(forKey: "tasks") as? NSArray))
            }) < 0

        }

    private func isDivPending(taskObject: NSObject) -> Bool {
        let taskTemplateId = taskObject.value(forKey: "template_id") as? Int
        let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
        let type = templateObject?.value(forKey: "type") as! String
        let artifactKey = dalCapture.getArtifactKey(object: templateObject ?? [] as NSObject)
        let artifactObject = dalCapture.getArtifactsObjectByKey(key: artifactKey)
        var divStat = "div_done"
        if type == "image"  {
            if let divStatus = artifactObject?.value(forKey: "div_status") as? String {
               divStat = divStatus
            }
        }
        return divStat == "div_pending"
    }
    private func isPanTameredPending(taskObject: NSObject) -> Bool {
        let taskTemplateId = taskObject.value(forKey: "template_id") as? Int
        let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
        let type = templateObject?.value(forKey: "type") as! String
        let artifactKey = dalCapture.getArtifactKey(object: templateObject ?? [] as NSObject)
        let artifactObject = dalCapture.getArtifactsObjectByKey(key: artifactKey)
        var panTamperedStatus = "pan_tampering_done"
        if type == "image"  {
            if let divStatus = artifactObject?.value(forKey: "pan_tampering_status") as? String {
                panTamperedStatus = divStatus
            }
        }
        return panTamperedStatus == "pan_tampering_pending"
    }
    private func artifactHashValue(taskObject:NSObject?) -> Bool {
        var hashValue = true
        let artifactArray = taskObject?.value(forKey: "artifacts") as? NSArray
        let taskTemplateId = taskObject?.value(forKey: "template_id") as? Int
        let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)")
        let type = templateObject?.value(forKey: "type") as! String
        if type == "image" || type == "text" || type == "filepicker" || type == "radio" || type == "dropdown" || type == "checkbox" {
            let isMandatory = templateObject?.value(forKey: "mandatory") as! Bool
            var error = ""
            let artifactObject = dalCapture.getArtifactsObjectByKey(key: artifactArray?[0] as! String)
            if let result = artifactObject?.value(forKey: "error") as? String {
                error = result
            } else if let _ = artifactObject?.value(forKey: "error") as? NSNull {
                error = ""
            }
            if isMandatory && artifactArray != nil {
                    hashValue = artifactObject?.value(forKey: "present") as! Bool && error == ""
            } else {
                hashValue = error == ""
            }
        }
        return hashValue
    }

}
