//
//  CaptureSocket.swift
//  core
//
//  Created by Admin on 18/04/22.
//

import Foundation
import SwiftPhoenixClient
import Starscream


public class CaptureSocket:NSObject {

    private var webSocketUrl:String = ""
    private var captureId:String = ""
    private var socket:Socket?
    private var sessionChannel:Channel?
    private var artifactsChannel:Channel?
    private var pgLogger:Logger?
    private var iCaptureCallback:ICaptureCallback?
    private static var SOCKET_INTERVAL:Int = 5000
    private static var SOCKET_RECONNECT:Bool = true
    private var isSocketDisconnected:Bool = false
    private var SESSION_SOCKET_TIMEOUT:String = "Session socket time out"
    private var ARTIFACT_SOCKET_TIMEOUT:String = "Artifact socket time out"

    public override init() {
    }
    public func setUpSocket(webSocketUrl:String,captureId:String,pgLogger:Logger,iCaptureCallback:ICaptureCallback) {
        self.webSocketUrl = webSocketUrl
        self.captureId = captureId
        self.pgLogger = pgLogger
        self.iCaptureCallback = iCaptureCallback

        initCaptureSocket()

    }

    private func initCaptureSocket() {
        DispatchQueue.main.async { [self] in
            socket = Socket(webSocketUrl)
            socket?.connect()
            socket?.onError { [self] error in
                DALCapture.printf("Core", "Capture socket connection error", error)
                if !isSocketDisconnected {
                    isSocketDisconnected = true
                    self.iCaptureCallback?.onSocketError()
                }

                var logs = ModalLogDetails()
                logs.service = "SocketConnect"
                logs.event_name = "\(error)"
                logs.event_type = "OnError"
                logs.event_source = "setupSocket"

                pgLogger?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
            }
            socket?.onClose { [self] in
                var logs = ModalLogDetails()
                logs.service = "SocketConnect"
                logs.event_name = "Close"
                logs.event_type = "Closed"
                logs.event_source = "setupSocket"

                pgLogger?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
                DALCapture.printf("Core", "Capture socket connection closed", "")
//                self.iCaptureCallback?.onSocketError()

            }
            socket?.onOpen { [self] in
                var logs = ModalLogDetails()
                logs.service = "SocketConnect"
                logs.event_name = "\(webSocketUrl)"
                logs.event_type = "Invoked"
                logs.event_source = "setupSocket"

                isSocketDisconnected = false

                self.iCaptureCallback?.onSocketOpen()

                pgLogger?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])

                DALCapture.printf("Core", "Capture socket connection opened", "")

                initCaptureChannel()
                initArtifactsChannel()
            }
//            if socket?.isConnected ?? false {
//                DALCapture.printf("Core", "Capture socket connection connected", "")
//                initCaptureChannel()
//                initArtifactsChannel()
//            } else {
//                DALCapture.printf("Core", "Capture socket connection not connected", "")
//            }
        }
    }
    private func initCaptureChannel() {
        sessionChannel = socket?.channel("session:" + captureId, params: [:])
        sessionChannel?.join()
            .receive("error") { message in

                var data  = ModalLogDetails()
                data.service = "ChannelJoin"
                data.event_type = "Error"
                data.exceptionName = message.payload["reason"] as? String
                data.exceptionDescription = "Channel join error callback + \(message.payload["reason"] as! String)"
                self.pgLogger?.log(logLevel: LogLevel.shared.Error, logDetails: data, meta: [:])
                DALCapture.printf("Core", "Capture Channel error", message.payload)
                self.iCaptureCallback?.onSessionReceive(event: .SESSION_ERROR, payload: message.payload as NSObject)
            }
            .receive("ok") {message in

                var data  = ModalLogDetails()
                data.service = "ChannelJoin"
                data.event_type = "Success"
                data.event_name = self.sessionChannel?.topic ?? ""
                data.event_source = "getPageSequence"
                self.pgLogger?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: [:])

                DALCapture.printf("Core", "Capture Channel Joined", "Okay")
                self.iCaptureCallback?.onSessionReceive(event: .SESSION_OK, payload: message.payload as NSObject)
            }
        sessionChannel?.on("_disconnect") { message in
            DALCapture.printf("Core", "Capture Channel Disconnected", message.payload)
            self.iCaptureCallback?.onSessionReceive(event: .SESSION_DISCONNECTED, payload: message.payload as NSObject)
        }
        sessionChannel?.on("reconnecting") { message in
            DALCapture.printf("Core", "Capture Channel Reconnecting", message.payload)
            self.iCaptureCallback?.onSessionReceive(event: .SESSION_RECONNECTING, payload: message.payload as NSObject)
        }
        sessionChannel?.on("connected") { message in
            DALCapture.printf("Core", "Capture Channel Connected", message.payload)
            self.iCaptureCallback?.onSessionReceive(event: .SESSION_CONNECTED, payload: message.payload as NSObject)
        }

    }
    private func initArtifactsChannel() {
        artifactsChannel = socket?.channel("artifact:" + captureId, params: [:])
        artifactsChannel?.join()
            .receive("artifact error") {message in
                DALCapture.printf("Core", "Artifact Channel error", message.payload)
                self.iCaptureCallback?.onArtifactReceive(event: .ARTIFACT_ERROR, payload: message.payload as NSObject)
            }
            .receive("ok") {message in
                DALCapture.printf("Core", "Artifact Channel Joined", message.payload)
            }
        artifactsChannel?.on("artifact:div_response") { message in
            DALCapture.printf("Core", "Artifact Channel div response", "success")
            self.iCaptureCallback?.onArtifactReceive(event: .ARTIFACT_DIV_RESPONSE, payload: message.payload as NSObject)
        }
        artifactsChannel?.on("artifact:check_response") { message in
            DALCapture.printf("Core", "Artifact Channel check response", "success")
            self.iCaptureCallback?.onArtifactReceive(event: .ARTIFACT_CHECK_RESPONSE, payload: message.payload as NSObject)
        }
        artifactsChannel?.on("artifact:notify_document_retrieval") { message in
            DALCapture.printf("Core", "Artifact Channel doc check response", "success")
            self.iCaptureCallback?.onArtifactReceive(event: .ARTIFACT_NOTIFY_DOC_RETRIEVAL, payload: message.payload as NSObject)
        }

        artifactsChannel?.on("artifact:notify_document_retrieval") { message in
            DALCapture.printf("Core", "Artifact Channel Doc retrivel","message")
            self.iCaptureCallback?.onArtifactReceive(event: .ARTIFACT_DOC_RETRIEVAL, payload: message.payload as NSObject)
        }
    }
    public func fetchSessionChannel(event:String,payload:Payload,completion:@escaping(_ onSessionSuccess: NSObject?, _ onSessionFailure: String?) -> Void) {
        do {
            sessionChannel?.push(event, payload: payload, timeout: 30)

                .receive("ok") { payload in
                    DALCapture.printf("Core", "Capture Session Channel push","Success")
                    completion(payload.payload as NSObject,nil)
                }

                .receive("error") { payload in DALCapture.printf("Core", "Session Channel push error", payload.payload) }

                .receive("timeout") { payload in DALCapture.printf("Core", "Session Channel push timeout", payload.payload)
                    completion(nil,self.SESSION_SOCKET_TIMEOUT)
                }
        }

    }
    public func fetchArtifactChannel(event:String,payload:Payload, completion:@escaping(_ onArtifactSuccess: NSObject?, _ onArtifactFailure: String?) -> Void) {
        do {
            artifactsChannel?.push(event, payload: payload, timeout: 30)

                .receive("ok") { payload in DALCapture.printf("Core", "Artifact Channel push ok", payload.payload)
                    completion(payload.payload as NSObject,nil)
                }

                .receive("error") { payload in DALCapture.printf("Core", "Artifact Channel push error", payload.payload) }

                .receive("timeout") { payload in DALCapture.printf("Core", "Artifact Channel push timeout", payload.payload)
                    completion(nil,self.ARTIFACT_SOCKET_TIMEOUT)
                }
        }

    }
    public func disconnect() {
        artifactsChannel?.leave()
        sessionChannel?.leave()
        socket?.disconnect()
        socket = nil
        artifactsChannel = nil
        sessionChannel = nil
    }
}
