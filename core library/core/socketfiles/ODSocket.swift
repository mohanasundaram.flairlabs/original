//
//  ODSocket.swift
//  app
//
//  Created by Admin on 02/09/21.
//

import UIKit
import Foundation
import Alamofire
import SwiftPhoenixClient
import Starscream

public class ODSocket: NSObject {

    var socket:Socket?
    var channel: Channel?
    var session_token:String = ""
    private var OD_URI:String = ""
    var callback:ODSocketCallBack?
    var dalCapture = DALCapture()
    var loggerVs: Logger?
    var loggerStartTime: Double = 0
    var networkErrorOccured:Bool = false
    var paramsObj:[String: Any] = [:]


    public init(delegate: ODSocketCallBack? = nil) {
        self.callback = delegate
    }

    public func initiate(sessionToken: String, socketUrl: String,dalCapture:DALCapture,loggerWrapper:LoggerWrapper) {
        self.dalCapture = dalCapture
        self.session_token = sessionToken
        OD_URI = socketUrl + "t=" + dalCapture.getToken() + "&public_id=" + dalCapture.getRequestId() + "&session_token=" + sessionToken
        
        var logDetails = ModalLogDetails()
        logDetails.component = "AvkycSocket"
        logDetails.reference_id = dalCapture.getRequestId()
        logDetails.reference_type = "AV.TaskID"
        loggerVs?.setDefaults(defaultLogDetails: logDetails)
        
        // set meta defaults
        var metaDefaults: [String: Any] = [:]
        metaDefaults.updateValue(dalCapture.getRequestId(), forKey: "request_uid")
        metaDefaults.updateValue(dalCapture.getCaptureSessionId(), forKey: "session_id")
        metaDefaults.updateValue(OD_URI, forKey: "endpoint")

        loggerVs?.setMetaDefaults(metaDefaults: metaDefaults)
       
        var log = ModalLogDetails()
        log.service_category = "Capture"
        log.service = "ConnectVSGateway"
        log.event_name = "Invoked"
        log.event_type = "Initiate"
        log.event_source = "createSession"
        log.component = "AvkycSocket"
        
        let params = ["t": dalCapture.getToken(), "public_id": dalCapture.getRequestId(), "session_token": sessionToken] as NSObject
        loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: log, meta: ["params": params])

        connectSocket()

    }
    public func connectSocket() {

        socket = Socket(OD_URI)
        socket?.connect()

        socket?.onError { [self] error in
            print("socket connection error: ", error.localizedDescription)

            var logs = ModalLogDetails()
            logs.service = "SocketConnect"
            logs.event_name = "\(error)"
            logs.event_type = "OnError"
            logs.event_source = "setupSocket"

            loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])

            if !networkErrorOccured {
                networkErrorOccured = true
                self.callback?.onDisconnect(code: "NETWORK_DISCONNECTED")
            }
        }
        socket?.onClose { [self] in
            var logs = ModalLogDetails()
            logs.service = "SocketConnect"
            logs.event_name = "Close"
            logs.event_type = "Closed"
            logs.event_source = "setupSocket"

            loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
            DALCapture.printf("Core", "OD Socket connection closed", "")
        }
        socket?.onOpen { [self] in
            var logs = ModalLogDetails()
            logs.service = "SocketConnect"
            logs.event_name = "\(OD_URI)"
            logs.event_type = "Invoked"
            logs.event_source = "setupSocket"

            loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
            DALCapture.printf("Core", "OD Socket connection: opened", "")
        }


        channel = socket?.channel("session:" +  dalCapture.getRequestId(), params: [:])
        channel?.join()
            .receive("error") {message in
                DALCapture.printf("Core", "OD Socket connection error", "")

                self.callback?.onDisconnect(code: "\(message.payload["status"] ?? "")")

                var logs = ModalLogDetails()
                logs.service = "ChannelJoin"
                logs.event_type = "Error"
                logs.event_name = "\(message.payload)"
                logs.event_source = "createSession"
                logs.exceptionName = "\(message.payload)"
                logs.exceptionDescription = "\(message.payload)"

                self.loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])

            }
            .receive("ok") {message in
                DALCapture.printf("Core", "OD Socket channel joined", "")

                var logs = ModalLogDetails()
                logs.service = "ChannelJoin"
                logs.event_name = "\(message.payload)"
                logs.event_type = "Joined"
                logs.event_source = "createSession"

                self.loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
            }
        channel?.on("fetch_task_data") { message in
            DALCapture.printf("Core", "OD Socket in fetch task data", "")
            self.sendTaskDataPayload()

            var logs = ModalLogDetails()
            logs.service = "OnChannel"
            logs.event_name = "fetch_task_data"
            logs.event_type = "eventReceived"
            logs.event_source = "createSession"

            self.loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: ["data": message.payload])

        }
        channel?.on("waiting_status") { message in
            DALCapture.printf("Core", "OD Socket in waiting status", "")
            if message.payload["agents_available"] as! Int == 0 {
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.callback?.onDisconnect(code: "\(message.payload["agents_available"] ?? "")")
                }
            }
            if message.payload["agents_available"] as! Bool == true {
                self.fetchWaitTime()
            }
            var logs = ModalLogDetails()
            logs.service = "OnChannel"
            logs.event_name = "waiting_status"
            logs.event_type = "eventReceived"
            logs.event_source = "createSession"

            self.loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: ["data": message.payload])
        }
        channel?.on("response") { message in
            DALCapture.printf("Core", "OD Socket response", message)

            var logs = ModalLogDetails()
            logs.service = "OnChannel"
            logs.event_name = "response"
            logs.event_type = "eventReceived"
            logs.event_source = "createSession"

            self.loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: ["data": message.payload])
        }

        channel?.on("connected") { message in
            DALCapture.printf("Core", "OD Socket connected", "")

            let room_id = message.payload["room_sid"]
            let participant_id = message.payload["user_sid"]

            print(room_id as? String ?? "")
            print(participant_id as? String ?? "")

            self.callback?.onConnected(roomId: room_id as! String, participantId: participant_id as! String)

            var logs = ModalLogDetails()
            logs.service = "OnChannel"
            logs.event_name = "connected"
            logs.event_type = "eventReceived"
            logs.event_source = "createSession"

            self.loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: ["data": message.payload])
        }
        channel?.on("_disconnect") { message in
            DALCapture.printf("Core", "OD Socket agent call disconnected ", "")
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.callback?.onDisconnect(code: "\(message.payload["code"] ?? "")")
            }
            var logs = ModalLogDetails()
            logs.service = "OnChannel"
            logs.event_name = "_disconnect. \(message.payload)"
            logs.event_type = "eventReceived"
            logs.event_source = "createSession"

            self.loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: ["data": message.payload])
        }
        channel?.on("screenshot") { message in
            DALCapture.printf("Core", "OD Socket received screenshot request", "")

            let artifact_key = message.payload["artifact_key"] as! String
            var link: String = message.payload["link"] as! String
            let transaction_id = message.payload["transaction_id"] as! String
            let storageService = message.payload["storage_provider"] as! String

            if !artifact_key.isEmpty {

                link = link+"&t="+self.dalCapture.getToken()+"%2F"+self.dalCapture.getCaptureSessionId()+"&auth_service=pg&type=session"
                print("New upload Link: \(link)")
                self.sendScreenShotStatus(status: "screenshot_initiated", event: "Initiated", artifactKey:artifact_key, transactionId: transaction_id)

                var logs = ModalLogDetails()
                logs.service_category = "CaptureSDK"
                logs.service = "TakeScreenshot"
                logs.event_name = artifact_key
                logs.event_type = "Initiated"
                logs.event_source = "createSession"
                logs.component = "CameraCapture"
                logs.publish_to_dlk = true
                self.sendScreenShotStatus(status: "screenshot_captured", event: "Captured", artifactKey:artifact_key, transactionId: transaction_id)

                var log = ModalLogDetails()
                log.service_category = "CaptureSDK"
                log.service = "TakeScreenshot"
                log.event_name = artifact_key
                log.event_type = "Captured"
                log.event_source = "createSession"
                log.component = "CameraCapture"
                log.publish_to_dlk = true

                self.loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: ["link": link, "artifact_key": artifact_key])

                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.callback?.setScreenShotUpload(artifact: artifact_key, link: link,transactionId: transaction_id, storageProvider: storageService)
                }

            }

        }
        channel?.on("completed") { message in
            DALCapture.printf("Core", "OD Socket connection completed status", "")
            self.callback?.onCompleted(message: "\(message.payload["status"] ?? "")")

            var logs = ModalLogDetails()
            logs.service = "OnChannel"
            logs.event_name = "connected"
            logs.event_type = "eventReceived"
            logs.event_source = "createSession"

            self.loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: ["data": message.payload])

        }
        channel?.on("chat") { message in
            DALCapture.printf("Core", "OD Socket connection received chat messages", "")
            let x = message.payload["x"] as! Int
            let y = message.payload["y"] as! Int
            self.callback?.onTouch(x: x, y: y)
        }

    }
    public func sendScreenShotStatus(status:String,event: String,artifactKey:String,transactionId:String) {

        channel?.push(status, payload: ["event": event,"artifact_key":artifactKey,"transaction_id":transactionId], timeout: 30)

            .receive("ok") { payload in print("channel push resp: ", payload.payload) }

            .receive("error") { payload in print("channel push failed", payload.payload) }

            .receive("timeout") { payload in print("channel push timeout", payload.payload) }
    }
    public func disconnectOdSocket() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            if socket != nil {
                channel?.leave()
                socket?.disconnect()
                socket = nil
                channel = nil
            }
        }
    }
    public func sendTaskDataPayload() {
        guard let location = LocationManager.sharedInstance.currentLocation else {
            LocationManager.sharedInstance.showLocationPermissionAlert()
            sendTaskDataPayload()
            return
        }
        let dataArr: Payload =  ["timestamp": "\(currentDate())", "speed": "\(location.speed)", "longitude": "\(location.coordinate.longitude)", "latitude": "\(location.coordinate.latitude)", "accuracy": "\(location.speedAccuracy)", "heading": "", "altitude": "\(location.altitude)", "altitudeAccuracy": "\(location.horizontalAccuracy)"]

        let payload: NSObject = ["attr": "location", "data": dataArr as NSObject] as NSObject
        
        DALCapture.printf("Core", "OD Socket channel sending task data payload", "")
        
        self.channel?.push("task_data", payload: ["payload": [payload]], timeout: 30)
            
            .receive("ok") { [self] payload in
                DALCapture.printf("Core", "OD Socket channel push response", "ok")
                var logs = ModalLogDetails()
                logs.service = "FetchTaskData"
                logs.event_name = dalCapture.getTatSince(start: loggerStartTime)
                logs.event_type = "task_data.TAT"
                logs.event_source = "sendTaskDataPayload"
                logs.reference_type = "AV.TaskID"
                logs.publish_to_dlk = false
                
                loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
                
                if let response = payload.payload["response"] as? NSObject, let validation = response.value(forKey: "validation") as? String, !validation.isEmpty {
                    var logs = ModalLogDetails()
                    logs.service = "TaskDataValidation"
                    logs.event_name = "Location"
                    logs.event_type = "Failed"
                    logs.event_source = "sendTaskDataPayload"
                    logs.exceptionDescription = "Error during task data validation"
                    logs.exceptionName = "Error during task data validation"
                    logs.reference_type = "AV.TaskID"
                    logs.publish_to_dlk = false

                    callback?.onDisconnect(code: "LOCATION_MISMATCH")
                    
                    loggerVs?.log(logLevel: LogLevel.shared.Error, logDetails: logs, meta: ["validations": payload.payload, "inputs": payload.payload])
                } else {
                    var logs = ModalLogDetails()
                    logs.service = "TaskDataValidation"
                    logs.event_name = "Location"
                    logs.event_type = "Passed"
                    logs.event_source = "sendTaskDataPayload"
                    logs.reference_type = "AV.TaskID"
                    logs.publish_to_dlk = false
                    
                    loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: ["validations": payload.payload, "inputs": payload.payload])
                }
            }
            
            .receive("error") { payload in
                DALCapture.printf("Core", "OD Socket channel push response", "error")
            }
            
            .receive("timeout") { [self] payload in
                DALCapture.printf("Core", "OD Socket channel push response", "timeout")
                var logs = ModalLogDetails()
                logs.service = "TaskDataValidation"
                logs.event_name = "TIMEDOUT"
                logs.event_type = "Timeout"
                logs.event_source = "sendTaskDataPayload"
                logs.exceptionDescription = ""
                logs.exceptionName = "Error received while submitting validation data for capture id: \(dalCapture.getRequestId()) during vKyc"
                logs.reference_type = "AV.TaskID"
                logs.publish_to_dlk = false
                
                loggerVs?.log(logLevel: LogLevel.shared.Error, logDetails: logs, meta: ["validations": "", "inputs": payload.payload])
            }
    }
    public func fetchWaitTime() {
        channel?.push("fetch_wait_time", payload: ["":""], timeout: 30)

            .receive("ok") { payload in

                guard let response = payload.payload["response"] as? NSObject else { return }
                let averageWaitTime = response.value(forKey: "average_wait_time") as! Int
                self.callback?.fetchWaitTime(time: averageWaitTime)

                print("Fetch wait time push ok:", payload.payload) }

            .receive("error") { payload in print("Fetch wait time push failed", payload.payload) }

            .receive("timeout") { payload in print("Fetch wait time push timeout", payload.payload) }
    }
    public  func sendNoAudio_Video(message: String) {
        
        channel?.push("chat", payload: ["message": message], timeout: 30)
            
            .receive("ok") { payload in print("Chat message push ok:", payload.payload) }
            
            .receive("error") { payload in print("Chat message push failed", payload.payload) }
            
            .receive("timeout") { payload in print("Chat message push timeout", payload.payload) }
    }
    public func sendEndCallEvent(code: String, reason: String, manual: Bool) {
        
        var logs = ModalLogDetails()
        logs.service = "EndCall"
        logs.event_name = "endcall"
        logs.event_type = "eventSent"
        logs.event_source = "sendEndCallEvent"
        
        loggerVs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: ["code": code, "reason": reason, "manual": manual])
        
        channel?.push("end_call", payload: ["code": code, "reason": reason, "manual": "\(manual)"], timeout: 30)
            
            .receive("ok") { payload in
                DALCapture.printf("Core", "OD Socket channel end call push ", "ok")
            }
            .receive("error") { payload in
                DALCapture.printf("Core", "OD Socket channel end call push ", "error")

            }
            .receive("timeout") { payload in
                DALCapture.printf("Core", "OD Socket channel end call push ", "timeout")
            }
    }
    public func sendWebStats(model: NSObject) {
        let dataArr: Payload =  ["currentOutgoingBitrate": "\(model.value(forKey: "currentOutgoingBitrate") ?? 0)", "averageOutgoingBitrate": "\(model.value(forKey: "averageOutgoingBitrate") ?? 0)", "streamType": "\(model.value(forKey: "publish") as! Bool == true ? "user":"operator" )", "streamId": "\(model.value(forKey: "streamId") ?? "")", "audioLevel": "\(model.value(forKey: "audioLevel") ?? 0)", "token": "\(model.value(forKey: "token") ?? "")", "currentIncomingBitrate": "\(model.value(forKey: "currentIncomingBitrate") ?? 0)", "fractionLost": "\(model.value(forKey: "fractionLost") ?? 0)", "ragStatus": "\(model.value(forKey: "ragStatus") ?? "")", "averageIncomingBitrate": "\(model.value(forKey: "averageIncomingBitrate") ?? 0)", "packetsLost": "\(model.value(forKey: "packetsLost") ?? 0)", "publish": "\(model.value(forKey: "publish") ?? false)", "currentTimestamp": Double(model.value(forKey: "currentTimestamp") as! Substring)!]
        channel?.push("webrtc_stats", payload: dataArr, timeout: 30)

            .receive("ok") { _ in }

            .receive("error") { _ in }

            .receive("timeout") { _ in }
    }
}
