//
//  MSSocket.swift
//  app
//
//  Created by Admin on 02/09/21.
//

import UIKit
import Foundation
import Alamofire
import SwiftPhoenixClient
import Starscream
public class MSSocket: NSObject {

    var channel: Channel?
    var socket: Socket?
    private var participantId: String = ""
    private var roomId: String = ""
    private var streamSessionId:String = ""
    var callBack: MSSocketCallBack?
    private var isSocketDisconnected: Bool = false
    var dalcapture = DALCapture()
    var loggerMs: Logger?
    var loggerStartTime: Double = 0
    private var ms_room_id: Int = 0
    private var msSocketURL: String = ""
    private var reconnecting: Bool = false

    public override init() { }

    public func initiate(roomId: String, participantId: String,msSocketURL:String,loggerMs:Logger,dalCapture:DALCapture,callback: MSSocketCallBack ) {
        self.roomId = roomId
        self.participantId = participantId
        self.dalcapture = dalCapture
        self.callBack = callback
        self.msSocketURL = msSocketURL
        self.loggerMs = loggerMs

        setupSocket()
    }
    public func setupSocket() {
        let url: String = msSocketURL+"participant_id="+participantId
        // set defaults
        var logDetails = ModalLogDetails()
        logDetails.component = "MsAdaptor"
        logDetails.reference_id = dalcapture.getRequestId()
        logDetails.reference_type = "AV.TaskID"
        loggerMs?.setDefaults(defaultLogDetails: logDetails)
        
        // set meta defaults
        var metaDefaults: [String: Any] = [:]
        metaDefaults.updateValue("", forKey: "MSRoomID")
        metaDefaults.updateValue(participantId, forKey: "participant_id")
        metaDefaults.updateValue(roomId, forKey: "room_id")
        metaDefaults.updateValue("HealthCheck", forKey: "service")
        loggerMs?.setMetaDefaults(metaDefaults: metaDefaults)
        
        var log = ModalLogDetails()
        log.service = "connectMSSocket"
        log.event_name = ""
        log.event_type = "Invoked"
        log.event_source = "setupSocket"
        
        loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: log, meta: [:])
        
        var logs = ModalLogDetails()
        logs.service = "ConnectMsControllerParticipant"
        logs.event_name = ""
        logs.event_type = "Connect"
        logs.event_source = "setupSocket"
        
        loggerStartTime = Double(Date.currentTimeStamp)
        
        loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])

        socket = Socket(url)
        socket?.connect()
        socket?.onError { [self] error in
            print("socket connection error: ", error)
            if !isSocketDisconnected {
                isSocketDisconnected = true
                self.callBack?.msSocketStateErrorCallback(error: "SOCKET_ERROR", message: "", canReconnect: false)
            }

            var logs = ModalLogDetails()
            logs.service = "SocketConnect"
            logs.event_name = "\(error)"
            logs.event_type = "OnError"
            logs.event_source = "setupSocket"

            loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
        }
        socket?.onClose { [self] in
            var logs = ModalLogDetails()
            logs.service = "SocketConnect"
            logs.event_name = "Close"
            logs.event_type = "Closed"
            logs.event_source = "setupSocket"

            loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
            print("Ms socket connection closed")
            if !isSocketDisconnected {
                isSocketDisconnected = true
                self.callBack?.onSocketDisconnected()
            }

        }
        socket?.onOpen { [self] in
            var logs = ModalLogDetails()
            logs.service = "SocketConnect"
            logs.event_name = "\(msSocketURL)"
            logs.event_type = "Invoked"
            logs.event_source = "setupSocket"

            isSocketDisconnected = false

            self.callBack?.onSocketOpen()

            loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])

            print("socket connection: opened")
        }

        channel = socket?.channel( "room:" + self.roomId, params: [:])
        channel?.join()

            .receive("error") {message in
                print("MSSocket Channel error: ", message)
                print("Payload: \(message.payload)")
                self.callBack?.msSocketStateErrorCallback(error: "errorJoiningRoom", message: "Cannot join room", canReconnect: true)

                var logs = ModalLogDetails()
                logs.service = "JoinMsControllerRoom"
                logs.event_name = "\(message.payload)"
                logs.event_source = "setupSocket"
                logs.exceptionName = "\(message.payload)"
                logs.exceptionDescription = "\(message.payload)"

                self.loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
            }
            .receive("ok") {message in
                print("socket channel joined: ", message.payload)

                var logs = ModalLogDetails()
                logs.service = "JoinMsControllerRoom"
                logs.event_name = self.dalcapture.getTatSince(start: self.loggerStartTime)
                logs.event_type = "Joined"
                logs.event_source = "setupSocket"

                self.loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
            }
        channel?.on("start") { [self] message in
            if reconnecting {
                self.callBack?.cleanUpProvider()
            }
            self.reconnecting = false

            print("socket channel start: ", message.payload)
            let ms_room_reference_id = message.payload["ms_room_reference_id"] as? String ?? ""
            ms_room_id = message.payload["ms_room_id"] as? Int ?? 0
            self.callBack?.setRoomCall(roomCall: message.payload as NSObject)

            var logs = ModalLogDetails()
            logs.service = "RoomChannelEventReceive"
            logs.event_name = "Start"
            logs.event_type = "Received"
            logs.event_source = "setupSocket"

            var metaDefaults: [String: Any] = [:]
            metaDefaults.updateValue(message.payload, forKey: "message")
            metaDefaults.updateValue(ms_room_id, forKey: "ms_room_id")
            metaDefaults.updateValue(ms_room_reference_id, forKey: "ms_room_reference_id")
            loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: metaDefaults)

        }
        channel?.on("reconnecting") { message in

            var logs = ModalLogDetails()
            logs.service = "RoomChannelEventReceive"
            logs.event_name = "Reconnecting"
            logs.event_type = "Received"
            logs.event_source = "setupSocket"

            var metaDefaults: [String: Any] = [:]
            metaDefaults.updateValue(message.payload, forKey: "message")
            metaDefaults.updateValue(self.participantId, forKey: "participant_id")
            self.loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: metaDefaults)

            print("MS Channel socket channel reconnecting: ", message.payload)
            let participant_id = message.payload["participant_id"] as? String
            let msRoomId = message.payload["ms_room_id"] as? Int
            if msRoomId != self.ms_room_id {
                print("RECONNECTING RETURN BREAK room id not matching")
                return
            }
            if participant_id == self.participantId {
                print("RECONNECTING RETURN BREAK participant id same")
                return
            }
            if !self.reconnecting {
                self.reconnecting = true
                DispatchQueue.main.async {
                    self.callBack?.invokeMediaFinishedCallback(obj: ["participantId": participant_id] as NSObject)
                }
                self.callBack?.cleanUpProvider()
            }
        }
        channel?.on("disconnect") { message in
            print("MS Channel socket channel Disconnected: ", message.payload)

            self.socket?.disconnect()
            self.callBack?.onDisconnect(envelope: message.payload as NSObject)

        }
        channel?.on("error") { message in
            let reason = message.payload["reason"] as? String
            self.callBack?.msSocketStateErrorCallback(error: reason ?? "", message: reason ?? "", canReconnect: true)

            var logs = ModalLogDetails()
            logs.service = "JoinMsControllerRoom"
            logs.event_name = "OnError"
            logs.event_type = "OnError"
            logs.event_source = "setupSocket"
            logs.exceptionDescription = "Channel on error callback \(message.payload)"

            self.loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logs, meta: [:])
        }

    }
    public func disconnectSocket() {
        if socket != nil {
            channel?.leave()
            socket?.disconnect()
            channel = nil
            socket = nil
        }
    }
    public func reconnect(type: String, reason: String, ms_room_id: Int) {
        if channel != nil {
            if reconnecting || self.ms_room_id == 0 || self.ms_room_id != ms_room_id {
                return
            }
            reconnecting = true
            
            let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "ReconnectRoom", timestamp: "", event_type: "Request", event_name: "reconnect", component: "", event_source: "reconnect", logger_session_id: "")
            
            var meta: [String: Any] = [:]
            meta.updateValue(ms_room_id, forKey: "ms_room_id")
            meta.updateValue(type, forKey: "type")
            meta.updateValue(reason, forKey: "reason")
            
            loggerStartTime = Double(Date.currentTimeStamp)
            
            loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)
            
            let payload: Payload = ["ms_room_id": ms_room_id, "type": type, "reason": reason]
            self.channel?.push("reconnect", payload: payload, timeout: 30)
                .receive("ok", callback: { [self] message in
                    print("Reconnect call back: \(message.payload)")
                    
                    let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "ReconnectRoom", timestamp: "", event_type: "Success", event_name: dalcapture.getTatSince(start: loggerStartTime), component: "", event_source: "reconnect", logger_session_id: "")
                    
                    var meta: [String: Any] = [:]
                    meta.updateValue(message.payload, forKey: "response")
                    loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)
                    
                })
                .receive("error", callback: { [self] error in
                    print("Reconnect call back: \(error.payload)")
                    self.callBack?.msSocketStateErrorCallback(error: "reconnectError", message: "Reconnect failed", canReconnect: true)
                    
                    let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "ReconnectRoom", timestamp: "", event_type: "Success", event_name: "Error", component: "", event_source: "sendEvent", logger_session_id: "", exceptionName: "reconnectError", exceptionDescription: "Reconnect failed  \(error.payload)")
                    
                    loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
                })
                .receive("timeout", callback: { _ in
                    
                })
        }
    }
    public func sendStatsDataPayload(stats: [String: Any]?) {
        self.channel?.push("webrtc_stats", payload: stats ?? [:], timeout: 30)
            .receive("ok", callback: { message in
                print("WebrtcStats call back: \(message.payload)")
            })
            .receive("error", callback: { error in
                print("WebrtcStats call back: \(error.payload)")
            })
    }
    public func sendAudioLevelDataPayload(audioLevel: NSObject) {
        self.channel?.push("audio_level", payload: ["data": audioLevel], timeout: 30)
    }
    public func leave() {
        if channel != nil {
            var log = ModalLogDetails()
            log.service = "LeaveRoom"
            log.event_name = ""
            log.event_type = "Request"
            log.event_source = "leave"
            
            loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: log, meta: [:])
            
            loggerStartTime = Double(Date.currentTimeStamp)
            
            channel?.leave()
                .receive("ok", callback: { [self] _ in
                    var log = ModalLogDetails()
                    log.service = "LeaveRoom"
                    log.event_name = dalcapture.getTatSince(start: loggerStartTime)
                    log.event_type = "Success"
                    log.event_source = "Leave"
                    
                    loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: log, meta: [:])
                    
                    self.callBack?.cleanUp()
                })
                .receive("error", callback: { [self] message in
                    var log = ModalLogDetails()
                    log.service = "LeaveRoom"
                    log.event_name = "Timeout"
                    log.event_type = "timeout"
                    log.event_source = "Leave"
                    log.exceptionDescription = "Network Timeout "
                    
                    loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: log, meta: [:])
                    
                    callBack?.cleanUp()
                    callBack?.msSocketStateErrorCallback(error: "networkError", message: "LeaveRoomTimeout", canReconnect: true)
                })
        } else {
            var log = ModalLogDetails()
            log.service = "LeaveRoom"
            log.event_name = "ChannelNotConnected"
            log.event_type = "Success"
            log.event_source = "Leave"
            
            loggerMs?.log(logLevel: LogLevel.shared.Info, logDetails: log, meta: [:])
            
            callBack?.cleanUp()
        }
    }
}
