//
//  TaskHelper.swift
//  core
//
//  Created by FL Mohan on 14/06/22.
//

import Foundation
import UIKit

public class TaskHelper: IPageSequenceCallback, IPageDataCallback, IStatusCallback {

    private var dalCapture = DALCapture()
    private var taskManager = TaskManager()
    private var taskType = ""
    private var taskKey = ""
    private var isForm = false
    private var dependentList:[String] = []
    private var tasksList:[Tasks] = []
    private var formTaskManagerHashMap:[String:FormTaskManager] = [:]
    private var iRequiredTask:IRequiredTask?
    private var formTaskManager:FormTaskManager?

    public init() {
    }
    public init(dalCapture:DALCapture,taskManager:TaskManager) {
        self.dalCapture = dalCapture
        self.taskManager = taskManager
    }
    public func initiateTask(taskKey:String,iTaskResultCallback:ITaskResultCallback) {
        let formTaskManager = formTaskManagerHashMap[taskKey]
        if formTaskManager != nil {
            if !(formTaskManager?.isAllTaskDone() ?? false) {
                print("All task is not completed")
            } else {
                dalCapture.initiateTask(taskKey: taskKey, iTaskResultCallback: iTaskResultCallback)
            }
        }
    }
    public func resetTask(taskKey:String) {
        dalCapture.resetTask(taskKey: taskKey)
    }
    public func saveTask(taskKey:String,changesLogArray:[String]) {
        dalCapture.saveTask(taskKey: taskKey, value: changesLogArray)
    }
    public func saveTask(taskKey:String,valueStrings:[String]) {
        dalCapture.saveTask(taskKey: taskKey, value: valueStrings)
    }
    public func saveTask(view:UIView,taskKey:String,contentType:String,documentType:String,data:Data,artifactsCallback:ArtifactsCallback) {
        dalCapture.saveTask(view:view,taskKey: taskKey, contentType: contentType, documentType: documentType, data: data, artifactCallback: artifactsCallback)
    }
    public func getRequiredtasks(iRequiredTask:IRequiredTask?) {
        guard let requiredTaskDelegate = iRequiredTask else { return }
        self.iRequiredTask = requiredTaskDelegate
        dalCapture.getCaptureStatusDetails(sessionOverride: false, callback: self)

    }
    public func onStatusSuccess(object: NSObject) {
        self.processCapture(iRequiredTask: iRequiredTask!)
    }
    private func processCapture(iRequiredTask:IRequiredTask) {
        let status_string = Status(rawValue: dalCapture.getStatus())

        switch status_string {
        case .Status_Approved,.Status_Rejected,.Status_Completed:
            iRequiredTask.onFailure(msg: "Your application has been completed.")
            break
        case .Status_Cancelled, .Status_Capture_Expired,.Status_Purged:
            iRequiredTask.onFailure(msg: "This link is no longer active.Please contact customer support to know more details.")
            break
        case .Status_In_Progress,.Status_Initiated,.Status_Processed:
            iRequiredTask.onFailure(msg: "Your application is in progress.")
            break
        case .Status_Review,.Status_Review_Onhold,.Status_Review_Required:
            iRequiredTask.onFailure(msg: "Your application is currently under review.")
            break
        case .Status_Capture_Pending, .Status_Recapture_Pending:
            self.handleCaptureSocket(iRequiredTask: iRequiredTask)
            break
        default:
            break
        }
    }
    public func onStatusFailure(message: String) {
        iRequiredTask?.onFailure(msg: message)
    }
    private func handleCaptureSocket(iRequiredTask:IRequiredTask) {
        dalCapture.getPageSequence(iPSCallback: self)
    }
    public func onPageSequenceSuccess(object: NSObject?) {
        self.fetechCapturePageData(iRequiredtask: iRequiredTask!)
    }

    public func onPageSequenceIntermediate() {

    }

    public func onPageSequenceFailure(message: String?) {
        iRequiredTask?.onFailure(msg: message ?? "")
    }
    private func fetechCapturePageData(iRequiredtask:IRequiredTask) {
        dalCapture.fetchPageData(page: "capture", iPageDataCallback: self)
    }
    public func onPageDataSuccess(page: String, object: NSObject) {
        processtask(payload: object, iRequiredTask: iRequiredTask!)
    }

    public func onPageDataFailure(message: String) {
        iRequiredTask?.onFailure(msg: "server error")
    }
    private func processtask(payload:NSObject,iRequiredTask:IRequiredTask) {
        guard let configObject = payload.value(forKey: "config") as? NSObject else { return }
        guard let captureArray = configObject.value(forKey: "capture") as? NSArray else { return }
        taskManager.setCaptureArray(captureArray: captureArray)
        setCaptureItem(captureArray: captureArray)
        iRequiredTask.onSuccess(tasksList: tasksList)
    }
    private func setCaptureItem(captureArray:NSArray) {
        for (_,value) in captureArray.enumerated() {
            guard let jsonArray = value as? NSArray else { return }
            for (_,val) in jsonArray.enumerated() {
                guard let jsonObject = val as? NSObject else { return }
                if isForm {
                    tasksList.append(Tasks(type: "Initiate", taskKey: taskKey, taskType: taskType, dependentTask: dependentList))
                    if let formTaskManager = formTaskManager {
                        formTaskManagerHashMap.updateValue(formTaskManager, forKey: taskKey)
                    }
                    dependentList = []
                    self.formTaskManager = nil
                    taskKey = ""
                    taskType = ""
                    isForm = false
                }
                iterateAllCaptureItem(jsonObject: jsonObject)
            }
        }
    }
    private func iterateAllCaptureItem(jsonObject:NSObject) {
        if (jsonObject.value(forKey: "tasks") != nil) {
            var optionList:[String] = []
            var artifactKey = ""
            let taskArray = jsonObject.value(forKey: "tasks") as! NSArray
            for taskArray in taskArray {
                guard let taskObject = taskArray as? NSObject else { return }
                let taskTemplateId = taskObject.value(forKey: "template_id") as? Int
                let task_key = taskObject.value(forKey: "task_key") as? String
                let task_type = taskObject.value(forKey: "task_type") as? String
                guard let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskTemplateId ?? 0)") else { return }
                let type = templateObject.value(forKey: "type") as? String
                var side = templateObject.value(forKey: "side") as? String
                let categoryType = templateObject.value(forKey: "category_type") as? String
                let mandatory = templateObject.value(forKey: "mandatory") as? Bool
                var document_type_key = templateObject.value(forKey: "document_type_key") as? String
                if type == "form" {
                    dependentList = []
                    taskKey = task_key ?? ""
                    taskType = task_type ?? ""
                    formTaskManager = FormTaskManager(dalCapture: dalCapture, formArray: taskObject.value(forKey: "tasks") as! NSArray,taskKey: taskKey)
                    isForm = true
                }
                if type == "image" || type == "text" || type == "filepicker" || type == "radio" || type == "dropdown" || type == "checkbox" {
                    if task_type == "data_capture.data_capture" {
                        artifactKey = dalCapture.getArtifactKey(object: taskObject)
                        formTaskManager?.registerToArtifactKey(artifactKey: artifactKey)
                        if dependentList != [] {
                            dependentList.append(task_key ?? "")
                        }
                        guard let artifactObject = dalCapture.getArtifactsObjectByKey(key: artifactKey) else { return }
                        if document_type_key == "" {
                            document_type_key = artifactObject.value(forKey: "document_type_key") as? String
                        }
                        let value = artifactObject.value(forKey: "value") as? String
                        let present = artifactObject.value(forKey: "present") as? Bool
                        if side == "" {
                            side = artifactObject.value(forKey: "side") as? String
                        }
                        optionList = dalCapture.getOptionHashMap(artifactKey: artifactKey) ?? []
                        if ((categoryType?.contains("poa")) != nil) && optionList == [] {
                            optionList = dalCapture.getPoaOptionList()
                        } else if ((categoryType?.contains("poi")) != nil) && optionList == [] {
                            optionList = dalCapture.getPoiOptionList()
                        }
                        tasksList.append(Tasks(type: type ?? "", artifactKey: artifactKey, taskKey: task_key ?? "", taskType: task_type ?? "", templateId: taskTemplateId ?? 0, documentType: document_type_key ?? "", mandatory: mandatory ?? false, side: side ?? "", value: value ?? "", present: present ?? false, optionList: optionList))
                    } else {
                        tasksList.append(Tasks(type: type ?? "", taskKey: task_key ?? "", taskType: task_type ?? "", mandatory: mandatory ?? false, templateId: taskTemplateId ?? 0))
                    }
                }
                iterateAllCaptureItem(jsonObject: taskObject)
            }
        }
    }
    public func submitTasks(iArtifactSubmitCallback:IArtifactSubmitCallback) {
        if taskManager.isAllTaskCompleted() {
            dalCapture.submitArtifacts { onResponse in
                iArtifactSubmitCallback.onSubmitSuccess(object: onResponse)
            }
        }
    }
}
