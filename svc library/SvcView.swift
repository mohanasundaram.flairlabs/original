//
//  SvcView.swift
//  svc library
//
//  Created by Admin on 10/03/22.
//

import Foundation
import UIKit
import videocomponent
import AVFoundation
import core
import healthcheck

public protocol SVCCallBack {
    func onSvcSuccess(object: NSObject)
    func onSvcIntermediate(object: NSObject)
    func onSvcFailure(object: NSObject)
}
public enum Task {
    case TASK_STARTED
    case READ_RANDOM_DIGITS
    case READ_TEXT
}

public class SvcView: UIView, VideoComponentCallBack, HealthCheckCallback, iTaskRespCallBack {


    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var svcContentView: UIView!
    @IBOutlet weak var messageView:UIView!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var messageStackView:UIStackView!
    @IBOutlet weak var loadingView:UIView!
    @IBOutlet weak var endSvcCallView: UIView!
    @IBOutlet weak var submitCallButton:UIButton!
    @IBOutlet weak var recaptureButton:UIButton!
    @IBOutlet weak var titleText:UILabel!
    @IBOutlet weak var subTitleText:UILabel!

    let kCONTENT_XIB_NAME           =   "SvcView"

    var configObject = NSObject()

    var audioRecorder:RecordingAudio?

    var videoStart:Bool = false

    var optionList:[String] = []

    var title:String = ""

    private var roomId: String = ""

    private var participantId: String = ""

    private var activity_count:Int = 0

    private var currentTask:Int = 0

    private var svcSessionId:String = ""

    var videoStartTime:Double = 0

    var task:NSArray = []

    var delegate: SVCCallBack?

    var modelSVCConfig = ModelSVCConfig()

    var infoView = InstructionView()

    var healthCheck = HealthCheckView()

    var videoComponentView:VideoComponentView?

    var loggerWrapper:LoggerWrapper?

    var loggerVC:Logger?

    var loggerStartTime: Double = 0

    var videoComponent:VideoComponent?

    var healthCheckWrapper:HealthCheck?

    var dalCapture = DALCapture()

    var svcWrapper:SVCComponentFactory?

    public init(frame: CGRect,dalCapture:DALCapture,healthCheck:HealthCheck, loggerWrapper:LoggerWrapper,modelSvcConfig:ModelSVCConfig,videoComponent:VideoComponent) {
        self.dalCapture = dalCapture
        self.healthCheckWrapper = healthCheck
        self.loggerWrapper = loggerWrapper
        self.videoComponent = videoComponent
        self.modelSVCConfig = modelSvcConfig
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle(for: type(of: self)).loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)

        initLogger()

        submitButton.tintColor = .gray
        submitButton.isEnabled = false
        submitButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)

        loadingView.isHidden = true
        endSvcCallView.isHidden = true

        endSvcCallView.backgroundColor = ThemeConfig.shared.getSecondaryMainColor()

        submitCallButton.tintColor = ThemeConfig.shared.getPrimaryMainColor()
        recaptureButton.tintColor = ThemeConfig.shared.getPrimaryMainColor()
        titleText.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        subTitleText.textColor = ThemeConfig.shared.getSecondaryContrastColor()
        messageView.backgroundColor = ThemeConfig.shared.getSecondaryContrastColor()
        contentView.backgroundColor = ThemeConfig.shared.getSecondaryContrastColor()
    }
    public func initialiseSvc(view: UIView,configObject:NSObject,delegate:SVCCallBack) {
        self.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        infoView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.backgroundColor = dalCapture.getThemeConfig().getSecondaryMainColor()
        self.configObject = configObject
        self.delegate = delegate

        let logDetails = ModalLogDetails(service:"RoutePage",event_type: "self_video",event_name: "buttonClick", component: "SelfVideoService",event_source: "routeToSVC")
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        loggerVC?.logPageRender(loggerStartTime: loggerStartTime, component: "SelfVideoKYC",referenceId: dalCapture.getRequestId() != "" ? dalCapture.getRequestId() : dalCapture.getCaptureId(),referenceType: dalCapture.getReferenceType(), meta: [:])

        parseSvcDataUi()
    }
    func initLogger() {
        loggerVC = loggerWrapper?.getLoggers(key: EnumLogger.VC.rawValue)
        if loggerVC == nil {
            loggerVC = loggerWrapper?.createLogger(key: EnumLogger.VC.rawValue, token: dalCapture.getToken(), loggerUrl: modelSVCConfig.getSvcLoggerUrl())
        }
        dalCapture.setReferenceType(type: "SV.TaskID")
        let data = ModalLogDetails(service_category: "CaptureSDK.iOS", component: "SelfVideoService",reference_id: dalCapture.getRequestId(), reference_type: dalCapture.getReferenceType())
        loggerVC?.setDefaults(defaultLogDetails: data)
    }
    func parseSvcDataUi() {
        let tasks = configObject.value(forKey: "task") as? NSObject
        let title = configObject.value(forKey: "title") as? String ?? ""
        self.submitButton.setTitle(title, for: .normal)
        let networkConfigObject = configObject.value(forKey: "network_check_config") as! NSObject
        let prerequisiteCard = configObject.value(forKey: "prerequisite_card") as? NSObject
        self.title = prerequisiteCard?.value(forKey: "title") as? String ?? ""
        let requestionArray = prerequisiteCard?.value(forKey: "prerequisite_items") as? NSArray
        if let swiftArray = requestionArray! as NSArray as? [String] {
            optionList = swiftArray
            updateInitialLMessage()
        }
        let object = ["event": "header_title", "title": self.title] as NSObject
        self.delegate?.onSvcIntermediate(object: object)

        dalCapture.setRequestId(id: tasks?.value(forKey: "request_uid") as? String ?? "")
        let checkObject = networkConfigObject.value(forKey: "checks") as? NSObject
        let isRoomJoineeded = checkObject?.value(forKey: "room_join") as! Bool
        let isNetworkCheckNeeded = networkConfigObject.value(forKey: "enabled") as? Bool ?? false
        let timeout = networkConfigObject.value(forKey: "timeout") as! Int

        if isNetworkCheckNeeded && isRoomJoineeded {
            DispatchQueue.main.async { [self] in
                let modelHealthCheck = ModelHealthCheck(modelVcConfig: modelSVCConfig.getModelVcConfig(), init_url: modelSVCConfig.getUrlInitHealthCheck(), submit_url: modelSVCConfig.getUrlSubmitHealth(), requestId: dalCapture.getRequestId(), isRoomJoinNeeded: isRoomJoineeded, timeOut: timeout)
                healthCheck = (healthCheckWrapper?.startHealthCheck(modelHealthCheck:modelHealthCheck))!
                healthCheck.initialiseHealthCheck(view: self, delegate: self)
                self.addSubview(healthCheck)
            }
        } else {
            getTaskStatus()
        }
    }
    public func BackPressed() {
        self.delegate?.onSvcIntermediate(object: ["event":"USER_BACKPRESS"] as NSObject)
    }
    func updateInitialLMessage() {

        let object = ["event": "header_title", "title": self.title] as NSObject
        self.delegate?.onSvcIntermediate(object: object)

        messageStackView.removeSubviews()
        for(_,value) in optionList.enumerated() {
            let label = UILabel()
            label.textAlignment = .center
            label.text = value
            label.textColor = ThemeConfig.shared.getSecondaryMainColor()
            messageStackView.addArrangedSubview(label)
        }
    }
    public func onSuccessCallback() {
        DispatchQueue.main.async { [self] in
            if healthCheck.isDescendant(of: self) {
                healthCheck.removeFromSuperview()
            }
        }
        getTaskStatus()
    }

    public func onFailureCallback(code: String) {

    }
    func getTaskStatus() {

        DispatchQueue.main.async { [self] in
            videoComponentView =  videoComponent?.startVcView()
            videoComponentView?.InitialiseCameraView(modelVcConfig: modelSVCConfig.getModelVcConfig(), view: svcContentView, delegate: self)
            svcContentView.addSubview(videoComponentView!)
            videoComponentView?.hideAllOptions()
            videoComponentView?.showFaceDetectionView()
        }

        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "FetchTaskStatus", event_name: "Initiate", component: "SelfVideoService", event_source: "getTaskStatus"), meta: [:])
        loggerStartTime = Double(Date.currentTimeStamp)

        let param: [String: Any] = [
            "task_id": dalCapture.getRequestId()
        ]
        print(param)
        guard let data = try? JSONSerialization.data(withJSONObject: param, options: []) else { return  }
        dalCapture.callApi(view: self, url: modelSVCConfig.getUrlFetchTaskStatus(),data: data, completion: { [self] onSuccess, message in
            if onSuccess != nil {
                if let sessionId = onSuccess?.value(forKey: "session_id") as? String {
                    svcSessionId = sessionId

                    var metaDefault: [String: Any] = [:]
                    metaDefault.updateValue(svcSessionId, forKey: "sv_session_id")
                    loggerVC?.setMetaDefaults(metaDefaults: metaDefault)

                    var meta: [String: Any] = [:]
                    meta.updateValue(onSuccess!, forKey: "data")
                    loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "FetchTaskStatus", event_name: "Initiate", component: "SelfVideoService", event_source: "getTaskStatus"), meta: meta)

                    getTaskStarted()

                } else if onSuccess?.value(forKey: "error") as? String == "unauthorized" {
                    let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                    self.delegate?.onSvcFailure(object: object)
                }
            } else {
                logError(eventType: "onSessionFailure", eventName: "failure", eventSource: "getTaskStatus", exceptionName: "\(String(describing: message))", exceptionDesc: "\(String(describing: message))")
                let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                self.delegate?.onSvcFailure(object: object)
            }
        })
    }
    func getTaskStarted() {

        var body: [String: Any] = [:]
        body.updateValue(svcSessionId, forKey: "session_id")
        loggerVC?.setMetaDefaults(metaDefaults: body)

        var meta: [String: Any] = [:]
        meta.updateValue(body, forKey: "data")
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "TaskStarted", event_name: "Initiate", component: "SelfVideoService", event_source: "getTaskStarted"), meta: meta)


        let param: [String: Any] = [
            "session_id": svcSessionId
        ]
        guard let data = try? JSONSerialization.data(withJSONObject: param, options: []) else { return  }
        dalCapture.callApi(view: self, url: modelSVCConfig.getUrlTaskStarted(),data: data) { [self] onSuccess, onFailure in
            if onSuccess != nil {

                var meta: [String: Any] = [:]
                meta.updateValue(onSuccess!, forKey: "data")
                loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "TaskStarted.TAT", event_name: String(loggerStartTime), component: "SelfVideoService", event_source: "getTaskStarted"), meta: meta)

                participantId = onSuccess?.value(forKey: "participant_id") as! String
                activity_count = onSuccess?.value(forKey: "activity_count") as! Int
                roomId = onSuccess?.value(forKey: "room_id") as! String
                DispatchQueue.main.async { [self] in
                    videoComponentView?.updateFaceDetectionMessage()
                    self.submitButton.setTitle("START", for: .normal)
                    submitButton.isEnabled = true
                    submitButton.tintColor = ThemeConfig.shared.getPrimaryMainColor()
                }
            } else {
                logError(eventType: "onSessionFailure", eventName: "failure", eventSource: "getTaskStarted", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")

                let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                self.delegate?.onSvcFailure(object: object)
            }

        }

    }
    @IBAction func didTapSubmitButton(_ sender:UIButton) {
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "buttonClick", event_name: "\(String(describing: sender.titleLabel?.text))", component: "SelfVideoService", event_source: "setOnClickListener"), meta: [:])

        if sender.titleLabel?.text == "START" {
            DispatchQueue.main.async { [self] in
                submitButton.loadingIndicator(true)
                taskResponseEventSource()
                videoComponentView?.setUpSocket(roomId: roomId, participantId: participantId, msSocketURL:"", purpose: "")
            }
        } else if sender.titleLabel?.text == "NEXT ACTION" {
            loadingView.isHidden = false
            audioRecorder?.stopRecording()
            submitButton.loadingIndicator(true)
            self.updateTask()
        } else if sender.titleLabel?.text == "DONE" {
            loadingView.isHidden = false
            updateTask()
            audioRecorder?.stopRecording()
        }
    }


    func taskResponseEventSource() {
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "TaskResponse", event_name: "Initiate", component: "SelfVideoService", event_source: "getTaskResponse"), meta: [:])

        dalCapture.initEventSource(url: modelSVCConfig.getUrlTaskResponse(), svcSessionId: svcSessionId, delegate: self)
    }
    public func onOpen() { }
    public func onEvent(type: String, data: [String : Any]) {
        loadingView.isHidden = true

        var meta: [String: Any] = [:]
        meta.updateValue(type, forKey: "type")
        meta.updateValue(data, forKey: "data")
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "TaskResponse.TAT", event_name: String(loggerStartTime), component: "SelfVideoService", event_source: "getTaskResponse"), meta: meta)

        if data["performed_correctly"] as! Bool == true {
            checkForNextPage()
        } else {
            self.window?.rootViewController?.showAlert(title: "Please Try Again", message: "Numbers are not recognized.Please ensure that you read the numbers loudly and clearly.", actionTitle: "RETRY", completion: { success in
                self.getFetchActivity()

            })
        }
    }
    public func onFailure(message: String) {
        logError(eventType: "onFailure", eventName: "failure", eventSource: "getTaskResponse", exceptionName: "\(String(describing: message))", exceptionDesc: "\(String(describing: message))")
    }
    public func onClosed() { }

    func getFetchActivity() {

        var meta: [String: Any] = [:]
        meta.updateValue(currentTask, forKey: "index")
        meta.updateValue(svcSessionId, forKey: "session_id")
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "FetchActivity", event_name: "Initiate", component: "SelfVideoService", event_source: "getFetchActivity"), meta: meta)
        loggerStartTime = Double(Date.currentTimeStamp)
        videoStartTime = Double(Date.currentTimeStamp)

        let param: [String: Any] = [
            "index": currentTask,
            "session_id": svcSessionId
        ]
        let object = ["event": "header_title", "title" : "Action \(currentTask + 1) of \(activity_count)"] as NSObject
        self.delegate?.onSvcIntermediate(object: object)
        guard let data = try? JSONSerialization.data(withJSONObject: param, options: []) else { return  }
        dalCapture.callApi(view: self, url: modelSVCConfig.getUrlFetchActivity(),data: data) { [self] onSuccess, onFailure in
            if onSuccess != nil {

                var meta: [String: Any] = [:]
                meta.updateValue(onSuccess!, forKey: "data")
                loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "FetchActivity.TAT", event_name: String(loggerStartTime), component: "SelfVideoService", event_source: "getFetchActivity"), meta: meta)

                task = onSuccess?.value(forKey: "tasks") as! NSArray
                let data = onSuccess?.value(forKey: "data") as! String
                let type = onSuccess?.value(forKey: "type") as! String
                DispatchQueue.main.async { [self] in
                    messageStackView.removeSubviews()
                    let label1 = UILabel()
                    let label2 = UILabel()
                    label1.textColor = dalCapture.getThemeConfig().getSecondaryMainColor()
                    label2.textColor = dalCapture.getThemeConfig().getSecondaryMainColor()
                    label1.numberOfLines = 0
                    label1.textAlignment = .center
                    if type == "read_random_digits" {
                        label1.text = "Read each digit out loud"
                        label2.font = UIFont(name: "Helvetica", size: 25.0)
                        label2.text = data.addingDashes()
                    } else if type == "read_text" {
                        label1.text =  "Read this text clearly"
                        label2.text = data
                        label2.font = UIFont(name: "Helvetica", size: 20.0)
                    }
                    if (currentTask + 1) == activity_count {
                        submitButton.setTitle("DONE", for: .normal)
                    } else {
                        submitButton.setTitle("NEXT ACTION", for: .normal)
                    }
                    messageStackView.addArrangedSubview(label1)
                    label2.numberOfLines = 0
                    label2.textAlignment = .center
                    messageStackView.addArrangedSubview(label2)
                    submitButton.loadingIndicator(false)
                }
                if videoStart == false {
                    videoStarted()
                } else {
                    activityStarted()
                }
                audioRecorder = RecordingAudio()
                audioRecorder?.startRecording()


            } else {
                logError(eventType: "onSessionFailure", eventName: "failure", eventSource: "getFetchActivity", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")

                let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                self.delegate?.onSvcFailure(object: object)
            }

        }
    }
    func videoStarted() {
        var video_params: [String: Any] = [:]
        video_params.updateValue(videoStartTime, forKey: "video_start_time")
        var body: [String: Any] = [:]
        body.updateValue(svcSessionId, forKey: "session_id")
        body.updateValue(video_params, forKey: "video_params")
        var meta: [String: Any] = [:]
        meta.updateValue(body, forKey: "data")
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "VideoStarted", event_name: "Initiate", component: "SelfVideoService", event_source: "videoStarted"), meta: meta)

        self.videoStart = true
        DispatchQueue.main.async { [self] in
            videoComponentView?.startRecording()

            loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "StartRecording", event_name: "Initiate", component: "SelfVideoService", event_source: "StartRecording"), meta: [:])
        }
        let videoParam:[String:Any] = [
            "video_start_time": Date.currentTimeStamp
        ]
        let param: [String: Any] = [
            "session_id": svcSessionId,
            "video_params": videoParam
        ]
        guard let data = try? JSONSerialization.data(withJSONObject: param, options: []) else { return  }
        dalCapture.callApi(view: self, url: modelSVCConfig.getUrlVideoStarted(),data: data) { [self] onSuccess, onFailure in
            if onSuccess != nil {
                var meta: [String: Any] = [:]
                meta.updateValue(onSuccess!, forKey: "data")
                loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "VideoStarted.TAT", event_name: String(loggerStartTime), component: "SelfVideoService", event_source: "videoStarted"), meta: meta)

                let status = onSuccess?.value(forKey: "status") as! String
                if status == "success" {
                    activityStarted()
                }
            } else {

                logError(eventType: "onSessionFailure", eventName: "failure", eventSource: "videoStarted", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")

                let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                self.delegate?.onSvcFailure(object: object)
            }

        }
    }
    func activityStarted() {
        videoStartTime = Double(Date.currentTimeStamp)

        var body: [String: Any] = [:]
        body.updateValue(currentTask, forKey: "index")
        body.updateValue(svcSessionId, forKey: "session_id")
        body.updateValue(videoStartTime, forKey: "start_time")
        var meta: [String: Any] = [:]
        meta.updateValue(body, forKey: "data")
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "ActivityStarted", event_name: "Initiate", component: "SelfVideoService", event_source: "activityStarted"), meta: meta)
        loggerStartTime = Double(Date.currentTimeStamp)

        let param: [String: Any] = [
            "index": currentTask,
            "session_id": svcSessionId,
            "start_time": Date.currentTimeStamp
        ]
        guard let data = try? JSONSerialization.data(withJSONObject: param, options: []) else { return  }
        dalCapture.callApi(view: self, url:modelSVCConfig.getUrlActivityStarted(),data: data) { [self] onSuccess, onFailure in
            if onSuccess != nil {
                var meta: [String: Any] = [:]
                meta.updateValue(onSuccess!, forKey: "data")
                loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "ActivityStarted.TAT", event_name: String(loggerStartTime), component: "SelfVideoService", event_source: "ActivityStarted"), meta: meta)

                let status = onSuccess?.value(forKey: "status") as! String
                if status == "success" {
                }
            } else {
                logError(eventType: "onSessionFailure", eventName: "failure", eventSource: "activityStarted", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")

                let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                self.delegate?.onSvcFailure(object: object)
            }

        }
    }
    func updateTask() {
        guard let data = try? Data(contentsOf:(audioRecorder?.getFileUrl())!) else { return }
        var updateTask = task as! [[String:Any]]
        if task.count > 1 {
            for (index, values) in task.enumerated() {
                var obj = values as! [String:Any]
                if let value = obj["value"] as? String {

                    var meta: [String: Any] = [:]
                    var metaData:[String:Any] = [:]
                    metaData.updateValue(value, forKey: "uploadUrl")
                    meta.updateValue(metaData, forKey: "data")
                    loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "UpdateTask", event_name: "Initiate", component: "SelfVideoService", event_source: "updateTask"), meta: meta)
                    loggerStartTime = Double(Date.currentTimeStamp)

                    dalCapture.submitAudio(view: self, url: value, data: data) { [self] onSuccess in
                        if onSuccess == "success" {
                            DispatchQueue.main.async {
                                submitButton.loadingIndicator(false)
                            }
                            loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "UpdateTask.TAT", event_name: String(loggerStartTime), component: "SelfVideoService", event_source: "updateTask"), meta: [:])

                            obj.updateValue("pending", forKey: "status")
                            updateTask[index] = obj
                            task = updateTask as NSArray
                            activityEnded(provider: false)
                        } else {
                            logError(eventType: "onSessionFailure", eventName: "failure", eventSource: "updateTask", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")
                        }
                    }

                } else {
                    logError(eventType: "onSessionFailure", eventName: "exception", eventSource: "updateTask", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")

                    obj.updateValue("success", forKey: "status")
                    updateTask[index] = obj
                    task = updateTask as NSArray
                }
            }
        } else if task.count == 0 {
            activityEnded(provider: true)
        } else {
            var obj = task[0] as! [String:Any]
            obj.updateValue("success", forKey: "status")
            updateTask[0] = obj
            task = updateTask as NSArray
            activityEnded(provider: true)
        }

    }

    public func activityEnded(provider:Bool) {
        var body:[String:Any] = [:]
        var params:[String:Any] = [:]

        params.updateValue(Date.currentTimeStamp, forKey: "end_time")
        params.updateValue(currentTask, forKey: "index")
        params.updateValue(task, forKey: "tasks")

        body.updateValue(svcSessionId, forKey: "session_id")
        body.updateValue(params as NSObject, forKey: "params")

        print(body)

        var meta: [String: Any] = [:]
        meta.updateValue(body, forKey: "data")
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "ActivityEnded", event_name: "Initiate", component: "SelfVideoService", event_source: "activityEnded"), meta:meta)
        loggerStartTime = Double(Date.currentTimeStamp)

        guard let data = try? JSONSerialization.data(withJSONObject: body as NSObject, options: []) else { return  }
        dalCapture.callApi(view: self, url: modelSVCConfig.getUrlActivityEnded(),data: data) { [self] onSuccess, onFailure in
            if onSuccess != nil {

                var meta: [String: Any] = [:]
                meta.updateValue(onSuccess!, forKey: "data")
                loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "ActivityEnded.TAT", event_name: String(loggerStartTime), component: "SelfVideoService", event_source: "ActivityEnded"), meta: meta)

                let status = onSuccess?.value(forKey: "performed_correctly") as! Bool
                if status == true {
                    print("Activity ended")
                    DispatchQueue.main.async {
                        self.loadingView.isHidden = true
                    }
//                    if provider {
                        checkForNextPage()
//                    } else {
//                        videoEnded()
                        dalCapture.disconnectEventSource()
//                    }
                } else {
                    self.window?.rootViewController?.showAlert(title: "Please Try Again", message: "Numbers are not recognized.Please ensure that you read the numbers loudly and clearly.", actionTitle: "RETRY", completion: { success in
                        self.activityStarted()
                    })
                }
            } else {
                logError(eventType: "onSessionFailure", eventName: "failure", eventSource: "activityEnded", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")

                let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                self.delegate?.onSvcFailure(object: object)
            }

        }

    }
    func checkForNextPage() {
        let number = currentTask + 1
        if activity_count > number {
            currentTask = currentTask + 1
            getFetchActivity()
        } else {
            videoEnded()
        }
    }
    func videoEnded() {

        var video_params: [String: Any] = [:]
        video_params.updateValue(Date.currentTimeStamp, forKey: "video_end_time")
        var body: [String: Any] = [:]
        body.updateValue(svcSessionId, forKey: "session_id")
        body.updateValue(video_params, forKey: "video_params")
        var meta: [String: Any] = [:]
        meta.updateValue(body, forKey: "data")
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "VideoEnded", event_name: "Initiate", component: "SelfVideoService", event_source: "videoEnded"), meta: meta)
        loggerStartTime = Double(Date.currentTimeStamp)

        videoStart = false
        currentTask = 0
        videoComponentView?.stopRecording()

        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "buttonClick", event_name: "stop", component: "SelfVideoService", event_source: "stopRecording"), meta: [:])

        let videoParam:[String:Any] = [
            "video_end_time": Date.currentTimeStamp
        ]
        let param: [String: Any] = [
            "session_id": svcSessionId,
            "video_params": videoParam
        ]
        guard let data = try? JSONSerialization.data(withJSONObject: param, options: []) else { return  }
        dalCapture.callApi(view: self, url: modelSVCConfig.getUrlVideoEnded(),data: data) { [self] onSuccess, onFailure in
            if onSuccess != nil {

                var meta: [String: Any] = [:]
                meta.updateValue(onSuccess!, forKey: "data")
                loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "VideoEnded.TAT", event_name: String(loggerStartTime), component: "SelfVideoService", event_source: "videoEnded"), meta: meta)

                let status = onSuccess?.value(forKey: "status") as! String
                if status == "success" {
                    print("Video Submitted for verification")
                    DispatchQueue.main.async { [self] in
                        endSvcCallView.isHidden = false
                        videoComponentView?.cleanUp()

                        let object = ["event": "header_title", "title": ""] as NSObject
                        self.delegate?.onSvcIntermediate(object: object)
                    }
                }
            } else {
                logError(eventType: "onSessionFailure", eventName: "failure", eventSource: "videoEnded", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")

                let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                self.delegate?.onSvcFailure(object: object)
            }

        }
    }
    @IBAction func didTapSubmitCallButton(_ sender:UIButton) {
        guard let location = LocationManager.sharedInstance.currentLocation else { return }

        //        let dataArray: [String:Any] =  ["timestamp": "\(currentDate())", "speed": "\(location.speed)", "longitude": "\(location.coordinate.longitude)", "latitude": "\(location.coordinate.latitude)", "accuracy": "\(location.speedAccuracy)", "heading": "", "altitude": "\(location.altitude)", "altitudeAccuracy": "\(location.horizontalAccuracy)","source":"GPS"]
        //        let data:NSObject = dataArray as NSObject
        let geoLocation: [String:Any] = [
            "geo_location" : "{\"latitude\":\(location.coordinate.latitude),\"longitude\":\(location.coordinate.longitude),\"altitude\":null,\"accuracy\":1756.7765219327807,\"altitudeAccuracy\":null,\"heading\":null,\"speed\":null,\"timestamp\":\"\(currentDate())\",\"source\":\"GPS\"}"
        ]
        let param: [String: Any] = [
            "session_id": svcSessionId,
            "params": geoLocation as NSObject
        ]

        var meta: [String: Any] = [:]
        meta.updateValue(param, forKey: "data")
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "buttonClick", event_name:"Initiate", component: "SelfVideoService", event_source: "submit"), meta: meta)
        loggerStartTime = Double(Date.currentTimeStamp)

        guard let data = try? JSONSerialization.data(withJSONObject: param, options: []) else { return  }
        dalCapture.callApi(view: self, url: modelSVCConfig.getUrlSubmit(),data: data) { [self] onSuccess, onFailure in
            if onSuccess != nil {

                var meta: [String: Any] = [:]
                meta.updateValue(onSuccess!, forKey: "data")
                loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "Submit", event_name: String(loggerStartTime), component: "SelfVideoService", event_source: "submit"), meta: meta)

                let status = onSuccess?.value(forKey: "status") as! String
                if status == "success" {
                    print("Call Submitted for verification")
                    DispatchQueue.main.async { [self] in
                        if ((videoComponentView?.isDescendant(of: svcContentView)) != nil) {
                            videoComponentView?.removeFromSuperview() // Remove it
                        }
                        //TODO Call back to base view
                        delegate?.onSvcSuccess(object: ["event": "CALL_END"] as NSObject)
                    }

                } else {
                    logError(eventType: "onSessionSuccess", eventName: "exception", eventSource: "submit", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")
                }
            } else {
                logError(eventType: "onSessionFailure", eventName: "failure", eventSource: "submit", exceptionName: "\(String(describing: onFailure))", exceptionDesc: "\(String(describing: onFailure))")

                let object = ["event": "NETWORK_DISCONNECTED"] as NSObject
                self.delegate?.onSvcFailure(object: object)
            }

        }
    }
    @IBAction func didTapRecaptureButton(_ sender:UIButton) {
        loggerVC?.log(logLevel: LogLevel.shared.Info, logDetails: ModalLogDetails(service: "SelfVideoService.SVC",event_type: "buttonClick", event_name: "recapture", component: "SelfVideoService", event_source: "setOnClickListener"), meta: [:])

        if infoView.isDescendant(of: contentView) {
            infoView.removeFromSuperview()
        }
        endSvcCallView.isHidden = true
        getTaskStatus()
        updateInitialLMessage()

    }
    public func onDisconnect(code: String) {

    }

    public func onCompleted(message: String) {
        
    }
    public func fetchWaitTime(time:Int) {

    }
    public func onVCStateCallBack(event: String, object: NSObject) {
        if event ==  "start" {
            getFetchActivity()
        }

    }
    public func onVCError(code: String, message: String, canReconnect: Bool) {
        
    }
    private func logGrantedPermission(eventName: String, eventSource: String) {

        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "PermissionsCheck"
        logDetails.event_name = eventName
        logDetails.event_type = "Granted"
        logDetails.event_source = eventSource
        logDetails.component = "PermissionsService"
        logDetails.publish_to_dlk = true

        loggerWrapper?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
    }
    private func logDeniedPermission(eventName: String, eventSource: String, errorMessage: String) {

        var logDetails = ModalLogDetails()
        logDetails.service_category = "captureSDK"
        logDetails.service = "PermissionsCheck"
        logDetails.event_name = eventName
        logDetails.event_type = "Denied"
        logDetails.event_source = eventSource
        logDetails.component = "PermissionsService"
        logDetails.publish_to_dlk = true

        var meta: [String: Any] = [:]
        meta.updateValue(errorMessage, forKey: "error_message")

        loggerWrapper?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)

    }
}
extension SvcView {

    func accessCameraPermission() {
        DispatchQueue.main.async {
            self.window?.rootViewController?.showAlertForCameraAccess(completion: { success in
                if success == true {
                    AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                        if granted == true {
                            self.logGrantedPermission(eventName: "Camera", eventSource: "getCameraPermission")
                        } else {
                            DispatchQueue.main.async {
                                self.window?.rootViewController?.showAlert(title: "", message: "Without camera access can't proceed.", actionTitle: "Ok", completion: { _ in
                                    self.logDeniedPermission(eventName: "Camera", eventSource: "accessCameraPermission", errorMessage: "userDeniedPermission")

                                })
                            }
                        }
                    })
                } else {

                }
            })
        }
    }
    func accessMicroPhonePermission() {
        DispatchQueue.main.async {
            self.window?.rootViewController?.showAlertForMicroPhoneAccess(completion: { [self] success in
                if success == true {
                    AVCaptureDevice.requestAccess(for: AVMediaType.audio, completionHandler: { (granted: Bool) -> Void in
                        if granted == true {
                            self.logGrantedPermission(eventName: "Microphone", eventSource: "getMicrophonePermission")
                        } else {
                            DispatchQueue.main.async {
                                self.window?.rootViewController?.showAlert(title: "", message: "Without microphone access can't proceed.", actionTitle: "Ok", completion: { _ in
                                    self.logDeniedPermission(eventName: "Microphone", eventSource: "accessMicroPhonePermission", errorMessage: "userDeniedPermission")

                                })
                            }
                        }
                    })
                } else {
                }
            })
        }
    }
    func accessLocationPermission() {
        DispatchQueue.main.async {
            self.window?.rootViewController?.showAlertForLocationAccess(completion: { success in
                if success == true {
                    if let _ = LocationManager.sharedInstance.currentLocation {
                        self.logGrantedPermission(eventName: "Location", eventSource: "getLocationPermission")
                    } else {
                        DispatchQueue.main.async {
                            self.window?.rootViewController?.showAlert(title: "", message: "Without location access can't proceed.", actionTitle: "Ok", completion: { _ in
                                self.logDeniedPermission(eventName: "Location", eventSource: "accessLocationPermission", errorMessage: "userDeniedPermission")
                            })
                        }
                    }
                } else {
                }
            })
        }
    }
    private func logError(eventType:String,eventName:String,eventSource:String,exceptionName:String,exceptionDesc:String) {
        var logDetails = ModalLogDetails()
        logDetails.service = "SelfVideoService.SVC"
        logDetails.event_name = eventName
        logDetails.event_type = eventType
        logDetails.event_source = eventSource
        logDetails.exceptionName = exceptionName
        logDetails.exceptionDescription = exceptionDesc

        loggerVC?.log(logLevel: LogLevel.shared.Error, logDetails: logDetails, meta: [:])
    }
}
