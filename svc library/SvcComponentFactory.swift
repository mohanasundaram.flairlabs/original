//
//  SvcComponentFactory.swift
//  svc library
//
//  Created by Admin on 05/04/22.
//

import Foundation
import Cleanse
import videocomponent
import core
import healthcheck

public class SVCComponentFactory {

    public static func getSVCComponent(dalComponent:DalComponent.Root) -> SvcComponent.Root {
        let vcComponent = VCComponentFactory.getVcComponent(dalComponent: dalComponent)
        let hcComponent = HCComponentFactory.getHealthCheckComponent(dalComponent: dalComponent)

        let svcComponent = try! ComponentFactory.of(SvcComponent.self).build(SvcFeed(dalComponent: dalComponent, healthCheck: hcComponent, videoComponent: vcComponent))
        return svcComponent
    }
}

