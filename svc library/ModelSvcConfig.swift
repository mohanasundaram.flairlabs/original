//
//  ModelSvcConfig.swift
//  svc library
//
//  Created by Admin on 10/03/22.
//

import Foundation
import videocomponent

public class ModelSVCConfig {
    private var svcLoggerUrl: String = ""
    private var enableLogging: Bool = false
    private var urlFetchTaskStatus: String = ""
    private var urlTaskStarted: String = ""
    private var urlTaskResponse: String = ""
    private var urlFetchActivity: String = ""
    private var urlVideoStarted: String = ""
    private var urlActivityStarted: String = ""
    private var urlActivityEnded: String = ""
    private var urlVideoEnded: String = ""
    private var urlSubmit: String = ""

    private var svcInitHealthCheckUrl: String = ""
    private var svcSubmitAdvHealth: String = ""
    private var svcSubmitHealthUrl: String = ""
    private var modelVcConfig = ModelVcConfig()

    public init() {}

    private init(svcBuilder: SvcBuilder) {

        self.svcLoggerUrl = svcBuilder.svcLoggerUrl
        self.enableLogging = svcBuilder.enableLogging
        self.urlFetchTaskStatus = svcBuilder.urlFetchTaskStatus
        self.urlTaskStarted = svcBuilder.urlTaskStarted
        self.urlTaskResponse = svcBuilder.urlTaskResponse
        self.urlFetchActivity = svcBuilder.urlFetchActivity
        self.urlVideoStarted = svcBuilder.urlVideoStarted
        self.urlActivityStarted = svcBuilder.urlActivityStarted
        self.urlActivityEnded = svcBuilder.urlActivityEnded
        self.urlVideoEnded = svcBuilder.urlVideoEnded
        self.urlSubmit = svcBuilder.urlSubmit

        self.svcInitHealthCheckUrl = svcBuilder.svcInitHealthCheckUrl
        self.svcSubmitAdvHealth = svcBuilder.svcSubmitAdvHealth
        self.svcSubmitHealthUrl = svcBuilder.svcSubmitHealthUrl
        self.modelVcConfig = svcBuilder.modelVcConfig

    }

    public func getSvcLoggerUrl() -> String { return svcLoggerUrl }
    public func isLoggerEnabled() -> Bool { return enableLogging }
    public func getUrlFetchTaskStatus() -> String { return urlFetchTaskStatus }
    public func getUrlTaskStarted() -> String { return urlTaskStarted }
    public func getUrlTaskResponse() -> String { return urlTaskResponse }
    public func getUrlFetchActivity() -> String { return urlFetchActivity }
    public func getUrlVideoStarted() -> String { return urlVideoStarted }
    public func getUrlActivityStarted() -> String { return urlActivityStarted }
    public func getUrlActivityEnded() -> String { return urlActivityEnded }
    public func getUrlVideoEnded() -> String { return urlVideoEnded }
    public func getUrlSubmit() -> String { return urlSubmit }
    public func getUrlInitHealthCheck() -> String { return svcInitHealthCheckUrl }
    public func getUrlAdvSubmitHealth() -> String { return svcSubmitAdvHealth }
    public func getUrlSubmitHealth() -> String { return svcSubmitHealthUrl }
    public func getModelVcConfig() -> ModelVcConfig { return modelVcConfig }


    public class SvcBuilder {
        private(set) var svcLoggerUrl: String = ""
        private(set) var enableLogging: Bool = true
        private(set) var urlFetchTaskStatus: String = "/fetch_task_status"
        private(set) var urlTaskStarted: String = "/task_started"
        private(set) var urlTaskResponse: String = "/task_response"
        private(set) var urlFetchActivity: String = "/fetch_activity"
        private(set) var urlVideoStarted: String = "/video_started"
        private(set) var urlActivityStarted: String = "/activity_started"
        private(set) var urlActivityEnded: String = "/activity_ended"
        private(set) var urlVideoEnded: String = "/video_ended"
        private(set) var urlSubmit: String = "/submit"

        private(set) var svcInitHealthCheckUrl: String = "/initiate_check"
        private(set) var svcSubmitAdvHealth: String = "/submit_advanced_conn_stats"
        private(set) var svcSubmitHealthUrl: String = "/submit_connection_stats"

        private(set) var modelVcConfig = ModelVcConfig()


        public init() {}

        public func setLoggerUrl(loggerUrl: String) -> SvcBuilder {
            self.svcLoggerUrl = loggerUrl
            return self
        }
        public func enableLogging(enableLogging: Bool) -> SvcBuilder {
            self.enableLogging = enableLogging
            return self
        }
        public func setSvcServiceBaseUrl(svcServiceBaseUrl: String) -> SvcBuilder {
            self.urlFetchTaskStatus = svcServiceBaseUrl + urlFetchTaskStatus
            self.urlTaskStarted = svcServiceBaseUrl + urlTaskStarted
            self.urlFetchActivity = svcServiceBaseUrl + urlFetchActivity
            self.urlVideoStarted = svcServiceBaseUrl + urlVideoStarted
            self.urlActivityStarted = svcServiceBaseUrl + urlActivityStarted
            self.urlActivityEnded = svcServiceBaseUrl + urlActivityEnded
            self.urlVideoEnded = svcServiceBaseUrl + urlVideoEnded
            self.urlSubmit = svcServiceBaseUrl + urlSubmit

            return self
        }
        public func setSvcSseBaseUrl(svcSseBaseUrl: String) -> SvcBuilder {
            self.urlTaskResponse = svcSseBaseUrl + urlTaskResponse
            return self
        }
        public func setNetworkCheckBaseUrl(avkycNetworkCheckBaseUrl: String) -> SvcBuilder {
            self.svcInitHealthCheckUrl = avkycNetworkCheckBaseUrl + self.svcInitHealthCheckUrl
            self.svcSubmitAdvHealth = avkycNetworkCheckBaseUrl + self.svcSubmitAdvHealth
            self.svcSubmitHealthUrl = avkycNetworkCheckBaseUrl + self.svcSubmitHealthUrl
            return self
        }
        public func modelVcConfig(mediaServerUrl: String, loggerUrl: String? = nil, frameWidth: Int? = nil, frameHeight: Int? = nil, framePerSec: Int? = nil) -> SvcBuilder {
            self.modelVcConfig = ModelVcConfig.VCBuilder()
                .mediaSocketUrl(mediaSocketUrl: mediaServerUrl)
                .vc_logger_url(vc_logger_url: loggerUrl ?? "")
                .frame_width(frame_width: frameWidth ?? 480)
                .frame_height(frame_height: frameHeight ?? 640)
                .frame_per_sec(frame_per_sec: framePerSec ?? 10)
                .build()
            return self
        }
        public func build() -> ModelSVCConfig {

            let modelSvcConfig = ModelSVCConfig(svcBuilder: self)
            return modelSvcConfig
        }

    }
}

