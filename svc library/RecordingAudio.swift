//
//  RecordingAudio.swift
//  svc library
//
//  Created by Admin on 11/03/22.
//

import Foundation
import AVFoundation


class RecordingAudio:NSObject, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer:AVAudioPlayer?

    override init() {

        audioRecorder = AVAudioRecorder()
        recordingSession = AVAudioSession.sharedInstance()

        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)

            recordingSession.requestRecordPermission() { allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allowed to record")
                    } else {
                        print("Fail to record")
                    }
                }
            }
        } catch {
            // failed to record!
        }
    }
    func startRecording() {

        do {
            try FileManager.default.removeItem(at: getDocumentsDirectory())
        } catch let error as NSError {
            print("Error: \(error.domain)")
        }


        do {
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100,
                AVNumberOfChannelsKey: 2,
                AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
            ]
            audioRecorder = try AVAudioRecorder(url: getFileUrl(), settings: settings)

            audioRecorder.delegate = self
            audioRecorder.record()
            print("recording started")
        } catch {
            self.stopRecording()
        }
    }
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    func getFileUrl() -> URL {
        let filename = "myRecording.mp4"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
    return filePath
    }
    func stopRecording() {
        if audioRecorder != nil {
        audioRecorder.stop()
        audioRecorder = nil
//        print("recording stopped")
//            convertAudioFile()

        }
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            stopRecording()
        }
    }
}
