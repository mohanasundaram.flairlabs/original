//
//  SVCComponent.swift
//  svc library
//
//  Created by Admin on 01/04/22.
//

import Foundation
import Cleanse
import core
import videocomponent
import healthcheck

public struct SvcStructure {

    let videoComponent: VideoComponent
    let healthCheck:HealthCheck

    public func getVideoComponent() -> VideoComponent {
        return videoComponent
    }
    public func getHealthCheck() -> HealthCheck {
        return healthCheck
    }
}
public struct SvcComponent : Cleanse.RootComponent {
    public typealias Root = SvcStructure
    public typealias Seed = SvcFeed

    public static func configure(binder: UnscopedBinder) {
        binder.install(dependency: DalComponent.self)
        binder.install(dependency: VcComponent.self)
        binder.install(dependency: HCComponent.self)
    }

    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dd:Seed) in
            Root.init(videoComponent: dd.videoComponent.getVideoComponent(), healthCheck: dd.healthCheck.gethealthCheck())
        }
    }
}
public struct SvcFeed {
    let dalComponent:DalComponent.Root
    let videoComponent:VcComponent.Root
    let healthCheck: HCComponent.Root

    public init(dalComponent:DalComponent.Root,healthCheck:HCComponent.Root,videoComponent:VcComponent.Root) {
        self.dalComponent = dalComponent
        self.healthCheck = healthCheck
        self.videoComponent = videoComponent
    }
}

