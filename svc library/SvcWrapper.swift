//
//  SvcWrapper.swift
//  svc library
//
//  Created by Admin on 01/04/22.
//
import Foundation
import core
import videocomponent
import healthcheck
public class SelfVideoKycFactory {

    var selfVideoKycFragment:SvcView?

    public init() {

    }
    public func startSvc(svcConfig:ModelSVCConfig) -> SvcView {
        let dalComponent:DalComponent.Root?
        dalComponent = DalComponentFactory.sharedInstance.getDalComponent()

        let svcComponent = SVCComponentFactory.getSVCComponent(dalComponent: dalComponent!)
        selfVideoKycFragment = SvcView(frame: .zero, dalCapture: (dalComponent?.getDalCapture())!, healthCheck: svcComponent.getHealthCheck(), loggerWrapper: (dalComponent?.getLoggerWrapper())!, modelSvcConfig: svcConfig, videoComponent: svcComponent.getVideoComponent())
        return selfVideoKycFragment!
    }
}

//var assistedVideoKycFragment:AvkycView?
//
//public init() {
//}
//public func startAvkyc(modelAVKYCConfig:ModelAVKYCConfig) -> AvkycView {
//
//    let dalComponent:DalComponent.Root?
//
//    dalComponent = DalComponentFactory().getDalComponent()
//
//    let avkycComponent = AVKYCComponentFactory.getAvkycComponent(dalComponent: dalComponent!)
//
//    assistedVideoKycFragment = AvkycView(frame: .zero, dalCapture: (dalComponent?.getDalCapture())!, modelAVKYCConfig: modelAVKYCConfig, healthCheck: avkycComponent.getHealthCheck(), videoComponent:avkycComponent.getVideoCallVideoComponent(), loggerWrapper: (dalComponent?.getLoggerWrapper())!)
//
//    return assistedVideoKycFragment!
//}
