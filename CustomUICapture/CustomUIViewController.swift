//
//  CustomUIViewController.swift
//  CustomUICapture
//
//  Created by FL Mohan on 10/06/22.
//

import Foundation
import UIKit
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import core


class CustomUIViewController:UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate, ArtifactsCallback, IRequiredTask, IInitiateTask {


    @IBOutlet var panCaptureButton:UIButton!
    @IBOutlet var aadharFrontCaptureButton:UIButton!
    @IBOutlet var aadharBackCaptureButton:UIButton!
    @IBOutlet var titleText:UILabel!
    @IBOutlet var textField1:MDCFilledTextField!
    @IBOutlet var textField2:MDCFilledTextField!
    @IBOutlet var panImage:UIImageView!
    @IBOutlet var aadhaarFrontImage:UIImageView!
    @IBOutlet var aadharBackImage:UIImageView!
    @IBOutlet var panCloseButton:UIButton!
    @IBOutlet var aadharFrontCloseButton:UIButton!
    @IBOutlet var aadharBackCloseButton:UIButton!


    var taskArray:[Tasks] = []
    var dalCapture = DALCapture()
    var taskHelper = TaskHelper()

    var pickerController = UIImagePickerController()
    var panTask:Tasks?
    var aadharFrontTask:Tasks?
    var aadharbackTask:Tasks?
    var initiateTask:Tasks?
    var tag = 0



    public override func viewDidLoad() {
        getRequiredDocuments()
    }
    override func viewDidDisappear(_ animated: Bool) {
    }
    func getRequiredDocuments() {
        self.taskHelper.getRequiredtasks(iRequiredTask: self)
    }
    func onSuccess(tasksList: [Tasks]) {
        self.taskArray = tasksList
        DispatchQueue.main.async { [self] in
            didSetUi()
            pickerController.delegate = self
        }
        
    }
    func onFailure(msg: String) {
    }
    func didSetUi() {
        print(taskArray)
            for taskArray in taskArray {
                if taskArray.getArtifactKey() == "name.nil.name.1.nil" {
                    let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskArray.getTemplateId())")
                    textField1.label.text = templateObject?.value(forKey: "label") as? String
                    textField1.placeholder = templateObject?.value(forKey: "value") as? String
                    textField1.setUnderlineColor(.blue, for: .normal)
                } else if taskArray.getArtifactKey() == "dob.nil.dob.1.nil" {
                    let templateObject = dalCapture.getTemplateObjectByTemplateId(templateId: "\(taskArray.getTemplateId())")
                    textField2.label.text = templateObject?.value(forKey: "label") as? String
                    textField2.placeholder = templateObject?.value(forKey: "value") as? String
                    textField2.setUnderlineColor(.blue, for: .normal)
                } else if taskArray.getArtifactKey() == "poi.ind_pan.nil.1.front" {
                    panTask = taskArray
                } else if taskArray.getArtifactKey() == "poa.nil.nil.1.front" {
                    aadharFrontTask = taskArray
                } else if taskArray.getArtifactKey() == "poa.nil.nil.1.back" {
                    aadharbackTask = taskArray
                } else if taskArray.getTaskType() == "Initiate" {
                    initiateTask = taskArray
                }
            }
            panCloseButton.isHidden = true
            aadharFrontCloseButton.isHidden = true
            aadharBackCloseButton.isHidden = true
    }
    @IBAction func didTapPanCapture(sender:UIButton) {
        tag = 1
        pickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(pickerController, animated: true, completion: nil)
    }
    @IBAction func didTapAadharFrontCapture(sender:UIButton) {
        tag = 2
        pickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(pickerController, animated: true, completion: nil)
    }
    @IBAction func didTapAadharBackCapture(sender:UIButton) {
        tag = 3
        pickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(pickerController, animated: true, completion: nil)
    }
    @IBAction func didClearPanImage(sender:UIButton) {
        panCloseButton.isHidden = true
        panImage.image = nil
        taskHelper.resetTask(taskKey: "key26")
    }
    @IBAction func didClearAadhaarFrontImage(sender:UIButton) {
        aadharFrontCloseButton.isHidden = true
        aadhaarFrontImage.image = nil
    }
    @IBAction func didClearAadharbackImage(sender:UIButton) {
        aadharBackCloseButton.isHidden = true
        aadharBackImage.image = nil
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        var task:Tasks?
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if tag == 1 {
                panImage.image = image
                panCloseButton.isHidden = false
                task = panTask
            } else if tag == 2 {
                aadhaarFrontImage.image = image
                aadharFrontCloseButton.isHidden = false
                task = aadharFrontTask
            } else if tag == 3 {
                aadharBackCloseButton.isHidden = false
                aadharBackImage.image = image
                task = aadharbackTask
            }
            self.taskHelper.saveTask(taskKey: task?.getTaskKey() ?? "", contentType:  "image/jpeg", documentType: task?.getDocumentType() ?? "", image: image,artifactsCallback: self)
        }
    }
    func onFileUploaded(event: String, jsonObject: NSObject) {
        print(event)
    }

    func onUpdateDiv(event: String, jsonObject: NSObject) {
        print(event)
    }

    func onInitiateDiv(event: String, jsonObject: NSObject) {
        print(event)
    }

    func onArtifactFailure(event: String, jsonObject: NSObject) {
        print(event)
    }
    @IBAction func submitForm(sender:UIButton) {
        taskHelper.initiateTask(taskKey: "key300", iInitiateTask: self)
    }
    func onInitiateSuccess(response: NSObject) {
print("onInitiateSuccess ==> \(response)")
    }

    func onInitiateFailed(message: String) {
        print("onInitiateFailed ==> \(message)")
    }

    func onOCRSuccess(responseArray: NSArray) {
        print("onOCRSuccess ==> \(responseArray)")
    }
}
