//
//  ViewController.swift
//  CustomUICapture
//
//  Created by FL Mohan on 10/06/22.
//

import Foundation
import AVFoundation
import UIKit
import core

public protocol HostAppCallBack {
    func onCaptureSuccess(object: NSObject)
    func onCaptureFailure(object: NSObject)
}

public class ViewController: UIViewController, InstructionActionDelegate,IStatusCallback,IReqDocCallback {

    @IBOutlet weak var tokenView: UIView!
    @IBOutlet weak var tokenTextField: UITextField!

    private var dalCapture = DALCapture()

    private var taskHelper = TaskHelper()

    public override func viewDidLoad() {

        tokenTextField.text = "8e54PGlnj8kh"

        title   =   "Custom Capture UI"


    }
    @IBAction func didTapStart(_ sender:UIButton) {
        view.endEditing(true)
        self.view.gestureRecognizers?.forEach(view.removeGestureRecognizer)
        guard tokenTextField.text != "" else { return ProgressHUD.show("Please enter a valid token", icon: .question, interaction: true) }
        if let envConfig = ApiController.getEnvConfig() {
            let dalConfig = DalConfig.DalConfigBuilder()
                .setToken(token: tokenTextField.text ?? "")
                .enableLogging(enableLogging: true)
                .setLoggerUrl(loggerUrl: envConfig.getREACT_APP_LOGGER_URL())
                .setCaptureServerUrl(serverUrl: envConfig.getREACT_APP_API_SERVER_URI())
                .setCaptureSocketUrl(captureSocketUrl: envConfig.getREACT_APP_CAPTURE_SOCKET_URI())
                .build()

            dalCapture = DalCaptureFactory().getDalCapture(dalConfig: dalConfig)
            taskHelper = TaskHelperFactory.getTaskHelper()
        }

        self.initialiseCaptureView()
    }
    func initialiseCaptureView() {
        getCaptureStatusDetails(sessionOverride: false)
    }
    func getCaptureStatusDetails(sessionOverride: Bool) {
        dalCapture.getCaptureStatusDetails(sessionOverride: sessionOverride, callback: self)
    }
    public func onStatusSuccess(object: NSObject) {
        DispatchQueue.main.async {
        let customUIVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomUIViewController") as! CustomUIViewController
            customUIVc.dalCapture = self.dalCapture
            customUIVc.taskHelper = self.taskHelper
        self.navigationController?.pushViewController(customUIVc, animated: true)
        }
    }

    public func onStatusFailure(message: String) {
        if message == "You don't have sufficient permissions to perform this action." {
            DispatchQueue.main.async {
                self.showAlert(title: "Multiple Sessions Available", message: "You have more than one active sessions. Do you wish to close all other active sessions?", actionTitle1: "Yes", actionTitle2: "No") { success in
                    if success == true {
                        self.getCaptureStatusDetails(sessionOverride: true)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
                self.showAlert(title: "Invalid Token", message: "", actionTitle: "Close", completion: { _ in
                    self.onDestroy()
                })
            }
        }
    }
    public func onDestroy() {
        dalCapture.onDestroy()
        dalCapture.disconnectDalSocket()
        dalCapture = DALCapture()
    }

   
    public func onReqDocSuccess(artifactsList: [Tasks]) {

    }

    public func onReqDocFailure(message: String) {

    }
    public func didSetRetryButton(screen: RetryOptions) {

    }
}

