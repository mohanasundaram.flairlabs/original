//
//  ViewController.swift
//  customCapture
//
//  Created by Admin on 09/03/22.
//

import Foundation
import AVFoundation
import UIKit
import core
import capture
import RxSwift

public protocol HostAppCallBack {
    func onCaptureSuccess(object: NSObject)
    func onCaptureFailure(object: NSObject)
}

public class ViewController: UIViewController,IRequiredTask, IInitiateTask,IArtifactSubmitCallback {

    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var tokenView: UIView!
    @IBOutlet weak var tokenTextField: UITextField!
    @IBOutlet weak var captureContentView:UIView!
    @IBOutlet weak var submitButton:UIButton!


    let kCONTENT_XIB_NAME           =   "CaptureComponent"

    private var captureView = UIScrollView()
    private var contentOfScrollView = UIStackView()


    var infoView = InstructionView()

    var delegate: HostAppCallBack?

    var artifactList:[Artifacts] = []

    private var dalCapture = DALCapture()

    private var taskManager = TaskManager()

    var taskHelper = TaskHelper()

    private var disposableList:[Disposable] = []

    private var panTaskKey = ""

    public override func viewDidLoad() {

        tokenTextField.text = "A7EIoGU-oWI8"

        setupScrollView()

    }
    @IBAction func didTapStart(_ sender:UIButton) {
        view.endEditing(true)
        self.view.gestureRecognizers?.forEach(view.removeGestureRecognizer)
        guard tokenTextField.text != "" else { return ProgressHUD.show("Please enter a valid token", icon: .question, interaction: true) }
        tokenView.isHidden = true
        if let envConfig = ApiController.getEnvConfig() {
            let dalConfig = DalConfig.DalConfigBuilder()
                .setToken(token: tokenTextField.text ?? "")
                .enableLogging(enableLogging: true)
                .setLoggerUrl(loggerUrl: envConfig.getREACT_APP_LOGGER_URL())
                .setCaptureServerUrl(serverUrl: envConfig.getREACT_APP_API_SERVER_URI())
                .setCaptureSocketUrl(captureSocketUrl: envConfig.getREACT_APP_CAPTURE_SOCKET_URI())
                .build()

            dalCapture = DalCaptureFactory().getDalCapture(dalConfig: dalConfig)
            taskManager = TaskManager(dalCapture: dalCapture)
            taskHelper = TaskHelper(dalCapture: dalCapture, taskManager: taskManager)
        }

        self.initialiseCaptureView()
    }
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        self.contentView.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        contentView.endEditing(true)
    }
    func initialiseCaptureView() {
        ProgressHUD.show()

        infoView.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        submitButton.tintColor = .red
        submitButton.isEnabled = false

        self.taskHelper.getRequiredtasks(iRequiredTask: self)

    }
    public func onSuccess(tasksList: [Tasks]) {
        DispatchQueue.main.async { [self] in
            ProgressHUD.dismiss()
            contentOfScrollView.backgroundColor = .systemGray2
            let label = UILabel()
            label.text = "CUSTOM CAPTURE"
            label.textAlignment = .center
            label.textColor = .red
            contentOfScrollView.addArrangedSubview(label)
            for tasksList in tasksList {
                let type = tasksList.getType()
                switch type {
                case "text":
                    let textView = TextView(frame: contentOfScrollView.frame)
                    textView.setConfig(dalCapture: dalCapture, templateId: tasksList.getTemplateId(), taskManager: taskManager)
                    contentOfScrollView.addArrangedSubview(textView)
                    break
                case "image":
                    let imageView = Image(frame: contentOfScrollView.frame)
                    let themeConfig = dalCapture.getThemeConfig()
                    themeConfig.setPrimaryMainColor(primaryMainColor: "")
                    themeConfig.setPrimaryContrastColor(primaryContrastColor: "")
                    themeConfig.setSecondaryMainColor(secondaryMainColor: "")
                    themeConfig.setSecondaryContrastColor(secondaryContrastColor: "")
                    imageView.setConfig(dalCapture: dalCapture, templateId: tasksList.getTemplateId(), taskManager: taskManager,themeConfig:themeConfig)
                    let stackedInfoView = UIStackView(frame: self.contentOfScrollView.frame)
                    stackedInfoView.axis = .horizontal
                    stackedInfoView.alignment = .leading
                    stackedInfoView.translatesAutoresizingMaskIntoConstraints = false
                    stackedInfoView.addArrangedSubview(imageView)
                    stackedInfoView.addArrangedSubview(UIView())
                    contentOfScrollView.addArrangedSubview(stackedInfoView)
                case "Initiate":
                    panTaskKey = tasksList.getTaskKey()
                    break
                case "checkbox":
                    let checkBox = CheckBox(frame: contentOfScrollView.frame)
                    checkBox.setConfig(dalCapture: dalCapture, templateId: tasksList.getTemplateId(), taskManager: taskManager)
                    contentOfScrollView.addArrangedSubview(checkBox)
                    break
                case "radio":
                    let radio = RadioBox(frame: contentOfScrollView.frame)
                    radio.setConfig(dalCapture: dalCapture, templateId: tasksList.getTemplateId(), taskManager: taskManager)
                    contentOfScrollView.addArrangedSubview(radio)
                    break
                case "dropdown":
                    let dropDown = DropDown(frame: contentOfScrollView.frame)
                    dropDown.setConfig(dalCapture: dalCapture, templateId: tasksList.getTemplateId(), taskManager: taskManager)
                    contentOfScrollView.addArrangedSubview(dropDown)
                    break
                case "button":
                    let button = Button(frame: contentOfScrollView.frame)
                    button.setConfig(dalCapture: dalCapture, templateId: tasksList.getTemplateId(), taskManager: taskManager)
                    contentOfScrollView.addArrangedSubview(button)
                    break
                case "filepicker":
                    let filePicker = FilePicker(frame: contentOfScrollView.frame)
                    filePicker.setConfig(dalCapture: dalCapture, templateId: tasksList.getTemplateId(), taskManager: taskManager)
                    contentOfScrollView.addArrangedSubview(filePicker)
                    break
                case "section":
                    let section = SectionView(frame: contentOfScrollView.frame)
                    section.setConfig(dalCapture: dalCapture, templateId: tasksList.getTemplateId(), taskManager: taskManager)
                    contentOfScrollView.addArrangedSubview(section)
                    break
                default:
                    break
                }
            }
            let button = UIButton()
            button.setTitle("Submit Form", for: .normal)
            button.backgroundColor = .blue
            button.addTarget(self, action:#selector(self.pressed) , for: .touchUpInside)
            contentOfScrollView.addArrangedSubview(button)
            subscribeTask()
        }

    }
    @objc func pressed() {
        taskHelper.initiateTask(taskKey: panTaskKey, iInitiateTask: self)
    }
    public func onFailure(msg: String) {
        showAlert(title: "Oops!", message: "Something wrong. Please try again after sometimes", actionTitle:"Okay", completion: { success in
            ProgressHUD.dismiss()
            DispatchQueue.main.async {
                self.tokenView.isHidden = false
            }
        })
    }
    public func onDestroy() {
        dalCapture.onDestroy()
        dalCapture.disconnectDalSocket()
        dalCapture = DALCapture()
    }
    public func subscribeTask() {
        disposableList.append(taskManager.allTaskDone.subscribe { [self] status in
            if (status.element ?? false) ?? false {
                submitButton.isEnabled = true
                submitButton.backgroundColor = ThemeConfig.shared.getPrimaryMainColor()
            } else if status.element == false {
                submitButton.isEnabled = false
                submitButton.backgroundColor = .gray
            }
        } )
    }
    @IBAction func submitCapture(sender:UIButton) {
        taskHelper.submitTasks(iArtifactSubmitCallback: self)
    }
    public func onInitiateSuccess(response: NSObject) {
        print("onInitiateSuccess ==> \(response)")
    }

    public func onInitiateFailed(message: String) {
        print("onInitiateFailed ==> \(message)")
    }

    public func onOCRSuccess(responseArray: NSArray) {
        print("onOCRSuccess ==> \(responseArray)")
    }
    public func onSubmitSuccess(object: NSObject) {
        let response = object.value(forKey: "response") as? NSObject
        if response?.value(forKey: "success") as? Bool == true {
            DispatchQueue.main.async {
                self.showAlertOnMessage(title:  "Awesome!!!", message: "Your request has been submitted with us. Thank you for using Idfy.") { success in
                }
            }
        }
    }

    public func onSubmitFailure(message: String) {

    }
}
extension ViewController {
    // view setup
    private func setupScrollView() {

        captureView.backgroundColor = .clear

        captureContentView.addSubview(captureView)

        captureView.translatesAutoresizingMaskIntoConstraints = false
        captureView.showsHorizontalScrollIndicator = false
        captureView.showsVerticalScrollIndicator = false

        captureView.leadingAnchor.constraint(equalTo: captureContentView.leadingAnchor).isActive = true
        captureView.trailingAnchor.constraint(equalTo: captureContentView.trailingAnchor).isActive = true
        captureView.topAnchor.constraint(equalTo: captureContentView.topAnchor).isActive = true
        captureView.bottomAnchor.constraint(equalTo: captureContentView.bottomAnchor).isActive = true

        setUpContentOfScrollView()

    }

    private func setUpContentOfScrollView() {

        contentOfScrollView.axis            = .vertical
        contentOfScrollView.distribution    = .fill
        contentOfScrollView.alignment       = .fill
        contentOfScrollView.spacing         = 25

        captureView.addSubview(contentOfScrollView)

        contentOfScrollView.translatesAutoresizingMaskIntoConstraints = false
        contentOfScrollView.leadingAnchor.constraint(equalTo: captureContentView.leadingAnchor).isActive = true
        contentOfScrollView.trailingAnchor.constraint(equalTo: captureContentView.trailingAnchor).isActive = true
        contentOfScrollView.topAnchor.constraint(equalTo: captureView.topAnchor).isActive = true
        contentOfScrollView.bottomAnchor.constraint(equalTo: captureView.bottomAnchor).isActive = true
    }
    @objc func keyboardWillShow(sender: NSNotification) {
        if let keyboardSize = (sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            captureView.contentInset.bottom = keyboardSize.height
        }
    }

    @objc func keyboardWillHide(sender: NSNotification) {
        captureView.contentInset.bottom = 0
    }
}
