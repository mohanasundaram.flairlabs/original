////
////  CaptureView.swift
////  Hostapp
////
////  Created by Admin on 08/03/22.
////
//
//import Foundation
//import AVFoundation
//import UIKit
//import core
//import avkyc
//import capture
//
//public class CustomCaptureView: UIView, ComponentCallBack,IReqDocCallback {
//
//    @IBOutlet weak var contentView: UIView!
//    @IBOutlet weak var captureContentView:UIView!
//    @IBOutlet weak var submitButton:UIButton!
//
//    let kCONTENT_XIB_NAME           =   "CustomCaptureView"
//
//    var delegate:ComponentCallBack?
//
//    var artifactList:[Artifacts] = []
//
//    var captureView = UIScrollView()
//
//    var contentOfScrollView = UIStackView()
//
//    var loggerWrapper = LoggerWrapper.sharedInstance
//
//    var loggerStartTime: Double = 0
//
//    private var dalCapture: DALCapture {
//        DALCapture.shared
//    }
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        commonInit()
//    }
//
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//        commonInit()
//    }
//    private func commonInit() {
//        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
//        contentView.fixInView(self)
//
//        setupScrollView()
//        setupContentofScrollView()
//
//        submitButton.backgroundColor = hexStringToUIColor(hex: "\(ThemeConfig.shared.getPrimaryMainColor())")
//        submitButton.setCornerBorder(color: .gray, cornerRadius: 10, borderWidth: 0)
//
//    }
//    func renderCustomCapture(delegate:ComponentCallBack) {
//        self.delegate = delegate
//        artifactList = dalCapture.getRequiredDocuments(callBack: self)
//        DispatchQueue.main.async { [self] in
//            var artifactset:[ArtifactsSets] = []
//            let imageView = ImageView()
//            for(_,value) in artifactList.enumerated() {
//                if value.getArtifactKey() == "poa.nil.nil.1.front" || value.getArtifactKey() == "poa.nil.nil.1.back" {
//                    if artifactset.isEmpty {
//                        artifactset = [ArtifactsSets(taskKey: value.getTaskKey(), taskType: value.getTaskType(), artifactKey: value.getArtifactKey(), side: value.getSide(), headerTitle:"", message: "")]
//                    } else {
//                        artifactset.append(ArtifactsSets(taskKey: value.getTaskKey(), taskType: value.getTaskType(), artifactKey: value.getArtifactKey(), side: value.getSide(), headerTitle:"", message: ""))
//                    }
//                    imageView.setImage(group:true,documentList: [], artifactSet: artifactset)
//                    contentOfScrollView.addArrangedSubview(imageView)
//                } else if value.getArtifactKey() == "poi.ind_pan.nil.1.front" {
//                    let imageView = ImageView()
//                    imageView.setImage(group:false,artifactSet: [ArtifactsSets(taskKey: value.getTaskKey(), taskType: value.getTaskType(), artifactKey: value.getArtifactKey(), side: value.getSide(), headerTitle:"", message: "")])
//                    contentOfScrollView.addArrangedSubview(imageView)
//                }
//
//            }
//        }
//    }
//    public func onReqDocSuccess(artifactsList:[Artifacts]) {
//
//    }
//    public func onReqDocFailure(message:String) {
//
//    }
//    public func didPressButton(_ tag: UIButton) {
//        delegate?.didPressButton(tag)
//    }
//    @IBAction func didTapButton(sender:UIButton) {
//        delegate?.didPressButton(sender)
//    }
//
//}
//extension CustomCaptureView {
//
//    private func setupScrollView() {
//
//        captureView.backgroundColor = .clear
//
//        captureContentView.addSubview(captureView)
//
//        captureView.translatesAutoresizingMaskIntoConstraints = false
//
//        captureView.leadingAnchor.constraint(equalTo: captureContentView.leadingAnchor).isActive = true
//        captureView.trailingAnchor.constraint(equalTo: captureContentView.trailingAnchor).isActive = true
//        captureView.topAnchor.constraint(equalTo: captureContentView.topAnchor).isActive = true
//        captureView.bottomAnchor.constraint(equalTo: captureContentView.bottomAnchor).isActive = true
//    }
//
//    private func setupContentofScrollView() {
//
//        contentOfScrollView.axis            = .vertical
//        contentOfScrollView.distribution    = .fill
//        contentOfScrollView.alignment       = .fill
//        contentOfScrollView.spacing         = 15
//
//        captureView.addSubview(contentOfScrollView)
//
//        contentOfScrollView.translatesAutoresizingMaskIntoConstraints = false
//        contentOfScrollView.leadingAnchor.constraint(equalTo: captureContentView.leadingAnchor).isActive = true
//        contentOfScrollView.trailingAnchor.constraint(equalTo: captureContentView.trailingAnchor).isActive = true
//        contentOfScrollView.topAnchor.constraint(equalTo: captureView.topAnchor).isActive = true
//        contentOfScrollView.bottomAnchor.constraint(equalTo: captureView.bottomAnchor).isActive = true
//    }
//}
