//
//  VideoComponentView.swift
//  app
//
//  Created by Admin on 10/11/21.
//

import Foundation
import UIKit
import ReplayKit
import AVKit
import AVFoundation
import core
import msadaptor
import WebRTC

public protocol VideoComponentCallBack {
    func onVCStateCallBack(event: String, object: NSObject)
    func onVCError(code: String, message: String, canReconnect: Bool)
}

public class VideoComponentView: UIView, AVCaptureVideoDataOutputSampleBufferDelegate, MsAdaptorCallBack, CameraCaptureDelegate {
    
    @IBOutlet var cameraView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet private weak var functionStackView: UIStackView!
    @IBOutlet private weak var microPhoneButton: UIButton!
    @IBOutlet private weak var videoButton: UIButton!
    @IBOutlet private weak var cameraButton: UIButton!
    @IBOutlet private weak var btnDisconnectCall: UIButton!
    @IBOutlet private weak var btnCantHear: UIButton!
    @IBOutlet private weak var btnCantSee: UIButton!
    @IBOutlet private weak var remoteView: UIView!
    @IBOutlet private weak var networkIndicator: UIView!
    @IBOutlet private weak var infoMessageView: UIView!
    @IBOutlet private weak var infoMessage: UITextField!
    @IBOutlet private weak var btnOptions: UIButton!
    @IBOutlet private weak var hideStackView: UIStackView!
    @IBOutlet private weak var statsTableView: UITableView!
    @IBOutlet private weak var messageAlertView: UIView!
    @IBOutlet private weak var faceDetectionOverlayView: UIView!
    @IBOutlet private weak var recordingTimerLabel:UILabel!
    @IBOutlet private weak var faceDetectionMessageLabel:UILabel!
    
    let kCONTENT_XIB_NAME           =   "VideoComponentView"
    
    var indicator = SignalStrengthIndicator()
    
    var infoView = InstructionView()

    private var msAdapter: MsAdapter {
        MsAdapter.shared
    }
    
    var loggerStartTime: Double = 0
    
    var loggerCapture,loggerMsAdapter: Logger?
    
    var cantHearTimer: Timer?
    
    var cantSeeTimer: Timer?

    weak var recordingTimer: Timer?
    
    var buttonPlaybackTime = 5

    var timeMin = 0
    var timeSec = 0
    
    let pinchGestureRecognizer = UIPinchGestureRecognizer()
    
    public var delegate: VideoComponentCallBack?
    
    let cameraManager = CameraManager()
    
    var statsViewList: [StatsViewData] = []
    
    var isPublishVideo: Bool = true

    var modelVcConfig = ModelVcConfig()
    
    var dalCapture = DALCapture()
    var loggerWrapper = LoggerWrapper()
    var AVERAGE_NETWORK_THRESHOLD   = 50
    var STRONG_NETWORK_THRESHOLD    = 100
    var AVERAGE_INCOMING_BITRATE    = 0
    var AVERAGE_OUTGOING_BITRATE    = 0
    
    public init(dalCapture:DALCapture,loggerWrapper:LoggerWrapper) {
        self.dalCapture = dalCapture
        self.loggerWrapper = loggerWrapper
        super.init(frame: .zero)
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
    }
   
    public func InitialiseCameraView(modelVcConfig: ModelVcConfig, view: UIView, delegate: VideoComponentCallBack) {
        self.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.delegate = delegate
        self.modelVcConfig = modelVcConfig
        initLogger()
        setupCamera()
        initStatsView()
        loadStrengthIndicator()
        addOverlay()
        
        infoMessageView.isHidden = true
        messageAlertView.isHidden = true
        faceDetectionOverlayView.isHidden = true
        
        pinchGestureRecognizer.addTarget(self, action: #selector(handler))
        cameraView.addGestureRecognizer(pinchGestureRecognizer)

        functionStackView.arrangedSubviews.compactMap({ $0 as? UIButton }).forEach({ $0.isEnabled = true })
        hideStackView.arrangedSubviews.compactMap({ $0 as? UIButton }).forEach({ $0.isEnabled = true })
    }
    func initLogger() {
        loggerMsAdapter = loggerWrapper.createLogger(key: EnumLogger.MSADAPTOR.rawValue, token: dalCapture.getToken(), loggerUrl: modelVcConfig.getVc_logger_url())
        loggerCapture = loggerWrapper.getLoggers(key: EnumLogger.CAPTURE.rawValue)
        if loggerCapture == nil {
            loggerCapture = loggerWrapper.createLogger(key: EnumLogger.CAPTURE.rawValue, token: dalCapture.getToken(), loggerUrl: modelVcConfig.getVc_logger_url())
        }
        loggerMsAdapter?.setDefaults(defaultLogDetails: ModalLogDetails(reference_id: dalCapture.getRequestId(), reference_type:dalCapture.getReferenceType() ))

    }
    public func setUpSocket(roomId: String, participantId: String, msSocketURL: String, purpose: String) {
        self.msAdapter.setUpSocket(url: modelVcConfig.getVc_socket_url(), roomId: roomId, participantId: participantId, msSocketURL: msSocketURL, bitRate: modelVcConfig.getBitrate(),dalCapture: self.dalCapture)
        self.msAdapter.delegate = self
        self.msAdapter.mediaPlayStarted = false
        if purpose != "" {
        showConnectingDialog(purpose: purpose)
        }
    }
    var isPublishStarted: Bool = false
    var isPlayStarted: Bool = false
    func hideShowUIComponents() {
        if isPlayStarted && isPublishStarted {
            callConnected()
        } else {
            DispatchQueue.main.async { [self] in
                if self.subviews.contains(infoView) {
                    self.infoView.removeFromSuperview() // Remove it
                }
            }
        }
    }
    public func onMSStateCallBack(event: String, object: NSObject) {
        
        switch event {
        case "publish_started":
            isPublishStarted = true
            hideShowUIComponents()
            self.delegate?.onVCStateCallBack(event: event, object: object)
            break
        case "play_started":
            isPlayStarted = true
            hideShowUIComponents()
            self.delegate?.onVCStateCallBack(event: event, object: object)
            break
        case "publish_finished":
            isPublishStarted = false
            self.delegate?.onVCStateCallBack(event: event, object: object)
            self.showConnectingDialog(purpose: "Reconnecting")
            break
        case "play_finished":
            isPlayStarted = false
            self.delegate?.onVCStateCallBack(event: event, object: object)
            self.showConnectingDialog(purpose: "Reconnecting")
            break
        case "left_room":
            DispatchQueue.main.async { [self] in
                if self.subviews.contains(infoView) {
                    self.infoView.removeFromSuperview() // Remove it
                }
            }
            self.delegate?.onVCStateCallBack(event: "USER_EXIT", object: object)
            break
        case "RECONNECTS_EXHAUSTED", "server_ended_room", "USER_EXIT", "operator_exit":
            DispatchQueue.main.async { [self] in
                if self.subviews.contains(infoView) {
                    self.infoView.removeFromSuperview() // Remove it
                }
            }
            self.delegate?.onVCStateCallBack(event: event, object: object)
            break
        case "start":
            self.delegate?.onVCStateCallBack(event: event, object: object)
            break
        case "webrtc_stats":
            self.updateNetworkStatsData(data: object)
            break
        case "WebRTCIceStateChanged":
            DispatchQueue.main.async {
                self.hideAllOptions()
                self.showConnectingDialog(purpose: "Reconnecting")
            }
            self.delegate?.onVCStateCallBack(event: event, object: object)
            break
        case "reconnecting", "socket_reconnecting":
//            self.showConnectingDialog(purpose: "Reconnecting")
            break
        default:
            break
        }
        
    }
    
    public func onMSError(code: String, message: String, canReconnect: Bool) {
        self.delegate?.onVCError(code: code, message: message, canReconnect: canReconnect)
    }
    public  func BackPressed() {
        self.window?.rootViewController?.showAlert(title: "Warning!", message: "Are you sure you want to end the video call?", actionTitle1: "Yes", actionTitle2: "No") { [self] _  in
            self.delegate?.onVCStateCallBack(event: "USER_EXIT", object: [:] as NSObject)
        }
    }
    public func addRemoteView(view: UIView) {
        let renderView = RTCMTLVideoView(frame: view.bounds)
        renderView.layer.cornerRadius = 4
        renderView.layer.masksToBounds = true
        view.addSubview(renderView)
        msAdapter.attachRemoteView(renderer: renderView)
    }
    public func showHideCameraView(state: Bool) {
        cameraView.isHidden = state
    }
    public func showHideStatsSignalView(state: Bool) {
        networkIndicator.isHidden = state
    }
    public func showHideCantSeeHearButton(state: Bool) {
            btnCantSee.isHidden = state
            btnCantHear.isHidden = state
    }
    public func showConnectingDialog(purpose: String) {
        hideAllOptions()
        DispatchQueue.main.async { [self] in
            if self.subviews.contains(infoView) {
                self.infoView.removeFromSuperview() // Remove it
            }
            self.infoView.loadConnectingView(message: purpose)
            self.addSubview(infoView)
        }
    }
    @objc func handler(gesture: UIPanGestureRecognizer) {
        let location = gesture.location(in: self)
        let draggedView = gesture.view
        draggedView?.center = location
        
        if gesture.state == .ended {
            if self.remoteView.frame.midX >= self.layer.frame.width / 2 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    self.remoteView.center.x = self.layer.frame.width - 100
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    self.remoteView.center.x = 100
                }, completion: nil)
            }
        }
    }
    
    public func callConnected() {
        DispatchQueue.main.async { [self] in
            if infoView.isDescendant(of: self) {
                infoView.removeFromSuperview()
            }
            self.showAllOptions()
            self.addRemoteView(view: remoteView)
        }
    }
    public func setupCamera() {
        
        loggerCapture?.logPageVisit(pageName: "VideoComponentLoad", pageComponent: "VideoComponent", source: "")
        loggerCapture?.logPageRender(loggerStartTime: loggerStartTime, component: "VideoComponent",referenceId: dalCapture.getRequestId() != "" ? dalCapture.getRequestId() : dalCapture.getCaptureId(),referenceType: dalCapture.getReferenceType(), meta: [:])
        
        cameraManager.shouldEnableExposure = true
        cameraManager.shouldFlipFrontCameraImage = false
        cameraManager.cameraDevice = .front
        cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: CameraOutputMode.videoWithMic)
        cameraManager.cameraOutputQuality = .medium
        cameraManager.delegate = self
        cameraManager.cameraOutputMode = .videoWithMic
        cameraManager.showErrorsToUsers = true
        cameraManager.startRecordingVideo()

        self.delegate?.onVCStateCallBack(event: "VC_STARTED", object: [:] as NSObject)
      
    }
    public func screenShot() -> UIImage? {
        return msAdapter.screenShot()
    }
    public func cleanUp() {
        cameraManager.stopCaptureSession()
        msAdapter.cleanUp()   
    }
    func captureVideoOutput(sampleBuffer: CMSampleBuffer) {
        if let _ = CMSampleBufferGetImageBuffer(sampleBuffer) {
            if CMSampleBufferGetNumSamples(sampleBuffer) != 1 || !CMSampleBufferIsValid(sampleBuffer) ||
                !CMSampleBufferDataIsReady(sampleBuffer) {
                return
            }
            DispatchQueue.main.async { [self] in
                let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
                let ciimage = CIImage(cvPixelBuffer: imageBuffer)
                if recordingTimer != nil {
                detectFaces(image: ciimage)
                }
                let image = core.convert(cmage: ciimage)
                msAdapter.renderVideo(bufferImage: image, buffers: sampleBuffer, status: self.isPublishVideo)
            }
        }
        
    }
    func scheduleBuffer(_ sampleBuffer: CMSampleBuffer?) -> AVAudioPCMBuffer? {

            var sDescr: CMFormatDescription? = nil
            if let sampleBuffer = sampleBuffer {
                sDescr = CMSampleBufferGetFormatDescription(sampleBuffer)
            }
            var numSamples: CMItemCount? = nil
            if let sampleBuffer = sampleBuffer {
                numSamples = CMSampleBufferGetNumSamples(sampleBuffer)
            }
        var avFmt: AVAudioFormat? = nil
                avFmt = AVAudioFormat(cmAudioFormatDescription: sDescr!)
            var pcmBuffer: AVAudioPCMBuffer? = nil

            if let avFmt = avFmt {
                pcmBuffer = AVAudioPCMBuffer(pcmFormat: avFmt, frameCapacity: AVAudioFrameCount(UInt(numSamples ?? 0)))
            }
            pcmBuffer?.frameLength = AVAudioFrameCount(UInt(numSamples ?? 0))
            if let sampleBuffer = sampleBuffer, let mutableAudioBufferList = pcmBuffer?.mutableAudioBufferList {
                CMSampleBufferCopyPCMDataIntoAudioBufferList(sampleBuffer, at: 0, frameCount: Int32(numSamples ?? 0), into: mutableAudioBufferList)
            }
            return pcmBuffer

        }
    @IBAction private func btnCantHearAction(_ sender: UIButton) {
        
        btnCantHear.backgroundColor = .systemOrange
       
        cantHearTimer = Timer.scheduledTimer(timeInterval: TimeInterval(buttonPlaybackTime), target: self, selector: #selector(cantHearTimeoutOccured(_:)), userInfo: 3, repeats: false)
        
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "VideoCallState", timestamp: "", event_type: "Clicked", event_name: "CantHear", component: "videoComponent", event_source: "btnCantHearAction", logger_session_id: "")
      
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        self.delegate?.onVCStateCallBack(event: "no_audio", object: [] as NSObject)
        self.messageAlertView.isHidden = false
    }
    @objc func cantHearTimeoutOccured(_ timer: Timer) {
        btnCantHear.backgroundColor = .clear
        cantHearTimer?.invalidate()
        cantHearTimer = nil
        if cantSeeTimer == nil {
        self.messageAlertView.isHidden = true
        }
    }
    @IBAction private func btnCantSeeAction(_ sender: UIButton) {
        btnCantSee.backgroundColor = .systemOrange

        cantSeeTimer = Timer.scheduledTimer(timeInterval: TimeInterval(buttonPlaybackTime), target: self, selector: #selector(cantSeeTimeoutOccured(_:)), userInfo: 3, repeats: false)
        
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "VideoCallState", timestamp: "", event_type: "Clicked", event_name: "CantSee", component: "videoComponent", event_source: "btnCantSeeAction", logger_session_id: "")
      
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        self.delegate?.onVCStateCallBack(event: "no_video", object: [] as NSObject)
        self.messageAlertView.isHidden = false
    }
    @objc func cantSeeTimeoutOccured(_ timer: Timer) {
        btnCantSee.backgroundColor = .clear
        cantSeeTimer?.invalidate()
        cantSeeTimer = nil
        if cantHearTimer == nil {
        self.messageAlertView.isHidden = true
        }
    }
    @IBAction private func switchCamera(_ sender: UIButton) {
        
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "VideoCallState", timestamp: "", event_type: "Clicked", event_name: "\(cameraManager.cameraDevice == CameraDevice.front ? CameraDevice.back : CameraDevice.front)", component: "videoComponent", event_source: "switchCamera", logger_session_id: "")
      
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
        cameraManager.cameraDevice = cameraManager.cameraDevice == CameraDevice.front ? CameraDevice.back : CameraDevice.front
        }
        self.delegate?.onVCStateCallBack(event: "camera_position", object: ["position": cameraManager.cameraPosition] as NSObject)
    }
    @IBAction private func micphoneAction(_ sender: UIButton) {
        
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "VideoCallState", timestamp: "", event_type: "Clicked", event_name: "\(sender.isSelected ? "Mute" : "Unmute")", component: "videoComponent", event_source: "micphoneAction", logger_session_id: "")
      
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        sender.isSelected.toggle()
        if sender.isSelected {
            msAdapter.muteUnMute(event: true)
            infoMessageView.isHidden = false
            infoMessage.text = "Your mic is off."
            microPhoneButton.backgroundColor = .red
            if videoButton.isSelected {
                infoMessage.text = "Your mic & video is off."
            }
        } else if videoButton.isSelected {
            infoMessage.text = "Your video is off."
            videoButton.backgroundColor = .red
            msAdapter.muteUnMute(event: false)
            microPhoneButton.backgroundColor = .darkGray
        } else {
            microPhoneButton.backgroundColor = .darkGray
            msAdapter.muteUnMute(event: false)
            infoMessageView.isHidden = true
        }
        
    }
    @IBAction private func speakerAction(_ sender: UIButton) {
        sender.isSelected.toggle()
        let image = sender.image(for: sender.isSelected ? .selected : .normal)?
            .withTintColor(.white)
            .withRenderingMode(.alwaysOriginal)
        
        if sender.isSelected {
            ProgressHUD.showSuccess("Speaker Off", image: image)
            msAdapter.speakerOff()
        } else {
            ProgressHUD.showSuccess("Speaker On", image: image)
            msAdapter.speakerOn()
        }
    }
    @IBAction private func videoAction(_ sender: UIButton) {
        sender.isSelected.toggle()
        
        let data = ModalLogDetails(service_category: "CaptureSDK", service: "VideoCallState", timestamp: "", event_type: "Clicked", event_name: "\(sender.isSelected ? "VideoOff" : "VideoOn")", component: "videoComponent", event_source: "videoAction", logger_session_id: "")
      
        let meta: [String: Any] = ["request_uid": dalCapture.getRequestId(), "av_session_id": dalCapture.getCaptureSessionId()]
        loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
        
        if sender.isSelected {
            cameraView.isHidden = true
            infoMessageView.isHidden = false
            microPhoneButton.isUserInteractionEnabled = false
            msAdapter.muteUnMute(event: true)
            infoMessage.text = "Your video is off."
            videoButton.backgroundColor = .red
            self.isPublishVideo = false
            if microPhoneButton.isSelected {
                infoMessage.text = "Your mic & video is off."
            }
        } else if microPhoneButton.isSelected {
            infoMessage.text = "Your mic  is off."
            microPhoneButton.backgroundColor = .red
            cameraView.isHidden = false
            microPhoneButton.isUserInteractionEnabled = true
            msAdapter.muteUnMute(event: true)
            self.isPublishVideo = true
            videoButton.backgroundColor = .darkGray
        } else {
            infoMessageView.isHidden = true
            cameraView.isHidden = false
            microPhoneButton.isUserInteractionEnabled = true
            msAdapter.muteUnMute(event: false)
            self.isPublishVideo = true
            videoButton.backgroundColor = .darkGray
        }
        
    }
    @IBAction private func disconnectCallAction(_ sender: UIButton) {
        self.window?.rootViewController?.showAlert(title: "Warning!", message: "Are you sure you want to end the video call?", actionTitle1: "Yes", actionTitle2: "No") { [self] _  in
            
            let data = ModalLogDetails(service_category: "CaptureSDK", service: "VideoCallState", timestamp: "", event_type: "Clicked", event_name: "CallEndConfirmation", component: "videoComponent", event_source: "disconnectCallAction", logger_session_id: "")
          
            let meta: [String: Any] = ["request_uid": msAdapter.reconnectingId, "av_session_id": ""]
            loggerCapture?.log(logLevel: LogLevel.shared.Info, logDetails: data, meta: meta)
            self.delegate?.onVCStateCallBack(event: "user_partially_exit", object: [] as NSObject)
            
            msAdapter.cleanUp()
        }
    }
    @IBAction private func hideAndShowButtonView(_ sender: UIButton) {
        
        UIStackView.transition(with: hideStackView, duration: 0.4,
                               options: .transitionFlipFromBottom,
                               animations: {
            if self.hideStackView.isHidden == true {
                self.hideStackView.isHidden = false
            } else {
                self.hideStackView.isHidden = true
            }
            
        })
        
    }
    
    public func hideAllOptions() {
        infoMessageView.isHidden = true
        self.remoteView.isHidden = true
        self.functionStackView.isHidden = true
        self.btnCantSee.isHidden = true
        self.btnCantHear.isHidden = true
        self.hideStackView.isHidden = true
        networkIndicator.isHidden = true
        
    }
    public func showAllOptions() {
        if self.subviews.contains(infoView) {
            self.infoView.removeFromSuperview() // Remove it
        }
        if microPhoneButton != nil {
            if self.microPhoneButton.isSelected == true {
                self.microPhoneButton.isSelected = false
                microPhoneButton.backgroundColor = .darkGray
                msAdapter.muteUnMute(event: false)
            }
        }
        if videoButton != nil {
            if self.videoButton.isSelected == true {
                self.videoButton.isSelected = false
                videoButton.backgroundColor = .darkGray
                self.cameraView.isHidden = false
                self.isPublishVideo = true
            }
        }
        self.infoMessageView.isHidden = true
        self.remoteView.isHidden = false
        self.functionStackView.isHidden = false
        self.btnCantSee.isHidden = false
        self.btnCantHear.isHidden = false
        networkIndicator.isHidden = true
        microPhoneButton.isUserInteractionEnabled = true
        btnCantSee.setCornerBorder(color: .white, cornerRadius: 10, borderWidth: 1)
        btnCantHear.setCornerBorder(color: .white, cornerRadius: 10, borderWidth: 1)
        
    }
    func loadStrengthIndicator() {
        indicator.frame = CGRect(x: 0, y: 0, width: self.networkIndicator.frame.width, height: self.networkIndicator.frame.height)
        self.networkIndicator.addSubview(indicator)
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: self.networkIndicator.frame.width, height: self.networkIndicator.frame.height)
        self.networkIndicator.addSubview(button)
        button.addTarget(self, action: #selector(showStats), for: .touchUpInside)
        networkIndicator.isHidden = true
    }
    func initStatsView() {
        infoView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.statsTableView.register(UINib(nibName: "StatisticsTableViewCell", bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: "StatisticsTableViewCell")
        self.statsTableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        self.statsTableView.separatorColor = .white
        statsTableView.backgroundColor = .clear
        statsTableView.tableFooterView = UIView()
        statsTableView.isUserInteractionEnabled = false
        let typeName = ["Average incoming BitRate (kbps)", "Average outgoing BitRate (kbps)", "Current incoming BitRate (kbps)", "Current outgoing BitRate (kbps)", "Current Time", "Current Bytes Sent", "Current Bytes Received", "Packets Lost", "Fraction Lost"]
        for value in typeName {
            if statsViewList.isEmpty {
                statsViewList = [StatsViewData(typeName: value, localUserData: "00", remoteUserData: "00")]
            } else {
                statsViewList.append(StatsViewData(typeName: value, localUserData: "00", remoteUserData: "00"))
            }
        }
        statsTableView.isHidden = true
    }
    @objc func showStats() {
        if statsTableView.isHidden == true {
            self.statsTableView.isHidden = false
        } else {
            self.statsTableView.isHidden = true
        }
    }
}
// OVerlay content for face detection
extension VideoComponentView {
    public func showFaceDetectionView() {
        self.faceDetectionOverlayView.isHidden = false
        self.recordingTimerLabel.isHidden = true
        initialMessageOnFaceDetection()
    }
    func initialMessageOnFaceDetection() {
        faceDetectionMessageLabel.setCornerBorder(color: .clear, cornerRadius: 10.0, borderWidth: 0.5)
        faceDetectionMessageLabel.backgroundColor = .systemYellow
        let image = (UIImage(systemName: "face.smiling.fill")?.withTintColor(.white))!
        faceDetectionMessageLabel.addLeading(image: image, text: "  One moment")
    }
    public func updateFaceDetectionMessage() {
        faceDetectionMessageLabel.backgroundColor = .systemGreen
        let image = (UIImage(systemName: "face.smiling.fill")?.withTintColor(.white))!
        faceDetectionMessageLabel.addLeading(image: image, text: "  Click start to continue the journey")
    }
    public func hideFaceDectionView() {
        self.faceDetectionOverlayView.isHidden = true
    }
    func addOverlay() {
        faceDetectionOverlayView.backgroundColor =  UIColor.black.withAlphaComponent(0.4)
        let maskLayer = CALayer()
        maskLayer.frame = faceDetectionOverlayView.bounds
        let circleLayer = CAShapeLayer()
        circleLayer.frame = CGRect(x:0 , y:0,width: faceDetectionOverlayView.frame.size.width,height: faceDetectionOverlayView.frame.size.height)
        let finalPath = UIBezierPath(roundedRect: CGRect(x:0 , y:0,width: faceDetectionOverlayView.frame.size.width,height: faceDetectionOverlayView.frame.size.height), cornerRadius: 0)
        let circlePath = UIBezierPath(ovalIn: CGRect(x:faceDetectionOverlayView.center.x - 150, y:faceDetectionOverlayView.center.y - 370, width: 300, height: faceDetectionOverlayView.frame.width))
        finalPath.append(circlePath.reversing())
        circleLayer.path = finalPath.cgPath
        circleLayer.borderColor = UIColor.white.withAlphaComponent(1).cgColor
        circleLayer.borderWidth = 1
        maskLayer.addSublayer(circleLayer)
        faceDetectionOverlayView.layer.mask = maskLayer
    }
    func detectFaces(image:CIImage) {
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
               let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
               let faces = faceDetector?.features(in: image, options: [CIDetectorSmile:true, CIDetectorEyeBlink: true])
                if !faces!.isEmpty {
                    if faces!.count > 1 {
                        faceDetectionMessageLabel.backgroundColor = .systemRed
                        let image = (UIImage(systemName: "face.smiling.fill")?.withTintColor(.white))!
                        faceDetectionMessageLabel.addLeading(image: image, text: "  Multiple faces present")
                    } else {
                        faceDetectionMessageLabel.backgroundColor = .systemGreen
                        let image = (UIImage(systemName: "face.smiling.fill")?.withTintColor(.white))!
                        faceDetectionMessageLabel.addLeading(image: image, text: "  Face visible")
                    }
                } else {
                    faceDetectionMessageLabel.backgroundColor = .systemRed
                    let image = (UIImage(systemName: "face.smiling.fill")?.withTintColor(.white))!
                    faceDetectionMessageLabel.addLeading(image: image, text: "  Face not Visible")
                }
        }
    public func startRecording() {

        self.recordingTimerLabel.isHidden = false
        self.startTimer()
    }
    public func stopRecording() {
        stopTimer()
        resetTimerAndLabel()
        DispatchQueue.main.async {
            self.recordingTimerLabel.isHidden = true
        }
    }
    fileprivate func startTimer(){
        let image = (UIImage(systemName: "record.circle.fill")?.withTintColor(.red))!
        recordingTimerLabel.addLeading(image: image, text: String(format: "%02d:%02d", timeMin, timeSec))
        stopTimer()
        recordingTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
            self?.timerTick()
        }
    }
    @objc fileprivate func timerTick(){
         timeSec += 1
         if timeSec == 60 {
             timeSec = 0
             timeMin += 1
         }
        DispatchQueue.main.async { [self] in
            let image = (UIImage(systemName: "record.circle.fill")?.withTintColor(.red))!
            self.recordingTimerLabel.addLeading(image: image, text: String(format: "%02d:%02d", timeMin, self.timeSec))
        }
    }
    @objc fileprivate func resetTimerToZero(){
         timeSec = 0
         timeMin = 0
         stopTimer()
    }
    @objc fileprivate func resetTimerAndLabel(){
        resetTimerToZero()
        DispatchQueue.main.async { [self] in
            let image = (UIImage(systemName: "record.circle.fill")?.withTintColor(.red))!
            recordingTimerLabel.addLeading(image: image, text: String(format: "%02d:%02d", timeMin, timeSec))
        }

    }
    @objc fileprivate func stopTimer(){
         recordingTimer?.invalidate()
    }

}
extension VideoComponentView {
    func updateNetworkStatsData(data: NSObject) {
        DispatchQueue.main.async { [self] in
            if data.value(forKey: "publish") as! Bool == true {
                statsViewList[0].localUserData = "\(data.value(forKey: "averageIncomingBitrate") ?? 0)"
                statsViewList[1].localUserData = "\(data.value(forKey: "averageOutgoingBitrate") ?? 0)"
                statsViewList[2].localUserData = "\(data.value(forKey: "currentIncomingBitrate") ?? 0)"
                statsViewList[3].localUserData = "\(data.value(forKey: "currentOutgoingBitrate") ?? 0)"
                statsViewList[4].localUserData = "\(getCurrentShortTimewithAP())"
                statsViewList[7].localUserData = "\(data.value(forKey: "packetsLost") ?? 0)"
                statsViewList[8].localUserData = "\(data.value(forKey: "fractionLost") ?? 0)"
            } else {
                statsViewList[0].remoteUserData = "\(data.value(forKey: "averageIncomingBitrate") ?? 0)"
                statsViewList[1].remoteUserData = "\(data.value(forKey: "averageOutgoingBitrate") ?? 0)"
                statsViewList[2].remoteUserData = "\(data.value(forKey: "currentIncomingBitrate") ?? 0)"
                statsViewList[3].remoteUserData = "\(data.value(forKey: "currentOutgoingBitrate") ?? 0)"
                statsViewList[4].remoteUserData = "\(getCurrentShortTimewithAP())"
                statsViewList[7].remoteUserData = "\(data.value(forKey: "packetsLost") ?? 0)"
                statsViewList[8].remoteUserData = "\(data.value(forKey: "fractionLost") ?? 0)"
            }
            AVERAGE_INCOMING_BITRATE = data.value(forKey: "averageIncomingBitrate") as! Int
            AVERAGE_OUTGOING_BITRATE = data.value(forKey: "averageOutgoingBitrate") as! Int
            
            indicator.backgroundColor = .clear
            if  AVERAGE_INCOMING_BITRATE <= AVERAGE_NETWORK_THRESHOLD && AVERAGE_OUTGOING_BITRATE <= AVERAGE_NETWORK_THRESHOLD {
                indicator.level = .low
                indicator.color = .yellow
                indicator.spacing = 5
                //            data.setValue("amber", forKey: "ragStatus")
            } else if AVERAGE_INCOMING_BITRATE > AVERAGE_NETWORK_THRESHOLD && AVERAGE_INCOMING_BITRATE <= STRONG_NETWORK_THRESHOLD || AVERAGE_OUTGOING_BITRATE > AVERAGE_NETWORK_THRESHOLD && AVERAGE_OUTGOING_BITRATE <= STRONG_NETWORK_THRESHOLD {
                indicator.level = .good
                indicator.color = .red
                indicator.spacing = 5
                //            data.setValue("red", forKey: "ragStatus")
            } else if AVERAGE_INCOMING_BITRATE > STRONG_NETWORK_THRESHOLD || AVERAGE_OUTGOING_BITRATE > STRONG_NETWORK_THRESHOLD {
                indicator.level = .excellent
                indicator.color = .green
                indicator.spacing = 5
                //            data.setValue("green", forKey: "ragStatus")
            }
            self.delegate?.onVCStateCallBack(event: "webrtc_stats", object: data)
            if self.subviews.contains(infoView) {
                networkIndicator.isHidden = true
            } else {
                networkIndicator.isHidden = false
            }
            statsTableView.reloadData()
        }
    }
}
extension VideoComponentView: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statsViewList.count + 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "StatisticsTableViewCell") as? StatisticsTableViewCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        if indexPath.row == 0 {
            cell.initCell(title: "Stats", local: "User Local", remote: "User Remote")
        } else {
            cell.initCell(title: "\(statsViewList[indexPath.row - 1].typeName ?? "")", local: "\(statsViewList[indexPath.row - 1].localUserData ?? "")", remote: "\(statsViewList[indexPath.row - 1].remoteUserData ?? "")")
        }
        return cell
        
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.contentView.frame.height / 13 
    }
}
public struct StatsViewData: Codable {
    var typeName: String?
    var localUserData: String?
    var remoteUserData: String?
}
extension UILabel {

    func addTrailing(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image

        let attachmentString = NSAttributedString(attachment: attachment)
        let string = NSMutableAttributedString(string: text, attributes: [:])

        string.append(attachmentString)
        self.attributedText = string
    }

    func addLeading(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image

        let attachmentString = NSAttributedString(attachment: attachment)
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentString)

        let string = NSMutableAttributedString(string: text, attributes: [:])
        mutableAttributedString.append(string)
        self.attributedText = mutableAttributedString
    }
}
@IBDesignable class PaddingLabel: UILabel {

    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }

    override var bounds: CGRect {
        didSet {
            // ensures this works within stack views if multi-line
            preferredMaxLayoutWidth = bounds.width - (leftInset + rightInset)
        }
    }
}
