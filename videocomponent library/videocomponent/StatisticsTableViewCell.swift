//
//  StatisticsTableViewCell.swift
//  app
//
//  Created by Admin on 27/09/21.
//

import UIKit

class StatisticsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var statsLabel: UILabel!
    @IBOutlet weak var localLabel: UILabel!
    @IBOutlet weak var remoteLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell(title: String, local: String, remote: String) {
        statsLabel.textColor = .white
        localLabel.textColor = .white
        remoteLabel.textColor = .white
        statsLabel.text = title
        localLabel.text = local
        remoteLabel.text = remote
        
    }
    
}
