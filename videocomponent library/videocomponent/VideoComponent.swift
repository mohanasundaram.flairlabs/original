//
//  vcwrapper.swift
//  captureapp
//
//  Created by Admin on 31/03/22.
//

import Foundation
import core
import Cleanse
public class VideoComponent {

    var dalCapture: DALCapture
    var loggerWrapper:LoggerWrapper

    public init(dalCapture:DALCapture,loggerWrapper:LoggerWrapper) {
        self.dalCapture = dalCapture
        self.loggerWrapper = loggerWrapper
    }
    public func startVcView() -> VideoComponentView {
        let vc = VideoComponentView(dalCapture: dalCapture,loggerWrapper: loggerWrapper)
        return vc
    }
}


