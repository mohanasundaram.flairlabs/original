//
//  BitRateTableViewCell.swift
//  app
//
//  Created by Admin on 08/09/21.
//

import UIKit

class BitRateTableViewCell: UITableViewCell {

    @IBOutlet weak var bitRateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
