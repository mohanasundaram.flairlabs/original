//
//  VCComponentFactory.swift
//  Hostapp
//
//  Created by Admin on 30/03/22.
//

import Foundation
import Cleanse
import core


public class VCComponentFactory {

    private static var privateSharedInstance: VCComponentFactory?

    public static var vcComponent:VcComponent.Root?

    public static var sharedInstance: VCComponentFactory = {

        if privateSharedInstance    == nil {
            privateSharedInstance = VCComponentFactory()
        }

        return privateSharedInstance!
    }()

    public static func getVcComponent(dalComponent:DalComponent.Root) -> VcComponent.Root {
        if vcComponent == nil {
            vcComponent = try! ComponentFactory.of(VcComponent.self).build((dalComponent))
        }
        return vcComponent!
    }
}
