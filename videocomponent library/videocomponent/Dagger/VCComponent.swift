//
//  VCComponent.swift
//  Hostapp
//
//  Created by Admin on 30/03/22.
//


import Foundation
import Cleanse
import core

public struct VCStructure {

    public let videoComponent: VideoComponent
    public func getVideoComponent() -> VideoComponent {
        return videoComponent
    }
}
public struct VcComponent : Cleanse.RootComponent {
    public typealias Root = VCStructure
    public typealias Seed = DalComponent.Root

    public static func configure(binder: UnscopedBinder) {
        binder.install(dependency: DalComponent.self)
    }

    public static func configureRoot(binder bind: ReceiptBinder<Root>) -> BindingReceipt<Root> {
        return bind.to { (dc: Seed) in
            Root.init(videoComponent: VideoComponent(dalCapture: dc.getDalCapture(), loggerWrapper: dc.getLoggerWrapper()))
        }
    }
}
