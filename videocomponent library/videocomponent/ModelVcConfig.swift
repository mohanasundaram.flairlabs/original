//
//  ModelVcConfig.swift
//  videocomponent
//
//  Created by Admin on 27/01/22.
//

import Foundation

public class ModelVcConfig {

    private var mediaSocketUrl:String = ""
    private var vc_logger_url:String = ""
    private var frame_width:Int = 0
    private var frame_height:Int = 0
    private var frame_per_sec:Int = 0
    private  var bitrate:Int = 0
    private var enableVideoOverlay:Bool = false
    private var latitude:String = ""
    private var longitude:String = ""

    // UI controll components
    private var showCantSee:Bool = false
    private var showCantHear:Bool = false

    private var showSignalStrenghth:Bool = false
    private var showRemoteView:Bool = false
    private var showLocalView:Bool = true

    private var showOptionBtn:Bool = false
    private var showMuteBtn:Bool = false
    private var showPubUnPubBtn:Bool = false

    private var showSwitchBtn:Bool = false
    private var showCallDiscBtn:Bool = false
    private var faceOverLay:Bool = false



    public init() {}
    private init(vcBuilder: VCBuilder) {
        self.mediaSocketUrl = vcBuilder.mediaSocketUrl
        self.vc_logger_url = vcBuilder.vc_logger_url
        self.frame_width = vcBuilder.frame_width
        self.frame_height = vcBuilder.frame_height
        self.frame_per_sec = vcBuilder.frame_per_sec
    }
    public func getVc_socket_url() -> String {
        return mediaSocketUrl
    }
    public func getVc_logger_url() -> String {
        return vc_logger_url
    }
    public func getFrame_height() -> Int {
        return frame_height
    }
    public func getFrame_width() -> Int {
        return frame_width
    }
    public func getFrame_per_sec() -> Int {
        return frame_per_sec
    }
    public func getLatitude() -> String {
        return latitude
    }
    public func getLongitude() -> String {
        return longitude
    }
    public func getBitrate() -> Int {
        return bitrate
    }
    public func isEnableVideoOverlay() -> Bool {
        return enableVideoOverlay;
    }

    public func setBitrate(bitrate:Int) {
        self.bitrate = bitrate
    }

    public func setEnableVideoOverlay(enableVideoOverlay:Bool) {
        self.enableVideoOverlay = enableVideoOverlay
    }

    public func setLatitude(latitude:String) {
        self.latitude = latitude
    }

    public func setLongitude(longitude:String) {
        self.longitude = longitude
    }

    // ******************* option Button ********************
    public func isShowCantSee() -> Bool {
        return showCantSee
    }

    func setShowCantSee(showCantSee:Bool) {
        self.showCantSee = showCantSee;
    }

    public func isShowCantHear() -> Bool {
        return showCantHear
    }

    public func setShowCantHear(showCantHear:Bool) {
        self.showCantHear = showCantHear;
    }

    public func isShowSignalStrenghth() -> Bool {
        return showSignalStrenghth
    }

    public func setShowSignalStrenghth(showSignalStrenghth:Bool) {
        self.showSignalStrenghth = showSignalStrenghth
    }

    public func isShowRemoteView() -> Bool {
        return showRemoteView
    }

    public func setShowRemoteView(showRemoteView:Bool) {
        self.showRemoteView = showRemoteView
    }

    public func isShowLocalView() -> Bool {
        return showLocalView
    }

    public func setShowLocalView(showLocalView:Bool) {
        self.showLocalView = showLocalView
    }

    public func isShowOptionBtn() -> Bool {
        return showOptionBtn
    }

    public func setShowOptionBtn(showOptionBtn:Bool) {
        self.showOptionBtn = showOptionBtn
    }

    public func isShowMuteBtn() -> Bool {
        return showMuteBtn
    }

    public func setShowMuteBtn(showMuteBtn:Bool) {
        self.showMuteBtn = showMuteBtn
    }

    public func isShowPubUnPubBtn() -> Bool {
        return showPubUnPubBtn
    }

    public func setShowPubUnPubBtn(showPubUnPubBtn:Bool) {
        self.showPubUnPubBtn = showPubUnPubBtn
    }

    public func isShowSwitchBtn() -> Bool {
        return showSwitchBtn
    }

    public func setShowSwitchBtn(showSwitchBtn:Bool) {
        self.showSwitchBtn = showSwitchBtn
    }

    public func isShowCallDiscBtn() -> Bool {
        return showCallDiscBtn
    }

    public func setShowCallDiscBtn(showCallDiscBtn:Bool) {
        self.showCallDiscBtn = showCallDiscBtn
    }

    public func isFaceOverLay() -> Bool {
        return faceOverLay
    }

    public func setFaceOverLay(faceOverLay:Bool) {
        self.faceOverLay = faceOverLay
    }
    public class VCBuilder {

        private(set) var mediaSocketUrl: String = "/websocket?"
        private(set) var vc_logger_url: String = ""
        private(set) var frame_width: Int = 480
        private(set) var frame_height: Int = 640
        private(set) var frame_per_sec: Int = 10

        public init() {}
        public func mediaSocketUrl(mediaSocketUrl: String) -> VCBuilder {
            self.mediaSocketUrl = mediaSocketUrl + self.mediaSocketUrl
            return self
        }
        public func vc_logger_url(vc_logger_url: String) -> VCBuilder {
            self.vc_logger_url = vc_logger_url
            return self
        }
        public func frame_width(frame_width: Int) -> VCBuilder {
            if frame_width > 0 {
                self.frame_width = frame_width
            }
            return self
        }
        public func frame_height(frame_height: Int) -> VCBuilder {
            if frame_height > 0 {
                self.frame_height = frame_height
            }
            return self
        }
        public func frame_per_sec(frame_per_sec: Int) -> VCBuilder {
            if frame_per_sec > 0 {
                self.frame_per_sec = frame_per_sec
            }
            return self
        }
        public func build() -> ModelVcConfig {
            return ModelVcConfig(vcBuilder: self)
        }
    }
}
