//
//  JanusRoomManager.swift
//  app
//
//  Created by Admin on 02/09/21.
//

import UIKit
import WebRTC
import core
public protocol ProviderDelegate {
    func signalingStateChangeNote(state: SignalingConnectionState)
}
public protocol JanusStateCallback {
    func onReconnectCall(type: String, roomId: Int, reason: String)
    func stateChangeCallback(event: String, object: NSObject)
    func onCleanUpCompleted()
}
public enum JanusRoomState {
    case `default`, joined, publishingSubscribing, subscribingOnly, left
    
    var isStreaming: Bool {
        self == .publishingSubscribing || self == .default
    }
}

/// Public Notifications
/// 当前仅同时处理一个房间, 切换房间请先调用 `JanusRoomManager.reset()`,  并更改房间号
/// Currently Support Single Room at the same time
public final class JanusProvider: JanusRTCInterface {
    
    public static let shared = JanusProvider()
    
    var loggerStartTime: Double = 0
    
    var loggerMsAdapter: Logger?
    
    /// Current Room Number, default: 1234 房间号, 默认1234
    var roomId: String = ""
    
    var reconnectingRoomId: Int = 0
    
    var streamId: String = ""
    
    var url: String = ""
    
    var isRemoteStreamReceived: Bool = false
    
    var delegate: ProviderDelegate?
    
    var isIceConnected: String = "notconnected"
//    var url: URL = URL(string: "ws://localhost:8188")!
    /// Local Publisher Display Name 作为发布者在房间中显示的名称
    var roomDisplayName: String = UIDevice.current.name
    /// Session ID
    private(set) var sessionID: Int64 = 0
    /// Handle ID
    private(set) var handleID: Int64 = 0

    var currentRoom: JanusJoinedRoom?

    var connections: [JanusConnection] = []
    
    public var remoteConnection: JanusConnection?
    /// WebSocket Signaling Client
    /// Handle Send & Process Jannus Messages
    var signalingClient: WebSocketChannel!
    
    var roomState: JanusRoomState = .default
    
    var iceServers: [RTCIceServer] = []
    
    public var janusDelegate: JanusStateCallback?
    
    private var iceServerArray: NSArray = []
    
    var dalCapture = DALCapture()

    var isBroadcasting: Bool = false {
        didSet {
            if isBroadcasting {
                roomDisplayName = UIDevice.current.name + "-Screen"
            }
        }
    }

    private(set) var isJoinedAsPublisher: Bool = false
    
    private init() {
//        loggerMsAdapter = LoggerWrapper.sharedInstance.getLoggers(key: "msAdaptor")
        return }

    /// Reset, Call this func after leaving a room
    public func JanusProviderInit(iceServerArray: NSArray, janusStateCallback: JanusStateCallback) {
        self.janusDelegate = janusStateCallback
        self.iceServerArray = iceServerArray
        
        for value in iceServerArray {
            let urls = (value as AnyObject).value(forKey: "urls") as! NSArray
            if (value as AnyObject).value(forKey: "credential") != nil && (value as AnyObject).value(forKey: "username") != nil {
                iceServers.append(RTCIceServer(urlStrings: (urls as NSObject) as! [String], username: ((value as AnyObject).value(forKey: "username") as? String)!, credential: ((value as AnyObject).value(forKey: "credential") as? String)!))
            } else {
                iceServers.append(RTCIceServer(urlStrings: (urls as NSObject) as! [String]))
            }
        }
    }
    public func StartConnection(url: String, roomId: String, reconnectingRoomId: Int, streamId: String,bitRate:Int) {
        
        self.roomId = roomId
        self.url = url
        self.reconnectingRoomId = reconnectingRoomId
        self.streamId = streamId
        self.isRemoteStreamReceived = false
        
        signalingClient = WebSocketChannel(url: URL(string: "\(url)")!, logger: loggerMsAdapter,bitRate: bitRate)
        signalingClient.delegate = self
        
        self.connect()
        DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
            self.signalingClient.createRoomSession(room: roomId)
        }
    }
    public func reset() {
        // reset timer in play & publish related timers.
        connections.forEach({ $0.rtcClient?.destory() })
        connections.removeAll()
        sessionID = 0
        handleID = 0
        currentRoom = nil
        
        roomState = .default
        isJoinedAsPublisher = false
    }
}

/// Connection Handling
extension JanusProvider {
    
    public func connection(for handleID: Int64) -> JanusConnection? {
        connections.first(where: { $0.handleID == handleID })
    }
    
    public func connection(for rtcClient: PeerConnection) -> JanusConnection? {
        connections.first(where: { $0.rtcClient == rtcClient })
    }
    
    public var localConnection: JanusConnection? {
        connection(for: handleID)
    }
    
    public func removeLocalConnection() {
        connections.removeAll(where: { $0.handleID == handleID })
    }
    
    public func onPublisherJoined(handleId: Double) {
        
    }
    
    public func onPublisherRemoteJsep(handleId: Double, jsep: NSObject) {
        
    }
    
    public func subscriberHandleRemoteJsep(handleId: Double, jsep: NSObject) {
        
    }
    
    public func onLeaving(handleId: Double) {
        
    }
    
    public func onVideoPublish(isVideoPublish: Bool) {
        
        if isVideoPublish {
            janusDelegate?.stateChangeCallback(event: "publish_started", object: ["msRoomId": roomId] as NSObject)
        } else {
            janusDelegate?.stateChangeCallback(event: "publish_finished", object: [] as NSObject)
        }
    }
    
    public func onReconnectCall(type: String, roomId: Int, reason: String) {
        janusDelegate?.onReconnectCall(type: type, roomId: roomId, reason: reason)
    }
    
    public func onSubscriberAttached() {
    
    }
}

/// Signaling Control
extension JanusProvider {
    /// connect to the singaling server
    public func connect() {
        if signalingClient.isConnected {
            return
        }
        signalingClient.connectionDelegate = self
        signalingClient.responseHandler = self
        signalingClient.connect()
    }
    
    /// disconnect
    public func disconnect() {
        if signalingClient != nil {
        signalingClient.disconnect()
        }
    }
    
    /// Join a room with specific room number
    public func joinRoom(room: String) {
        self.roomId = room
        print("Janus Room ID: \(self.roomId)")
        signalingClient.createRoomSession(room: room)
    }
    
    /// leave & destroy current room
    public func leaveCurrentRoom() {
        if signalingClient != nil {
        signalingClient.leaveRoom()
        }
    }
    
    public func publish() {
        /// Create Local Connection
        createLocalJanusConnection()
        
        if !isJoinedAsPublisher {
            /// Join Room As Publisher
            signalingClient.joinRoomAsPublisher(id: sessionID, handleID: handleID)
        } else {
            /// Publish Local Media
            publishLocalMediaStream()
        }
    }
    
    public func unpubish() {
        signalingClient.unpublish()
    }
}
 
/// WebSocketSignalingClientDelegate
extension JanusProvider: SignalingClientConnectionDelegate {
    
    public func signalingClient(didChangeState state: SignalingConnectionState) {
        self.delegate?.signalingStateChangeNote(state: state)
    }
}

/// JanusResponseHandler
extension JanusProvider: JanusResponseHandler {
    
    public func janusHandler(receivedError reason: String) {
        print("Received Janus Response Error: \(reason)")
    }
    
    public func janusHandler(didCreateSession sessionID: Int64) {
        self.sessionID = sessionID
    }
    
    public func janusHandler(received remoteSdp: RTCSessionDescription, handleID: Int64) {
        print("Received remote sdp")
        let connection_ = connection(for: handleID)
        let rtcClient = connection_?.rtcClient
        self.remoteConnection = connection_
        rtcClient?.set(remoteSdp: remoteSdp) { [weak self] (_) in
            guard let self = self else { return }
            
            if handleID == self.handleID {
                /// Local
                DispatchQueue.main.async {
                    self.isJoinedAsPublisher = true
                }
            } else {
                /// Others
                rtcClient?.answer { [weak self] (sdp) in
                    guard let self = self else { return }
                    self.signalingClient.sendAnswer(sdp: sdp.sdp, handleID: handleID)
                }
            }
        }
    }
    
    public func janusHandler(received candidate: RTCIceCandidate) {
        print("Received remote candidate")
    }
    
    public func janusHandler(fetched handleID: Int64) {
        /// Save local handleID
        self.handleID = handleID
        /// Join Room As Publisher
        signalingClient.joinRoomAsPublisher(id: sessionID, handleID: handleID)
        /// Local Connection
        createLocalJanusConnection()
    }
    
    public func janusHandler(joinedRoom handleID: Int64) {
        if handleID == self.handleID {
            /// Joined as publisher
            janusDelegate?.stateChangeCallback(event: "start", object: [] as NSObject)
            if roomState == .default {
                publishLocalMediaStream()
            } else if roomState == .subscribingOnly {
                roomState = .publishingSubscribing
                publishLocalMediaStream()
            }
        }
    }
    
    public func janusHandler(leftRoom handleID: Int64, reason: String?) {
        
        let targetConnection = connection(for: handleID)
        /// Update Publishers
        let removedPublisherID = targetConnection?.publisher.id
        currentRoom?.publishers.removeAll(where: { $0.id == removedPublisherID })
        
        connections.removeAll(where: { $0.handleID == handleID })
    }
    
    public func janusHandler(didAttach publisher: JanusPublisher, handleID: Int64) {
     
        let rtcClient = PeerConnection(iceServers: iceServers, id: "\(handleID)")
        rtcClient.delegate = self
        let connection = JanusConnection(handleID: handleID, publisher: publisher, rtcPeerConnection: rtcClient.peerConnection)
        connection.rtcClient = rtcClient
        connections.append(connection)
    }
    
    public func janusHandlerDidLeaveRoom() {
        roomState = .left
        reset()
    }
    
    /// Send Offer to Remote
    public func publishLocalMediaStream() {
        localConnection?.rtcClient?.offer(completion: { [weak self] (sdp) in
            guard let self = self else { return }
            self.signalingClient.sendOffer(sdp: sdp.sdp, isConfiguration: true)
        })
    }
    
    /// Create Local Connection Object to Share Screen or Camera
    public func createLocalJanusConnection() {
        if localConnection != nil {
            return
        }
        let rtcClient = PeerConnection(iceServers: iceServers, id: "\(handleID)", delegate: self)
        let localPublisher = JanusPublisher(id: String(sessionID), display: roomDisplayName)
        let localConnection = JanusConnection(handleID: handleID, publisher: localPublisher, rtcPeerConnection: rtcClient.peerConnection)
        localConnection.rtcClient = rtcClient
        connections.insert(localConnection, at: 0)
    }
    public func createFreshConnectionwithExistingData() {
        
    }
    public func getHandleId() -> Int64 {
        return remoteConnection?.handleID ?? 0
    }
    public func attachRemoteView(renderer: RTCVideoRenderer) {
        if remoteConnection?.handleID != self.handleID {
            remoteConnection?.rtcClient?.renderRemoteVideo(to: renderer)
        }
    }
    public func didCaptureLocalFrame(_ videoFrame: RTCVideoFrame) {
        localConnection?.rtcClient?.didCaptureLocalFrame(videoFrame)
    }
    public func muteAudio() {
        localConnection?.rtcClient?.muteAudio()
    }
    public func unmuteAudio() {
        localConnection?.rtcClient?.unmuteAudio()
    }
    public func speakerOn() {
        localConnection?.rtcClient?.speakerOn()
    }
    public func speakerOff() {
        localConnection?.rtcClient?.speakerOff()
    }
    public func hideVideo() {
        localConnection?.rtcClient?.hideVideo()
    }
    public func showVideo() {
        localConnection?.rtcClient?.showVideo()
    }
}

extension JanusProvider: WebRTCClientDelegate {
    public func peerConnection(event: String, obj: NSObject) {
        if event == "remote_stream_received" {
            isRemoteStreamReceived = true
        }
        janusDelegate?.stateChangeCallback(event: event, object: obj)
    }
    
    public func peerConnection(_ client: PeerConnection, didDiscoverLocalCandidate candidate: RTCIceCandidate) {
        print("Discovered local candidate")
        guard let handleID = connection(for: client)?.handleID else { return }
        signalingClient.send(candidate: candidate, handleID: handleID)
    }
    
    public func peerConnection(_ client: PeerConnection, didChangeConnectionState state: RTCIceConnectionState) {
        print("Connection state did change: \(state.description)")
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "WebRTCIceConnection", timestamp: "", event_type: "StateChange", event_name: "\(state.description)", component: "", event_source: "peerConnection", logger_session_id: "")
        loggerMsAdapter?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
        janusDelegate?.stateChangeCallback(event: "WebRTCIceStateChanged", object: ["state": state.description] as NSObject)
    }
    
    public func peerConnection(_ client: PeerConnection, didAdd stream: RTCMediaStream) {
        
    }
    
//    public func peerConnection(_ client: PeerConnection, didCreate externalSampleCapturer: ScreenSampleCapturer?) {
//        NotificationCenter.default.post(name: JanusRoomManager.externalSampleCapturerDidCreateNote, object: externalSampleCapturer)
//    }
}
