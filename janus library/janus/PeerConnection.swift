//
//  WebRTCClient.swift
//  app
//
//  Created by Admin on 02/09/21.
//

import UIKit
import WebRTC
import ReplayKit
import core

public protocol WebRTCClientDelegate: AnyObject {
    func peerConnection(_ client: PeerConnection, didDiscoverLocalCandidate candidate: RTCIceCandidate)
    func peerConnection(_ client: PeerConnection, didChangeConnectionState state: RTCIceConnectionState)
    func peerConnection(_ client: PeerConnection, didAdd stream: RTCMediaStream)
    func peerConnection(event: String, obj: NSObject)
//    func peerConnection(_ client: PeerConnection, didCreate externalSampleCapturer: ScreenSampleCapturer?)
}

extension WebRTCClientDelegate {
//    func peerConnection(_ client: PeerConnection, didCreate externalSampleCapturer: ScreenSampleCapturer?) {}
}

public final class PeerConnection: NSObject {
    
    // The `RTCPeerConnectionFactory` is in charge of creating new RTCPeerConnection instances.
    // A new RTCPeerConnection should be created every new call, but the factory is shared.
    private static let factory: RTCPeerConnectionFactory = {
        RTCInitializeSSL()
        let videoEncoderFactory = RTCDefaultVideoEncoderFactory()
        let videoDecoderFactory = RTCDefaultVideoDecoderFactory()
        return RTCPeerConnectionFactory(encoderFactory: videoEncoderFactory, decoderFactory: videoDecoderFactory)
    }()
    var identifier: String
    weak var delegate: WebRTCClientDelegate?
    
    public var remoteVideoTrack: RTCVideoTrack?
    public var localVideoTrack: RTCVideoTrack?
    public var localAudioTrack: RTCAudioTrack?
    public var remoteAudioTrack: RTCAudioTrack?
    
    var loggerStartTime: Double = 0
    
    var loggerMsAdapter: Logger?
    
    public var playStartTimer: Timer?
    public var playResumeTimer: Timer?
    
//    public var isPlayStarted:Bool = false
    
    private var isCameraCaputuring: Bool = false
    
    var isPublish: Bool = false
    
    var videoCapturer: RTCVideoCapturer?
    private var videoFrame: RTCVideoFrame?
    
    private var localVideoSource: RTCVideoSource?
    
    let peerConnection: RTCPeerConnection
    private let rtcAudioSession =  RTCAudioSession.sharedInstance()
    private let audioQueue = DispatchQueue(label: "audio")
    private let mediaConstrains = [kRTCMediaConstraintsOfferToReceiveAudio: kRTCMediaConstraintsValueTrue,
                                   kRTCMediaConstraintsOfferToReceiveVideo: kRTCMediaConstraintsValueTrue]
    
    public var roomManager: JanusProvider {
        JanusProvider.shared
    }
    @available(*, unavailable)
    override init() {
//        loggerMsAdapter = LoggerWrapper.sharedInstance.getLoggers(key: "msAdaptor")
        fatalError("WebRTCClient:init is unavailable")
    }
    
    deinit {
        print("WebRTCClient is deinit...")
    }
    
    required init(iceServers: [RTCIceServer], id: String, delegate: WebRTCClientDelegate? = nil) {
        let config = RTCConfiguration()
       
        config.iceServers = iceServers
       
        // Unified plan is more superior than planB
        config.sdpSemantics = .unifiedPlan
        
        // gatherContinually will let WebRTC to listen to any network changes and send any new candidates to the other client
        config.continualGatheringPolicy = .gatherContinually
        
        // Define media constraints. DtlsSrtpKeyAgreement is required to be true to be able to connect with web browsers.
        let constraints = RTCMediaConstraints(mandatoryConstraints: nil,
                                              optionalConstraints: ["DtlsSrtpKeyAgreement": kRTCMediaConstraintsValueTrue])
        peerConnection = PeerConnection.factory.peerConnection(with: config, constraints: constraints, delegate: nil)!
        
        identifier = id
        self.delegate = delegate
        
        super.init()
        
//        if roomManager.peerConnections.isEmpty {
//            roomManager.peerConnections = [JanusPeerConnection(handleID: id, pc: peerConnection)]
//        } else {
//            roomManager.peerConnections.append(JanusPeerConnection(handleID: id, pc: peerConnection))
//        }
        createMediaSenders()
        configureAudioSession()
        peerConnection.delegate = self
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "streamRecieved", timestamp: "", event_type: "Callback", event_name: "onlocalstream", component: "", event_source: "Init", logger_session_id: "")
        loggerMsAdapter?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
    }
    
    public func destory() {
        
        resetAllTimers()
        peerConnection.delegate = nil
        peerConnection.close()
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "Cleanup", timestamp: "", event_type: "Callback", event_name: "oncleanup", component: "", event_source: "destory", logger_session_id: "")
        loggerMsAdapter?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Configurations
extension PeerConnection {
    func createMediaSenders() {
        #if !TARGET_IS_EXTENSION
        // Audio
        let audioTrack = createAudioTrack()
        localAudioTrack = audioTrack
        peerConnection.add(audioTrack, streamIds: [identifier])
        #endif
        // Video
        let videoTrack = createVideoTrack()
        localVideoTrack = videoTrack
        peerConnection.add(videoTrack, streamIds: [identifier])
    }
    
    private func configureAudioSession() {
        rtcAudioSession.lockForConfiguration()
        do {
            try rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
            try rtcAudioSession.setMode(AVAudioSession.Mode.voiceChat.rawValue)
        } catch let error {
            debugPrint("Error changeing AVAudioSession category: \(error)")
        }
        rtcAudioSession.unlockForConfiguration()
    }
    
    func createAudioTrack() -> RTCAudioTrack {
        let audioConstrains = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        let audioSource = PeerConnection.factory.audioSource(with: audioConstrains)
        let audioTrack = PeerConnection.factory.audioTrack(with: audioSource, trackId: "audio0")
        return audioTrack
    }
    
    private func createVideoTrack() -> RTCVideoTrack {
        let videoSource = PeerConnection.factory.videoSource()
        localVideoSource = videoSource
        #if TARGET_IS_EXTENSION
        let videoCapturer = ScreenSampleCapturer(delegate: videoSource)
        self.videoCapturer = videoCapturer
        delegate?.webRTCClient(self, didCreate: videoCapturer)
        #else
        let videoCapturer = RTCCameraVideoCapturer(delegate: videoSource)
        //        videoCapturer.rotationDelegate = self
        self.videoCapturer = videoCapturer
        #endif
        
        let videoTrack = PeerConnection.factory.videoTrack(with: videoSource, trackId: "video0")
        return videoTrack
    }
    
    public func didCaptureLocalFrame(_ videoFrame: RTCVideoFrame) {
        guard let videoSource = localVideoSource,
            let videoCapturer = videoCapturer else { return }
        
        videoSource.capturer(videoCapturer, didCapture: videoFrame)
    }
    @available(iOSApplicationExtension, unavailable)
    public func detach(renderer: RTCVideoRenderer, isLocal: Bool) {
        if isLocal {
            localVideoTrack?.remove(renderer)
        } else {
            remoteVideoTrack?.remove(renderer)
        }
    }
    public func sendVideoFrames(frame: RTCVideoFrame) {
        videoCapturer?.delegate?.capturer(videoCapturer!, didCapture: frame)
    }
    public func stopCapture() {
        guard let capturer = videoCapturer as? RTCCameraVideoCapturer else { return }
        capturer.stopCapture()
    }
}
// MARK: - Signaling
extension PeerConnection {
    
    public func offer(completion: @escaping (_ sdp: RTCSessionDescription) -> Void) {
        let constrains = RTCMediaConstraints(mandatoryConstraints: self.mediaConstrains,
                                             optionalConstraints: nil)
        peerConnection.offer(for: constrains) { [weak self] (sdp, error) in
            guard let self = self else { return }
            guard let sdp = sdp else { return }
            
            self.peerConnection.setLocalDescription(sdp, completionHandler: { (_) in
                completion(sdp)
            })
        }
    }
   
    public func answer(completion: @escaping (_ sdp: RTCSessionDescription) -> Void) {
        let constrains = RTCMediaConstraints(mandatoryConstraints: self.mediaConstrains,
                                             optionalConstraints: nil)
        peerConnection.answer(for: constrains) { [weak self] (sdp, error) in
            guard let self = self else { return }
            guard let sdp = sdp else { return }
            
            self.peerConnection.setLocalDescription(sdp, completionHandler: { (_) in
                completion(sdp)
            })
        }
    }
    
    public func set(remoteSdp: RTCSessionDescription, completion: @escaping (Error?) -> Void) {
        peerConnection.setRemoteDescription(remoteSdp, completionHandler: completion)
    }
    
    public func set(remoteCandidate: RTCIceCandidate) {
        peerConnection.add(remoteCandidate) { error in
            debugPrint("peerConnection add remoteCandidate failed, Error: \(error?.localizedDescription ?? "!no reason")")
        }
    }
    public func removeLocalVideoRender(from renderer: RTCVideoRenderer) {
        localVideoTrack?.remove(renderer)
    }
    
    public func renderRemoteVideo(to renderer: RTCVideoRenderer) {
        remoteVideoTrack?.add(renderer)
    }
    public func renderLocalVideo(to renderer: RTCVideoRenderer) {
        localVideoTrack?.add(renderer)
    }
    public func removeRemoteVideoRender(from renderer: RTCVideoRenderer) {
        remoteVideoTrack?.remove(renderer)
    }
}

extension PeerConnection: RTCPeerConnectionDelegate {
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        debugPrint("peerConnection new signaling state: \(stateChanged)")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        debugPrint("peerConnection did add stream")
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "RemoteFeedStreamRecieved", timestamp: "", event_type: "Callback", event_name: "onremotestream", component: "", event_source: "peerConnection", logger_session_id: "")
        var meta: [String: Any] = [:]
        meta.updateValue(stream.videoTracks.count, forKey: "remoteStreamVideoTrackLength")
        meta.updateValue(stream.audioTracks.count, forKey: "remoteStreamAudioTrackLength")
        loggerMsAdapter?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: meta)
        
        remoteVideoTrack = stream.videoTracks.first
        remoteAudioTrack = stream.audioTracks.first
        delegate?.peerConnection(self, didAdd: stream)
        
        delegate?.peerConnection(event: "remote_stream_received", obj: ["remote_stream": "true"] as NSObject)
        print("Video track count:\(stream.videoTracks.count), Audio track count:\(stream.audioTracks.count)")
        if stream.videoTracks.count == 1 && stream.audioTracks.count == 1 {
            
            delegate?.peerConnection(event: "play_started", obj: ["subscriberStreamId": "", "msRoomId": ""] as NSObject)
            print("Sending Play Started callback")
            resetAllTimers()
//            self.isPlayStarted = true
            
//        } else if self.isPlayStarted && (stream.videoTracks.count == 0 || stream.audioTracks.count == 0) {
        } else if stream.videoTracks.count == 0 || stream.audioTracks.count == 0 {
            print("Play event finished")
            delegate?.peerConnection(event: "play_finished", obj: ["subscriberStreamId": ""] as NSObject)
            
            playResumeTimerOn()
        }
    }
   
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        debugPrint("peerConnection did remove stream")
    }
    
    public func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        debugPrint("peerConnection should negotiate")
    }
    
    public  func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        debugPrint("peerConnection new connection state: \(newState)")
        delegate?.peerConnection(self, didChangeConnectionState: newState)
        // callback - WebRTCIceStateChanged
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        debugPrint("peerConnection new gathering state: \(newState)")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        delegate?.peerConnection(self, didDiscoverLocalCandidate: candidate)
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        debugPrint("peerConnection did remove candidate(s)")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        debugPrint("peerConnection did open data channel")
    }
}
extension PeerConnection {
    
    private func setTrackEnabled<T: RTCMediaStreamTrack>(_ type: T.Type, isEnabled: Bool) {
        peerConnection.transceivers
            .compactMap { return $0.sender.track as? T }
            .forEach { $0.isEnabled = isEnabled }
    }
}

// MARK: - Video Control
extension PeerConnection {
    
    public func hideVideo() {
        setVideoEnabled(false)
    }
    
    public func showVideo() {
        setVideoEnabled(true)
    }
    
    private func setVideoEnabled(_ isEnabled: Bool) {
        setTrackEnabled(RTCVideoTrack.self, isEnabled: isEnabled)
    }
}

// MARK: - Audio Control
extension PeerConnection {
    
    public func muteAudio() {
        setAudioEnabled(false)
    }
    
    public func unmuteAudio() {
        setAudioEnabled(true)
    }
    
    // Fallback to the default playing device: headphones/bluetooth/ear speaker
    public func speakerOff() {
        audioQueue.async { [weak self] in
            guard let self = self else { return }
            
            self.rtcAudioSession.lockForConfiguration()
            do {
                try self.rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
                try self.rtcAudioSession.overrideOutputAudioPort(.none)
            } catch let error {
                debugPrint("Error setting AVAudioSession category: \(error)")
            }
            self.rtcAudioSession.unlockForConfiguration()
        }
    }
    // Force speaker
    public func speakerOn() {
        audioQueue.async { [weak self] in
            guard let self = self else { return }
            
            self.rtcAudioSession.lockForConfiguration()
            do {
                try self.rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
                try self.rtcAudioSession.overrideOutputAudioPort(.speaker)
                try self.rtcAudioSession.setActive(true)
            } catch let error {
                debugPrint("Couldn't force audio to speaker: \(error)")
            }
            self.rtcAudioSession.unlockForConfiguration()
        }
    }
    
    private func setAudioEnabled(_ isEnabled: Bool) {
        setTrackEnabled(RTCAudioTrack.self, isEnabled: isEnabled)
    }
    func playStartTimerOn() {
        guard playStartTimer == nil else { return }
        playStartTimer = Timer.scheduledTimer(timeInterval: TimeInterval(10), target: self, selector: #selector(self.playNotStartedReconnect), userInfo: nil, repeats: true)
    }
    @objc func playNotStartedReconnect() {
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "AddRemoteFeed", timestamp: "", event_type: "", event_name: "PlayNotStarted", component: "", event_source: "playNotStartedReconnect", logger_session_id: "", exceptionName: "PlayNotStarted", exceptionDescription: "Failed to start playing within 10s")
        loggerMsAdapter?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
        delegate?.peerConnection(event: "publish_finished", obj: ["subscriberStreamId": "", "msRoomId": ""] as NSObject)
    }
    func playResumeTimerOn() {
        guard playResumeTimer == nil else { return }
        playResumeTimer = Timer.scheduledTimer(timeInterval: TimeInterval(10), target: self, selector: #selector(self.playResumeReconnect), userInfo: nil, repeats: true)
    }
    @objc func playResumeReconnect() {
        
        let logDetails = ModalLogDetails(service_category: "CaptureSDK", service: "RemoteFeedStreamRecieved", timestamp: "", event_type: "", event_name: "PlayDidNotResume", component: "", event_source: "playResumeReconnect", logger_session_id: "", exceptionName: "PlayDidNotResume", exceptionDescription: "Failed to resume playing within 10s")
        loggerMsAdapter?.log(logLevel: LogLevel.shared.Info, logDetails: logDetails, meta: [:])
        
        delegate?.peerConnection(event: "play_finished", obj: ["subscriberStreamId": "", "msRoomId": ""] as NSObject)
    }
    func resetAllTimers() {
        playStartTimer = nil
        playStartTimer?.invalidate()
        playResumeTimer = nil
        playResumeTimer?.invalidate()
    }

}
