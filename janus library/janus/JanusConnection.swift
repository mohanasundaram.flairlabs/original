//
//  JanusConnection.swift
//  app
//
//  Created by Admin on 02/09/21.
//

import Foundation
import WebRTC

public class JanusConnection: Equatable {
	
	public var handleID: Int64
	public var publisher: JanusPublisher
	public var rtcClient: PeerConnection?
    public var rtcPeerConnection: RTCPeerConnection?
	
    required init(handleID: Int64, publisher: JanusPublisher, rtcPeerConnection: RTCPeerConnection) {
		self.handleID = handleID
		self.publisher = publisher
        self.rtcPeerConnection = rtcPeerConnection
	}
	
    public static func == (lhs: JanusConnection, rhs: JanusConnection) -> Bool {
		lhs.handleID == rhs.handleID
	}
	
	public var isLocal: Bool {
		handleID == JanusProvider.shared.handleID
	}
}
public class JanusPeerConnection {
    
    public var handleID: String?
    public var rtcPeerConnection: RTCPeerConnection?
    
    required init(handleID: String, pc: RTCPeerConnection) {
        self.handleID = handleID
        self.rtcPeerConnection = pc
    }
}
